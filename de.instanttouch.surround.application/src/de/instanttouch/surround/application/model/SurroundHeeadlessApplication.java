package de.instanttouch.surround.application.model;

public class SurroundHeeadlessApplication extends SurroundApplication {

	public SurroundHeeadlessApplication() {
		super();
		initHeadlessApplication();
	}

	public SurroundHeeadlessApplication(String name) {
		super(name);
		initHeadlessApplication();
	}

	private void initHeadlessApplication() {
		// TODO Auto-generated method stub

	}

}
