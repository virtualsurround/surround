package de.instanttouch.surround.application.model;

import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.collection.SurroundList;

public class SurroundApplication extends SurroundActor {

	public SurroundApplication() {
		super();
		initApplication();
	}

	public SurroundApplication(String name) {
		super(name);
		initApplication();
	}

	private void initApplication() {
		add(new SurroundList<SurroundServiceThread>("threads", SurroundServiceThread.class, "serviceThread"));
	}

	public SurroundList<SurroundServiceThread> getServiceThreads() {
		return (SurroundList<SurroundServiceThread>) findByName("threads");
	}

}
