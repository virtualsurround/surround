package de.instanttouch.surround.application.model;

import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundServiceThread extends SurroundDynamicEntity {

	public SurroundServiceThread() {
		initServiceThread();
	}

	public SurroundServiceThread(String name) {
		super(name);
		initServiceThread();
	}

	private void initServiceThread() {
		add(new SurroundList<SurroundActor>("components", SurroundActor.class, "component"));
	}

	public SurroundList<SurroundActor> getComponents() {
		return (SurroundList<SurroundActor>) findByName("components");
	}

}
