package de.instanttouch.surround.application.model.ui;

import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.collection.SurroundList;

public class SurroundToolbar extends SurroundActor {

	public SurroundToolbar() {
		initToolbar();
	}

	public SurroundToolbar(String name) {
		super(name);
		initToolbar();
	}

	private void initToolbar() {
		add(new SurroundList<SurroundAction>("actions"));
	}

	public SurroundToolbar addAction(SurroundAction action) {
		getActions().add(action);
		return this;
	}

	public SurroundList<SurroundAction> getActions() {
		return (SurroundList<SurroundAction>) findByName("actions");
	}

}
