package de.instanttouch.surround.application.model.ui;

import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundDashboard extends SurroundActor {

	public class PartList extends SurroundList<SurroundPart<SurroundDynamicEntity>> {

		public PartList(String name) {
			super(name);
		}

		@Override
		public SurroundList<SurroundPart<SurroundDynamicEntity>> add(SurroundPart<SurroundDynamicEntity> newElement) {
			connectDelegate(newElement);
			return super.add(newElement);
		}

		@Override
		public SurroundList<SurroundPart<SurroundDynamicEntity>> addTop(SurroundPart<SurroundDynamicEntity> newElement) {
			connectDelegate(newElement);
			return super.addTop(newElement);
		}

		@Override
		public SurroundList<SurroundPart<SurroundDynamicEntity>> add(int index, SurroundPart<SurroundDynamicEntity> newElement) {
			connectDelegate(newElement);
			return super.add(index, newElement);
		}

		@Override
		public boolean remove(SurroundPart<SurroundDynamicEntity> child) {
			// TODO Auto-generated method stub
			return super.remove(child);
		}

	}

	public SurroundDashboard() {
		initDashboard();
	}

	public SurroundDashboard(String name) {
		super(name);
		initDashboard();
	}

	private void initDashboard() {
		add(new PartList("top"));
		add(new PartList("bottom"));
		add(new PartList("left"));
		add(new PartList("right"));
		add(new PartList("center"));
	}

	public <T extends SurroundDynamicEntity> SurroundPart<T> newPart(Class<T> modelClass) {
		SurroundPart<T> part = new SurroundPart<>();
		return part;
	}

	public SurroundDashboard addTop(SurroundPart<SurroundDynamicEntity> part) {
		((PartList) findByName("top")).add(part);
		return this;
	}

	public SurroundDashboard addBottom(SurroundPart<SurroundDynamicEntity> part) {
		((PartList) findByName("bottom")).add(part);
		return this;
	}

	public SurroundDashboard addLeft(SurroundPart<SurroundDynamicEntity> part) {
		((PartList) findByName("left")).add(part);
		return this;
	}

	public SurroundDashboard addRight(SurroundPart<SurroundDynamicEntity> part) {
		((PartList) findByName("right")).add(part);
		return this;
	}

	public SurroundDashboard addCenter(SurroundPart<SurroundDynamicEntity> part) {
		((PartList) findByName("center")).add(part);
		return this;
	}
}
