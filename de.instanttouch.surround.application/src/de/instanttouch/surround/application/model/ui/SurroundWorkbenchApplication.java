package de.instanttouch.surround.application.model.ui;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import de.instanttouch.surround.application.model.SurroundApplication;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;

public class SurroundWorkbenchApplication extends SurroundApplication {

	protected ISurroundApplicationUI concreteUI;

	static int threadCount = 0;
	protected ExecutorService workerPool = Executors.newFixedThreadPool(3, new ThreadFactory() {

		@Override
		public Thread newThread(Runnable r) {
			return new Thread("workbenchthread - " + ++threadCount);
		}
	});

	public class ActorList<A extends SurroundActor> extends SurroundList<A> {

		public ActorList(String name) {
			super(name);
			// TODO Auto-generated constructor stub
		}

		@Override
		public SurroundList<A> add(A newElement) {
			connectDelegate(newElement);
			return super.add(newElement);
		}

		@Override
		public SurroundList<A> add(int index, A newElement) {
			connectDelegate(newElement);
			return super.add(index, newElement);
		}

		@Override
		public boolean remove(A child) {
			// TODO Auto-generated method stub
			return super.remove(child);
		}

	}

	public interface ISurroundApplicationUI {

		public void showView(SurroundView<?> view);

		public void showEditor(SurroundEditor<?> editor);

		public void showDashboard(SurroundDashboard dashboard);
	}

	public SurroundWorkbenchApplication() {
		super();
		initWorkbench();
	}

	public SurroundWorkbenchApplication(String name) {
		super(name);
		initWorkbench();
	}

	private void initWorkbench() {
		add(new ActorList<SurroundView<?>>("views"));
		add(new ActorList<SurroundEditor<?>>("editors"));
		add(new ActorList<SurroundDashboard>("dashboards"));
		add(new SurroundList<SurroundAction>("actions"));

		SurroundPlayer player = new SurroundPlayer();
		player.setExecutor(workerPool);
	}

	public ISurroundApplicationUI getConcreteUI() {
		return concreteUI;
	}

	public void setConcreteUI(ISurroundApplicationUI concreteUI) {
		this.concreteUI = concreteUI;
	}

	public ActorList<SurroundView<?>> getViews() {
		return (ActorList<SurroundView<?>>) findByName("views");
	}

	public ActorList<SurroundEditor<?>> getEditors() {
		return (ActorList<SurroundEditor<?>>) findByName("editors");
	}

	public ActorList<SurroundDashboard> getDashboards() {
		return (ActorList<SurroundDashboard>) findByName("dashboards");
	}

	public SurroundList<SurroundAction> getActions() {
		return (SurroundList<SurroundAction>) findByName("actions");
	}

	public void showView(String viewName) {

		SurroundBase viewObj = getViews().findByName(viewName);
		if (viewObj instanceof SurroundView<?>) {
			SurroundView<?> view = (SurroundView<?>) viewObj;

			if (concreteUI == null) {
				throw new SurroundNotInitializedException("concreteUI is missing");
			}
			concreteUI.showView(view);
		}

	}
}
