package de.instanttouch.surround.application.model.ui;

import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundView<M extends SurroundDynamicEntity> extends SurroundActor {

	protected M model;

	public SurroundView() {
		super();
		initView();
	}

	public SurroundView(String name) {
		super(name);
		initView();
	}

	private void initView() {

		SurroundToolbar toolbar = new SurroundToolbar("toolbar");
		connectDelegate(toolbar);
		add(toolbar);
	}

	public SurroundToolbar getToolbar() {
		return (SurroundToolbar) findByName("toolbar");
	}

	public M getModel() {
		return model;
	}

	public void setModel(M model) {
		this.model = model;
	}

}
