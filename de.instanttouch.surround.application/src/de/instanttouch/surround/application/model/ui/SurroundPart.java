package de.instanttouch.surround.application.model.ui;

import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundPart<M extends SurroundDynamicEntity> extends SurroundActor {

	protected M model;

	public SurroundPart() {
		initPart();
	}

	public SurroundPart(String name) {
		super(name);
		initPart();
	}

	private void initPart() {
		// TODO Auto-generated method stub

	}

	public M getModel() {
		return model;
	}

	public void setModel(M model) {
		this.model = model;
	}

}
