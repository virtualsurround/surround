package de.instanttouch.surround.application.model.ui;

import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundEditor<M extends SurroundDynamicEntity> extends SurroundActor {

	M model;

	public SurroundEditor() {
		super();
		initeditor();
	}

	public SurroundEditor(String name) {
		super(name);
		initeditor();
	}

	private void initeditor() {
		setPlayer(new SurroundPlayer());
	}

	public M getModel() {
		return model;
	}

	public void setModel(M model) {
		this.model = model;
	}

}
