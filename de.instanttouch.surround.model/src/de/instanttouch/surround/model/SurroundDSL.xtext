grammar de.instanttouch.surround.dsl.SurroundDSL with org.eclipse.xtext.xbase.Xbase

generate surroundDSL "http://www.instanttouch.de/surround/dsl/SurroundDSL"

SurroundDSL:
	'package' name=FQN
	(imports+=Import)*
	(types+=SurroundTypeModel)*
	(messaging+=SurroundMessagingModel)*
	(applications+=SurroundApplicationModel)*;



FQN:
	ID ('.' ID)*;

Import:
	'import' (importedNamespace=ImportedFQN 'from' | 'model') importURI=STRING;

ImportedFQN:
	FQN ('.' '*')?;

SurroundTypeModel:
	'types' name=ID '{'
	 types+=SurroundTypeEntry*
	'}';

SurroundTypeEntry:
	Entity|List|Map|Model
;

SurroundMessagingModel:
	'messaging' name=ID '{'
	messages+=Message*
	'}';

SurroundApplicationModel:
	"application"+ name=ID '{'
	channel=MessageChannel?
	windows+=Window*
	'}';

MessageChannel:
	'SurroundChannel' name=ID '{'
	'id:' channel=('akka' | 'jetty' | 'socket' | 'internal')
	'type:' type=('client' | 'server')
	'}';

Window:
	'window' name=QualifiedName '{'
	('windowId' windowId=ID)?
	('title' title=ID)?
	('content:' content=Entity)
	'}';

Dashboard:
	'dashboard' name=QualifiedName '{'
	('top' '{' top+=View* '}')?
	('center' '{' center+=View* '}')?
	('bottom' '{' bottom+=View* '}')?
	('left' '{' left+=View* '}')?
	('right' '{' right+=View* '}')?
	'}';

View:
	'view' name=QualifiedName '{'
	('viewId' viewId=ID)?
	('title' title=ID)?
	'content:' content=(Entity | List | Map)
	'}';

Message:
	Request | Response | Topic;

Request:
	'SurroundRequest' name=ID '{'
	args+=Property*
	('response' response=[Response])?
	'}';

Response:
	'SurroundResponse' name=ID '{'
	args+=Property*
	'}';

Topic:
	'SurroundTopic' name=ID '{'
	content=Property
	'}';

MessageId:
	messageId=ID;

BasicType:
	type=('SurroundString' | 'SurroundInteger' | 'SurroundBoolean' | 'SurrundLong' | 'SurroundDouble' | 'SurroundDate' |
	'SurroundTimestamp');

File:
	'SurroundFile' name=QualifiedName ('{' 'path' url=ID '}')?;

Folder:
	'SurroundDirectory' name=QualifiedName ('{' 'path' url=ID '}')?;

Url:
	'SurroundUrl' name=QualifiedName ('{' 'url' url=ID '}')?;

Image:
	'SurrounImage' name=QualifiedName ('{' 'url' url=ID '}')?;

ReferenceType:
	File | Folder | Url | Image;

List:
	'SurroundList' name=QualifiedName '{'
	'elementType:' element=(ElementType)
	'}';

ListRef:
	list=[List] name=QualifiedName;

Map:
	'SurroundMap' name=QualifiedName '{'
	'key:' key=(ElementType)
	'value:' value=(ElementType)
	'}';

MapRef:
	map=[Map] name=QualifiedName;


ElementType:
	BasicType name=QualifiedName | ReferenceType | EntityRef;

Entity:
	'SurroundEntity' name=QualifiedName '{'
	elements+=ElementType*
	'}';

EntityRef:
	entity=[Entity] name=QualifiedName;

Model:
	'SurroundModel' name=QualifiedName '{'
	coordinate=Coordinate
	elements+=ElementType*
	'}';

ModelRef:
	model=[Model] name=QualifiedName;

Coordinate:
	'coordinate' '{'
	'index' index=ID
	'type' type=ID
	'}';

Property:
	ElementType;