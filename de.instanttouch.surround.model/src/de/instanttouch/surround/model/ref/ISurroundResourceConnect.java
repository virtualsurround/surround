/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.ref;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ISurroundResourceConnect {

	public OutputStream getOutputStream() throws IOException;

	public InputStream getInputStream() throws IOException;

}
