package de.instanttouch.surround.model.ref;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundAdapter extends SurroundDynamicEntity {

	private Object adaptee;

	public SurroundAdapter() {
		super();
	}

	public SurroundAdapter(String name) {
		super(name);
	}

	public Object getAdaptee() {
		return adaptee;
	}

	public void setAdaptee(Object adaptee) {
		this.adaptee = adaptee;
	}

	public SurroundAdapter(String name, Object adaptee) {
		super(name);
		this.adaptee = adaptee;
	}

	@Override
	public List<SurroundBase> getChildren() {

		List<SurroundBase> children = new ArrayList<SurroundBase>();

		Field[] fields = getClass().getDeclaredFields();
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				Object obj = field.get(this);
				if (obj instanceof SurroundBase) {
					children.add((SurroundBase) obj);
				} else {
					children.add(new SurroundAdapter(field.getName(), obj));
				}

			} catch (IllegalArgumentException | IllegalAccessException e) {
				SurroundLog.log("could not access member", e);
			}
		}

		return children;
	}

}
