package de.instanttouch.surround.model.ref;

import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundHttpReference<T extends SurroundBase> extends SurroundReference<T> {

	public SurroundHttpReference() {
		super();
		initWebReference();
	}

	public SurroundHttpReference(String name) {
		super(name);
		initWebReference();
	}

	private void initWebReference() {
		setProtocol("http");
	}

	@Override
	public T resolve() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SurroundHttpReference<T> buildURL() {

		String urlStr = getProtocol() + "//";
		urlStr += getHost();
		if (getPort() > 0) {
			urlStr += ":" + getPort();
		}
		if (getPath() != null && getPath().length() > 0) {
			urlStr += getPath();
		}
		if (getDeepLink() != null && getDeepLink().length() > 0) {
			urlStr += "#" + getDeepLink();
		}
		getURL().set(urlStr);

		return this;
	}

}
