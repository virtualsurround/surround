/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.ref;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.io.SurroundJSONReader;
import de.instanttouch.surround.model.io.SurroundJSONWriter;

public class SurroundFile extends SurroundString implements ISurroundResourceConnect {

	public SurroundFile() {
		super();
		initSurroundFile();
	}

	public SurroundFile(String name) {
		super(name);
		initSurroundFile();
	}

	private void initSurroundFile() {
		// TODO Auto-generated method stub

	}

	public String getFileName() {
		String fileName = toString();

		int lastIndex = fileName.lastIndexOf(File.separator);
		if (lastIndex < 0) {
			lastIndex = fileName.lastIndexOf('/');
		}

		if (lastIndex >= 0) {
			fileName = fileName.substring(lastIndex + 1);
		}

		return fileName;
	}

	public String getPath() {
		String path = toString();
		return path;
	}

	@Override
	public SurroundFile set(String value) {
		String newValue = value.replaceAll("\\\\", "/");
		super.set(newValue);

		return this;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		String path = toString();
		return new FileOutputStream(path);
	}

	@Override
	public InputStream getInputStream() throws IOException {
		String path = toString();
		return new FileInputStream(path);
	}

	public FileReader getReader() throws IOException {
		String path = toString();
		return new FileReader(path);
	}

	public FileWriter getWriter() throws IOException {
		String path = toString();
		return new FileWriter(path);
	}

	public File getFile() {
		return new File(toString());
	}

	public boolean isNew() {
		return !getFile().exists() || getFile().length() == 0;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

	public SurroundFile read(SurroundBase entity) {

		try (InputStream input = getInputStream(); SurroundJSONReader reader = new SurroundJSONReader(input)) {

			Object jsonObject = reader.parse();
			reader.read(entity, jsonObject);

		} catch (Exception ex) {
			throw new SurroundBaseException("couldn't read entity: " + entity.getName() + " from file: " + getPath(),
					ex);
		}

		return this;
	}

	public SurroundFile write(SurroundBase entity) {

		try (OutputStream output = getOutputStream(); OutputStreamWriter fileWriter = new OutputStreamWriter(output)) {

			SurroundJSONWriter jsonWriter = new SurroundJSONWriter();
			jsonWriter.write(entity);

			String content = jsonWriter.beautify();
			jsonWriter.close();

			fileWriter.write(content);
			fileWriter.flush();

		} catch (Exception ex) {
			throw new SurroundBaseException("error saving entity: " + entity.getName() + " from file: " + getPath(),
					ex);
		}
		return this;
	}

	public SurroundFile create(SurroundBase model) {

		write(model);
		return this;
	}

	public SurroundFile delete(SurroundBase model) {

		File file = getFile();
		file.delete();

		return this;
	}

}
