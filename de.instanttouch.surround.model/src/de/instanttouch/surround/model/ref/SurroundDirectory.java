/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.ref;

import de.instanttouch.surround.model.base.SurroundString;

public class SurroundDirectory extends SurroundString {

	public SurroundDirectory() {
		super();
		initDirectory();
	}

	public SurroundDirectory(String name) {
		super(name);
		initDirectory();
	}

	private void initDirectory() {
	}

	@Override
	public SurroundDirectory set(String newValue) {
		super.set(newValue);

		return this;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
