/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.ref;

import java.util.ArrayList;
import java.util.List;

public class SurroundPath {

	protected List<String> segments = new ArrayList<>();

	public SurroundPath() {
		super();
	}

	public List<String> getSegments() {
		return segments;
	}

	public void addSegment(String segment) {
		segments.add(segment);
	}

	public void removeSegment(String segment) {
		segments.remove(segment);
	}

}
