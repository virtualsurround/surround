package de.instanttouch.surround.model.ref;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundEmailAddress extends SurroundURL {

	String prefix = "mailto:";

	public SurroundEmailAddress() {
		initEmailAddress();
	}

	public SurroundEmailAddress(String name) {
		super(name);
		initEmailAddress();
	}

	private void initEmailAddress() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

	@Override
	public SurroundString set(String newValue) {

		String emailAddress = checkMailAddress(newValue);
		return super.set(emailAddress);
	}

	private String checkMailAddress(String newValue) {
		String emailAddress = newValue.startsWith(prefix) ? newValue : prefix + newValue;
		return emailAddress;
	}

	@Override
	public String toString() {
		String emailAddress = checkMailAddress(super.toString());
		return emailAddress;
	}

}
