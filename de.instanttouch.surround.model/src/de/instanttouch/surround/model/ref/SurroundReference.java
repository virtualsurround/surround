/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.ref;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.url.SurroundURL;

public abstract class SurroundReference<T extends SurroundBase> extends SurroundDynamicEntity {

	protected boolean isResolved = false;

	public abstract T resolve();

	public abstract SurroundReference<T> buildURL();

	public SurroundReference() {
		super();
		initTypeRef();
	}

	public SurroundReference(String name) {
		super(name);
		initTypeRef();
	}

	private void initTypeRef() {
		add(new SurroundURL("url"));
	}

	public SurroundURL getURL() {
		return (SurroundURL) findByName("url");
	}

	public boolean isResolved() {
		return isResolved;
	}

	public SurroundReference<T> setResolved(boolean isResolved) {
		this.isResolved = isResolved;
		return this;
	}

	public String getCluster() {
		return findByName("cluster") == null ? "" : findByName("cluster").toString();
	}

	public SurroundReference<T> setCluster(String cluster) {
		replace("cluster", new SurroundString("cluster").set(cluster));
		buildURL();
		return this;
	}

	public String getIndex() {
		return findByName("index") == null ? "" : findByName("index").toString();
	}

	public SurroundReference<T> setIndex(String index) {
		replace("index", new SurroundString("index").set(index));
		buildURL();
		return this;
	}

	public String getRole() {
		return findByName("role") == null ? "" : findByName("role").toString();
	}

	public SurroundReference<T> setRole(String role) {
		replace("role", new SurroundString("role").set(role));
		buildURL();
		return this;
	}

	public String getPath() {
		return findByName("path") == null ? "" : findByName("path").toString();
	}

	public SurroundReference<T> setPath(String path) {
		replace("path", new SurroundString("path").set(path));
		buildURL();
		return this;
	}

	public String getHost() {
		return findByName("host") != null ? findByName("host").toString() : "";
	}

	public SurroundReference<T> setHost(String host) {
		replace("host", new SurroundString("host").set(host));
		buildURL();
		return this;
	}

	public int getPort() {
		return findByName("port") != null ? findByName("port").asInt() : -1;
	}

	public SurroundReference<T> setPort(int port) {
		replace("port", new SurroundString("port").set(port));
		buildURL();
		return this;
	}

	public String getProtocol() {
		return findByName("protocol").toString();
	}

	public SurroundReference<T> setProtocol(String protocol) {
		replace("protocol", new SurroundString("protocol").set(protocol));
		buildURL();
		return this;
	}

	public String getType() {
		return findByName("type") == null ? "" : findByName("type").toString();
	}

	public SurroundReference<T> setType(String type) {
		replace("type", new SurroundString("type").set(type));
		buildURL();
		return this;
	}

	public String getDomain() {
		return findByName("domain") == null ? "" : findByName("domain").toString();
	}

	public SurroundReference<T> setDomain(String domain) {
		replace("domain", new SurroundString("domain").set(domain));
		buildURL();
		return this;
	}

	public SurroundURL getWiki() {
		return (SurroundURL) findByName("wiki");
	}

	public SurroundReference<T> setWiki(SurroundURL wikiUrl) {

		replace("wiki", wikiUrl);
		buildURL();
		return this;
	}

	public String getKey() {
		return findByName("key") != null ? findByName("key").toString() : null;
	}

	public SurroundReference<T> setKey(String key) {
		replace("key", new SurroundString("key").set(key));
		buildURL();
		return this;
	}

	public String getDeepLink() {
		return findByName("deepLink") == null ? "" : findByName("deepLink").toString();
	}

	public SurroundReference<T> setDeepLink(String deepLink) {
		replace("deepLink", new SurroundString("deepLink").set(deepLink));
		buildURL();
		return this;
	}

	public String getLogin() {
		return findByName("login") == null ? "" : findByName("login").toString();
	}

	public SurroundReference<T> setLogin(String login) {
		replace("login", new SurroundString("login").set(login));
		buildURL();
		return this;
	}

	public String getPassword() {
		return findByName("password") == null ? "" : findByName("password").toString();
	}

	public SurroundReference<T> setPassword(String password) {
		replace("password", new SurroundString("password").set(password).getConstraints().setPassword(true));
		buildURL();
		return this;
	}

	public String getMethod() {
		return findByName("method") == null ? "" : findByName("method").toString();
	}

	public SurroundReference<T> setMethod(String method) {
		replace("method", new SurroundString("method").set(method));
		buildURL();
		return this;
	}

	public SurroundFile getFile() {
		return (SurroundFile) findByName("filePath");
	}

	public SurroundReference<T> setFile(String filePath) {
		replace("filePath", new SurroundFile("filePath").set(filePath));
		buildURL();
		return this;
	}

	public boolean isFile() {
		return getFile() != null;
	}
}
