package de.instanttouch.surround.model.chart;

import de.instanttouch.surround.model.collection.SurroundList;

public class SurroundDataSeries<T extends SurroundDataPoint> extends SurroundList<T> {

	public SurroundDataSeries() {
		super();
	}

	public SurroundDataSeries(Class<T> elementClass) {
		super(elementClass);
	}

	public SurroundDataSeries(String name) {
		super(name);
	}

	public SurroundDataSeries(String name, Class<T> elementClass, String elementName) {
		super(name, elementClass, elementName);
	}
}
