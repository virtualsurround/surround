package de.instanttouch.surround.model.chart;

import de.instanttouch.surround.model.composite.SurroundValueType;

public class SurroundDataPoint extends SurroundValueType {

	public SurroundDataPoint() {
		super();
	}

	public SurroundDataPoint(String name) {
		super(name);
	}

}
