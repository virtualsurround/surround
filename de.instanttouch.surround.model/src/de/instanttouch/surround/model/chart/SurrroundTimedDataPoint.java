package de.instanttouch.surround.model.chart;

import de.instanttouch.surround.model.time.SurroundTimestamp;

public class SurrroundTimedDataPoint extends SurroundDataPoint {

	private SurroundTimestamp timestamp = new SurroundTimestamp();

	public SurrroundTimedDataPoint() {
		super();
	}

	public SurrroundTimedDataPoint(String name) {
		super(name);
	}

	public SurroundTimestamp getTimestamp() {
		return timestamp;
	}
}
