package de.instanttouch.surround.model.chart;

public class SurroundTimeSeries<T extends SurrroundTimedDataPoint> extends SurroundDataSeries<T> {

	public SurroundTimeSeries() {
		super();
	}

	public SurroundTimeSeries(Class<T> elementClass) {
		super(elementClass);
	}

	public SurroundTimeSeries(String name, Class<T> elementClass, String elementName) {
		super(name, elementClass, elementName);
	}

	public SurroundTimeSeries(String name) {
		super(name);
	}

}
