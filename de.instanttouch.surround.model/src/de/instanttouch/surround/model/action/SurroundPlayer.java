package de.instanttouch.surround.model.action;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import de.instanttouch.surround.model.action.SurroundAction.IProgressInfoListener;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.util.SurroundLog;
import javafx.application.Platform;

public class SurroundPlayer extends SurroundDynamicEntity {

	public static class ProgressAdapter implements SurroundAction.IProgressInfoListener {

		protected String currentTask;
		protected int steps = 0;
		protected int count = 0;
		protected boolean isCanceled = false;

		@Override
		public ProgressAdapter setMain(String mainTask) {
			SurroundLog.info("starting: " + mainTask);
			return this;
		}

		@Override
		public ProgressAdapter setCurrent(String currentTask) {
			this.currentTask = currentTask;
			SurroundLog.info("currentTask: " + currentTask);
			return this;
		}

		@Override
		public ProgressAdapter setTotalWork(int steps) {
			this.steps = steps;
			return this;
		}

		@Override
		public ProgressAdapter incWork() {
			count++;
			SurroundLog.info(currentTask + " finished:" + count + "of: " + steps);
			return this;
		}

		@Override
		public ProgressAdapter setPercentage(double percentage) {
			SurroundLog.info(currentTask + " finished:" + percentage + " %");
			return this;
		}

		@Override
		public boolean isCanceled() {
			return isCanceled;
		}

		@Override
		public IProgressInfoListener setCanceled(boolean isCanceled) {
			this.isCanceled = isCanceled;
			return this;
		}

	}

	protected ExecutorService executor;

	public SurroundPlayer() {
		initPlayer();
	}

	public SurroundPlayer(String name) {
		super(name);
		initPlayer();
	}

	private void initPlayer() {
	}

	public ExecutorService getExecutor() {
		return executor;
	}

	public SurroundPlayer setExecutor(ExecutorService executor) {
		this.executor = executor;
		return this;
	}

	public void play(SurroundAction surroundAction) {

		switch (surroundAction.getThreadType()) {
		case SurroundAction.UI_THREAD:
			playInUIThread(surroundAction);
			break;

		case SurroundAction.BACKGROUND:
			playInBackgroundThread(surroundAction);
			break;

		case SurroundAction.CURRENT:
		default:
			playInCurrentThread(surroundAction);
			break;
		}
	}

	public void playInCurrentThread(SurroundAction action) {
		try {
			action.play(new ProgressAdapter());
		} catch (Exception e) {
			SurroundLog.log(e);
		}
	}

	public void playInUIThread(final SurroundAction action) {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				try {
					action.play(new ProgressAdapter());
				} catch (Exception e) {
					SurroundLog.log(e);
				}
			}
		});

	}

	public void playInBackgroundThread(final SurroundAction action) {

		if (executor != null) {
			Callable<Void> playCallable = new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					action.play(new ProgressAdapter());
					return null;
				}
			};

			executor.submit(playCallable);
		} else {
			playInCurrentThread(action);
		}

	}
}
