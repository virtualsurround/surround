/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundActionRegistry {

	protected static Map<Class<? extends SurroundBase>, ArrayList<SurroundAction>> actionRegistry = new HashMap<Class<? extends SurroundBase>, ArrayList<SurroundAction>>();

	public static void registerAction(Class<? extends SurroundBase> SurroundBase, SurroundAction action) {

		ArrayList<SurroundAction> actions = actionRegistry.get(SurroundBase);
		if (actions == null) {
			actions = new ArrayList<SurroundAction>();
			actionRegistry.put(SurroundBase, actions);
		}

		if (!actions.contains(action)) {
			actions.add(action);
		}
	}

	public static List<SurroundAction> getAllAvailableActions(SurroundBase SurroundBase) {
		List<SurroundAction> actions = new ArrayList<SurroundAction>();

		for (Class<? extends SurroundBase> checkBase : actionRegistry.keySet()) {
			if (checkBase.isInstance(SurroundBase)) {
				actions.addAll(actionRegistry.get(checkBase));
			}
		}

		return actions;
	}

}
