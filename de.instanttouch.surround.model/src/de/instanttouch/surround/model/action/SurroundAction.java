package de.instanttouch.surround.model.action;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public abstract class SurroundAction extends SurroundDynamicEntity {

	public interface IProgressInfoListener {

		public IProgressInfoListener setMain(String mainTask);

		public IProgressInfoListener setCurrent(String currentTask);

		public IProgressInfoListener setTotalWork(int steps);

		public IProgressInfoListener incWork();

		public IProgressInfoListener setPercentage(double percentage);

		public boolean isCanceled();

		public IProgressInfoListener setCanceled(boolean isCanceled);
	}

	public static final String UI_THREAD = "uiThread";
	public static final String BACKGROUND = "background";
	public static final String CURRENT = "currentThread";

	protected SurroundBase model;

	public SurroundAction() {
		super();
		initAction();
	}

	public SurroundAction(String name) {
		super(name);
		initAction();
	}

	private void initAction() {
		add(new SurroundString("threadType").set(CURRENT));
		add(new SurroundString("imageBundle"));
		add(new SurroundString("imagePath"));
	}

	public SurroundBase getModel() {
		return model;
	}

	public SurroundAction setModel(SurroundBase model) {
		this.model = model;
		return this;
	}

	public String getImageBundle() {
		return findByName("imageBundle").toString();
	}

	public SurroundAction setImageBundle(String bundle) {
		findByName("imageBundle").set(bundle);
		return this;
	}

	public String getImagePath() {
		return findByName("imagePath").toString();
	}

	public SurroundAction setImagePath(String path) {
		findByName("imagePath").set(path);
		return this;
	}

	public String getThreadType() {
		return findByName("threadType").toString();
	}

	public SurroundAction setThreadType(String threadType) {
		findByName("threadType").set(threadType);
		return this;
	}

	public abstract boolean play(IProgressInfoListener progressInfoListener);

}
