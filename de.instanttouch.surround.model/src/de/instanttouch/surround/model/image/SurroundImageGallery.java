package de.instanttouch.surround.model.image;

import de.instanttouch.surround.model.collection.SurroundList;

public class SurroundImageGallery extends SurroundList<SurroundImage> {

	public SurroundImageGallery() {
		super(SurroundImage.class);
	}

	public SurroundImageGallery(String name) {
		super(name, SurroundImage.class, "image");
	}

}
