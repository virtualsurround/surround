package de.instanttouch.surround.model.image;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundImage extends SurroundDynamicEntity {

	public SurroundImage() {
		initImage();
	}

	public SurroundImage(String name) {
		super(name);
		initImage();
	}

	private void initImage() {
		add(new SurroundString("bundle"));
		add(new SurroundString("path"));
	}

	public String getBundle() {
		return findByName("bundle").toString();
	}

	public SurroundImage setBundle(String bundle) {
		findByName("bundle").set(bundle);
		return this;
	}

	public String getPath() {
		return findByName("path").toString();
	}

	public SurroundImage setPath(String path) {
		findByName("path").set(path);
		return this;
	}

}
