/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.graph;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundRelationship extends SurroundModel {

	public interface ISurroundRelationshipCreator {
		public SurroundRelationship create(Object source);
	}

	public SurroundRelationship() {
		super();
		init();
	}

	public SurroundRelationship(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundNodeReference("from"));
		add(new SurroundNodeReference("to"));
		add(new SurroundString("label"));
	}

	public String getLabel() {
		return findByName("label").toString();
	}

	public SurroundRelationship setLabel(String label) {
		findByName("label").set(label);
		return this;
	}

	public SurroundNodeReference getStartNode() {
		return (SurroundNodeReference) findByName("from");
	}

	public SurroundRelationship setStartNode(SurroundURL nodeURL) {
		getStartNode().getURL().set(nodeURL.toString());
		return this;
	}

	public SurroundNodeReference getEndNode() {
		return (SurroundNodeReference) findByName("to");
	}

	public SurroundRelationship setEndNode(SurroundURL nodeURL) {
		getEndNode().getURL().set(nodeURL.toString());
		return this;
	}

	public static SurroundRelationship create(SurroundNode start, SurroundNode end, String label) {

		SurroundRelationship relationship = new SurroundRelationship(label);
		relationship.setStartNode(start.getURL()).setEndNode(end.getURL()).setLabel(label);

		return relationship;
	}
}
