/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.graph;

import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.domain.SurroundModel;

public class SurroundHyperNodeRelationship extends SurroundModel {

	public SurroundHyperNodeRelationship() {
		super();
		init();
	}

	public SurroundHyperNodeRelationship(String name) {
		super(name);
		init();
	}

	private void init() {

		add(new SurroundList<SurroundNode>("nodes"));
	}

	public SurroundList<SurroundNode> getNodes() {
		return (SurroundList<SurroundNode>) findByName("nodes");
	}

}
