package de.instanttouch.surround.model.graph;

import de.instanttouch.surround.model.ref.SurroundReference;

public class SurroundNodeReference extends SurroundReference<SurroundNode> {

	public SurroundNodeReference() {
		super();
	}

	public SurroundNodeReference(String name) {
		super(name);
	}

	@Override
	public SurroundNode resolve() {

		SurroundNode resolved = SurroundModelRepository.getNode(getURL());
		return resolved;
	}

	@Override
	public SurroundReference<SurroundNode> buildURL() {
		// TODO Auto-generated method stub
		return null;
	}

}
