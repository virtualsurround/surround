package de.instanttouch.surround.model.graph;

import java.util.HashMap;
import java.util.Map;

import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundModelRepository extends SurroundDynamicEntity {

	protected static Map<String, SurroundNode> modelMap = new HashMap<>();

	public static SurroundNode getNode(SurroundURL url) {

		String nodeStr = url.toString();

		SurroundNode surroundNode = modelMap.get(nodeStr);
		return surroundNode;
	}

	public static void putNode(SurroundURL url, SurroundNode node) {

		String nodeStr = url.toString();
		modelMap.put(nodeStr, node);
	}

}
