/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundUnsupportedException;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundNode extends SurroundModel {

	public static final String MODEL_NODE = "modelNode";
	public static final String APPLICATION_NODE = "applicationNode";
	public static final String VIEW_NODE = "viewNode";
	public static final String DIALOG_NODE = "dialogNode";
	public static final String EDITOR_NODE = "editorNode";
	public static final String PRESENTER_NODE = "presenterNode";
	public static final String ACTOR_NODE = "actorNode";

	protected static AtomicLong nodeId = new AtomicLong(0);

	public interface ISurroundNodeCreator {
		public SurroundNode create(Object source);
	}

	public SurroundNode() {
		super();
		initNode();
	}

	public SurroundNode(String name) {
		super(name);
		initNode();
	}

	public SurroundURL getURL() {
		SurroundURL url = getReference() != null ? getReference().getURL() : (SurroundURL) findByName("url");
		return url;
	}

	private void initNode() {

		add(new SurroundList<SurroundRelationship>("incomingRelationShips"));
		add(new SurroundList<SurroundRelationship>("outgoingRelatioinShip"));
		add(new SurroundString("type"));

		SurroundList<SurroundNode> embedded = new SurroundList<SurroundNode>("embeddedNodes", SurroundNode.class,
				"embedded");
		add(embedded);

		SurroundURL url = new SurroundURL("url");
		url.set("node:" + nodeId.incrementAndGet());
		add(url);

		SurroundModelRepository.putNode(getURL(), this);
	}

	public String getType() {
		return findByName("type").toString();
	}

	public SurroundNode setType(String type) {
		findByName("type").set(type);
		return this;
	}

	public SurroundList<SurroundRelationship> getIncomingRelationsships() {
		return (SurroundList<SurroundRelationship>) findByName("incomingRelationShips");
	}

	public SurroundList<SurroundRelationship> getOutgoingRelationsships() {
		return (SurroundList<SurroundRelationship>) findByName("outgoingRelatioinShip");
	}

	public SurroundList<SurroundNode> getEmbeddedNodes() {
		return (SurroundList<SurroundNode>) findByName("embeddedNodes");
	}

	public void addRelationShip(SurroundRelationship relationship) {

		String start = relationship.getStartNode().getURL().toString();
		String end = relationship.getEndNode().getURL().toString();

		SurroundURL url = getURL();
		String urlStr = url.toString();

		if (start.equals(urlStr)) {
			getOutgoingRelationsships().add(relationship);
		} else if (end.equals(urlStr)) {
			getIncomingRelationsships().add(relationship);
		} else {
			throw new SurroundUnsupportedException("relationship is not related to current node");
		}
	}

	public List<SurroundNode> findRelated(String label, boolean outgoing) {

		List<SurroundNode> related = new ArrayList<SurroundNode>();

		List<SurroundRelationship> relationships = outgoing ? getOutgoingRelationsships().getChildren()
				: getIncomingRelationsships().getChildren();

		for (SurroundRelationship rel : relationships) {

			if (outgoing) {
				SurroundNodeReference surroundRef = rel.getEndNode();
				SurroundNode endNode = surroundRef.resolve();
				related.add(endNode);

			} else {
				SurroundNodeReference surroundRef = rel.getStartNode();
				SurroundNode startNode = surroundRef.resolve();
				related.add(startNode);
			}
		}

		return related;
	}

	public SurroundRelationship relateTo(SurroundNode node, String label, boolean outgoing) {

		SurroundNode start = outgoing ? this : node;
		SurroundNode end = outgoing ? node : this;

		SurroundModelRepository.putNode(node.getURL(), node);

		SurroundRelationship relationship = SurroundRelationship.create(start, end, label);
		start.addRelationShip(relationship);

		relationship = SurroundRelationship.create(start, end, label);
		end.addRelationShip(relationship);

		return relationship;
	}

}
