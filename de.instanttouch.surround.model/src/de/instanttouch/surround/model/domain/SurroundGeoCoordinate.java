/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.domain;

import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundGeoCoordinate extends SurroundDynamicEntity {

	public static final String GEO = "geo";

	public SurroundGeoCoordinate() {
		super();
		initGeoCoordinate();
	}

	public SurroundGeoCoordinate(String name) {
		super(name);
		initGeoCoordinate();
	}

	private void initGeoCoordinate() {

		add(new SurroundString().setName("ellipsoid").set(GEO));
		add(new SurroundDouble("x"));
		add(new SurroundDouble("y"));
	}

	public double getX() {
		return findByName("x").asLong();
	}

	public SurroundGeoCoordinate setX(double x) {
		findByName("x").set(x);
		return this;
	}

	public double getY() {
		return findByName("y").asLong();
	}

	public SurroundGeoCoordinate setY(double y) {
		findByName("y").set(y);
		return this;
	}

	public String getEllipsoid() {
		return findByName("ellipsoid").toString();
	}

	public SurroundGeoCoordinate setEllipsoid(String ellipsoid) {
		findByName("ellipsoid").set(ellipsoid);
		return this;
	}

	@Override
	public String toString() {
		return "x:" + getX() + ", y:" + getY();
	}

}
