/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.domain;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundDefaultFactory;
import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundMap.SurroundValueFactory;
import de.instanttouch.surround.model.collection.SurroundStringMap;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.ref.SurroundDirectory;
import de.instanttouch.surround.model.ref.SurroundFile;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundModel extends SurroundDynamicEntity {

	public SurroundModel() {
		super();
		initModel();
	}

	public SurroundModel(String name) {
		super(name);
		initModel();
	}

	private void initModel() {

		SurroundStringMap<SurroundBase> propMap = new SurroundStringMap<SurroundBase>("properties");

		SurroundDefaultFactory<SurroundString> keyCreator = new SurroundDefaultFactory<>(SurroundString.class);

		SurroundValueFactory<SurroundBase> valueCreator = new SurroundValueFactory<SurroundBase>();
		valueCreator.assignValueType(SurroundString.class);
		valueCreator.assignValueType(SurroundBoolean.class);
		valueCreator.assignValueType(SurroundLong.class);
		valueCreator.assignValueType(SurroundDouble.class);
		valueCreator.assignValueType(SurroundFile.class);
		valueCreator.assignValueType(SurroundDirectory.class);
		valueCreator.assignValueType(SurroundURL.class);

		propMap.assignFactories(keyCreator, valueCreator);

		add(propMap);
	}

	public SurroundStringMap<SurroundBase> getProperties() {
		return (SurroundStringMap<SurroundBase>) findByName("properties");
	}

	public SurroundModel addProperty(String propName, SurroundBase prop) {
		getProperties().put(propName, prop);
		return this;
	}

	public <M extends SurroundModel> SurroundReference<M> getModelCoordinate(M modelClass) {

		return (SurroundReference<M>) getReference();
	}

	public <M extends SurroundModel> SurroundModel setModelCoordinate(SurroundReference<M> modelCoordinate,
			Class<M> modelClass) {
		setReference(modelCoordinate);
		return this;
	}

}
