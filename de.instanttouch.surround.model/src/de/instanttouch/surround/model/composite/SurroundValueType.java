package de.instanttouch.surround.model.composite;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;
import de.instanttouch.surround.model.io.SurroundJSONReader;
import de.instanttouch.surround.model.io.SurroundJSONWriter;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundValueType extends SurroundEntity {

	public SurroundValueType() {
	}

	public SurroundValueType(String name) {
		super(name);
	}

	public String asJSON() {

		String entityStr = super.toString();

		try {
			SurroundJSONWriter writer = new SurroundJSONWriter();
			writer.write(this);

			entityStr = writer.beautify();

		} catch (Exception e) {
			throw new SurroundNotYetImplementedException("log exception");
		}

		return entityStr;
	}

	public SurroundValueType fromJSON(String jsonStr) throws SurroundBaseException, IOException, InstantiationException,
			IllegalAccessException, ClassNotFoundException {

		SurroundJSONReader reader = new SurroundJSONReader(jsonStr);
		Object jsonObject = reader.parse();

		reader.read(this, jsonObject);

		return this;
	}

	@Override
	public List<SurroundBase> getChildren() {

		List<SurroundBase> children = new ArrayList<SurroundBase>();

		Field[] fields = getClass().getDeclaredFields();
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				Object obj = field.get(this);
				if (obj instanceof SurroundBase) {
					children.add((SurroundBase) obj);
				}

			} catch (IllegalArgumentException | IllegalAccessException e) {
				SurroundLog.log("could not access member", e);
			}
		}

		return children;
	}

	@Override
	public int hashCode() {
		
		final int prime = 31;
		int result = 1;
		result = prime * result + getName() != null ? getName().hashCode() : 0;
		
		for (SurroundBase surroundBase : getChildren()) {
			result = prime * result + surroundBase.hashCode();
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundBase other = (SurroundBase) obj;
		if (!(other.getName().equals(getName()))) {
			return false;
		}
		
		for (SurroundBase surroundBase : getChildren()) {
			SurroundBase compareChild = other.findByName(surroundBase.getName());
			if(!(surroundBase.equals(compareChild))){
				return false;
			}
		}
		return true;
	}
}
