/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.composite;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.io.SurroundJSONReader;
import de.instanttouch.surround.model.io.SurroundJSONWriter;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundDynamicEntity extends SurroundEntity {

	protected List<SurroundBase> members = new CopyOnWriteArrayList<SurroundBase>();
	protected ISurroundFactory<SurroundBase> memberFactory = null;

	public SurroundDynamicEntity() {
		super();
		initDynamicEntity();
	}

	public SurroundDynamicEntity(String name) {
		super(name);
		initDynamicEntity();
	}

	protected void initDynamicEntity() {
	}

	public ISurroundFactory<SurroundBase> getMemberFactory() {
		return memberFactory;
	}

	public void setMemberFactory(ISurroundFactory<SurroundBase> memberCreator) {
		this.memberFactory = memberCreator;

		SurroundList<SurroundBase> dynamic = new SurroundList<SurroundBase>("dynamic");
		dynamic.setElementFactory(memberCreator);
		add(dynamic);
	}

	public SurroundDynamicEntity add(SurroundBase newMember) {

		if (!members.contains(newMember)) {
			members.add(newMember);

			SurroundChange<?> change = SurroundChange.forAdd(newMember, size() - 1);
			notifyChanged(change);

		}
		return this;
	}

	public SurroundDynamicEntity remove(SurroundBase itype) {
		SurroundBase type = itype;

		if (members.contains(type)) {
			members.remove(type);
		}
		return this;
	}

	public boolean replace(String name, SurroundBase newElement) {
		int index = 0;
		SurroundBase currentType = findByName(name);

		if (newElement == null) {

			if (currentType != null) {
				members.remove(currentType);
			}

			return true;
		}

		if (currentType != null) {
			synchronized (this) {
				members.remove(currentType);

				newElement.setName(name);
				members.add(newElement);

				notifyAll();
			}
		} else {
			newElement.setName(name);
			members.add(newElement);
		}

		return currentType != null;
	}

	@Override
	public int size() {
		return members.size();
	}

	public int indexOf(SurroundBase surroundType) {
		return members.indexOf(surroundType);
	}

	@Override
	public List<SurroundBase> getChildren() {

		SurroundReference<?> reference = getReference();
		if (reference != null) {
			if (!reference.isResolved()) {
				try {
					reference.resolve();
				} catch (Exception e) {
				}
			}
		}

		return members;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object clone = super.clone();
		if (!(clone instanceof SurroundDynamicEntity)) {
			throw new CloneNotSupportedException("unexpected type: " + clone.getClass().getName());
		}

		SurroundDynamicEntity cloneEntity = (SurroundDynamicEntity) clone;
		cloneEntity.members.clear();// members may have been added, do avoid

		for (SurroundBase type : members) {
			cloneEntity.add((SurroundBase) type.clone());
		}

		return clone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((members == null) ? 0 : members.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundDynamicEntity other = (SurroundDynamicEntity) obj;
		if (members == null) {
			if (other.members != null)
				return false;
		} else if (!members.equals(other.members))
			return false;
		return true;
	}

	@Override
	public String getPath4Type(SurroundBase child) {
		String selector = null;

		if (members.contains(child)) {
			selector = child.getName();
		}

		return selector;
	}

	@Override
	public SurroundBase selectTypeByPath(String pathSegement) {
		return findByName(pathSegement);
	}

	public SurroundBase get(int index) {
		return members.get(index);
	}

	public String asJSON() {

		String entityStr = super.toString();

		try {
			SurroundJSONWriter writer = new SurroundJSONWriter();
			writer.write(this);

			entityStr = writer.beautify();

		} catch (Exception e) {
			throw new SurroundBaseException("error writing entity", e);
		}

		return entityStr;
	}

	@Override
	public String toString() {
		String entityStr = asJSON();
		return entityStr;
	}

	public SurroundDynamicEntity fromJSON(String jsonStr) throws SurroundBaseException, IOException,
			InstantiationException, IllegalAccessException, ClassNotFoundException {

		SurroundJSONReader reader = new SurroundJSONReader(jsonStr);
		Object jsonObject = reader.parse();

		reader.read(this, jsonObject);

		return this;
	}

	@Override
	public SurroundDynamicEntity set(String text) {

		try {
			fromJSON(text);
		} catch (Exception e) {
			SurroundLog.log(e);
		}
		return this;
	}

}
