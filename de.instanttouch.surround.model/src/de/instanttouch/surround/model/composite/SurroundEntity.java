package de.instanttouch.surround.model.composite;

import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundEntity extends SurroundBase {

	public SurroundEntity() {
		// TODO Auto-generated constructor stub
	}

	public SurroundEntity(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public boolean hasOnlyBasicVisibleTypes() {
		boolean onlyBasic = true;

		for (SurroundBase checkType : getConstraints().getVisibleChildren()) {
			if (!checkType.handleAsBasic()) {
				onlyBasic = false;
				break;
			}
		}

		return onlyBasic;
	}

	public int size() {
		return getChildren().size();
	}

}
