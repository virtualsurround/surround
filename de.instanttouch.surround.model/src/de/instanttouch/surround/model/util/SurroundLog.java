/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.util;

import org.eclipse.osgi.framework.log.FrameworkLog;
import org.eclipse.osgi.framework.log.FrameworkLogEntry;
import org.osgi.service.log.LogService;

import de.instanttouch.surround.model.Activator;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;

public class SurroundLog {

	protected static ThreadLocal<SurroundLogger> logger = new ThreadLocal<SurroundLogger>() {
		@Override
		protected SurroundLogger initialValue() {
			return new SurroundLogger();
		}
	};

	public static class SurroundLogger {

		public SurroundLogger() {
			super();
		}

		public void log(String msg, Throwable ex) {
			ex.printStackTrace();
		}

		public void log(Throwable ex) {

			if (SurroundRuntimeTools.isEclipseRunning()) {
				if (getEclipseLogger() != null) {
					getEclipseLogger().log(LogService.LOG_ERROR, ex.getMessage(), ex);
				}
				useFrameworkLogger(FrameworkLogEntry.ERROR, ex.getMessage(), ex);
			} else {
				throw new SurroundNotYetImplementedException("use logging frameworks");
			}
		}

		public void info(String msg) {

			if (SurroundRuntimeTools.isEclipseRunning()) {
				if (getEclipseLogger() != null) {
					getEclipseLogger().log(LogService.LOG_INFO, msg);
				}
				useFrameworkLogger(FrameworkLogEntry.INFO, msg, null);
			} else {
				throw new SurroundNotYetImplementedException("use logging frameworks");
			}
		}

		public void warning(String msg) {

			if (SurroundRuntimeTools.isEclipseRunning()) {
				if (getEclipseLogger() != null) {
					getEclipseLogger().log(LogService.LOG_WARNING, msg);
				}
				useFrameworkLogger(FrameworkLogEntry.WARNING, msg, null);
			} else {
				throw new SurroundNotYetImplementedException("use logging frameworks");
			}
		}

		public void error(String msg) {

			if (SurroundRuntimeTools.isEclipseRunning()) {
				if (getEclipseLogger() != null) {
					getEclipseLogger().log(LogService.LOG_ERROR, msg);
				}
				useFrameworkLogger(FrameworkLogEntry.ERROR, msg, null);
			} else {
				throw new SurroundNotYetImplementedException("use logging frameworks");
			}
		}
	}

	public static void log(String msg, Throwable ex) {
		logger.get().log(msg, ex);
	}

	public static void info(String msg) {
		logger.get().info(msg);
	}

	public static void warning(String msg) {
		logger.get().warning(msg);
	}

	public static void error(String msg) {
		logger.get().warning(msg);
	}

	public static void log(Throwable t) {
		logger.get().log(t);
	}

	public static LogService getEclipseLogger() {
		LogService logService = Activator.getDefault().getLogService();
		return logService;
	}

	public static FrameworkLog getFrameworkLog() {

		FrameworkLog frameworkLog = Activator.getDefault().getFrameworkLog();
		return frameworkLog;
	}

	public static void useFrameworkLogger(int severity, String message, Throwable t) {

		if (getFrameworkLog() != null) {

			int bundleCode = 0;
			int stackCode = 0;
			FrameworkLogEntry children[] = null;

			FrameworkLogEntry logEntry = new FrameworkLogEntry("de.ids.snake", severity, bundleCode, message, stackCode,
					t, children);
			getFrameworkLog().log(logEntry);
		}
	}

	public static void main(String[] args) {

		try {
			SurroundLog.info("info");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
