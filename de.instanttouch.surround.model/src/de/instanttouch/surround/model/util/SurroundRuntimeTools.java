/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.exception.SurroundBaseException;

/**
 * 
 * check if eclipse is running.
 * 
 * Do not use SnakeLog here, to avoid recursive calls since SnakeLog is using
 * SnakeRuntimeTools
 *
 */
public class SurroundRuntimeTools {

	public static <T extends SurroundBase> SurroundClassInfo determineClassInfo(Class<T> elementClass) {

		SurroundClassInfo classInfo = new SurroundClassInfo();

		classInfo.setClassName(elementClass.getCanonicalName());

		try {
			Bundle bundle = FrameworkUtil.getBundle(elementClass);
			classInfo.setBundleName(bundle.getSymbolicName());
		} catch (Throwable e) {
		}

		return classInfo;
	}

	public static <T extends SurroundBase> T createInstanceFor(SurroundClassInfo classInfo) {

		T newInstance = null;
		if (SurroundRuntimeTools.isEclipseRunning()) {
			Bundle bundle = Platform.getBundle(classInfo.getBundleName());
			try {
				Class<?> loadClass = bundle.loadClass(classInfo.getClassName());
				newInstance = (T) loadClass.newInstance();
			} catch (Exception e) {
				throw new SurroundBaseException("creation error", e);
			}
		} else {
			try {
				newInstance = (T) Class.forName(classInfo.getClassName()).newInstance();
			} catch (Exception e) {
				throw new SurroundBaseException("creation error", e);
			}
		}

		return newInstance;
	}

	public static <T extends SurroundBase> T createInstanceFor(Class<T> myClass) {

		T newInstance = null;
		String className = myClass.getCanonicalName();

		try {
			Bundle bundle = FrameworkUtil.getBundle(myClass);
			if (bundle != null) {
				try {
					newInstance = (T) bundle.loadClass(className).newInstance();
				} catch (Exception e) {
					throw new SurroundBaseException("creation error", e);

				}
			}
		} catch (Throwable e) {

			try {
				newInstance = (T) Class.forName(className).newInstance();
			} catch (Exception e2) {
				throw new SurroundBaseException("creation error", e);
			}
		}

		return newInstance;
	}

	public static boolean isEclipseRunning() {
		boolean isRunning = false;
		try {
			isRunning = Platform.isRunning();
		} catch (Throwable e) {
		}

		return isRunning;
	}

	public static boolean isJavaVersionOrHigher(int major, int minor) {

		// get the JVM version
		String version = System.getProperty("java.version");
		int sys_major_version = Integer.parseInt(String.valueOf(version.charAt(2)));

		if (sys_major_version < major) {
			return false;
		} else if (sys_major_version > major) {
			return true;
		} else {
			int underlinepos = version.lastIndexOf("_");

			try {
				int mv = Integer.parseInt(version.substring(underlinepos + 1));
				return (mv >= minor);

			} catch (NumberFormatException e) {

				return false;
			}
		}

	}

	public static String getRuntimeLocation() {

		String path = ".";
		if (isEclipseRunning()) {
			path = Platform.getLocation().toOSString();
		}
		return path;
	}

	public static String getHostName() throws UnknownHostException {

		return InetAddress.getLocalHost().getHostName();
	}

}
