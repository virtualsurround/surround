/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.base.SurroundBase.ISurroundFactory;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundDefaultFactory<T extends SurroundBase> implements ISurroundFactory<T> {

	private static final long serialVersionUID = 1L;

	protected Class<T> exemplar = null;
	protected String name;

	public SurroundDefaultFactory(Class<T> candidate) {
		exemplar = candidate;
	}

	public SurroundDefaultFactory(Class<T> candidate, String name) {
		exemplar = candidate;
		this.name = name;
	}

	@Override
	public T create() throws InstantiationException, IllegalAccessException {

		T newElement = SurroundRuntimeTools.createInstanceFor(exemplar);

		if (name == null) {
			name = exemplar.getSimpleName();
		}
		newElement.setName(name);

		return newElement;
	}

}
