/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundOutOfRangeException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundLong extends SurroundBase {

	protected Long data = null;

	public SurroundLong() {

		super();
		initLong();
	}

	public SurroundLong(String name) {

		super(name);
		initLong();
	}

	private void initLong() {
	}

	@Override
	public SurroundLong set(String value) throws SurroundWrongTypeException {

		if (value.equals("null")) {
			data = null;
		} else {

			try {
				if (value.contains("x")) {
					setAsHexadecimal(true);
				} else {
					setAsHexadecimal(false);
				}
			} catch (Exception e) {
				throw new SurroundWrongTypeException(e.getMessage());
			}

			Long asLong;
			try {
				asLong = Long.decode(value);
			} catch (NumberFormatException e) {
				throw new SurroundWrongTypeException(e.getMessage());
			}
			checkAndSet(asLong);
		}
		return this;
	}

	@Override
	public String toString() {

		String text = isNull() ? "null" : data.toString();

		if (isAsHexadecimal()) {
			text = isNull() ? "0xFFFFFFFF" : "0x" + Long.toHexString(data.longValue());
		}

		return text;
	}

	@Override
	public SurroundLong set(long newValue) throws SurroundOutOfRangeException {

		checkAndSet(newValue);
		return this;
	}

	private void checkAndSet(long newValue) throws SurroundOutOfRangeException {

		if (newValue < getConstraints().getMin()) {
			throw new SurroundOutOfRangeException("value is less min constraint");
		}
		if (newValue > getConstraints().getMax()) {
			throw new SurroundOutOfRangeException("value is less min constraint");
		}

		if (isNull() || newValue != data.longValue()) {
			Long old = data;
			data = newValue;

			SurroundChange<Long> change = SurroundChange.forChange(old, newValue);
			notifyChanged(change);
		}
	}

	@Override
	public long asLong() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("value not set");
		}

		long value = data.longValue();
		return value;
	}

	@Override
	public SurroundLong set(int newValue) throws SurroundWrongTypeException, SurroundOutOfRangeException {
		checkAndSet(newValue);
		return this;

	}

	@Override
	public int asInt() throws SurroundWrongTypeException, SurroundNotInitializedException {

		long value = asLong();
		if (value < Integer.MIN_VALUE && value > Integer.MAX_VALUE) {
			throw new SurroundWrongTypeException("cannot be converted to integer value");
		}

		return (int) value;

	}

	@Override
	public SurroundLong set(double newValue) throws SurroundWrongTypeException, SurroundOutOfRangeException {

		if (newValue < Long.MIN_VALUE || newValue > Long.MAX_VALUE) {
			throw new SurroundOutOfRangeException("value:" + newValue + "can't be handled as long value");
		}

		long asLong = (long) newValue;
		checkAndSet(asLong);
		return this;

	}

	@Override
	public double asDouble() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("value not set");
		}

		double value = data.doubleValue();
		return value;
	}

	public boolean isAsHexadecimal() {
		return getConstraints().isHexadecimal();
	}

	public void setAsHexadecimal(boolean isHex) {
		getConstraints().setHexadecimal(isHex);
	}

	@Override
	public boolean isNull() {

		return data == null;
	}

	@Override
	public SurroundLong setNull() {
		data = null;
		return this;
	}

	@Override
	public Long getValue() {
		return data;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {

		Object clone = super.clone();
		if (!(clone instanceof SurroundLong)) {
			throw new CloneNotSupportedException("wrong object created: " + clone.getClass().getCanonicalName());
		}

		SurroundLong newLong = (SurroundLong) clone;
		try {
			if (!isNull()) {
				newLong.set(data.longValue());
			}
		} catch (SurroundOutOfRangeException e) {
			SurroundLog.log(e);
		}

		return newLong;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundLong other = (SurroundLong) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
