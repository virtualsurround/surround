/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundString extends SurroundBase {

	protected String data = null;

	public SurroundString() {
		super();
	}

	public SurroundString(String name) {
		super(name);
	}

	@Override
	public SurroundString set(String newValue) {
		String old = data;

		if (isNull() || !newValue.equals(data)) {
			data = newValue;

			SurroundChange<String> change = SurroundChange.forChange(old, newValue);
			notifyChanged(change);
		}

		return this;
	}

	@Override
	public String toString() {
		return isNull() ? "" : data;
	}

	@Override
	public String getValue() {
		return data;
	}

	@Override
	public boolean asBoolean() {

		if (isNull()) {
			throw new SurroundNotInitializedException("text not set");
		}

		boolean asBoolean;
		try {
			asBoolean = Boolean.parseBoolean(data);
		} catch (Exception e) {
			throw new SurroundWrongTypeException("couldn't convert to string", e);
		}
		return asBoolean;
	}

	public SurroundString set(boolean newValue) {
		String text = String.valueOf(newValue);
		set(text);
		return this;
	}

	@Override
	public SurroundString set(int newValue) throws SurroundWrongTypeException {
		String text = String.valueOf(newValue);
		set(text);
		return this;
	}

	@Override
	public int asInt() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("text not set");
		}

		int value;
		try {
			value = Integer.parseInt(data);
		} catch (NumberFormatException e) {
			throw new SurroundWrongTypeException("couldn't convert to string", e);
		}
		return value;
	}

	@Override
	public SurroundString set(long newValue) throws SurroundWrongTypeException {
		String text = String.valueOf(newValue);
		set(text);
		return this;
	}

	@Override
	public long asLong() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("text not set");
		}

		long value;
		try {
			value = Long.parseLong(data);
		} catch (NumberFormatException e) {
			throw new SurroundWrongTypeException("couldn't convert to string", e);
		}
		return value;
	}

	@Override
	public SurroundString set(double newValue) throws SurroundWrongTypeException {
		String text = String.valueOf(newValue);
		set(text);
		return this;
	}

	@Override
	public double asDouble() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("text not set");
		}

		double value;
		try {
			value = Double.parseDouble(data);
		} catch (NumberFormatException e) {
			throw new SurroundWrongTypeException("couldn't convert to string", e);
		}
		return value;
	}

	@Override
	public boolean isNull() {
		boolean isNull = data == null;
		return isNull;
	}

	@Override
	public SurroundString setNull() {
		data = null;
		return this;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object clone = super.clone();
		if (!(clone instanceof SurroundString)) {
			throw new CloneNotSupportedException("wrong object created: " + clone.getClass().getCanonicalName());
		}

		SurroundString newString = (SurroundString) clone;
		newString.set(data);

		return newString;
	}

	public boolean isEmpty() {
		return data == null || data.isEmpty();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundString other = (SurroundString) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
