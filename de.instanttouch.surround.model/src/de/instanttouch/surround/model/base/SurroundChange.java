/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;

public class SurroundChange<T> {

	protected Object source;
	protected T oldValue;
	protected T newValue;

	protected String kind = "change";

	protected int index;

	public SurroundChange(T oldValue, T newValue) {
		super();
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public SurroundChange(T newValue, int index) {
		super();
		this.newValue = newValue;
		this.index = index;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public Object getSource() {
		return source;
	}

	public void setSource(Object source) {
		this.source = source;
	}

	public static <T> SurroundChange<T> forChange(T oldValue, T newValue) {

		SurroundChange<T> modification = new SurroundChange<>(oldValue, newValue);
		modification.kind = "change";
		return modification;
	}

	public static <T> SurroundChange<T> forAdd(T newValue, int index) {

		SurroundChange<T> modification = new SurroundChange<>(newValue, index);
		modification.kind = "add";
		return modification;
	}

	public static <T> SurroundChange<T> forRemove(int index) {

		SurroundChange<T> modification = new SurroundChange<>(null, index);
		modification.kind = "remove";
		return modification;
	}

	public SurroundChange<T> undo() {
		throw new SurroundNotYetImplementedException("implement undo by oldValue");
	}

	public SurroundChange<T> redo() {
		throw new SurroundNotYetImplementedException("implement redo by newValue");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		result = prime * result + ((kind == null) ? 0 : kind.hashCode());
		result = prime * result + ((newValue == null) ? 0 : newValue.hashCode());
		result = prime * result + ((oldValue == null) ? 0 : oldValue.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundChange other = (SurroundChange) obj;
		if (index != other.index)
			return false;
		if (kind == null) {
			if (other.kind != null)
				return false;
		} else if (!kind.equals(other.kind))
			return false;
		if (newValue == null) {
			if (other.newValue != null)
				return false;
		} else if (!newValue.equals(other.newValue))
			return false;
		if (oldValue == null) {
			if (other.oldValue != null)
				return false;
		} else if (!oldValue.equals(other.oldValue))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SurroundChange [source=" + source + ", oldValue=" + oldValue + ", newValue=" + newValue + ", kind="
				+ kind + ", index=" + index + "]";
	}

}
