/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundUnsupportedException;
import de.instanttouch.surround.model.io.SurroundJSONReader;
import de.instanttouch.surround.model.io.SurroundJSONWriter;
import de.instanttouch.surround.model.ref.ISurroundResourceConnect;
import de.instanttouch.surround.model.ref.SurroundPath;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundBase {

	public interface ISurroundFactory<T extends SurroundBase> extends Serializable {

		T create() throws SurroundBaseException, InstantiationException, IllegalAccessException;
	}

	protected String name = "";

	protected SurroundConstraintMap constraints;

	public SurroundBase() {
		super();
		initSurroundBase();
		setName(getClass().getSimpleName());
	}

	public SurroundBase(String name) {
		super();
		this.name = name;
		initSurroundBase();
	}

	private void initSurroundBase() {
		constraints = new SurroundConstraintMap(this);
	}

	public String getName() {
		return name;
	}

	public SurroundBase setName(String name) {
		this.name = name;
		return this;
	}

	public SurroundConstraintMap getConstraints() {
		return constraints;
	}

	public SurroundBase addChangeNotificationListener(ISurroundChangeListener listener) {

		getConstraints().addChangeNotificationListener(listener);
		return this;
	}

	public SurroundBase removeChangeNotificationListener(ISurroundChangeListener listener) {

		getConstraints().removeChangeNotificationListener(listener);
		return this;
	}

	public void notifyChanged(SurroundChange<?> change) {

		if (constraints.hasChangeNotifier()) {
			constraints.getChangeNotifier().notifyChange(this, change);
		}
	}

	// children
	public List<? extends SurroundBase> getChildren() {
		return Collections.EMPTY_LIST;
	}

	public List<SurroundBase> getVisibleChildren() {

		List<SurroundBase> visibleChildren = new ArrayList<>();
		for (SurroundBase surroundBase : getChildren()) {
			if (surroundBase.getConstraints().isVisible()) {
				visibleChildren.add(surroundBase);
			}
		}

		return visibleChildren;
	}

	public boolean hasChildren() {

		for (SurroundBase surroundBase : getChildren()) {
			if (surroundBase.getConstraints().isVisible()) {
				return true;
			}
		}

		return false;
	}

	public SurroundReference<?> getReference() {
		return constraints.getReference();
	}

	public SurroundBase setReference(SurroundReference<?> ref) {
		constraints.setReference(ref);
		return this;
	}

	public SurroundBase findByName(String name) {

		SurroundBase foundedFirst = null;

		for (SurroundBase child : getChildren()) {
			String childName = child.getName();
			if (childName.equals(name)) {
				foundedFirst = child;
				break;
			}
		}

		return foundedFirst;
	}

	public <T extends SurroundBase> T findFirst(Class<T> childClass) {

		T founded = null;

		for (SurroundBase child : getChildren()) {

			if (childClass.isInstance(child)) {
				founded = (T) child;
			}
		}
		return founded;
	}

	public <T extends SurroundBase> List<T> findAll(Class<T> childClass) {

		List<T> founded = new ArrayList<>();

		for (SurroundBase child : getChildren()) {

			if (childClass.isInstance(child)) {
				founded.add((T) child);
			}
		}
		return founded;
	}

	public Object getValue() {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public Object getPesenter() {
		return constraints.getPresenter();
	}

	public SurroundBase setPresenter(Object presenter) {
		constraints.setPresenter(presenter);
		return this;
	}

	// type conversion
	public int asInt() {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public long asLong() {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public double asDouble() {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public boolean asBoolean() {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public SurroundBase set(String text) {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public SurroundBase set(int newValue) {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public SurroundBase set(long newValue) {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public SurroundBase set(double newValue) {
		throw new SurroundUnsupportedException("not available for this type");
	}

	public boolean isNull() {
		return true;
	}

	public SurroundBase setNull() {

		return this;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		try {
			SurroundBase type = getClass().newInstance();

			type.constraints = (SurroundConstraintMap) constraints.clone();
			type.setName(getName());

			return type;
		} catch (Exception e) {
			throw new CloneNotSupportedException(e.getMessage());
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getName() != null ? getName().hashCode() : 0;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundBase other = (SurroundBase) obj;
		if (!(other.getName().equals(getName()))) {
			return false;
		}
		return true;
	}

	public boolean handleAsBasic() {
		// true for all simple types
		return false;
	}

	public int compareTo(SurroundBase other) {

		int comp = -1;

		try {
			double a = asDouble();
			double b = other.asDouble();

			comp = Double.compare(a, b);

		} catch (Exception e) {

			String a = toString();
			String b = other.toString();

			comp = a.compareTo(b);
		}

		return comp;
	}

	public void save(ISurroundResourceConnect resource) {

		OutputStream outputStream = null;
		OutputStreamWriter fileWriter = null;
		try {
			outputStream = resource.getOutputStream();

			SurroundJSONWriter writer = new SurroundJSONWriter();
			writer.write(this);

			String content = writer.beautify();
			writer.close();

			fileWriter = new OutputStreamWriter(outputStream);
			fileWriter.write(content);
			fileWriter.flush();

		} catch (Exception e) {

			SurroundLog.log(e);

		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void read(ISurroundResourceConnect resource) {

		InputStream inputStream = null;
		SurroundJSONReader reader = null;

		try {
			inputStream = resource.getInputStream();

			reader = new SurroundJSONReader(inputStream);
			Object jsonObject = reader.parse();

			reader.read(this, jsonObject);

		} catch (Exception e) {
			throw new SurroundBaseException("reading json stream failed ", e);
		} finally {
			if (reader != null) {
				reader.close();
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getPath4Type(SurroundBase child) {
		return null;
	}

	public SurroundBase selectTypeByPath(String pathSegement) {
		return null;
	}

	public SurroundBase select(SurroundPath surroundPath) {
		return null;
	}

}
