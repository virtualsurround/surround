/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import de.instanttouch.surround.model.collection.SurroundOptions;
import de.instanttouch.surround.model.domain.SurroundGeoCoordinate;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.url.SurroundURL;

@SuppressWarnings("serial")
public class SurroundConstraintMap implements Serializable, Cloneable {

	public class SurroundConstraint<T> {

		T type;

		SurroundConstraint() {
			super();
		}

		public SurroundConstraint(T type) {
			super();
			this.type = type;
		}

		public T getType() {
			return type;
		}

		public void setType(T type) {
			this.type = type;
		}

		public boolean isSet() {
			return type != null;
		}
	}

	public class SurroundChangeNotificator {

		protected List<ISurroundChangeListener> changeListeners = new ArrayList<>();

		public SurroundChangeNotificator addChangeListener(ISurroundChangeListener listener) {

			if (!changeListeners.contains(listener)) {
				changeListeners.add(listener);
			}
			return this;
		}

		public SurroundChangeNotificator removeListener(ISurroundChangeListener listener) {

			changeListeners.remove(listener);

			return this;
		}

		public void notifyChange(SurroundBase type, SurroundChange<?> change) {

			for (ISurroundChangeListener changeListener : changeListeners) {
				changeListener.changed(type, change);
			}
		}

	}

	protected SurroundBase owner;
	protected Map<String, SurroundConstraint<?>> constraints = new HashMap<>();

	public SurroundConstraintMap() {
		super();
	}

	public SurroundConstraintMap(SurroundBase owner) {
		super();
		this.owner = owner;
	}

	public SurroundBase getOwner() {
		return owner;
	}

	public SurroundConstraintMap setOwner(SurroundBase owner) {
		this.owner = owner;
		return this;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public SurroundConstraint<?> get(String constraint) {

		SurroundConstraint<?> conconConstraintEntry = constraints.get(constraint);
		return conconConstraintEntry;
	}

	public <T> SurroundConstraint<T> get(String constraint, Class<T> constraintClass) {

		SurroundConstraint<T> typedConstraint = (SurroundConstraint<T>) constraints.get(constraint);
		return typedConstraint;
	}

	public boolean isMissingOrSet(String constraint) {

		boolean isOk = false;
		SurroundConstraint<?> checkConstraint = get(constraint);
		if (checkConstraint != null) {
			isOk = checkConstraint.isSet();
		} else {
			isOk = true;
		}
		return isOk;
	}

	public boolean isMissingOrNotSet(String constraint) {

		boolean isOk = false;
		SurroundConstraint<?> checkConstraint = get(constraint);
		if (checkConstraint != null) {
			isOk = !checkConstraint.isSet();
		} else {
			isOk = true;
		}
		return isOk;
	}

	public boolean isSet(String constraint) {

		boolean isSet = false;

		SurroundConstraint<?> checkConstraint = get(constraint);
		if (checkConstraint != null) {
			isSet = checkConstraint.isSet();
		}
		return isSet;
	}

	public boolean isVisible() {

		return isMissingOrSet("visible");
	}

	public SurroundBase setVisible(boolean bVisible) throws SurroundBaseException {

		SurroundConstraint<Boolean> checkConstraint = get("visible", Boolean.class);
		if (checkConstraint == null || !bVisible) {
			SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(bVisible);
			constraints.put("visible", constraintEntry);
		} else {
			checkConstraint.setType(bVisible);
		}

		return owner;
	}

	public boolean hasX() {
		return get("x", Double.class) != null;
	}

	public double getX() {
		SurroundConstraint<Double> xConstraint = get("x", Double.class);
		if (xConstraint != null) {
			return xConstraint.getType();
		}

		return -1;
	}

	public SurroundConstraintMap setX(double x) {

		SurroundConstraint<Double> xConstraint = get("x", Double.class);
		if (xConstraint == null) {
			xConstraint = new SurroundConstraint<Double>(x);
			constraints.put("x", xConstraint);
		} else {
			xConstraint.setType(x);
		}

		return this;
	}

	public boolean hasY() {
		return get("y", Double.class) != null;
	}

	public double getY() {
		SurroundConstraint<Double> yConstraint = get("y", Double.class);
		if (yConstraint != null) {
			return yConstraint.getType();
		}

		return -1;
	}

	public SurroundConstraintMap setY(double y) {

		SurroundConstraint<Double> yConstraint = get("y", Double.class);
		if (yConstraint == null) {
			yConstraint = new SurroundConstraint<Double>(y);
			constraints.put("y", yConstraint);
		} else {
			yConstraint.setType(y);
		}

		return this;
	}

	public boolean hasWidth() {
		return get("width", Double.class) != null;
	}

	public double getWidth() {
		SurroundConstraint<Double> widthConstraint = get("width", Double.class);
		if (widthConstraint != null) {
			return widthConstraint.getType();
		}

		return -1;
	}

	public SurroundConstraintMap setWidth(double width) {

		SurroundConstraint<Double> widthConstraint = get("width", Double.class);
		if (widthConstraint == null) {
			widthConstraint = new SurroundConstraint<Double>(width);
			constraints.put("width", widthConstraint);
		} else {
			widthConstraint.setType(width);
		}

		return this;
	}

	public boolean hasHeight() {
		return get("height", Double.class) != null;
	}

	public double getHeight() {
		SurroundConstraint<Double> heightConstraint = get("height", Double.class);
		if (heightConstraint != null) {
			return heightConstraint.getType();
		}

		return -1;
	}

	public SurroundConstraintMap setHeight(double width) {

		SurroundConstraint<Double> heightConstraint = get("height", Double.class);
		if (heightConstraint == null) {
			heightConstraint = new SurroundConstraint<Double>(width);
			constraints.put("height", heightConstraint);
		} else {
			heightConstraint.setType(width);
		}

		return this;
	}

	public boolean hasUUID() {

		return get("uuid", UUID.class) != null;
	}

	public UUID getUUID() {

		SurroundConstraint<UUID> constraint = get("uuid", UUID.class);
		if (constraint != null) {
			UUID uuid = constraint.getType();
			return uuid;
		}

		return null;
	}

	public SurroundConstraintMap setUUID(UUID uuid) {

		SurroundConstraint<UUID> constraint = get("uuid", UUID.class);
		if (constraint != null) {
			constraint.setType(uuid);
		} else {
			constraint = new SurroundConstraint<UUID>(uuid);
			constraints.put("uuid", constraint);
		}

		return this;
	}

	public List<SurroundBase> getVisibleChildren() {

		List<SurroundBase> listChildren = new ArrayList<SurroundBase>();
		for (SurroundBase child : owner.getChildren()) {
			if (child.getConstraints().isVisible()) {
				listChildren.add(child);
			}
		}
		return listChildren;
	}

	public boolean hasReference() {

		return get("ref", SurroundReference.class) != null;
	}

	public SurroundReference<?> getReference() {

		SurroundConstraint<SurroundReference> constraint = get("ref", SurroundReference.class);
		if (constraint != null) {
			SurroundReference<?> ref = constraint.getType();
			return ref;
		}

		return null;
	}

	public SurroundConstraintMap setReference(SurroundReference ref) {

		SurroundConstraint<SurroundReference> constraint = get("ref", SurroundReference.class);
		if (constraint != null) {
			constraint.setType(ref);
		} else {
			constraint = new SurroundConstraint<SurroundReference>(ref);
			constraints.put("ref", constraint);
		}

		return this;
	}

	public boolean hasMin() {
		return get("min", Double.class) != null;
	}

	public double getMin() {
		SurroundConstraint<Double> yConstraint = get("min", Double.class);
		if (yConstraint != null) {
			return yConstraint.getType();
		}

		return Double.NEGATIVE_INFINITY;
	}

	public SurroundConstraintMap setMin(double min) {

		SurroundConstraint<Double> yConstraint = get("min", Double.class);
		if (yConstraint == null) {
			yConstraint = new SurroundConstraint<Double>(min);
			constraints.put("min", yConstraint);
		} else {
			yConstraint.setType(min);
		}

		return this;
	}

	public boolean hasMax() {
		return get("max", Double.class) != null;
	}

	public double getMax() {
		SurroundConstraint<Double> yConstraint = get("max", Double.class);
		if (yConstraint != null) {
			return yConstraint.getType();
		}

		return Double.MAX_VALUE;
	}

	public SurroundConstraintMap setMax(double max) {

		SurroundConstraint<Double> yConstraint = get("max", Double.class);
		if (yConstraint == null) {
			yConstraint = new SurroundConstraint<Double>(max);
			constraints.put("max", yConstraint);
		} else {
			yConstraint.setType(max);
		}

		return this;
	}

	public boolean isHexadecimal() {

		boolean isHex = false;

		SurroundConstraint<Boolean> hexConstraint = get("hex", Boolean.class);
		if (hexConstraint != null) {
			isHex = hexConstraint.getType().booleanValue();
		}

		return isHex;
	}

	public SurroundConstraintMap setHexadecimal(boolean isHex) {

		SurroundConstraint<Boolean> yConstraint = get("hex", Boolean.class);
		if (yConstraint == null) {
			yConstraint = new SurroundConstraint<Boolean>(isHex);
			constraints.put("hex", yConstraint);
		} else {
			yConstraint.setType(isHex);
		}
		return this;
	}

	public boolean hasGeoCoordinate() {

		return get("geo_coordinate", SurroundGeoCoordinate.class) != null;
	}

	public SurroundGeoCoordinate getGeoCoordinate() {

		SurroundConstraint<SurroundGeoCoordinate> constraint = get("geo_coordinate", SurroundGeoCoordinate.class);
		if (constraint != null) {
			SurroundGeoCoordinate geoCoordinate = constraint.getType();
			return geoCoordinate;
		}

		return null;
	}

	public SurroundConstraintMap setGeoCoordinate(SurroundGeoCoordinate geoCoordinate) {

		SurroundConstraint<SurroundGeoCoordinate> constraint = get("geo_coordinate", SurroundGeoCoordinate.class);
		if (constraint != null) {
			constraint.setType(geoCoordinate);
		} else {
			constraint = new SurroundConstraint<SurroundGeoCoordinate>(geoCoordinate);
			constraints.put("geo_coordinate", constraint);
		}

		return this;
	}

	public boolean hasClassInfo() {

		return get("class_info", SurroundClassInfo.class) != null;
	}

	public SurroundClassInfo getClassInfo() {

		SurroundConstraint<SurroundClassInfo> constraint = get("class_info", SurroundClassInfo.class);
		if (constraint != null) {
			SurroundClassInfo ref = constraint.getType();
			return ref;
		}

		return null;
	}

	public SurroundConstraintMap setClassInfo(SurroundClassInfo ref) {

		SurroundConstraint<SurroundClassInfo> constraint = get("class_info", SurroundClassInfo.class);
		if (constraint != null) {
			constraint.setType(ref);
		} else {
			constraint = new SurroundConstraint<SurroundClassInfo>(ref);
			constraints.put("class_info", constraint);
		}

		return this;
	}

	public boolean hasOptions() {

		return get("options", SurroundOptions.class) != null;
	}

	public SurroundOptions<?> getOptions() {

		SurroundConstraint<SurroundOptions> constraint = get("options", SurroundOptions.class);
		if (constraint != null) {
			SurroundOptions options = constraint.getType();
			return options;
		}

		return null;
	}

	public SurroundConstraintMap setOptions(SurroundOptions<?> options) {

		SurroundConstraint<SurroundOptions> constraint = get("options", SurroundOptions.class);
		if (constraint != null) {
			constraint.setType(options);
		} else {
			constraint = new SurroundConstraint<SurroundOptions>(options);
			constraints.put("options", constraint);
		}

		return this;
	}

	public boolean hasChangeNotifier() {

		return get("notification", SurroundChangeNotificator.class) != null;
	}

	public SurroundChangeNotificator getChangeNotifier() {

		SurroundConstraint<SurroundChangeNotificator> constraint = get("notification", SurroundChangeNotificator.class);
		if (constraint != null) {
			SurroundChangeNotificator changeNotifier = constraint.getType();
			return changeNotifier;
		}

		return null;
	}

	public SurroundConstraintMap addChangeNotificationListener(ISurroundChangeListener listener) {

		SurroundConstraint<SurroundChangeNotificator> constraint = get("notification", SurroundChangeNotificator.class);
		if (constraint != null) {
			constraint.getType().addChangeListener(listener);
		} else {

			SurroundChangeNotificator changeNotifier = new SurroundChangeNotificator();
			changeNotifier.addChangeListener(listener);

			constraint = new SurroundConstraint<SurroundChangeNotificator>(changeNotifier);
			constraints.put("notification", constraint);
		}

		return this;
	}

	public SurroundConstraintMap removeChangeNotificationListener(ISurroundChangeListener listener) {

		SurroundConstraint<SurroundChangeNotificator> constraint = get("notification", SurroundChangeNotificator.class);
		if (constraint != null) {
			constraint.getType().removeListener(listener);
		}

		return this;
	}

	public boolean isExpanded() {

		return isMissingOrNotSet("expanded");
	}

	public SurroundBase setExpanded(boolean expanded) throws SurroundBaseException {

		SurroundConstraint<Boolean> checkConstraint = get("expanded", Boolean.class);
		if (checkConstraint == null) {
			if (expanded) {
				SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(Boolean.TRUE);
				constraints.put("expanded", constraintEntry);
			}
		} else {
			checkConstraint.setType(expanded);
		}

		return owner;
	}

	public boolean isPassword() {

		boolean isPassword = false;
		SurroundConstraint<Boolean> surroundConstraint = get("password", Boolean.class);
		if (surroundConstraint != null) {
			isPassword = surroundConstraint.getType();
		}

		return isPassword;
	}

	public SurroundBase setPassword(boolean password) throws SurroundBaseException {

		SurroundConstraint<Boolean> checkConstraint = get("password", Boolean.class);
		if (checkConstraint == null) {
			if (password) {
				SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(Boolean.TRUE);
				constraints.put("password", constraintEntry);
			}
		} else {
			checkConstraint.setType(password);
		}

		return owner;
	}

	public boolean isRequired() {

		SurroundConstraint<Boolean> surroundConstraint = get("required", Boolean.class);
		if (surroundConstraint != null) {
			return surroundConstraint.getType();
		}
		return false;
	}

	public SurroundBase setRequired(boolean required) throws SurroundBaseException {

		SurroundConstraint<Boolean> checkConstraint = get("required", Boolean.class);
		if (checkConstraint == null) {
			if (required) {
				SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(Boolean.TRUE);
				constraints.put("password", constraintEntry);
			}
		} else {
			checkConstraint.setType(required);
		}

		return owner;
	}

	public boolean isLarge() {

		boolean large = false;

		SurroundConstraint<Boolean> checkConstraint = get("large", Boolean.class);
		if (checkConstraint != null) {
			large = checkConstraint.getType();
		}

		return large;
	}

	public SurroundBase setLarge(boolean large) throws SurroundBaseException {

		SurroundConstraint<Boolean> checkConstraint = get("large", Boolean.class);
		if (checkConstraint == null) {
			if (large) {
				SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(Boolean.TRUE);
				constraints.put("large", constraintEntry);
			}
		} else {
			checkConstraint.setType(large);
		}

		return owner;
	}

	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isEnabled() {

		return isMissingOrSet("enabled");
	}

	public SurroundBase setEnabled(boolean enabled) throws SurroundBaseException {

		SurroundConstraint<Boolean> checkConstraint = get("enabled", Boolean.class);
		if (checkConstraint == null || !enabled) {
			SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(enabled);
			constraints.put("enabled", constraintEntry);
		} else {
			checkConstraint.setType(enabled);
		}

		return owner;
	}

	public SurroundBase setSelected(boolean selected) {

		SurroundConstraint<Boolean> checkConstraint = get("selected", Boolean.class);
		if (checkConstraint == null || !selected) {
			SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(selected);
			constraints.put("selected", constraintEntry);
		} else {
			checkConstraint.setType(selected);
		}

		return owner;
	}

	public boolean isSelected() {

		boolean selected = false;

		SurroundConstraint<Boolean> checkConstraint = get("selected", Boolean.class);
		if (checkConstraint != null) {
			selected = checkConstraint.getType();
		}

		return selected;
	}

	public boolean isWrapped() {

		boolean wrap = false;

		SurroundConstraint<Boolean> checkConstraint = get("wrap", Boolean.class);
		if (checkConstraint != null) {
			wrap = checkConstraint.getType();
		}

		return wrap;
	}

	public SurroundBase setWrapped(boolean isWrapped) {

		SurroundConstraint<Boolean> checkConstraint = get("wrap", Boolean.class);
		if (checkConstraint == null || !isWrapped) {
			SurroundConstraint<Boolean> constraintEntry = new SurroundConstraint<>(isWrapped);
			constraints.put("wrap", constraintEntry);
		} else {
			checkConstraint.setType(isWrapped);
		}

		return owner;
	}

	public boolean hasWiki() {

		SurroundConstraint<Boolean> checkConstraint = get("wiki", Boolean.class);
		return checkConstraint != null;
	}

	public SurroundURL getWiki() {

		SurroundConstraint<SurroundURL> checkConstraint = get("wiki", SurroundURL.class);
		if (checkConstraint != null) {
			return checkConstraint.getType();
		}

		return null;
	}

	public SurroundBase setWiki(SurroundURL wikiUrl) {

		SurroundConstraint<SurroundURL> checkConstraint = get("wiki", SurroundURL.class);
		if (checkConstraint == null) {
			SurroundConstraint<SurroundURL> constraintEntry = new SurroundConstraint<>(wikiUrl);
			constraints.put("wiki", constraintEntry);
		} else {
			checkConstraint.setType(wikiUrl);
		}

		return owner;
	}

	public Object getPresenter() {

		Object presenter = null;

		SurroundConstraint<Object> checkConstraint = get("presenter", Object.class);
		if (checkConstraint != null) {
			presenter = checkConstraint.getType();
		}

		return presenter;
	}

	public SurroundBase setPresenter(Object presenter) {

		SurroundConstraint<Object> checkConstraint = get("presenter", Object.class);
		if (checkConstraint == null) {
			SurroundConstraint<Object> constraintEntry = new SurroundConstraint<>(presenter);
			constraints.put("presenter", constraintEntry);
		} else {
			checkConstraint.setType(presenter);
		}

		return owner;
	}

	public SurroundBase getValue() {

		SurroundBase value = null;

		SurroundConstraint<SurroundBase> checkConstraint = get("value", SurroundBase.class);
		if (checkConstraint != null) {
			value = checkConstraint.getType();
		}

		return value;
	}

	public SurroundBase setValue(SurroundBase value) {

		if (value == null) {
			constraints.remove("value");
			return owner;
		}

		SurroundConstraint<SurroundBase> checkConstraint = get("value", SurroundBase.class);
		if (checkConstraint == null) {
			SurroundConstraint<Object> constraintEntry = new SurroundConstraint<>(value);
			constraints.put("value", constraintEntry);
		} else {
			checkConstraint.setType(value);
		}

		return owner;
	}

}
