/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundClassInfo extends SurroundDynamicEntity {

	public SurroundClassInfo() {
		super();
		initClassInfo();
	}

	public SurroundClassInfo(String name) {
		super(name);
		initClassInfo();
	}

	private void initClassInfo() {

		add(new SurroundString("bundleName"));
		add(new SurroundString("className"));
	}

	public String getBundleName() {

		return findByName("bundleName").toString();
	}

	public SurroundClassInfo setBundleName(String bundleName) {

		findByName("bundleName").set(bundleName);
		return this;
	}

	public String getClassName() {

		return findByName("className").toString();
	}

	public SurroundClassInfo setClassName(String className) {

		findByName("className").set(className);
		return this;
	}

	public String getNameWithoutPackage() {

		String className = getClassName();
		int sep = className.lastIndexOf(".");

		String simpleName = sep >= 0 ? className.substring(sep + 1) : className;

		return simpleName;
	}

}
