/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundOutOfRangeException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundInteger extends SurroundBase {

	protected Integer data = null;

	public SurroundInteger() {

		super();
		initLong();
	}

	public SurroundInteger(String name) {

		super(name);
		initLong();
	}

	private void initLong() {
	}

	@Override
	public SurroundInteger set(String value) throws SurroundWrongTypeException {

		if (value.equals("null")) {
			data = null;
		} else {

			try {
				if (value.contains("x")) {
					setAsHexadecimal(true);
				} else {
					setAsHexadecimal(false);
				}
			} catch (Exception e) {
				throw new SurroundWrongTypeException(e.getMessage());
			}

			int asInt;
			try {
				asInt = Integer.decode(value);
			} catch (NumberFormatException e) {
				throw new SurroundWrongTypeException(e.getMessage());
			}
			checkAndSet(asInt);
		}
		return this;
	}

	@Override
	public String toString() {

		String text = isNull() ? "null" : data.toString();

		if (isAsHexadecimal()) {
			text = isNull() ? "0xFFFFFFFF" : "0x" + Long.toHexString(data.longValue());
		}

		return text;
	}

	@Override
	public SurroundInteger set(int newValue) throws SurroundOutOfRangeException {

		checkAndSet(newValue);
		return this;
	}

	private void checkAndSet(int newValue) throws SurroundOutOfRangeException {

		if (newValue < getConstraints().getMin()) {
			throw new SurroundOutOfRangeException("value is less min constraint");
		}
		if (newValue > getConstraints().getMax()) {
			throw new SurroundOutOfRangeException("value is less min constraint");
		}

		if (isNull() || newValue != data.intValue()) {
			Integer old = data;
			data = newValue;

			SurroundChange<Integer> change = SurroundChange.forChange(old, newValue);
			notifyChanged(change);
		}
	}

	@Override
	public int asInt() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("value not set");
		}

		int value = data.intValue();
		return value;
	}

	@Override
	public SurroundInteger set(double newValue) throws SurroundWrongTypeException, SurroundOutOfRangeException {

		if (newValue < Integer.MIN_VALUE || newValue > Integer.MAX_VALUE) {
			throw new SurroundOutOfRangeException("value:" + newValue + "can't be handled as long value");
		}

		int asInt = (int) newValue;
		checkAndSet(asInt);
		return this;
	}

	@Override
	public double asDouble() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("value not set");
		}

		double value = data.doubleValue();
		return value;
	}

	@Override
	public SurroundInteger set(long newValue) throws SurroundWrongTypeException, SurroundOutOfRangeException {

		if (newValue < Integer.MIN_VALUE || newValue > Integer.MAX_VALUE) {
			throw new SurroundWrongTypeException("value:" + newValue + "can't be handled as long value");
		}

		int asInt = (int) newValue;
		checkAndSet(asInt);
		return this;

	}

	@Override
	public long asLong() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("value not set");
		}

		long value = data.longValue();
		return value;
	}

	public boolean isAsHexadecimal() {

		return getConstraints().isHexadecimal();
	}

	public void setAsHexadecimal(boolean isHex) throws SurroundBaseException {

		getConstraints().setHexadecimal(isHex);
	}

	@Override
	public boolean isNull() {

		return data == null;
	}

	@Override
	public SurroundInteger setNull() {

		data = null;
		return this;
	}

	@Override
	public Integer getValue() {
		return data;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {

		Object clone = super.clone();
		if (!(clone instanceof SurroundInteger)) {
			throw new CloneNotSupportedException("wrong object created: " + clone.getClass().getCanonicalName());
		}

		SurroundInteger newInt = (SurroundInteger) clone;
		try {
			if (!isNull()) {
				newInt.set(data.intValue());
			}
		} catch (SurroundOutOfRangeException e) {
			SurroundLog.log(e);
		}

		return newInt;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundInteger other = (SurroundInteger) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
