/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundBoolean extends SurroundBase {

	protected Boolean data = null;

	public SurroundBoolean() {
		super();
		initBoolean();
	}

	public SurroundBoolean(String name) {
		super(name);
		initBoolean();
	}

	private void initBoolean() {
	}

	@Override
	public SurroundBoolean set(String value) throws SurroundWrongTypeException {
		Boolean booleanValue;
		try {
			booleanValue = Boolean.valueOf(value);
			set(booleanValue);
		} catch (Exception e) {
			setNull();
		}

		return this;
	}

	@Override
	public String toString() {
		return data == null ? "not_set" : data.toString();
	}

	public boolean get() {
		return data == null ? false : data;
	}

	public SurroundBoolean set(boolean newValue) {

		if (isNull() || newValue != data.booleanValue()) {
			Boolean oldValue = data;
			data = newValue;

			SurroundChange<?> change = SurroundChange.forChange(oldValue, newValue);
			notifyChanged(change);
		}

		return this;
	}

	@Override
	public boolean isNull() {
		return data == null;
	}

	@Override
	public SurroundBoolean setNull() {
		data = null;
		return this;
	}

	@Override
	public boolean asBoolean() {
		return get();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object clone = super.clone();
		if (!(clone instanceof SurroundBoolean)) {
			throw new CloneNotSupportedException("wrong object created: " + clone.getClass().getCanonicalName());
		}

		SurroundBoolean newBoolean = (SurroundBoolean) clone;
		newBoolean.set(get());

		return newBoolean;
	}

	@Override
	public Boolean getValue() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundBoolean other = (SurroundBoolean) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
