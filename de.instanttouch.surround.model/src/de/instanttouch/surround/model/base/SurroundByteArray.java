/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundByteArray extends SurroundBase {

	protected byte[] data = null;

	public SurroundByteArray() {
		super();
		initByteArray(256);
	}

	public SurroundByteArray(int size) {
		super();
		initByteArray(size);
	}

	public SurroundByteArray(String name, int size) {
		super(name);
		initByteArray(size);
	}

	private void initByteArray(int size) {

		data = new byte[size];
	}

	public byte[] get() {
		return data;
	}

	public SurroundByteArray set(byte[] newData) {

		byte[] old = data;
		data = newData;

		SurroundChange<byte[]> change = SurroundChange.forChange(old, newData);
		notifyChanged(change);

		return this;
	}

	public void append(byte[] newData) {
		append(newData, 0, newData.length);
	}

	public void append(byte[] newData, int offset, int length) {

		ByteArrayOutputStream helper = new ByteArrayOutputStream();
		if (data != null) {
			helper.write(data, 0, data.length);
		}

		helper.write(newData, offset, length);
		set(helper.toByteArray());
	}

	public static String toHexString(byte value, boolean noPrefix) {

		String str = Integer.toHexString(value);
		if (str.length() > 2) {
			str = str.substring(str.length() - 2);
		}
		while (str.length() < 2) {
			str = "0" + str;
		}
		return (noPrefix ? "" : "0x") + str.toUpperCase();
	}

	@Override
	public String toString() {
		StringBuffer strBuffer = new StringBuffer();

		byte[] data = get();
		if (data != null) {
			for (int i = 0; i < data.length; i++) {
				strBuffer.append(toHexString(data[i], i != 0));
			}
		}

		return strBuffer.toString();
	}

	@Override
	public boolean isNull() {
		return data == null;
	}

	@Override
	public SurroundByteArray setNull() {
		data = null;
		return this;
	}

	@Override
	public SurroundByteArray set(String newValue) throws SurroundWrongTypeException {

		int index = 0;
		int bytes = newValue.length() / 2;

		if (newValue.startsWith("0x")) {
			index = 2;
			bytes = bytes - 1;
		}

		byte[] asByteArray = new byte[bytes];

		for (int i = 0; i < bytes; i++) {
			String singleByte = newValue.substring(index, index + 2);
			Short asShort = Short.parseShort(singleByte, 16);
			byte asByte = (byte) (asShort & 0x00FF);

			asByteArray[i] = asByte;
			index += 2;
		}

		set(asByteArray);

		return this;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		try {
			Object clone = super.clone();
			if (!(clone instanceof SurroundByteArray)) {
				throw new CloneNotSupportedException("unexpected type: " + clone.getClass().getName());
			}

			byte[] data = get();
			((SurroundByteArray) clone).set(data);

			return clone;
		} catch (Exception e) {
			throw new CloneNotSupportedException(e.getMessage());
		}

	}

	@Override
	public byte[] getValue() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(data);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundByteArray other = (SurroundByteArray) obj;
		if (!Arrays.equals(data, other.data))
			return false;
		return true;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
