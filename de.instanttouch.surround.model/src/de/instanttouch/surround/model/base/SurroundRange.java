package de.instanttouch.surround.model.base;

public class SurroundRange extends SurroundLong {

	public SurroundRange(long min, long max){
		super();
		initRange(min,max);
	}

	public SurroundRange(String name,long min, long max){
		super(name);
		initRange(min,max);
	}
	private void initRange(long min, long max) {
		getConstraints().setMin(min);
		getConstraints().setMax(max);
	}	
	
	public long getMinimum(){
		return (long)getConstraints().getMin();
	}
	public long getMaximum(){
		return (long)getConstraints().getMax();
	}
}
