/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.base;

import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundOutOfRangeException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundDouble extends SurroundBase {

	protected Double data = null;

	public SurroundDouble() {

		super();
		initDouble();
	}

	public SurroundDouble(String name) {

		super(name);
		initDouble();
	}

	private void initDouble() {
	}

	@Override
	public SurroundDouble set(String value) throws SurroundWrongTypeException {

		if (value.equals("null")) {
			data = null;
		} else {
			Double asDouble = Double.valueOf(value);
			checkAndSet(asDouble);
		}
		return this;
	}

	@Override
	public SurroundDouble set(int newValue) throws SurroundWrongTypeException, SurroundOutOfRangeException {
		checkAndSet(newValue);
		return this;
	}

	@Override
	public SurroundDouble set(long newValue) throws SurroundWrongTypeException, SurroundOutOfRangeException {
		checkAndSet(newValue);
		return this;
	}

	@Override
	public String toString() {

		return isNull() ? "null" : data.toString();
	}

	@Override
	public SurroundDouble set(double newValue) throws SurroundOutOfRangeException {
		checkAndSet(newValue);
		return this;
	}

	private void checkAndSet(double newValue) throws SurroundOutOfRangeException {

		if (newValue < getConstraints().getMin()) {
			throw new SurroundOutOfRangeException("value is less min constraint");
		}
		if (newValue > getConstraints().getMax()) {
			throw new SurroundOutOfRangeException("value is less min constraint");
		}

		if (isNull() || newValue != data.doubleValue()) {
			Double old = data;
			data = newValue;

			SurroundChange<Double> change = SurroundChange.forChange(old, newValue);
			notifyChanged(change);
		}
	}

	@Override
	public double asDouble() throws SurroundNotInitializedException {

		if (isNull()) {
			throw new SurroundNotInitializedException("double value not set");
		}

		double value = data;
		return value;
	}

	@Override
	public int asInt() throws SurroundWrongTypeException, SurroundNotInitializedException {

		double value = asDouble();
		if (value < Integer.MIN_VALUE && value > Integer.MAX_VALUE) {
			throw new SurroundWrongTypeException("cannot be converted to integer value");
		}

		return (int) value;
	}

	@Override
	public long asLong() throws SurroundWrongTypeException, SurroundNotInitializedException {

		double value = asDouble();

		if (value < Long.MIN_VALUE && value > Long.MAX_VALUE) {
			throw new SurroundWrongTypeException("cannot be converted to integer value");
		}

		return (long) value;
	}

	@Override
	public boolean isNull() {
		return data == null;
	}

	@Override
	public SurroundDouble setNull() {
		data = null;
		return this;
	}

	@Override
	public Double getValue() {
		return data;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {

		Object clone = super.clone();
		if (!(clone instanceof SurroundDouble)) {
			throw new CloneNotSupportedException("wrong object created: " + clone.getClass().getCanonicalName());
		}

		SurroundDouble newDouble = (SurroundDouble) clone;
		try {
			if (!isNull()) {
				newDouble.set(data.doubleValue());
			}
		} catch (SurroundOutOfRangeException e) {
			SurroundLog.log(e);
		}

		return newDouble;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundDouble other = (SurroundDouble) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
