package de.instanttouch.surround.model.url;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

public class SurroundURLStreamHandlerFactory implements URLStreamHandlerFactory {

	public SurroundURLStreamHandlerFactory() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public URLStreamHandler createURLStreamHandler(String urlStr) {

		URLStreamHandler urlStreamHandler = null;

		if (urlStr.startsWith("surround")) {
			urlStreamHandler = new SurroundURLStreamHandler();
		}

		return urlStreamHandler;
	}

}
