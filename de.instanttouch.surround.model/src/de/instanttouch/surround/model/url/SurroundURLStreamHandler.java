package de.instanttouch.surround.model.url;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class SurroundURLStreamHandler extends URLStreamHandler {

	public SurroundURLStreamHandler() {
	}

	@Override
	protected URLConnection openConnection(URL url) throws IOException {
		return new SurroundURLConnection(url);
	}

}
