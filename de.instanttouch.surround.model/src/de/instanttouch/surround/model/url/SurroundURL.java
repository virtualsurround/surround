/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.url;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLStreamHandler;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.ref.ISurroundResourceConnect;

public class SurroundURL extends SurroundString implements ISurroundResourceConnect {

	public SurroundURL() {
		super();
		initURL();
	}

	public SurroundURL(String name) {
		super(name);
		initURL();
	}

	private void initURL() {
	}

	public URL asURL() throws MalformedURLException {

		URL url = null;

		SurroundURLStreamHandlerFactory factory = new SurroundURLStreamHandlerFactory();
		URLStreamHandler streamHandler = factory.createURLStreamHandler(toString());

		if (streamHandler != null) {
			url = new URL(null, toString(), streamHandler);
		} else {
			url = new URL(toString());
		}
		return url;
	}

	public SurroundURL fromURL(String urlStr) throws MalformedURLException {
		set(urlStr);
		asURL();
		return this;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return asURL().openConnection().getOutputStream();
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return asURL().openConnection().getInputStream();

	}

	public void setExternal(boolean external) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
