package de.instanttouch.surround.model.url;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import de.instanttouch.surround.model.exception.SurroundUnsupportedException;

public class SurroundURLConnection extends URLConnection {

	private String path;

	public SurroundURLConnection(URL url) {
		super(url);

		String protocol = url.getProtocol();
		if (!protocol.equals("surround")) {
			throw new SurroundUnsupportedException("invalid protocol: " + protocol + " only surround supported");
		}

		path = url.getPath();
	}

	@Override
	public void connect() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public Object getContent() throws IOException {
		// TODO Auto-generated method stub
		return super.getContent();
	}

	@Override
	public InputStream getInputStream() throws IOException {

		FileInputStream inputStream = new FileInputStream(path);
		return inputStream;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {

		FileOutputStream output = new FileOutputStream(path);
		return output;
	}
}
