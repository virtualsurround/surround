/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.exception;

@SuppressWarnings("serial")
public class SurroundWrongTypeException extends SurroundBaseException {

	public SurroundWrongTypeException(String message) {
		super(message);
	}

	public SurroundWrongTypeException(String message, Throwable cause) {
		super(message, cause);
	}

}
