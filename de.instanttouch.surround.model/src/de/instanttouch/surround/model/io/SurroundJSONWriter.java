/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.StringTokenizer;

import org.json.simple.JSONValue;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.composite.SurroundValueType;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.time.SurroundDate;
import de.instanttouch.surround.model.util.SurroundLog;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundJSONWriter implements AutoCloseable {

	protected Writer writer = null;
	protected PrintWriter printWriter = null;

	public SurroundJSONWriter() {
		writer = new StringWriter();
		printWriter = new PrintWriter(writer);
	}

	public SurroundJSONWriter(OutputStream output) {
		writer = new OutputStreamWriter(output);
		printWriter = new PrintWriter(writer);
	}

	public void write(SurroundDynamicEntity entity) throws SurroundBaseException, IOException {

		printWriter.println("{");

		int i = 0;

		SurroundClassInfo classInfo = SurroundRuntimeTools.determineClassInfo(entity.getClass());
		if (classInfo != null) {

			printWriter.print(createElementName("entityClass") + " : ");
			printWriter.print(createElementName(classInfo.getClassName()));
			i++;

			printWriter.println(",");
			printWriter.print(createElementName("bundle") + " : ");
			printWriter.print(createElementName(classInfo.getBundleName()));
		}

		for (SurroundBase element : entity.getChildren()) {
			if (i > 0)
				printWriter.println(",");
			printWriter.print(createElementName(element.getName()) + " : ");
			write(element);
			i++;
		}
		printWriter.println();
		printWriter.println("}");
	}

	public void write(SurroundValueType valueType) throws SurroundBaseException, IOException {

		printWriter.println("{");

		int i = 0;
		for (SurroundBase element : valueType.getChildren()) {
			if (i > 0)
				printWriter.println(",");
			printWriter.print(createElementName(element.getName()) + " : ");
			write(element);
			i++;
		}
		printWriter.println();
		printWriter.println("}");
	}

	public void write(SurroundList<SurroundBase> list) throws SurroundBaseException, IOException {

		printWriter.println("[");

		int i = 0;
		for (SurroundBase element : list.getChildren()) {
			if (i > 0)
				printWriter.println(",");
			write(element);
			i++;
		}

		printWriter.println();
		printWriter.println("]");
	}

	public void write(SurroundMap<SurroundBase, SurroundBase> map) throws SurroundBaseException, IOException {

		printWriter.println("[");

		int i = 0;
		for (SurroundBase key : map.keySet()) {
			printWriter.println("{");

			printWriter.print("\"key\" : ");
			write(key);
			printWriter.println(",\n");

			printWriter.print("\"value\" : ");
			write(map.get(key));
			printWriter.println("\n");

			i++;
			printWriter.println("}");
			if (i < map.keySet().size())
				printWriter.println(",");
		}

		printWriter.println();
		printWriter.println("]");
	}

	public void write(SurroundBase type) throws SurroundBaseException, IOException {

		if (false && type.getConstraints().hasReference()) {
			SurroundReference<?> ref = type.getReference();
			write(ref.getURL());
		} else {
			if (type instanceof SurroundDynamicEntity) {
				SurroundDynamicEntity entity = (SurroundDynamicEntity) type;
				write(entity);
			} else if (type instanceof SurroundValueType) {
				SurroundValueType valueType = (SurroundValueType) type;
				write(valueType);
			} else if (type instanceof SurroundList<?>) {
				SurroundList<SurroundBase> list = (SurroundList<SurroundBase>) type;
				write(list);
			} else if (type instanceof SurroundMap<?, ?>) {
				SurroundMap<SurroundBase, SurroundBase> map = (SurroundMap<SurroundBase, SurroundBase>) type;
				write(map);
			} else {
				String str = createValueString(type);
				printWriter.print(str);
			}
		}
	}

	public static String beautify(String jsonString, boolean appendNewline) {

		String beautyString = appendNewline ? newliner(jsonString) : jsonString;

		String delim = String.valueOf("\r\n");
		StringTokenizer tokenizer = new StringTokenizer(beautyString, delim);

		StringWriter beautyWriter = new StringWriter();
		PrintWriter beautyPrinter = new PrintWriter(beautyWriter);

		int tabs = 0;
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (token.contains("}") || token.contains("]")) {
				tabs--;
			}

			beautyPrinter.println(printtabs(beautyPrinter, tabs) + token);
			if (token.contains("{") || token.contains("[")) {
				tabs++;
			}
		}

		return beautyWriter.toString();
	}

	private static String newliner(String jsonString) {
		String beauty = jsonString.replace("{", "\n{\n");
		beauty = beauty.replace("}", "\n}\n");
		beauty = beauty.replace("[", "\n[\n");
		beauty = beauty.replace("]", "\n]\n");
		beauty = beauty.replace(",", ",\n");
		beauty = beauty.replace("\\", "");

		return beauty;
	}

	public String beautify() {
		String jsonStr = writer.toString();
		String beauty = beautify(jsonStr, false);

		return beauty;
	}

	public String getContent() {
		String content = writer.toString();
		close();
		return content;
	}

	private static String printtabs(PrintWriter beautyPrinter, int tabs) {
		String tabStr = "";
		for (int i = 0; i < tabs; i++) {
			beautyPrinter.print("   ");
		}
		return tabStr;
	}

	private String createElementName(String name) {
		return "\"" + name + "\"";
	}

	private String createValueString(Object obj) throws SurroundWrongTypeException, SurroundNotInitializedException {
		String jsonString = null;

		if (obj instanceof SurroundBase) {
			SurroundBase type = (SurroundBase) obj;

			if (type instanceof SurroundDate) {
				jsonString = type.isNull() ? null : JSONValue.toJSONString(type.asLong());
			} else if (!type.handleAsBasic()) {
				throw new SurroundWrongTypeException("not supported type: " + type.getClass().getName());
			} else if (type instanceof SurroundBoolean) {
				jsonString = type.isNull() ? null : JSONValue.toJSONString(type.asBoolean());
			} else if (type instanceof SurroundInteger) {
				jsonString = type.isNull() ? null : JSONValue.toJSONString(type.asInt());
			} else if (type instanceof SurroundLong) {
				jsonString = type.isNull() ? null : JSONValue.toJSONString(type.asLong());
			} else if (type instanceof SurroundDouble) {
				jsonString = type.isNull() ? null : JSONValue.toJSONString(type.asDouble());
			} else if (type instanceof SurroundString) {
				jsonString = type.isNull() ? null : JSONValue.toJSONString(type.toString());
			}
		} else {
			jsonString = JSONValue.toJSONString(obj);
		}

		return jsonString;
	}

	public void flush() throws IOException {
		printWriter.flush();
		writer.flush();
	}

	@Override
	public void close() {
		if (printWriter != null) {
			printWriter.close();
		}

		if (writer != null) {
			try {
				writer.close();
			} catch (IOException e) {
				SurroundLog.log(e);
			}
		}
		printWriter = null;
		writer = null;
	}

}
