/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.eclipse.core.runtime.Platform;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.osgi.framework.Bundle;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.composite.SurroundValueType;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotFoundException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.url.SurroundURL;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundJSONReader implements AutoCloseable {

	protected String content = null;
	protected Reader reader = null;
	protected InputStream input = null;

	public SurroundJSONReader(String jsonStr) {
		this.content = jsonStr;
	}

	public SurroundJSONReader(InputStream input) {
		this.input = input;
		this.reader = new InputStreamReader(input);
	}

	public Object parse() throws IOException, SurroundBaseException {
		Object jsonObject = null;

		try {
			if (content != null) {
				jsonObject = JSONValue.parseWithException(content);
			} else if (reader != null) {
				jsonObject = JSONValue.parseWithException(reader);
			}
		} catch (ParseException e) {
			throw new SurroundBaseException("error on parsing", e);
		}

		return jsonObject;
	}

	public void read(SurroundBase type, Object value) throws SurroundBaseException, IOException, InstantiationException,
			IllegalAccessException, ClassNotFoundException {

		if (false && type.getConstraints().hasReference()) {
			SurroundReference ref = type.getReference();
			if (ref == null) {
				throw new SurroundNotInitializedException("reference is not initialized");
			}

			SurroundURL url = ref.getURL();
			if (url == null) {
				throw new SurroundNotInitializedException("no reference key container os given");
			}

			read(url, value);
		} else {
			if (type instanceof SurroundDynamicEntity) {
				readEntity((SurroundDynamicEntity) type, value);
			} else if (type instanceof SurroundValueType) {
				readValueType((SurroundValueType) type, value);
			} else if (type instanceof SurroundList) {
				readList((SurroundList) type, value);
			} else if (type instanceof SurroundMap) {
				readMap((SurroundMap) type, value);
			} else if (value == null) {
				type.setNull();
			} else if (value instanceof String) {
				type.set(value.toString());
			} else if (value instanceof Boolean) {
				type.set(value.toString());
			} else if (value instanceof Number) {
				Number number = (Number) value;

				if (value instanceof Double) {
					type.set(number.doubleValue());
				} else if (value instanceof Float) {
					type.set(number.floatValue());
				} else if (value instanceof Byte) {
					type.set(number.byteValue());
				} else if (value instanceof Short) {
					type.set(number.shortValue());
				} else if (value instanceof Integer) {
					type.set(number.intValue());
				} else if (value instanceof Long) {
					type.set(number.longValue());
				}
			}
		}

	}

	public void readEntity(SurroundDynamicEntity entity, Object value) throws SurroundBaseException, IOException,
			InstantiationException, IllegalAccessException, ClassNotFoundException {

		if (!(value instanceof JSONObject)) {
			throw new SurroundWrongTypeException("wrong type for entity: " + entity.getName() + " candidate: " + value);
		}

		JSONObject jsonObject = (JSONObject) value;

		for (SurroundBase member : entity.getChildren()) {
			String name = member.getName();
			Object object = jsonObject.get(name);
			if (object != null) {
				read(member, object);
			}
		}

		// dynamic members
		for (Object key : jsonObject.keySet()) {
			String name = key.toString();
			SurroundBase member = entity.findByName(name);
			if (member == null) {
				Object obj = jsonObject.get(key);
				if (obj instanceof JSONObject) {
					JSONObject inner = (JSONObject) obj;

					Object classInfo = inner.get("entityClass");
					Object bundleInfo = inner.get("bundle");

					SurroundDynamicEntity child = null;
					try {
						Bundle bundle = Platform.getBundle(bundleInfo.toString());
						if (bundle == null) {
							continue;
						}
						child = (SurroundDynamicEntity) bundle.loadClass(classInfo.toString()).newInstance();
						if (child == null) {
							continue;
						}
					} catch (Throwable e) {
						SurroundLog.warning("no osgi bundle, loading class using canonical name only");

						child = (SurroundDynamicEntity) Class.forName(classInfo.toString()).newInstance();
					}

					if (child == null) {
						throw new SurroundNotFoundException("loading entity was not possible");
					}

					child.setName(name);

					readEntity(child, inner);
					entity.replace(name, child);
				}
			}
		}
	}

	public void readValueType(SurroundValueType valueType, Object value) throws SurroundBaseException, IOException,
			InstantiationException, IllegalAccessException, ClassNotFoundException {

		if (!(value instanceof JSONObject)) {
			throw new SurroundWrongTypeException(
					"wrong type for value: " + valueType.getName() + " candidate: " + value);
		}

		JSONObject jsonObject = (JSONObject) value;

		for (SurroundBase member : valueType.getChildren()) {
			String name = member.getName();
			Object object = jsonObject.get(name);
			if (object != null) {
				read(member, object);
			}
		}
	}

	public void readList(SurroundList<SurroundBase> myList, Object value) throws SurroundBaseException, IOException,
			ClassNotFoundException, InstantiationException, IllegalAccessException {

		if (!(value instanceof JSONArray)) {
			throw new SurroundWrongTypeException(
					"couldn't extract list: " + myList.getName() + " from json object:" + value);
		}

		JSONArray jsonArray = (JSONArray) value;

		myList.clear();
		for (int i = 0; i < jsonArray.size(); i++) {
			Object object = jsonArray.get(i);

			SurroundBase child = myList.newElement();
			if (child == null) {
				SurroundLog
						.error("couldn't read list data: " + object + "for: " + myList.getClass().getCanonicalName());
				break;
			}

			read(child, object);

			myList.add(child);
		}
	}

	public void readMap(SurroundMap<SurroundBase, SurroundBase> myMap, Object jsonObject) throws SurroundBaseException,
			IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		if (!(jsonObject instanceof JSONArray)) {
			throw new SurroundWrongTypeException(
					"wrong type for map: " + myMap.getName() + " candidate: " + jsonObject);
		}

		myMap.clear();
		JSONArray jsonMap = (JSONArray) jsonObject;
		for (Object obj : jsonMap) {
			if (!(obj instanceof JSONObject)) {
				throw new SurroundWrongTypeException(
						"wrong element for map: " + myMap.getName() + " candidate: " + obj);
			}

			JSONObject single = (JSONObject) obj;
			Object key = single.get("key");
			if (key == null) {
				throw new SurroundNotFoundException(
						"couldn't extract key from entry: " + single + " for map: " + myMap.getName());
			}

			SurroundBase newKey = myMap.newKey();
			read(newKey, key);

			Object value = single.get("value");
			if (value != null) {
				SurroundBase newValue = myMap.newValue();
				read(newValue, value);

				myMap.put(newKey, newValue);
			} else {
				SurroundLog.warning("no value vof map element: " + key.toString());
			}
		}
	}

	public void parseFor(SurroundBase type) throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, SurroundBaseException, IOException {

		Object jsonObject = parse();
		read(type, jsonObject);
	}

	public SurroundDynamicEntity parseEntity() throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, SurroundBaseException, IOException {

		Object value = parse();
		if (!(value instanceof JSONObject)) {
			throw new SurroundWrongTypeException("wrong type for entity");
		}

		JSONObject jsonObject = (JSONObject) value;

		Object classInfo = jsonObject.get("entityClass");
		Object bundleInfo = jsonObject.get("bundle");

		Bundle bundle = Platform.getBundle(bundleInfo.toString());
		SurroundDynamicEntity entity = (SurroundDynamicEntity) bundle.loadClass(classInfo.toString()).newInstance();

		readEntity(entity, jsonObject);

		return entity;
	}

	@Override
	public void close() {
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e) {
				SurroundLog.log(e);
			}
		}

		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				SurroundLog.log(e);
			}
		}

	}

}
