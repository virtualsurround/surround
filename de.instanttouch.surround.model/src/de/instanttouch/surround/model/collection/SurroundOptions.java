/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.collection;

import java.util.ArrayList;
import java.util.List;

import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundOptions<M extends SurroundBase> extends SurroundList<M> {

	protected String resultField = "";

	public SurroundOptions() {
		super();
		initOptions();
	}

	public SurroundOptions(String name, Class<M> elementClass, String elementName) {
		super(name, elementClass, elementName);
		initOptions();
	}

	public SurroundOptions(String name) {
		super(name);
		initOptions();
	}

	private void initOptions() {
		// TODO Auto-generated method stub

	}

	public String getResultField() {
		return resultField;
	}

	public void setResultField(String resultField) {
		this.resultField = resultField;
	}

	public void setSelected(M newSelected) {
		for (M element : members) {
			if (element.toString().equals(newSelected.toString())) {
				element.getConstraints().setSelected(true);
				break;
			}
		}
	}

	public List<M> getSelected() {

		List<M> selected = new ArrayList<M>();

		for (M element : members) {
			if (element.getConstraints().isSelected()) {
				selected.add(element);
			}
		}

		return selected;
	}

	public String getSelectedResult() {

		String result = "";
		boolean first = true;

		if (getSelected().size() > 0) {
			M selected = getSelected().get(0);

			if (resultField != null && resultField.length() > 0) {
				for (SurroundBase child : selected.getChildren()) {
					if (child.getName().equals(resultField)) {
						if (!first) {
							result += ",";
						}
						first = false;
						result += child.toString();
					}
				}
			} else {
				result = selected.toString();
			}
		}

		return result;
	}

	public void clearSelection() {
		for (M element : members) {
			if (element.getConstraints().isSelected()) {
				element.getConstraints().setSelected(false);
			}
		}

	}

	@Override
	public SurroundBase selectTypeByPath(String selectString) {
		SurroundBase child = null;

		for (M option : getChildren()) {
			if (selectString.equals(option.toString())) {
				child = option;
				child.getConstraints().setSelected(true);
				break;
			}
		}

		return child;
	}

	public String getTitle(M model) {
		return model.toString();
	}

}
