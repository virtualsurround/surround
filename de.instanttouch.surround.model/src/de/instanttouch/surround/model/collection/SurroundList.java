/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.collection;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CopyOnWriteArrayList;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.base.SurroundDefaultFactory;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.io.SurroundJSONWriter;
import de.instanttouch.surround.model.ref.ISurroundResourceConnect;
import de.instanttouch.surround.model.ref.SurroundReference;

public class SurroundList<T extends SurroundBase> extends SurroundBase {

	ISurroundFactory<T> factory = null;

	protected List<T> members = new CopyOnWriteArrayList<T>();
	protected boolean duplicatesAllowed = false;

	public SurroundList() {
		super();
		initList();
	}

	public SurroundList(String name) {
		super(name);
		initList();
	}

	public SurroundList(Class<T> elementClass) {
		super();
		setElementFactory(new SurroundDefaultFactory<T>(elementClass));
		initList();
	}

	public SurroundList(String name, Class<T> elementClass, String elementName) {
		super(name);
		setElementFactory(new SurroundDefaultFactory<T>(elementClass, elementName));
		initList();
	}

	private void initList() {
	}

	public boolean isDuplicatesAllowed() {
		return duplicatesAllowed;
	}

	public SurroundList setDuplicatesAllowed(boolean duplicatesAllowed) {
		this.duplicatesAllowed = duplicatesAllowed;
		return this;
	}

	public T newElement()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, SurroundBaseException {

		T newElement = null;

		if (getElementFactory() != null) {
			newElement = getElementFactory().create();
		}
		return newElement;
	}

	public void setElementFactory(ISurroundFactory<T> creator) {
		this.factory = creator;
	}

	public ISurroundFactory<T> getElementFactory() {
		return factory;
	}

	public SurroundList<T> add(T newElement) {

		if (duplicatesAllowed || !members.contains(newElement)) {

			members.add(newElement);
			int index = members.indexOf(newElement);

			SurroundChange<T> change = SurroundChange.forAdd(newElement, index);
			notifyChanged(change);

		} else {
			throw new SurroundBaseException("element already in collection: " + newElement);
		}

		return this;
	}

	public SurroundList<T> addTop(T newElement) {

		if (duplicatesAllowed || !members.contains(newElement)) {

			members.add(0, newElement);
			int index = members.indexOf(newElement);

			SurroundChange<T> change = SurroundChange.forAdd(newElement, index);
			notifyChanged(change);
		} else {
			throw new SurroundBaseException("element already in collection: " + newElement);
		}

		return this;
	}

	public SurroundList<T> add(int index, T newElement) {

		if (duplicatesAllowed || !members.contains(newElement)) {
			members.add(index, newElement);

			SurroundChange<T> change = SurroundChange.forAdd(newElement, index);
			notifyChanged(change);
		} else {
			throw new SurroundBaseException("element already in collection: " + newElement);
		}

		return this;
	}

	public boolean remove(T child) {

		boolean bOK = false;

		if (members.contains(child)) {
			int index = members.indexOf(child);

			bOK = members.remove(child);

			SurroundChange<T> change = SurroundChange.forRemove(index);
			notifyChanged(change);
		} else {
			throw new SurroundBaseException("element for rremove not found in collection: " + child);
		}

		return bOK;
	}

	public SurroundList<T> clear() {

		members.clear();

		SurroundChange<?> change = SurroundChange.forChange(this, null);
		notifyChanged(change);

		return this;
	}

	public T get(int index) throws SurroundBaseException {
		return members.get(index);
	}

	public int size() {
		return members.size();
	}

	public ListIterator<T> iterator() {
		return members.listIterator();
	}

	public boolean contains(T element) {
		return members.contains(element);
	}

	@Override
	public boolean isNull() {
		return false;
	}

	public boolean isEmpty() {
		return members.isEmpty();
	}

	public SurroundList<T> set(int index, T newValue) {
		T old = members.set(index, newValue);

		SurroundChange<T> change = SurroundChange.forChange(old, newValue);
		notifyChanged(change);

		return this;
	}

	@Override
	public List<T> getChildren() {

		SurroundReference<?> reference = getReference();
		if (reference != null) {
			if (!reference.isResolved()) {
				try {
					reference.resolve();
				} catch (Exception e) {
					throw new SurroundNotYetImplementedException("log exception");
				}
			}
		}

		return Collections.unmodifiableList(members);
	}

	@Override
	public boolean handleAsBasic() {
		return false;
	}

	@Override
	public T findByName(String name) {
		return (T) super.findByName(name);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object clone = super.clone();
		if (!(clone instanceof SurroundList)) {
			throw new CloneNotSupportedException("unexpected type: " + clone.getClass().getName());
		}

		SurroundList<T> cloneList = (SurroundList<T>) clone;
		for (T type : members) {
			cloneList.add((T) type.clone());
		}
		cloneList.factory = factory;

		return clone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((members == null) ? 0 : members.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundList other = (SurroundList) obj;
		if (members == null) {
			if (other.members != null)
				return false;
		} else if (!members.equals(other.members))
			return false;
		return true;
	}

	public Comparator<T> comparator(final int column, final boolean ascending) {

		return new Comparator<T>() {

			@Override
			public int compare(T a, T b) {

				int comp = -1;

				if (column < a.getChildren().size() && column < b.getChildren().size()) {
					SurroundBase cellA = a.getChildren().get(column);
					SurroundBase cellB = b.getChildren().get(column);

					comp = ascending ? cellA.compareTo(cellB) : cellB.compareTo(cellA);
				}

				return comp;
			}
		};

	}

	public Comparator<T> nameComparator() {

		return new Comparator<T>() {

			@Override
			public int compare(T a, T b) {

				int comp = -1;
				comp = a.getName().compareTo(b.getName());

				return comp;
			}
		};

	}

	public SurroundList<T> sortByName() {
		Collections.sort(members, nameComparator());
		return this;
	}

	@Override
	public String getPath4Type(SurroundBase child) {
		String selector = null;
		for (int i = 0; i < size(); i++) {
			SurroundBase compMember = get(i);
			if (compMember.equals(child)) {
				selector = String.valueOf(i);
				break;
			}
		}

		return selector;
	}

	@Override
	public SurroundBase selectTypeByPath(String selectString) {
		SurroundBase child = null;

		try {
			int index = Integer.parseInt(selectString);
			if (index < size()) {
				child = get(index);
			}
		} catch (NumberFormatException e) {
			throw new SurroundNotYetImplementedException("log exception");
		}

		return child;
	}

	public static <T extends SurroundBase> SurroundList<T> emptySurroundList() {
		return new SurroundList<T>();
	}

	public int indexOf(T element) {
		return members.indexOf(element);
	}

	@Override
	public String toString() {

		String listStr = super.toString();
		try {
			SurroundJSONWriter writer = new SurroundJSONWriter();
			writer.write(this);

			listStr = writer.beautify();

		} catch (Exception e) {
			throw new SurroundNotYetImplementedException("log exception");
		}

		return listStr;
	}

	@Override
	public SurroundList<T> set(String text) throws SurroundWrongTypeException {

		// try {
		// SurroundJSONReader jsonReader = new SurroundJSONReader(text);
		// Object object = jsonReader.parse();
		//
		// jsonReader.readList(this, object);
		//
		// } catch (Exception e) {
		// throw new SurroundModelException(e);
		// }

		throw new SurroundNotYetImplementedException("parse jspn strinf for filling list");
	}

	@Override
	public void read(ISurroundResourceConnect resource) {

		clear();
		super.read(resource);
	}

}
