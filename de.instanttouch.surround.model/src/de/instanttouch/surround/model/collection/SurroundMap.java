/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.base.SurroundDefaultFactory;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;
import de.instanttouch.surround.model.exception.SurroundOutOfRangeException;
import de.instanttouch.surround.model.io.SurroundJSONWriter;
import de.instanttouch.surround.model.ref.ISurroundResourceConnect;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.util.SurroundLog;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundMap<K extends SurroundBase, V extends SurroundBase> extends SurroundBase {

	protected Map<K, V> map = new ConcurrentHashMap<K, V>();
	protected ISurroundFactory<K> keyFactory = null;
	protected ISurroundFactory<V> valueFactory = null;

	public static class SurroundMapEntry<K extends SurroundBase, V extends SurroundBase> extends SurroundBase {

		K key;
		V value;

		public SurroundMapEntry() {
			super();
		}

		public SurroundMapEntry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
		}

		public K getKey() {
			return key;
		}

		public void setKey(K key) {
			this.key = key;
		}

		@Override
		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}
	}

	@SuppressWarnings("serial")
	public static class SurroundKeyFactory<K extends SurroundBase> extends SurroundOptions<SurroundClassInfo>
			implements ISurroundFactory<K> {

		@Override
		public K create() throws SurroundBaseException, InstantiationException, IllegalAccessException {

			K key = null;

			try {
				if (size() == 0) {

					throw new SurroundNotInitializedException("no types assigned");

				} else if (size() == 1) {

					SurroundClassInfo classInfo = get(0);
					key = (K) SurroundRuntimeTools.createInstanceFor(classInfo);

				} else {

					if (getSelected().size() != 1) {
						throw new SurroundOutOfRangeException("select only one type");
					}

					SurroundClassInfo classInfo = getSelected().get(0);
					key = (K) SurroundRuntimeTools.createInstanceFor(classInfo);
				}

			} catch (Exception e) {
				SurroundLog.log(e);
			}

			// TODO Auto-generated method stub
			return key;
		}

		public void assignKeyType(Class<K> keyCLass) {
			SurroundClassInfo classInfo = SurroundRuntimeTools.determineClassInfo(keyCLass);
			classInfo.setName(classInfo.getNameWithoutPackage());
			add(classInfo);
		}

		@Override
		public String getTitle(SurroundClassInfo model) {
			return model.getNameWithoutPackage();
		}

		@Override
		public List<SurroundBase> getVisibleChildren() {
			return Collections.EMPTY_LIST;
		}

	}

	@SuppressWarnings("serial")
	public static class SurroundValueFactory<V extends SurroundBase> extends SurroundOptions<SurroundClassInfo>
			implements ISurroundFactory<V> {

		@Override
		public V create() throws SurroundBaseException, InstantiationException, IllegalAccessException {

			V value = null;

			try {
				if (size() == 0) {

					throw new SurroundNotInitializedException("no types assigned");

				} else if (size() == 1) {

					SurroundClassInfo classInfo = get(0);
					value = (V) SurroundRuntimeTools.createInstanceFor(classInfo);

				} else {

					if (getSelected().size() != 1) {
						throw new SurroundOutOfRangeException("select one type");
					}

					SurroundClassInfo classInfo = getSelected().get(0);
					value = (V) SurroundRuntimeTools.createInstanceFor(classInfo);
				}

			} catch (Exception e) {
				SurroundLog.log(e);
			}

			return value;
		}

		public void assignValueType(Class<? extends SurroundBase> valueType) {
			SurroundClassInfo classInfo = SurroundRuntimeTools.determineClassInfo(valueType);
			classInfo.setName(classInfo.getNameWithoutPackage());
			add(classInfo);
		}

		@Override
		public String getTitle(SurroundClassInfo model) {
			return model.getNameWithoutPackage();
		}

		@Override
		public List<SurroundBase> getVisibleChildren() {
			return Collections.EMPTY_LIST;
		}

	}

	public SurroundMap() {
		super();
		initMap();
	}

	public SurroundMap(String name) {
		super(name);
		initMap();
	}

	private void initMap() {
	}

	public void assignFactories(ISurroundFactory<K> keyFactory, ISurroundFactory<V> valueFactory) {
		this.keyFactory = keyFactory;
		this.valueFactory = valueFactory;
	}

	public void assignDefaultFactories(Class<K> keyClass, Class<V> valueClass) {

		keyFactory = keyClass != null ? new SurroundDefaultFactory<K>(keyClass, "key") : null;
		valueFactory = valueClass != null ? new SurroundDefaultFactory<V>(valueClass, "value") : null;
	}

	public V get(K key) {
		return map.get(key);
	}

	public boolean containsKey(K key) {
		boolean containsKey = map.containsKey(key);
		return containsKey;
	}

	public V put(K key, V value) {

		V oldValue = map.put(key, value);
		// key.getConstraints().setValue(value);

		if (oldValue != null) {
			SurroundChange<V> change = SurroundChange.forChange(oldValue, value);
			notifyChanged(change);

		} else {
			SurroundChange<V> change = SurroundChange.forChange(null, value);
			notifyChanged(change);
		}

		return oldValue;
	}

	public Set<K> keySet() {
		return map.keySet();
	}

	public Collection<V> values() {
		return map.values();
	}

	public V remove(K key) {
		V oldValue = map.remove(key);

		SurroundChange<V> change = SurroundChange.forChange(oldValue, null);
		notifyChanged(change);

		key.getConstraints().setValue(null);
		return oldValue;
	}

	public K newKey() throws SurroundBaseException, InstantiationException, IllegalAccessException {
		K type = null;

		if (keyFactory != null) {
			type = keyFactory.create();

		} else {
			throw new SurroundNotInitializedException("no creator for key is set");
		}

		return type;
	}

	public V newValue() throws SurroundBaseException, InstantiationException, IllegalAccessException {
		V type = null;

		if (valueFactory != null) {
			type = valueFactory.create();
		} else {
			throw new SurroundNotInitializedException("no creator for value is set");
		}

		return type;
	}

	@Override
	public List<SurroundBase> getChildren() {

		SurroundReference<?> reference = getReference();
		if (reference != null) {
			if (!reference.isResolved()) {
				try {
					reference.resolve();
				} catch (Exception e) {
					throw new SurroundNotYetImplementedException("no logging yet");
				}
			}
		}

		List<SurroundBase> children = new ArrayList<SurroundBase>();

		for (K key : keySet()) {
			V value = get(key);

			SurroundMapEntry<K, V> mapEntry = new SurroundMapEntry<K, V>(key, value);
			mapEntry.setName("map_entry");

			children.add(mapEntry);
		}

		return children;
	}

	@Override
	public List<SurroundBase> getVisibleChildren() {

		List<SurroundBase> children = new ArrayList<SurroundBase>();

		for (K key : keySet()) {

			V value = get(key);

			SurroundString keyStr = new SurroundString(key.toString());
			keyStr.getConstraints().setValue(value);

			children.add(keyStr);
		}

		return children;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object clone = super.clone();
		if (!(clone instanceof SurroundMap)) {
			throw new CloneNotSupportedException("unexpected type: " + clone.getClass().getName());
		}

		SurroundMap<K, V> cloneMap = (SurroundMap<K, V>) clone;

		for (K key : keySet()) {
			V value = get(key);

			cloneMap.put((K) key.clone(), (V) value.clone());
		}
		cloneMap.keyFactory = keyFactory;
		cloneMap.valueFactory = valueFactory;

		return clone;
	}

	public void clear() {
		map.clear();
	}

	public ISurroundFactory<K> keyFactory() {
		return keyFactory;
	}

	public ISurroundFactory<V> valueFactory() {
		return valueFactory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundMap other = (SurroundMap) obj;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		return true;
	}

	public K findKeyByName(String name) {
		K key = null;

		for (K check : keySet()) {
			if (check.getName().equals(name)) {
				key = check;
				break;
			}
		}

		return key;
	}

	public V findValueByName(String name) {
		V value = null;

		for (V check : values()) {
			if (check.getName().equals(name)) {
				value = check;
				break;
			}
		}

		return value;
	}

	public V find(String name) {
		V value = null;

		for (K key : keySet()) {
			if (key.getName() != null && key.getName().equals(name)) {
				value = get(key);
				break;
			}
		}

		return value;
	}

	public SurroundBase findBy(SurroundBase key) {

		SurroundBase founded = find(key.toString());
		return founded;
	}

	@Override
	public String toString() {

		String mapStr = super.toString();
		try {
			SurroundJSONWriter writer = new SurroundJSONWriter();
			writer.write(this);

			mapStr = writer.beautify();

		} catch (Exception e) {
		}

		return mapStr;
	}

	@Override
	public void read(ISurroundResourceConnect resource) {

		clear();
		super.read(resource);
	}

}
