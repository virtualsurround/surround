package de.instanttouch.surround.model.collection;

import java.util.ArrayList;
import java.util.List;

import de.instanttouch.surround.model.base.SurroundString;

public class SurroundStringList extends SurroundList<SurroundString> {

	public SurroundStringList() {
		super(SurroundString.class);
	}

	public SurroundStringList(String name) {
		super(name, SurroundString.class, "string");
	}

	public List<String> asSimpleList() {

		List<String> stringList = new ArrayList<String>();
		for (SurroundString surroundString : getChildren()) {
			stringList.add(surroundString.toString());
		}

		return stringList;
	}

	public SurroundStringList addSimpleString(String string) {
		add(new SurroundString().set(string));
		return this;
	}

	public String getSimpleString(int index) {
		SurroundString surroundString = get(index);
		return surroundString == null ? "" : surroundString.toString();
	}

	public SurroundStringList setSimpleString(int index, String string) {

		SurroundString surroundString = new SurroundString().set(string);
		set(index, surroundString);

		return this;
	}

}
