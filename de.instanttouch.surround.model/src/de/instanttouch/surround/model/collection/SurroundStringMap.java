/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.collection;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundDefaultFactory;
import de.instanttouch.surround.model.base.SurroundString;

public class SurroundStringMap<V extends SurroundBase> extends SurroundMap<SurroundString, V> {

	public SurroundStringMap() {
		super();
		initStringMap();
	}

	public SurroundStringMap(String name) {
		super(name);
		initStringMap();
	}

	public SurroundStringMap(String name, Class<V> valueClass) {
		super(name);
		initStringMap();
		assignDefaultValueFactory(valueClass);
	}

	private void initStringMap() {
		assignDefaultFactories(SurroundString.class, null);
	}

	public void assignDefaultValueFactory(Class<V> valueClass) {
		valueFactory = valueClass != null ? new SurroundDefaultFactory<V>(valueClass, "value") : null;
	}

	public V put(String key, V value) {

		SurroundString keyString = new SurroundString("key").set(key);
		V oldValue = put(keyString, value);

		return oldValue;
	}

	public V get(String key) {

		SurroundString keyString = new SurroundString("key").set(key);
		V value = get(keyString);

		return value;
	}

	public boolean containsKey(String key) {

		SurroundString keyString = new SurroundString("key").set(key);
		boolean contains = super.containsKey(keyString);

		return contains;
	}

	public V remove(String key) {

		SurroundString keyString = new SurroundString("key").set(key);
		V value = remove(keyString);

		return value;
	}
}
