/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.time;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundTimestamp extends SurroundBase {

	protected DateTimeFormatter dateTimeformatter = java.time.format.DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss",
			Locale.getDefault());
	protected LocalDateTime dateTime;

	public SurroundTimestamp() {
		super();
	}

	public SurroundTimestamp(String name) {
		super(name);
	}

	public DateTimeFormatter getDateTimeformatter() {
		return dateTimeformatter;
	}

	public SurroundTimestamp setDateTimeformatter(DateTimeFormatter dateTimeformatter) {
		this.dateTimeformatter = dateTimeformatter;
		return this;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public SurroundTimestamp setDateTime(LocalDateTime newDate) {

		LocalDateTime old = dateTime;
		dateTime = newDate;

		SurroundChange<LocalDateTime> change = SurroundChange.forChange(old, newDate);
		notifyChanged(change);

		return this;
	}

	@Override
	public SurroundTimestamp set(String newValue) throws SurroundWrongTypeException {
		try {
			if (newValue.equals("null")) {
				setNull();
			} else {
				LocalDateTime dateTime = LocalDateTime.parse(newValue, dateTimeformatter);
				setDateTime(dateTime);
			}
		} catch (Exception e) {
			throw new SurroundWrongTypeException("wong date string: " + newValue);
		}
		return this;
	}

	@Override
	public String toString() {
		String asString = dateTime != null ? dateTimeformatter.format(dateTime) : "null";
		return asString;
	}

	@Override
	public SurroundTimestamp set(long newValue) throws SurroundWrongTypeException {

		Instant epochMilli = Instant.ofEpochMilli(newValue);

		LocalDateTime newLocalDateTime = LocalDateTime.ofInstant(epochMilli, ZoneId.systemDefault());
		setDateTime(newLocalDateTime);

		return this;
	}

	@Override
	public long asLong() throws SurroundWrongTypeException, SurroundNotInitializedException {

		ZonedDateTime zdt = dateTime.atZone(ZoneId.systemDefault());
		long millis = zdt.toInstant().toEpochMilli();

		return millis;
	}

	@Override
	public boolean isNull() {
		return dateTime == null;
	}

	@Override
	public SurroundTimestamp setNull() {
		dateTime = null;
		return this;
	}

	@Override
	public LocalDateTime getValue() {
		return dateTime;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
