/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.time;

import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

@SuppressWarnings("serial")
public class SurroundTimeRange extends SurroundDynamicEntity {

	public SurroundTimeRange() {
		super();
		initTimeRange();
	}

	public SurroundTimeRange(String name) {
		super(name);
		initTimeRange();
	}

	private void initTimeRange() {
		add(new SurroundTimestamp("start"));
		add(new SurroundTimestamp("end"));
	}

	public SurroundTimestamp getStart() {
		return (SurroundTimestamp) findByName("start");
	}

	public SurroundTimestamp getEnd() {
		return (SurroundTimestamp) findByName("end");
	}

	public boolean isValid() {

		boolean valid = !getStart().isNull() && getStart().asLong() > 0;
		valid = valid && !getEnd().isNull() && getEnd().asLong() > 0;

		valid = valid && getStart().asLong() < getEnd().asLong();

		return valid;
	}

}
