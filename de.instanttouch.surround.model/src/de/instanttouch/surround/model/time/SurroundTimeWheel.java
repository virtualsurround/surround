/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.time;

import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

@SuppressWarnings("serial")
public class SurroundTimeWheel extends SurroundDynamicEntity {

	int historyIndex = -1;

	public SurroundTimeWheel() {
		super();
		initTimeWheel();
	}

	public SurroundTimeWheel(String name) {
		super(name);
		initTimeWheel();
	}

	private void initTimeWheel() {
		add(new SurroundTimeRange("total"));
		add(new SurroundTimeRange("current"));
		add(new SurroundLong("width"));
		add(new SurroundLong("step"));
		add(new SurroundList<SurroundTimeRange>("history"));

	}

	public SurroundTimeRange getTotalRange() {

		return (SurroundTimeRange) findByName("total");

	}

	public SurroundTimeRange getCurentRange() {

		return (SurroundTimeRange) findByName("current");
	}

	public SurroundList<SurroundTimeRange> getHistory() {
		return (SurroundList<SurroundTimeRange>) findByName("history");
	}

	public long getWidth() {
		return findByName("width").asLong();
	}

	public long getStep() {
		return findByName("step").asLong();
	}

	public SurroundTimeWheel init(long start, long end, long width, long step) {

		getTotalRange().getStart().set(start);
		getTotalRange().getEnd().set(end);

		getCurentRange().getStart().set(start);
		getCurentRange().getEnd().set(end);

		findByName("width").set(width);
		findByName("step").set(step);

		return this;
	}

	public double calcResultion() {

		double resolution = getCurentRange().getEnd().asLong();
		resolution -= getCurentRange().getStart().asLong();
		resolution /= findByName("width").asLong();
		resolution *= findByName("step").asLong();

		return resolution;
	}

	public double calcZoom() {

		double scale = 1.0;

		SurroundTimeRange curentRange = getCurentRange();
		SurroundTimeRange totalRange = getTotalRange();

		if (curentRange.isValid() && totalRange.isValid()) {

			double displayWidth = curentRange.getEnd().asLong();
			displayWidth -= curentRange.getStart().asLong();

			double totalWidth = totalRange.getEnd().asLong();
			totalWidth -= totalRange.getStart().asLong();

			scale = totalWidth / displayWidth;
		}

		return scale;
	}

	public SurroundTimeWheel useRange(long start, long end) {

		try {

			historyIndex = getHistory().size();
			getHistory().add((SurroundTimeRange) getCurentRange().clone());

		} catch (CloneNotSupportedException e) {
		}

		getCurentRange().getStart().set(start);
		getCurentRange().getEnd().set(end);

		return this;
	}

	public SurroundTimeWheel back() {

		SurroundTimeRange timeRange = null;

		if (getHistory().size() == 0) {
			return this;
		}

		if (historyIndex > 0) {
			historyIndex--;

		} else {
			if (historyIndex != 0 && getHistory().size() > 0) {
				historyIndex = getHistory().size() - 1;
			}
		}
		timeRange = getHistory().get(historyIndex);

		getCurentRange().getStart().set(timeRange.getStart().asLong());
		getCurentRange().getEnd().set(timeRange.getEnd().asLong());

		return this;
	}

	public SurroundTimeWheel next() {

		SurroundTimeRange timeRange = null;

		if (getHistory().size() == 0) {
			return this;
		}

		if (historyIndex < 0) {
			historyIndex = 0;
		} else {
			if (historyIndex < getHistory().size() - 1) {
				historyIndex++;
			}
		}
		timeRange = getHistory().get(historyIndex);

		getCurentRange().getStart().set(timeRange.getStart().asLong());
		getCurentRange().getEnd().set(timeRange.getEnd().asLong());

		return this;
	}

	public SurroundTimeWheel resize(long width) {

		return resize(width, findByName("step").asLong());
	}

	public SurroundTimeWheel resize(long width, long step) {

		findByName("width").set(width);
		findByName("step").set(step);

		return this;
	}

	public SurroundTimeRange useZoom(double posX, double zoom) {

		SurroundTimeRange timeRange = new SurroundTimeRange();

		double timePerPixel = calcResultion();
		timePerPixel /= zoom;
		timePerPixel /= getStep();

		double offset = posX;
		offset *= timePerPixel;

		double timeSpan = getWidth();
		timeSpan *= timePerPixel;

		long start = getTotalRange().getStart().asLong();
		start += offset;

		long end = start + (long) timeSpan;

		useRange(start, end);

		timeRange.getStart().set(start);
		timeRange.getEnd().set(end);

		return timeRange;
	}

	public boolean isInitialized() {
		return getTotalRange().isValid();
	}

	public void useTotalRange() {
		getCurentRange().getStart().set(getTotalRange().getStart().asLong());
		getCurentRange().getEnd().set(getTotalRange().getEnd().asLong());

		getHistory().clear();
	}

	public SurroundTimeRange selectRange(long startInMillis, long endInMillis) {

		useRange(startInMillis, endInMillis);

		SurroundTimeRange timeRange = new SurroundTimeRange();
		timeRange.getStart().set(startInMillis);
		timeRange.getEnd().set(endInMillis);

		dumpCurrentRange();

		return timeRange;
	}

	private void dumpCurrentRange() {

		String rangeStr = "current range (";

		String startStr = getCurentRange().getStart().toString();
		rangeStr += startStr + ", ";

		String endStr = getCurentRange().getEnd().toString();
		rangeStr += endStr + ")";

		System.out.println(rangeStr);

	}

	public long pixel2time(int x) {

		if (!getCurentRange().isValid()) {
			return 0;
		} else {
			long time = getCurentRange().getStart().asLong();

			double timePerPixel = calcResultion();
			timePerPixel /= getStep();

			double offset = x;
			offset *= timePerPixel;

			time += offset;

			return time;
		}
	}

	public int time2pixel(long time) {

		if (!getCurentRange().isValid()) {
			return 0;
		} else {

			double timePerPixel = calcResultion();
			timePerPixel /= getStep();

			double x = time;
			x -= getCurentRange().getStart().asLong();
			x /= timePerPixel;

			return (int) x;
		}
	}
}
