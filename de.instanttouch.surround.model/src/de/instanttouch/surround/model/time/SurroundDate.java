/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.model.time;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundDate extends SurroundBase {

	static protected DateTimeFormatter dateformatter = java.time.format.DateTimeFormatter.ofPattern("dd.MM.yyyy",
			Locale.getDefault());

	protected LocalDate date;

	public SurroundDate() {
		super();
		initDate();
	}

	public SurroundDate(String name) {
		super(name);
		initDate();
	}

	public static DateTimeFormatter getDateformatter() {
		return dateformatter;
	}

	public static void setDateformatter(DateTimeFormatter dateformatter) {
		SurroundDate.dateformatter = dateformatter;
	}

	private void initDate() {
	}

	public LocalDate getDate() {
		return date;
	}

	public SurroundDate setDate(LocalDate newDate) {

		LocalDate old = date;
		date = newDate;

		SurroundChange<LocalDate> change = SurroundChange.forChange(old, newDate);
		notifyChanged(change);

		return this;
	}

	@Override
	public SurroundDate set(String newValue) throws SurroundWrongTypeException {
		try {
			if (newValue.equals("null")) {
				setNull();
			} else {
				LocalDate date = LocalDate.parse(newValue, dateformatter);
				setDate(date);
			}
		} catch (Exception e) {
			throw new SurroundWrongTypeException("wong date string: " + newValue);
		}
		return this;
	}

	@Override
	public SurroundDate set(long newValue) throws SurroundWrongTypeException {

		Instant epochMilli = Instant.ofEpochMilli(newValue);

		LocalDateTime newLocalDateTime = LocalDateTime.ofInstant(epochMilli, ZoneId.systemDefault());
		LocalDate date = newLocalDateTime.toLocalDate();

		setDate(date);
		return this;
	}

	@Override
	public long asLong() throws SurroundWrongTypeException, SurroundNotInitializedException {

		if (isNull()) {
			return -1;
		} else {
			Date date = Date.from(getDate().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			return date.getTime();
		}
	}

	@Override
	public String toString() {
		String asString = date != null ? dateformatter.format(date) : "null";

		return asString;
	}

	@Override
	public boolean isNull() {
		return date == null;
	}

	@Override
	public SurroundDate setNull() {
		date = null;
		return this;
	}

	@Override
	public LocalDate getValue() {
		return date;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object clone = super.clone();
		if (!(clone instanceof SurroundDate)) {
			throw new CloneNotSupportedException("wrong object created: " + clone.getClass().getCanonicalName());
		}

		SurroundDate newDate = (SurroundDate) clone;
		newDate.setDate(getDate());

		return newDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurroundDate other = (SurroundDate) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

	@Override
	public boolean handleAsBasic() {
		return true;
	}

}
