![surround_studio.png](https://bitbucket.org/repo/BE9gMq/images/259853988-surround_studio.png)

##Concept
The surround studio is an application framework with a slim glue on embedding several opensource solutions in an easy way.
It is not a general purpose framework but instead follows a "dynamic" domain driven desgin where modeling is key and therefore the model 
plugin offers a rich typesystem with build in JSON support which is a base for the most common modeling use cases.

following the [Surround Studio Vision](http://surround.instanttouch.de).

The model is the base for a little "data mapping" framework, which will leverage several datastores especially NoSQL solutions.

a tiny messaging framework will support many of the well known [EIPatterns](http://www.enterpriseintegrationpatterns.com), 
where a channel has a special transport protocol and messaging format.

last but not least a dynamic ui will show all basic types using meaningful conventions.

With this ingredients several application extension can be build, like the feelgood monitor:
https://bitbucket.org/virtualsurround/feelgood

copyrights - all icons are from http://www.iconarchive.com and can be used for free (even commercial)

##Setup:
[Surround Setup](http://surround.instanttouch.de/setup/)