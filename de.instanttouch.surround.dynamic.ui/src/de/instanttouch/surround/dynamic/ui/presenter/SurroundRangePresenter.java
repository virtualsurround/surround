package de.instanttouch.surround.dynamic.ui.presenter;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.model.base.SurroundRange;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;

public class SurroundRangePresenter extends SurroundPresenter<SurroundRange> {

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Slider slider = createSlider();

		ScrollPane scrollPane = (ScrollPane) layoutContainer.getJavaFXContainer();
		scrollPane.setContent(slider);
	}

	public Slider createSlider() {

		SurroundRange surroundRange = getModel();
		long value = surroundRange.isNull() ? 0 : surroundRange.asLong();

		Slider slider = new Slider(surroundRange.getMinimum(), surroundRange.getMaximum(), value);
		slider.setShowTickMarks(true);
		slider.setShowTickLabels(true);
		slider.setMajorTickUnit(1f);
		slider.setBlockIncrement(1f);
		slider.setPrefHeight(40);
		slider.setPrefWidth(400);

		slider.valueProperty().addListener(event -> {
			double newValue = slider.getValue();
			surroundRange.set(newValue);
		});

		return slider;
	}

}
