package de.instanttouch.surround.dynamic.ui.presenter;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.messaging.SurroundRefreshInfo;
import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.channel.SurroundChannel;
import de.instanttouch.surround.messaging.model.channel.SurroundGenericMessageHandler;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.url.SurroundURL;

public abstract class SurroundPresenter<M extends SurroundBase> extends SurroundActor {

	public static String TYPE_JAVAFX = "typeJavaFx";
	public static String TYPE_SWT = "typeSwt";

	protected M model;
	protected SurroundPlayer player;
	protected Object filter;

	public SurroundPresenter() {
		super();
		initPresenter();
	}

	public SurroundPresenter(String name) {
		super(name);
		initPresenter();
	}

	private void initPresenter() {

		addMessageHandler(SurroundRefreshInfo.MESSAGE_ID,
				new SurroundGenericMessageHandler(SurroundRefreshInfo.MESSAGE_ID) {

					@Override
					public boolean handle(SurroundActor component, SurroundMessage message)
							throws IOException, SurroundMessagingException {

						SurroundRefreshInfo refreshInfo = new SurroundRefreshInfo();
						translate(message, refreshInfo);

						SurroundURL surroundURL = refreshInfo.getSourcePresenter().getURL();

						String url = surroundURL.toString();
						String self = getURL().toString();

						if (!url.equals(self)) {
							refresh();
						}
						return true;
					}

				});

	}

	public M getModel() {
		return model;
	}

	public void setModel(M model) {
		this.model = model;
		// model.addChangeNotificationListener(new ISurroundChangeListener() {
		// TODO
		//
		// @Override
		// public void changed(SurroundBase type, SurroundChange<?> change) {
		//
		// SurroundRefreshMessage refreshMessage = new SurroundRefreshMessage();
		//
		// refreshMessage.getRefreshInfo().setType(type);
		// refreshMessage.getRefreshInfo().setChange(change);
		//
		// SurroundURL surroundURL = getURL();
		// refreshMessage.getRefreshInfo().setSourceURL(surroundURL);
		//
		// superVision(refreshMessage);
		// };
		// });
		model.setPresenter(this);
	}

	public String getFrameworkType() {
		return TYPE_JAVAFX;
	}

	public ExecutorService getWorkerPool() {

		SurroundChannel channel = getChannel();
		if (channel != null) {
			return channel.getReceiverPool();
		}

		return null;
	}

	public void setFilter(Object filter) {
		this.filter = filter;
	}

	public Object getFilter() {

		if (filter != null) {
			return filter;
		} else {
			SurroundActor parent = findSuperVisor();
			while (parent != null) {

				SurroundActor actor = parent;
				if (parent instanceof SurroundPresenter<?>) {
					SurroundPresenter<?> parentPresenter = (SurroundPresenter<?>) actor;

					filter = parentPresenter.getFilter();
					if (filter != null) {
						return filter;
					}
				}

				parent = actor.findSuperVisor();
			}
		}
		return null;

	}

	public abstract void show(ISurroundLayoutContainer layoutContainer);

	public void refresh() {

	}

}
