/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.presenter;

import java.util.HashMap;
import java.util.Map;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundPresenterRegistry {

	protected static Map<Class<? extends SurroundBase>, SurroundClassInfo> presenterMap = new HashMap<>();

	public static void register(Class<? extends SurroundBase> modelClass,
			Class<? extends SurroundPresenter> presenterClass) {

		SurroundClassInfo classInfo = SurroundRuntimeTools.determineClassInfo(presenterClass);
		presenterMap.put(modelClass, classInfo);
	}

	public static <M extends SurroundBase> SurroundPresenter<M> getPresenter(M model) {

		SurroundPresenter<M> presenter = null;

		SurroundClassInfo classInfo = findPresenterForModelHierachie(model.getClass());
		if (classInfo != null) {
			presenter = SurroundRuntimeTools.createInstanceFor(classInfo);
			presenter.setModel(model);
		}

		return presenter;
	}

	public static SurroundClassInfo findPresenterForModelHierachie(Class<?> modelClass) {

		SurroundClassInfo classInfo = presenterMap.get(modelClass);

		Class<?> superClass = modelClass;
		while (classInfo == null) {

			superClass = superClass.getSuperclass();
			if (superClass.equals(Object.class)) {
				break;
			}
			classInfo = presenterMap.get(superClass);
		}

		return classInfo;
	}

}
