package de.instanttouch.surround.dynamic.ui.presenter.fx;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class SurroundFXParent implements ISurroundLayoutContainer {

	protected final Parent parent;
	protected eLayoutContraint constraints;

	public SurroundFXParent(Parent parent) {
		this.parent = parent;
	}

	@Override
	public Parent getJavaFXContainer() {
		return parent;
	}

	@Override
	public Object getSwtContainer() {
		// TODO Auto-generated method stub
		return null;
	}

	public SurroundFXParent addChild(Node child, eLayoutContraint constraints) {

		if (parent instanceof BorderPane) {

			BorderPane borderPane = (BorderPane) parent;
			borderPane.setCenter(child);

		} else if (parent instanceof Pane) {
			Pane pane = (Pane) parent;
			pane.getChildren().add(child);
		}

		return this;
	}

	@Override
	public eLayoutContraint getLayoutConstraint() {
		return constraints;
	}

	public SurroundFXParent setLayoutConstraints(eLayoutContraint constraint) {
		this.constraints = constraints;
		return this;
	}

	@Override
	public void layout() {
		// TODO Auto-generated method stub

	}

}
