package de.instanttouch.surround.dynamic.ui.messaging;

import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.graph.SurroundNodeReference;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundRefreshInfo extends SurroundMessageData {

	public final static String MESSAGE_ID = "RefreshMessage";

	protected SurroundBase type;
	protected SurroundChange<?> change;
	protected SurroundNodeReference sourcePresenter = new SurroundNodeReference("sourcePresenter");

	public SurroundRefreshInfo() {
		super("refresh");
		setMessageId(MESSAGE_ID);
	}

	public SurroundBase getType() {
		return type;
	}

	public void setType(SurroundBase type) {
		this.type = type;
	}

	public SurroundChange<?> getChange() {
		return change;
	}

	public void setChange(SurroundChange<?> change) {
		this.change = change;
	}

	public SurroundReference<?> getSourcePresenter() {
		return sourcePresenter;
	}

	public void setSourceURL(SurroundURL url) {

		String urlStr = url.toString();
		sourcePresenter.getURL().set(urlStr);
	}

}
