package de.instanttouch.surround.dynamic.ui.layout;

public interface ISurroundLayoutContainer {

	public enum eLayoutContraint {
		TOP, BOTTOM, CENTER, LEFT, RIGHT, HORIZONTAL_FILL, VERTICAL_FILL
	}

	public Object getSwtContainer();

	public Object getJavaFXContainer();

	public eLayoutContraint getLayoutConstraint();

	public void layout();

}
