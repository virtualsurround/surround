package de.instanttouch.surround.dynamic.ui.exceptions;

import de.instanttouch.surround.model.exception.SurroundBaseException;

@SuppressWarnings("serial")
public class SurroundUIException extends SurroundBaseException {

	public SurroundUIException(String message, Throwable cause) {
		super(message, cause);
	}

	public SurroundUIException(String message) {
		super(message);
	}

}
