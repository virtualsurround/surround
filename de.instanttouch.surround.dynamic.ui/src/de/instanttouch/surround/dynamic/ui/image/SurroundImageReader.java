package de.instanttouch.surround.dynamic.ui.image;

import de.instanttouch.surround.model.image.SurroundImage;

public abstract class SurroundImageReader {

	public abstract <I> I readImage(Class<I> imageClass, SurroundImage imageLocation);

}
