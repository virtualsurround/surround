package de.instanttouch.surround.examples.broadcast.common;


import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class Address extends SurroundDynamicEntity {

	public Address() {
		super();
		init();
	}

	public Address(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundString("street"));
		add(new SurroundString("city"));
		add(new SurroundString("phone"));
		add(new SurroundString("mobile"));
		add(new SurroundString("fax"));
		add(new SurroundString("email"));
	}
}
