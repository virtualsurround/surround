package de.instanttouch.surround.examples.broadcast.common;

import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class GeoCoordinate extends SurroundDynamicEntity {

	public GeoCoordinate() {
		init();
	}

	public GeoCoordinate(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundDouble("longitude"));
		add(new SurroundDouble("latitude"));
	}
}
