package de.instanttouch.surround.examples.broadcast.editor;

import de.instanttouch.surround.dynamic.ui.swt.editor.SurroundSwtSinglePageEditor;
import de.instanttouch.surround.examples.broadcast.BroadcastApplication;

public class BroadcastEditor extends SurroundSwtSinglePageEditor {

	public BroadcastEditor() {
		model = new BroadcastApplication();
	}

}
