package de.instanttouch.surround.examples.broadcast;

import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundByteArray;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.time.SurroundDate;

public class Attachment extends SurroundDynamicEntity {

	public Attachment() {
		init();
	}

	public Attachment(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundLong("version").getConstraints().setVisible(false));
		add(new SurroundString("description").getConstraints().setVisible(false));
		add(new SurroundString("filename").getConstraints().setVisible(false));
		add(new SurroundInteger("fileSize").getConstraints().setVisible(false));
		add(new SurroundString("contentType").getConstraints().setVisible(false));
		add(new SurroundString("type").getConstraints().setVisible(false));
		add(new SurroundBoolean("virusScanned").getConstraints().setVisible(false));
		add(new SurroundBoolean("confidential").getConstraints().setVisible(false));
		add(new SurroundDate("dateCreated").getConstraints().setVisible(false));
		add(new SurroundDate("lastUpdated").getConstraints().setVisible(false));
		add(new SurroundString("category2").getConstraints().setVisible(false));
		add(new SurroundInteger("letterType").getConstraints().setVisible(false));
		add(new SurroundInteger("idInSpectraPlus").getConstraints().setVisible(false));
		add(new SurroundInteger("attachmentNumber").getConstraints().setVisible(false));
		add(new SurroundInteger("numberOfPages").getConstraints().setVisible(false));
		add(new SurroundByteArray("content", 1));
	}
}
