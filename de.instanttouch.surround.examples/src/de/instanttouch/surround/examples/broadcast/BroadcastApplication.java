package de.instanttouch.surround.examples.broadcast;

import de.instanttouch.surround.examples.broadcast.technical_information.TechnicalInformation;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.domain.SurroundModel;

public class BroadcastApplication extends SurroundModel {

	public BroadcastApplication() {
		super();
		add(new AdministrativeInformation());
		add(new SurroundList<TechnicalInformation>("TechnocalInformationList", TechnicalInformation.class,
				"TechnocalInformation").setDuplicatesAllowed(true));
		add(new SurroundList<Attachment>("AttachmentList", Attachment.class, "Attachment").setDuplicatesAllowed(true));
	}
}
