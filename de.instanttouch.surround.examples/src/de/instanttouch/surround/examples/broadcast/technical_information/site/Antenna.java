package de.instanttouch.surround.examples.broadcast.technical_information.site;

import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class Antenna extends SurroundDynamicEntity {

	public Antenna() {
		init();
	}

	public Antenna(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundString("manufacturer"));
		add(new SurroundString("Model"));

		add(new SurroundDouble("gain"));
		add(new SurroundDouble("polarization"));
		add(new SurroundDouble("directivity"));
	}
}
