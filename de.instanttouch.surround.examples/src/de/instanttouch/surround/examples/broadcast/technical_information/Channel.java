package de.instanttouch.surround.examples.broadcast.technical_information;

import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class Channel extends SurroundDynamicEntity {

	public Channel() {
		init();
	}

	public Channel(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundString("channel"));
		add(new SurroundDouble("frequency"));
		add(new SurroundDouble("bandwidth"));
		add(new SurroundDouble("minFrequency"));
		add(new SurroundDouble("maxFrequency"));
		add(new SurroundDouble("description"));
	}
}
