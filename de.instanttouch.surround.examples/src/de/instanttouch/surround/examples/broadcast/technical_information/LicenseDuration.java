package de.instanttouch.surround.examples.broadcast.technical_information;

import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundOptions;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class LicenseDuration extends SurroundDynamicEntity {

	public LicenseDuration() {
		init();
	}

	public LicenseDuration(String name) {
		super(name);
		init();
	}

	private void init() {
		add(createDurationType());
		add(new SurroundLong("duration"));
		add(new SurroundString("unit").set("month(s)"));
	}

	public SurroundString createDurationType() {

		SurroundString durationType = new SurroundString("durationType");
		
		SurroundOptions<SurroundString> options = new SurroundOptions<>();
		options.add(new SurroundString("Annual"));
		options.add(new SurroundString("Temporary"));
		options.add(new SurroundString("Experimental"));

		durationType.getConstraints().setOptions(options);
		
		return durationType;
	}
}
