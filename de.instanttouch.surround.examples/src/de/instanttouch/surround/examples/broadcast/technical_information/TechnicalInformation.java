package de.instanttouch.surround.examples.broadcast.technical_information;

import de.instanttouch.surround.examples.broadcast.technical_information.site.Site;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class TechnicalInformation extends SurroundDynamicEntity {

	public TechnicalInformation() {
		init();
	}

	public TechnicalInformation(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new LicenseDuration());
		add(new SystemCharacteristics());
		add(new SurroundList<Channel>("ChannelList", Channel.class, "Channel").setDuplicatesAllowed(true));
		add(new SurroundList<Site>("SiteList", Site.class, "Site").setDuplicatesAllowed(true));
	}
}
