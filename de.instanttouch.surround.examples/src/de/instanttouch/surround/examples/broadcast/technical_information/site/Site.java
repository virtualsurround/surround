package de.instanttouch.surround.examples.broadcast.technical_information.site;


import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class Site extends SurroundDynamicEntity {

	public Site() {
		init();
	}

	public Site(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new AreaInformation());
		add(new Equipment());
		add(new Antenna());
	}
}
