package de.instanttouch.surround.examples.broadcast.technical_information.site;

import de.instanttouch.surround.examples.broadcast.common.GeoCoordinate;
import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class AreaInformation extends SurroundDynamicEntity {

	public AreaInformation() {
		init();
	}

	public AreaInformation(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundString("stationName").getConstraints().setRequired(true));
		add(new GeoCoordinate("geoCoordinate").getConstraints().setRequired(true));
		add(new SurroundString("code"));
		add(new SurroundString("description").getConstraints().setLarge(true));
		add(new SurroundDouble("serviceArea"));
	}
}
