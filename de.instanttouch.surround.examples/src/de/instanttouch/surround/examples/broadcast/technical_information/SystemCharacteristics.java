package de.instanttouch.surround.examples.broadcast.technical_information;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SystemCharacteristics extends SurroundDynamicEntity {

	public SystemCharacteristics() {
		init();
	}

	public SystemCharacteristics(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundString("systemType"));
		add(new SurroundString("modulation "));
		add(new SurroundString("codeRate"));
		add(new SurroundString("receptionMode"));
		add(new SurroundString("typeofSpectrumMask"));
	}
}
