package de.instanttouch.surround.examples.broadcast.technical_information.site;

import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class Equipment extends SurroundDynamicEntity {

	public Equipment() {
		init();
	}

	public Equipment(String name) {
		super(name);
		init();
	}

	private void init() {
		add(new SurroundString("manufacturer"));
		add(new SurroundString("model"));
		add(new SurroundDouble("minFrequency"));
		add(new SurroundDouble("maxFrequency"));
		add(new SurroundDouble("maxPower"));
	}
}
