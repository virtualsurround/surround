package de.instanttouch.surround.examples.broadcast;

import de.instanttouch.surround.examples.broadcast.common.Address;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class AdministrativeInformation extends SurroundDynamicEntity {

	public AdministrativeInformation() {
		super("AdministrativeInformation");

		add(new SurroundString("userCode"));
		add(new SurroundString("applicantName"));
		add(new SurroundString("fullName"));
		add(new Address("contactPerson"));
	}
}
