package de.instanttouch.surround.examples.player.types;

import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class MediaFormat extends SurroundDynamicEntity {

	public MediaFormat() {
		super("mediaFormat");

		add(new SurroundString("mimeType"));

		// video
		add(new SurroundInteger("height"));
		add(new SurroundInteger("width"));
		add(new SurroundInteger("frameRate"));

		// audio
		add(new SurroundInteger("sampleRate"));
		add(new SurroundInteger("channelCount"));
	}

	public String getMimeType() {
		return findByName("mimeType").toString();
	}

	public MediaFormat setMimeType(String mimeType) {
		findByName("mimeType").set(mimeType);
		return this;
	}

	public int getSampleRate() {
		return findByName("sampleRate").asInt();
	}

	public MediaFormat setSampleRate(int sampleRate) {
		findByName("sampleRate").set(sampleRate);
		return this;
	}

}