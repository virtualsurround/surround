package de.instanttouch.surround.examples.player.types;

import de.instanttouch.surround.data.model.SurroundDataReference;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class PlayList extends SurroundList<MediaInfo> {

	public PlayList() {
		super(MediaInfo.class);
		initPlayList();
	}

	public PlayList(String name) {
		super(name, MediaInfo.class, "mediaInfo");
		initPlayList();
	}

	private void initPlayList() {

		try {
			SurroundDataReference<?> dataReference = new SurroundDataReference<>("playListDB");
			dataReference.setKey("elasticsearch");

			String host = SurroundRuntimeTools.getHostName();
			dataReference.setHost(host).setCluster("media").setPath("playList");

			setReference(dataReference);

		} catch (Exception e) {
			SurroundSwtExceptionHandler.handle(e);
		}
	}

}