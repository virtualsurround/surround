package de.instanttouch.surround.examples.player.types;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.url.SurroundURL;

public class MediaInfo extends SurroundDynamicEntity {

	public MediaInfo() {
		super();
		initMediaInfo();
	}

	public MediaInfo(String name) {
		super(name);
		initMediaInfo();
	}

	private void initMediaInfo() {
		add(new SurroundString("title"));
		add(new SurroundURL("mediaSource"));
	}
}