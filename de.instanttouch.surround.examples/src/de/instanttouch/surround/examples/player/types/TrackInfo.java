package de.instanttouch.surround.examples.player.types;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class TrackInfo extends SurroundDynamicEntity {

	public static final String TYPE_AUDIO = "audio";
	public static final String TYPE_VIDEO = "video";
	public static final String TYPE_SUBTITLE = "subtitle";

	public TrackInfo() {
		super("trackInfo");

		add(new SurroundString("mediaType"));
		add(new SurroundString("language"));
		add(new MediaFormat());
	}

	public String getMediaType() {
		return findByName("mediaType").toString();
	}

	public TrackInfo setMediaType(String mediaType) {
		findByName("mediaType").set(mediaType);
		return this;
	}

	public String getLanguage() {
		return findByName("language").toString();
	}

	public TrackInfo setLanguage(String language) {
		findByName("language").set(language);
		return this;
	}

	public MediaFormat getMediaFormat() {
		return findFirst(MediaFormat.class);
	}

	public TrackInfo setMediaFormat(MediaFormat mediaFormat) {
		replace("mediaFormat", mediaFormat);
		return this;
	}
}