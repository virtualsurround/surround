package de.instanttouch.surround.examples.player;

import java.io.IOException;

import de.instanttouch.surround.examples.player.MediaPlayerMessaging.GetTrackInfo;
import de.instanttouch.surround.examples.player.MediaPlayerMessaging.SelectTrack;
import de.instanttouch.surround.examples.player.MediaPlayerMessaging.VolumnRequest;
import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.messaging.model.SurroundService;
import de.instanttouch.surround.messaging.model.channel.SurroundGenericMessageHandler;
import de.instanttouch.surround.messaging.model.channel.SurroundMessageHandler;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.url.SurroundURL;

public abstract class MediaPlayerBase extends SurroundActor {

	public final int MEDIA_TRACK_TYPE_AUDIO = 2;
	public final int MEDIA_TRACK_TYPE_METADATA = 5;
	public final int MEDIA_TRACK_TYPE_SUBTITLE = 4;
	public final int MEDIA_TRACK_TYPE_TIMEDTEXT = 3;
	public final int MEDIA_TRACK_TYPE_UNKNOWN = 0;
	public final int MEDIA_TRACK_TYPE_VIDEO = 1;

	public static class SurroundExampleTrackInfo extends SurroundDynamicEntity {

		public SurroundExampleTrackInfo() {
			super();
			initTrackInfo();
		}

		public SurroundExampleTrackInfo(String name) {
			super(name);
			initTrackInfo();
		}

		private void initTrackInfo() {
			add(new SurroundInteger("trackType"));
		}

	}

	public MediaPlayerBase() {
		initMediaPlayer();
	}

	public MediaPlayerBase(String name) {
		super(name);
		initMediaPlayer();
	}

	private void initMediaPlayer() {

		SurroundService mediaPlayer = new SurroundService("mediaPlayer", this);
		mediaPlayer.setMessagingModel(new MediaPlayerMessaging("mediaPlayer"));

		getServices().put("mediaPlayer", mediaPlayer);

		addMessageHandler("play", new SurroundMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				onPlay(message);
				return true;
			}
		});

		addMessageHandler("pause", new SurroundMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				onPause(message);
				return true;
			}
		});

		addMessageHandler("stop", new SurroundMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				onStop(message);
				return true;
			}
		});

		addMessageHandler("release", new SurroundMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				onRelease(message);
				return true;
			}
		});

		addMessageHandler("reset", new SurroundMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				onReset();
				return true;
			}
		});

		addMessageHandler("setVolume", new SurroundGenericMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				VolumnRequest volumnRequest = new VolumnRequest();
				translate(message, volumnRequest);

				onSetVolume(volumnRequest.getLeftVolume(), volumnRequest.getRightVolume());
				return true;
			}
		});

		addMessageHandler("selectTrack", new SurroundGenericMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				SelectTrack selectTrack = new SelectTrack();
				translate(message, selectTrack);

				onSelectTrack(selectTrack.getIndex());
				return true;
			}
		});

		addMessageHandler("getTrackInfo", new SurroundGenericMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				GetTrackInfo getTrackInfo = new GetTrackInfo();
				translate(message, getTrackInfo);

				onGetTrackInfo(message.getHeader().getReturnAddress(), message.getHeader().getCorrelationId());

				return true;
			}

		});
	}

	protected abstract void onSelectTrack(int index);

	protected abstract void onReset();

	protected abstract void onRelease(SurroundMessage message);

	protected abstract void onStop(SurroundMessage message);

	protected abstract void onPause(SurroundMessage message);

	protected abstract void onPlay(SurroundMessage message);

	protected abstract void onSeekTo(int ms);

	protected abstract void onSetVolume(double leftVolume, double rightVolume);

	protected abstract void selectTrack(int index);

	protected abstract void onGetTrackInfo(SurroundActorReference returnAddress, String correlatonId);

	protected abstract void onGetCurrentPosition();

	protected abstract void onSetDataSource(SurroundURL url);

}
