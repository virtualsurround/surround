package de.instanttouch.surround.examples.player.application;

import java.io.IOException;

import de.instanttouch.surround.application.model.ui.SurroundToolbar;
import de.instanttouch.surround.application.model.ui.SurroundView;
import de.instanttouch.surround.application.model.ui.SurroundWorkbenchApplication;
import de.instanttouch.surround.examples.player.MediaController;
import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.channel.SurroundChannel;
import de.instanttouch.surround.messaging.model.channel.SurroundConnectionInfo;
import de.instanttouch.surround.messaging.model.channel.SurroundJsonMessageTranslator;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.util.SurroundLog;

public class MediaPlayerApp extends SurroundWorkbenchApplication {

	public MediaPlayerApp() {
		initMediaPlayerApp();
	}

	public MediaPlayerApp(String name) {
		super(name);
		initMediaPlayerApp();
	}

	private void initMediaPlayerApp() {

		// TODO use protocol
		try {
			SurroundChannel channel = new SurroundChannel("media") {

				@Override
				public SurroundChannel start() throws SurroundMessagingException, IOException {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public SurroundChannel stop() throws SurroundMessagingException, IOException {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public SurroundConnectionInfo lookupService(SurroundClientInfo clientInfo) throws IOException {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void sendInternal(SurroundMessage message) throws IOException {
					// TODO Auto-generated method stub

				}
			};
			channel.setMessageTranslator(new SurroundJsonMessageTranslator());

			MediaController mediaController = new MediaController();
			channel.register(mediaController);

			SurroundView<MediaController> playerView = new SurroundView<>("mediaController");
			playerView.setModel(mediaController);

			SurroundToolbar toolbar = playerView.getToolbar();
			SurroundAction startAction = new SurroundAction() {

				@Override
				public boolean play(IProgressInfoListener progressInfoListener) {
					playerView.getModel().start();
					return true;
				}
			};
			startAction.setImageBundle("de.instanttouch.surround.dynamic.ui.swt");
			startAction.setImagePath("icons/play.png");
			startAction.setThreadType(SurroundAction.BACKGROUND);
			toolbar.addAction(startAction);

			SurroundAction pauseAction = new SurroundAction() {

				@Override
				public boolean play(IProgressInfoListener progressInfoListener) {
					playerView.getModel().pause();
					return true;
				}
			};
			pauseAction.setImageBundle("de.instanttouch.surround.dynamic.ui.swt");
			pauseAction.setImagePath("icons/pause.png");
			pauseAction.setThreadType(SurroundAction.BACKGROUND);
			toolbar.addAction(pauseAction);

			SurroundAction stopAction = new SurroundAction() {

				@Override
				public boolean play(IProgressInfoListener progressInfoListener) {
					playerView.getModel().stop();
					return true;
				}
			};
			stopAction.setImageBundle("de.instanttouch.surround.dynamic.ui.swt");
			stopAction.setImagePath("icons/stop.png");
			stopAction.setThreadType(SurroundAction.BACKGROUND);
			toolbar.addAction(stopAction);

			getViews().add(playerView);
			showView(playerView.getName());

		} catch (Exception e) {
			SurroundLog.log("error starting application", e);
		}
	}
}
