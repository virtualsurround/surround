package de.instanttouch.surround.examples.player;

import java.io.IOException;

import de.instanttouch.surround.examples.player.MediaPlayerMessaging.GetTrackInfo;
import de.instanttouch.surround.examples.player.MediaPlayerMessaging.SelectTrack;
import de.instanttouch.surround.examples.player.MediaPlayerMessaging.TrackInfoResponse;
import de.instanttouch.surround.examples.player.MediaPlayerMessaging.VolumnRequest;
import de.instanttouch.surround.examples.player.types.TrackInfo;
import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.SurroundClient;
import de.instanttouch.surround.messaging.model.channel.SurroundGenericMessageHandler;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;

public abstract class MediaControllerBase extends SurroundActor {

	public MediaControllerBase() {
		initMediaPlayer();
	}

	public MediaControllerBase(String name) {
		super(name);
		initMediaPlayer();
	}

	private void initMediaPlayer() {

		SurroundClient mediaClient = new SurroundClient("mediaControler", this);
		mediaClient.setMessagingModel(new MediaPlayerMessaging());

		SurroundClientInfo mediaPlayerClient = createPlayerClientInfo();
		getClients().put(mediaPlayerClient, mediaClient);

		addMessageHandler("trackInfo", new SurroundGenericMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				TrackInfoResponse trackInfoResponse = new TrackInfoResponse();
				translate(message, trackInfoResponse);

				onTrackInfo(trackInfoResponse.getInfo());

				return true;
			}

		});
	}

	private SurroundClientInfo createPlayerClientInfo() {
		SurroundClientInfo mediaPlayerClient = new SurroundClientInfo();
		mediaPlayerClient.getServiceName().set("mediaPlayer");
		mediaPlayerClient.getRole().set("mediaControler");
		return mediaPlayerClient;
	}

	public SurroundClient getMediaCLient() {
		return getClients().get(createPlayerClientInfo());
	}

	public MediaControllerBase start() {

		SurroundMessageData command = getMediaCLient().getMessagingModel().getMessage("play");
		getMediaCLient().send(command, 1000);

		return this;
	}

	public MediaControllerBase pause() {

		SurroundMessageData command = getMediaCLient().getMessagingModel().getMessage("pause");
		getMediaCLient().send(command, 1000);

		return this;
	}

	public MediaControllerBase stop() {
		SurroundMessageData command = getMediaCLient().getMessagingModel().getMessage("stop");
		getMediaCLient().send(command, 1000);

		return this;
	}

	public MediaControllerBase reset() {
		SurroundMessageData command = getMediaCLient().getMessagingModel().getMessage("reset");
		getMediaCLient().send(command, 1000);

		return this;
	}

	public MediaControllerBase release() {
		SurroundMessageData command = getMediaCLient().getMessagingModel().getMessage("release");
		getMediaCLient().send(command, 1000);

		return this;
	}

	public MediaControllerBase setVolume(double leftVolume, double rightVolume) {

		VolumnRequest volumnRequest = new VolumnRequest();
		volumnRequest.setLeftVolumne(leftVolume);
		volumnRequest.setRightVolumne(rightVolume);

		getMediaCLient().send(volumnRequest, 1000);

		return this;
	}

	public MediaControllerBase selectTrack(int index) {

		SelectTrack selectTrack = new SelectTrack();
		selectTrack.setIndex(index);

		getMediaCLient().send(selectTrack, 1000);

		return this;
	}

	public MediaControllerBase getTrackInfo() {

		GetTrackInfo getTrackInfo = new GetTrackInfo();
		getMediaCLient().send(getTrackInfo, 1000);

		return this;
	}

	public abstract void onTrackInfo(TrackInfo trackInfo);

}
