package de.instanttouch.surround.examples.player.ui;

import de.instanttouch.surround.dynamic.ui.swt.view.SurroundSwtView;
import de.instanttouch.surround.examples.player.types.PlayList;
import de.instanttouch.surround.model.base.SurroundBase;

public class MediaPlayListView extends SurroundSwtView {

	PlayList playList = new PlayList("playList");

	@Override
	public SurroundBase getModel() {
		return playList;
	}

}
