package de.instanttouch.surround.examples.player;

import de.instanttouch.surround.examples.player.types.TrackInfo;
import de.instanttouch.surround.messaging.model.SurroundMessagingModel;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.url.SurroundURL;

public class MediaPlayerMessaging extends SurroundMessagingModel {

	public static class VolumnRequest extends SurroundMessageData {

		public VolumnRequest() {
			super("setVolume");

			add(new SurroundDouble("leftVolume"));
			add(new SurroundDouble("rightVolume"));

			setMessageId("setVolumne");
		}

		public double getLeftVolume() {
			return findByName("leftVolume").asDouble();
		}

		public void setLeftVolumne(double leftVolume) {
			findByName("leftVolume").set(leftVolume);
		}

		public double getRightVolume() {
			return findByName("rightVolume").asDouble();
		}

		public void setRightVolumne(double leftVolume) {
			findByName("rightVolume").set(leftVolume);
		}
	}

	public static class SelectTrack extends SurroundMessageData {

		public SelectTrack() {
			super("selectTrack");

			add(new SurroundInteger("index"));
			setMessageId("setVolumne");
		}

		public int getIndex() {
			return findByName("index").asInt();
		}

		public void setIndex(int index) {
			findByName("index").set(index);
		}

	}

	public static class GetTrackInfo extends SurroundMessageData {

		public GetTrackInfo() {
			super("getTrackInfo");
			setMessageId("getTrackInfo");
		}
	}

	public static class TrackInfoResponse extends SurroundMessageData {

		public TrackInfoResponse() {
			super("trackInfo");
			setMessageId("trackInfo");

			add(new TrackInfo());
		}

		public TrackInfo getInfo() {
			return findFirst(TrackInfo.class);
		}
	}

	public MediaPlayerMessaging() {
		initMediaPlayerInterface();
	}

	public MediaPlayerMessaging(String name) {
		super(name);
		initMediaPlayerInterface();
	}

	private void initMediaPlayerInterface() {

		SurroundURL player4Android = new SurroundURL();
		player4Android.set("https://developer.android.com/reference/android/media/MediaPlayer.html");
		getConstraints().setWiki(player4Android);

		SurroundMessageData playCommand = new SurroundMessageData("playCommand");
		playCommand.setMessageId("play");
		getMessages().put("play", playCommand);

		SurroundMessageData pauseCommand = new SurroundMessageData("pauseCommand");
		pauseCommand.setMessageId("pause");
		getMessages().put("pause", pauseCommand);

		SurroundMessageData stopCommand = new SurroundMessageData("stopCommand");
		stopCommand.setMessageId("stop");
		getMessages().put("stop", stopCommand);

		SurroundMessageData releaseCommand = new SurroundMessageData("releaseCommand");
		releaseCommand.setMessageId("release");
		getMessages().put("release", releaseCommand);

		SurroundMessageData resetCommand = new SurroundMessageData("resetCommand");
		resetCommand.setMessageId("reset");
		getMessages().put("reset", resetCommand);

		VolumnRequest volumnRequest = new VolumnRequest();
		getMessages().put(volumnRequest.getMessageId(), volumnRequest);

		SelectTrack selectTrack = new SelectTrack();
		getMessages().put(selectTrack.getMessageId(), selectTrack);

		GetTrackInfo trackInfo = new GetTrackInfo();
		getMessages().put(trackInfo.getMessageId(), trackInfo);

		TrackInfoResponse trackInfoResponse = new TrackInfoResponse();
		getMessages().put(trackInfoResponse.getMessageId(), trackInfoResponse);
	}
}
