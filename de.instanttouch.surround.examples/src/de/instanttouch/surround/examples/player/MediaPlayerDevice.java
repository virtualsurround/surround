package de.instanttouch.surround.examples.player;

import de.instanttouch.surround.examples.player.MediaPlayerMessaging.TrackInfoResponse;
import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.url.SurroundURL;

public class MediaPlayerDevice extends MediaPlayerBase {

	public MediaPlayerDevice() {
	}

	public MediaPlayerDevice(String name) {
		super(name);
	}

	@Override
	protected void onSelectTrack(int index) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onReset() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onRelease(SurroundMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onStop(SurroundMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onPause(SurroundMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onPlay(SurroundMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSeekTo(int ms) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSetVolume(double leftVolume, double rightVolume) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void selectTrack(int index) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onGetTrackInfo(SurroundActorReference returnAddress, String correlatonId) {

		TrackInfoResponse infoResponse = new TrackInfoResponse();
		infoResponse.getInfo().set("todo");

		SurroundMessage message = createMessage(infoResponse, returnAddress);
		message.getHeader().setCorrelationId(correlatonId);

		send(message);
	}

	@Override
	protected void onGetCurrentPosition() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSetDataSource(SurroundURL url) {
		// TODO Auto-generated method stub

	}

}
