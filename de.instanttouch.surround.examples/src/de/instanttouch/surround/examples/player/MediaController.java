package de.instanttouch.surround.examples.player;

import de.instanttouch.surround.examples.player.types.PlayList;
import de.instanttouch.surround.examples.player.types.TrackInfo;

public class MediaController extends MediaControllerBase {

	public MediaController() {
		initMediaController();
	}

	public MediaController(String name) {
		super(name);
		initMediaController();
	}

	private void initMediaController() {
		add(new PlayList("playList"));
	}

	@Override
	public void onTrackInfo(TrackInfo trackInfo) {
		// TODO Auto-generated method stub

	}

}
