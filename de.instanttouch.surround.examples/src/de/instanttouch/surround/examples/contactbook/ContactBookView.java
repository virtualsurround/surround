package de.instanttouch.surround.examples.contactbook;

import de.instanttouch.surround.dynamic.ui.swt.view.SurroundSwtView;
import de.instanttouch.surround.model.base.SurroundBase;

public class ContactBookView extends SurroundSwtView {

	public ContactBookView() {
	}

	@Override
	public SurroundBase getModel() {
		return new ContactBook("contacts");
	}

}
