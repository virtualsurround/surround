package de.instanttouch.surround.examples.contactbook;

import de.instanttouch.surround.dynamic.ui.swt.editor.SurroundSwtSinglePageEditor;

public class ContactBookEditor extends SurroundSwtSinglePageEditor {

	public ContactBookEditor() {
		model = new ContactBook();
	}

}
