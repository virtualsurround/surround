package de.instanttouch.surround.examples.contactbook;

import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundValueType;
import de.instanttouch.surround.model.ref.SurroundEmailAddress;
import de.instanttouch.surround.model.url.SurroundURL;

public class ContactAsValueType extends SurroundValueType {

	SurroundString firstName = new SurroundString("firstName");
	SurroundString surName = new SurroundString("surName");

	SurroundString street = new SurroundString("street");
	SurroundLong zipCode = new SurroundLong("zipCode");
	SurroundString city = new SurroundString("city");

	SurroundEmailAddress enmail = new SurroundEmailAddress("email");
	SurroundURL homePage = new SurroundURL("homePage");

	public ContactAsValueType() {
	}

	public ContactAsValueType(String name) {
		super(name);
	}

	public SurroundString getFirstName() {
		return firstName;
	}

	public SurroundString getSurName() {
		return surName;
	}

	public SurroundString getStreet() {
		return street;
	}

	public SurroundLong getZipCode() {
		return zipCode;
	}

	public SurroundString getCity() {
		return city;
	}

	public SurroundEmailAddress getEnmail() {
		return enmail;
	}

	public SurroundURL getHomePage() {
		return homePage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((enmail == null) ? 0 : enmail.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((homePage == null) ? 0 : homePage.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((surName == null) ? 0 : surName.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactAsValueType other = (ContactAsValueType) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (enmail == null) {
			if (other.enmail != null)
				return false;
		} else if (!enmail.equals(other.enmail))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (homePage == null) {
			if (other.homePage != null)
				return false;
		} else if (!homePage.equals(other.homePage))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (surName == null) {
			if (other.surName != null)
				return false;
		} else if (!surName.equals(other.surName))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}

}
