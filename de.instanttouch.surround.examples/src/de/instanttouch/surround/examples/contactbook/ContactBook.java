package de.instanttouch.surround.examples.contactbook;

import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageRegistry;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.ref.SurroundFile;
import de.instanttouch.surround.model.url.SurroundURL;
import de.instanttouch.surround.model.util.SurroundLog;

public class ContactBook extends SurroundModel {

	public ContactBook() {
		super("contactBook");
		initContactBook();
	}

	public ContactBook(String name) {
		super(name);
		initContactBook();
	}

	private void initContactBook() {
		add(new ContactList());

		SurroundSwtImageRegistry imageRegistry = SurroundSwtImageRegistry.getInstance();
		if (imageRegistry != null) {
			imageRegistry.registerImage(Contact.class, "de.instanttouch.surround.examples",
					"icons/Contacts-2-icon.png");
		}
	}

	public ContactList getContactList() {
		return (ContactList) findByName("contactlist");
	}

	public static void main(String[] args) {

		try {
			ContactBook contactBook = new ContactBook();
			ContactList contactList = contactBook.getContactList();

			Contact contact = contactList.newElement();
			contact.setFirstName("Joachim");
			contact.setSurName("Tessmer");
			contact.setZipCode(76227);
			contact.setCity("Karlsruhe");

			contact.setEmailAddress("mailto:joachim.tessmer@web.de");
			contact.setHomepage("http://www.instanttouch.de");

			contactList.add(contact);

			SurroundFile file = new SurroundFile("contactBookFile");
			file.set(".contacts.json");

			contactBook.save(file);

			ContactBook contactBook2 = new ContactBook();
			contactBook2.read(file);

			assert (contactBook2.equals(contactBook));

			SurroundURL url = new SurroundURL();
			url.set("surround:./contacts.json");

			contactBook2.save(url);

			ContactBook contactBook3 = new ContactBook();
			contactBook3.read(url);

			assert (contactBook2.equals(contactBook));

		} catch (Exception e) {
			SurroundLog.log("error writering contact book", e);
		}

	}

}
