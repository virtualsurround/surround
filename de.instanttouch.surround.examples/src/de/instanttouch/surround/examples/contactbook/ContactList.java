package de.instanttouch.surround.examples.contactbook;

import de.instanttouch.surround.model.collection.SurroundList;

public class ContactList extends SurroundList<Contact> {

	public ContactList() {
		super("contactlist", Contact.class, "contact");
	}

}
