package de.instanttouch.surround.examples.contactbook;

import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.ref.SurroundEmailAddress;
import de.instanttouch.surround.model.url.SurroundURL;

public class Contact extends SurroundDynamicEntity {

	public Contact() {
		super("contact");

		add(new SurroundString("firstName"));
		add(new SurroundString("surName"));
		add(new SurroundString("street"));

		add(new SurroundLong("zipCode"));
		add(new SurroundString("city"));

		add(new SurroundEmailAddress("email"));
		add(new SurroundURL("homePage"));
	}

	public String getFirstName() {
		return findByName("firstName").toString();
	}

	public Contact setFirstName(String firstName) {
		findByName("firstName").set(firstName);
		return this;
	}

	public String getSurName() {
		return findByName("surName").toString();
	}

	public Contact setSurName(String surName) {
		findByName("surName").set(surName);
		return this;
	}

	public String getStreet() {
		return findByName("street").toString();
	}

	public Contact setStreet(String street) {
		findByName("street").set(street);
		return this;
	}

	public long getZipCode() {
		return findByName("zipCode").asLong();
	}

	public Contact setZipCode(long zipCode) {
		findByName("zipCode").set(zipCode);
		return this;
	}

	public String getCity() {
		return findByName("city").toString();
	}

	public Contact setCity(String city) {
		findByName("city").set(city);
		return this;
	}

	public String getEmailAddress() {
		return findByName("email").toString();
	}

	public Contact setEmailAddress(String email) {
		findByName("email").set(email);
		return this;
	}

	public String getHomepage() {
		return findByName("homePage").toString();
	}

	public Contact setHomepage(String newHomePage) {
		findByName("homePage").set(newHomePage);
		return this;
	}
}
