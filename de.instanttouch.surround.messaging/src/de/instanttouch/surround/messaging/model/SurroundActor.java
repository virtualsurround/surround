/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.channel.SurroundChannel;
import de.instanttouch.surround.messaging.model.channel.SurroundMessageHandler;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.messaging.model.statemachine.SurroundStateMachine;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.collection.SurroundStringMap;
import de.instanttouch.surround.model.composite.SurroundValueType;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.graph.SurroundNode;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundActor extends SurroundNode {

	public static final int WAIT_FOREVER = -1;

	public interface ISurroundErrorHandler {
		public void onError(SurroundActor component, SurroundMessage errorMessage);
	}

	protected SurroundStringMap<SurroundMessageHandler> messageHandlers = new SurroundStringMap<>();
	protected ISurroundErrorHandler errorHandler;
	protected SurroundChannel channel;
	protected SurroundPlayer player;

	private SurroundStringMap<SurroundService> serviceMap = new SurroundStringMap<>("services");
	private SurroundMap<SurroundClientInfo, SurroundClient> clientMap = new SurroundMap<>("clients");

	public static class SurroundClientInfo extends SurroundValueType {

		protected SurroundString serviceName = new SurroundString("serviceName");
		protected SurroundString role = new SurroundString("role");
		protected SurroundActorReference modelCoordinate = new SurroundActorReference("modelCoordinate");

		public SurroundString getServiceName() {
			return serviceName;
		}

		public SurroundString getRole() {
			return role;
		}

		public SurroundActorReference getModelCoordinate() {
			return modelCoordinate;
		}

	}

	public SurroundActor() {
		super();
		initActor();
	}

	public SurroundActor(String name) {
		super(name);
		initActor();
	}

	private void initActor() {
		add(new SurroundMailBox("mailBox"));
		add(new SurroundStateMachine("stateMachine"));
	}

	@Override
	public SurroundActorReference getReference() {
		return (SurroundActorReference) super.getReference();
	}

	@Override
	public SurroundBase setReference(SurroundReference<?> ref) {

		if (!(ref instanceof SurroundActorReference)) {
			throw new SurroundWrongTypeException("only actor references are allowed");
		}
		return super.setReference(ref);
	}

	public SurroundStateMachine getStateMachine() {
		return (SurroundStateMachine) findByName("stateMachine");
	}

	public SurroundStringMap<SurroundService> getServices() {
		return serviceMap;
	}

	public SurroundMap<SurroundClientInfo, SurroundClient> getClients() {
		return clientMap;
	}

	public ISurroundErrorHandler getErrorHandler() {
		return errorHandler;
	}

	public void setErrorHandler(ISurroundErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	public SurroundMailBox getMailBox() {
		SurroundMailBox mailBox = (SurroundMailBox) findByName("mailBox");
		return mailBox;
	}

	public SurroundActor findSuperVisor() {

		SurroundActor actor = null;

		List<SurroundNode> actors = findRelated("super", true);
		if (actors.size() > 1) {
			throw new SurroundMessagingException("inconsistent node graph: multiple super nodes found");
		}

		if (actors.size() == 1) {
			actor = (SurroundActor) actors.get(0);
		}

		return actor;
	}

	public SurroundActor connectDelegate(SurroundActor child) {

		if (false) {
			return null;
		}
		relateTo(child, "super", false);
		return this;
	}

	public SurroundActor connectSuperVisor(SurroundActor superVisor) {

		if (false) {
			return null;
		}

		relateTo(superVisor, "super", true);
		return this;
	}

	public List<SurroundActor> findDelegates() {

		List<SurroundActor> actorList = new ArrayList<>();

		List<SurroundNode> nodeList = findRelated("super", false);
		for (SurroundNode surroundNode : nodeList) {
			if (surroundNode instanceof SurroundActor) {
				actorList.add((SurroundActor) surroundNode);
			}
		}

		return actorList;
	}

	public SurroundChannel getChannel() {

		if (channel != null) {
			return channel;
		} else {
			SurroundActor parent = findSuperVisor();
			while (parent != null) {

				SurroundActor actor = parent;

				channel = actor.getChannel();
				if (channel != null) {
					return channel;
				}

				parent = actor.findSuperVisor();
			}
		}
		return null;
	}

	public void setChannel(SurroundChannel channel) {
		this.channel = channel;
	}

	public SurroundPlayer getPlayer() {

		if (player != null) {
			return player;
		} else {
			SurroundActor parent = findSuperVisor();
			while (parent != null) {

				SurroundActor actor = parent;

				player = actor.getPlayer();
				if (player != null) {
					return player;
				}

				parent = actor.findSuperVisor();
			}
		}
		return null;
	}

	public void setPlayer(SurroundPlayer player) {
		this.player = player;
	}

	public SurroundActor addMessageHandler(String messageId, SurroundMessageHandler handler) {

		handler.setComponent(this);
		messageHandlers.put(messageId, handler);
		return this;
	}

	public SurroundMessageHandler getMessageHandler(String messageId) {
		return messageHandlers.get(messageId);
	}

	public SurroundActor removeMessageHandler(String messageId) {
		SurroundMessageHandler handler = messageHandlers.remove(messageId);
		handler.setComponent(null);
		return this;
	}

	public SurroundMessage createMessage(SurroundMessageData messageData, SurroundActorReference targetAddress) {

		SurroundChannel channel = getChannel();
		if (channel == null) {
			throw new SurroundNotInitializedException("no channel assigned");
		}

		SurroundMessage message = channel.createMessage(messageData, this, targetAddress);

		String sessionId = getSessionId();
		if (sessionId != null && sessionId.length() > 0) {
			message.getHeader().setSessionId(sessionId);
		}

		return message;

	}

	public SurroundMessage createErrorMessage(SurroundMessage affectedMessage, String error) {

		SurroundChannel channel = getChannel();
		if (channel == null) {
			throw new SurroundMessagingException("no channel assigned");
		}

		SurroundMessage message = channel.createErrorMessage(affectedMessage, error);
		return message;
	}

	public SurroundActor send(SurroundMessage message) {

		SurroundChannel channel = getChannel();
		if (channel == null) {
			throw new SurroundNotInitializedException("no channel assigned");
		}

		channel.send(message);

		return this;
	}

	public String getSessionId() {
		return findByName("sessionId").toString();
	}

	public SurroundActor setSessionId(String sessionId) {
		findByName("sessionId").set(sessionId);
		return this;
	}

	public boolean onMessage(SurroundMessage message) throws IOException {
		boolean bHandled = false;

		if (message.getHeader().isError()) {

			onError(message);

		} else {

			String messageId = message.getHeader().getMessageId();
			SurroundMessageHandler handler = getMessageHandler(messageId);
			if (handler != null) {
				synchronized (handler) {
					try {
						bHandled = handler.handle(this, message);
					} catch (SurroundMessagingException e) {
						SurroundLog.log(e);
						throw new SurroundMessagingException("error on handling message", e);
					}
				}
			} else {
				getMailBox().getIncomingMessages().add(message);
			}
		}
		return bHandled;
	}

	private void onError(SurroundMessage message) {
		if (getErrorHandler() != null) {
			getErrorHandler().onError(this, message);
		}
	}

	public boolean isHandledMessage(String messageId) {

		SurroundMessageHandler messageHandler = getMessageHandler(messageId);
		return messageHandler != null;
	}

	public boolean superVision(SurroundMessage message) {

		boolean handled = false;

		SurroundActor root = findSuperVisor();
		while (root != null) {
			SurroundActor checkActor = root;
			root = checkActor.findSuperVisor();

			if (root == null) {
				root = checkActor;
				break;
			}
		}

		if (root != null) {
			root.dispatch(message);
		}

		return handled;
	}

	protected boolean handleRequired(SurroundMessage message) throws SurroundMessagingException, IOException {

		boolean handled = false;

		SurroundMessageHandler messageHandler = getMessageHandler(message.getHeader().getMessageId());
		if (messageHandler != null) {
			handled = messageHandler.handle(this, message);
		}

		return handled;
	}

	public boolean delegate(SurroundMessage message) throws SurroundMessagingException, IOException {

		boolean handled = handleRequired(message);
		if (handled) {
			return true;
		}

		List<SurroundActor> children = findDelegates();

		for (SurroundNode childActor : children) {

			if (childActor instanceof SurroundActor) {
				handled = handled || ((SurroundActor) childActor).delegate(message);
			}
		}

		return handled;
	}

	public SurroundActor dispatch(SurroundMessage message) {

		List<SurroundActor> requiredBy = collectRequiredBy(message);

		for (SurroundActor surroundActor : requiredBy) {
			try {
				surroundActor.onMessage(message);
			} catch (Exception e) {
				SurroundLog.log(e);
			}
		}

		return this;
	}

	private List<SurroundActor> collectRequiredBy(SurroundMessage message) {

		List<SurroundActor> required = new ArrayList<>();
		if (isHandledMessage(message.getHeader().getMessageId())) {
			required.add(this);
		}

		List<SurroundActor> children = findDelegates();
		for (SurroundActor childActor : children) {
			required.addAll(childActor.collectRequiredBy(message));
		}

		return required;
	}

	public List<SurroundActor> collectProvider(SurroundMessage message) {

		List<SurroundActor> provider = new ArrayList<>();
		if (isProvided(message)) {
			provider.add(this);
		}

		List<SurroundActor> children = findDelegates();
		for (SurroundActor childActor : children) {
			provider.addAll(childActor.collectProvider(message));
		}

		return provider;
	}

	public boolean isProvided(SurroundMessage message) {

		String messageId = message.getHeader().getMessageId();
		return isHandledMessage(messageId);
	}

}
