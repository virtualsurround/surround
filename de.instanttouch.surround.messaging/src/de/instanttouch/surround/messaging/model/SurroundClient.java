/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.channel.SurroundBlockingMessageHandler;
import de.instanttouch.surround.messaging.model.channel.SurroundConnectionInfo;
import de.instanttouch.surround.messaging.model.channel.SurroundMessageHandler;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.messaging.model.message.SurroundSubscribeRequest;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundClient extends SurroundDynamicEntity {

	private SurroundActor actor;

	public SurroundClient(SurroundActor actor) {
		super();
		this.actor = actor;
		initClient();
	}

	public SurroundClient(String name, SurroundActor actor) {
		super(name);
		this.actor = actor;
		initClient();
	}

	private void initClient() {
		// TODO Auto-generated method stub

	}

	public SurroundMessagingModel getMessagingModel() {
		return (SurroundMessagingModel) findByName("messagingModel");
	}

	public SurroundClient setMessagingModel(SurroundMessagingModel model) {
		replace("messagingModel", model);
		return this;
	}

	public void connect(SurroundConnectionInfo connectionInfo) {
		setConnectionInfo(connectionInfo);
	}

	public SurroundConnectionInfo getConnectionInfo() {
		SurroundConnectionInfo connectionInfo = findFirst(SurroundConnectionInfo.class);
		return connectionInfo;
	}

	public boolean isConnected() {
		return getConnectionInfo() != null;
	}

	public SurroundClient setConnectionInfo(SurroundConnectionInfo connectionInfo) {
		replace("connectionInfo", connectionInfo);
		return this;
	}

	public SurroundClient send(SurroundMessageData request, long timeout) {

		waitForConnect(timeout);

		SurroundActorReference serviceAddress = getConnectionInfo().target();

		SurroundMessage message = actor.createMessage(request, serviceAddress);
		actor.send(message);

		request.setCorrelationId(message.getHeader().getCorrelationId());

		return this;
	}

	public SurroundClient sendReceive(SurroundMessageData request, SurroundMessageData response, long timeout) {

		String responseMessageId = response.getMessageId();
		SurroundMessageHandler currentHandler = actor.getMessageHandler(responseMessageId);
		if (currentHandler != null) {
			actor.removeMessageHandler(responseMessageId);
		}

		SurroundBlockingMessageHandler handler = new SurroundBlockingMessageHandler(response);
		actor.addMessageHandler(responseMessageId, handler);

		try {
			long currentTime = System.currentTimeMillis();
			long endTime = currentTime + timeout;

			send(request, timeout);

			String correlationId = request.getCorrelationId();
			currentTime = System.currentTimeMillis();

			waitForReceive(handler, correlationId, endTime - currentTime);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			handler.clear();
			actor.addMessageHandler(responseMessageId, currentHandler);
		}
		return this;
	}

	public boolean waitForReceive(SurroundBlockingMessageHandler handler, String correlationId, long timeout)
			throws InterruptedException {

		long currentTime = System.currentTimeMillis();
		long endTime = currentTime + timeout;

		// wait for connection
		waitForConnect(timeout == SurroundActor.WAIT_FOREVER ? 30000 : timeout);

		// wait for response
		currentTime = System.currentTimeMillis();
		long newTimeout = timeout == SurroundActor.WAIT_FOREVER ? 1 : endTime - currentTime;

		if (newTimeout > 0) {
			// wait for connection
			synchronized (handler) {
				while (!handler.hasReceived(correlationId)) {
					if (timeout != SurroundActor.WAIT_FOREVER) {
						currentTime = System.currentTimeMillis();
						if (currentTime >= endTime) {
							throw new SurroundMessagingException("response timed out of component: " + getName());
						}
					}

					handler.wait(endTime - currentTime);
				}
			}
		}

		return true;
	}

	public SurroundClient waitForConnect(long timeout) {

		long currentTime = System.currentTimeMillis();
		long endTime = currentTime + timeout;

		// wait for connection
		try {
			synchronized (this) {
				while (!isConnected()) {
					currentTime = System.currentTimeMillis();
					if (currentTime >= endTime) {
						throw new SurroundMessagingException(
								"timeout (" + timeout + " ms) for connection of component: " + getName());
					}
					wait(endTime - currentTime);
				}
			}
		} catch (InterruptedException e) {
			throw new SurroundMessagingException(
					"timeout (" + timeout + " ms) for connection of component: " + getName());
		}

		return this;
	}

	public <TOPIC extends SurroundMessageData> SurroundClient subscribe(String topicMessageId, long timeout) {

		try {
			waitForConnect(timeout);

			SurroundSubscribeRequest subscriptionRequest = new SurroundSubscribeRequest("subscribe:" + topicMessageId);

			SurroundActorReference address = (SurroundActorReference) actor.getModelCoordinate(actor).clone();
			subscriptionRequest.setTopicMessageId(topicMessageId).setNotified(true).setReturnAddress(address);

			send(subscriptionRequest, timeout);

		} catch (Exception e) {
			throw new SurroundMessagingException("eror on subcribing topic: " + topicMessageId, e);
		}

		return this;
	}

	public SurroundClient unsubscribe(String topicMessageId, long timeout) {

		try {
			waitForConnect(timeout);

			SurroundSubscribeRequest subscriptionRequest = new SurroundSubscribeRequest(
					"unsubscribe:" + topicMessageId);

			SurroundActorReference address = (SurroundActorReference) actor.getModelCoordinate(actor).clone();
			subscriptionRequest.setTopicMessageId(topicMessageId).setNotified(false).setReturnAddress(address);

			send(subscriptionRequest, timeout);

		} catch (CloneNotSupportedException e) {
			throw new SurroundMessagingException("eror on unsubcribing topic: " + topicMessageId, e);
		}

		return this;
	}

}
