/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model;

import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundMailBox extends SurroundDynamicEntity {

	public SurroundMailBox() {
		super();
		initMailBox();
	}

	public SurroundMailBox(String name) {
		super(name);
		initMailBox();
	}

	private void initMailBox() {

		add(new SurroundList<SurroundMessage>("in", SurroundMessage.class, "message"));
		add(new SurroundList<SurroundMessage>("out", SurroundMessage.class, "message"));
	}

	public SurroundList<SurroundMessage> getIncomingMessages() {
		SurroundList<SurroundMessage> incoming = (SurroundList<SurroundMessage>) findByName("in");
		return incoming;
	}

	public SurroundList<SurroundMessage> getOutgoingMessages() {
		SurroundList<SurroundMessage> outgoing = (SurroundList<SurroundMessage>) findByName("out");
		return outgoing;
	}
}
