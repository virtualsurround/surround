/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.message;

import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundString;

public class SurroundSubscribeRequest extends SurroundMessageData {

	public SurroundSubscribeRequest() {
		super();
		initSubscription();
	}

	public SurroundSubscribeRequest(String name) {
		super(name);
		initSubscription();
	}

	private void initSubscription() {
		setMessageId("subscribe");

		add(new SurroundBoolean("notifiy"));
		add(new SurroundString("topicId"));
		add(new SurroundActorReference("returnAddress"));
	}

	public boolean isNotified() {
		return ((SurroundBoolean) findByName("notifiy")).get();
	}

	public SurroundSubscribeRequest setNotified(boolean notify) {
		((SurroundBoolean) findByName("notifiy")).set(notify);
		return this;
	}

	public String getTopicMessageId() {
		return findByName("topicId").toString();
	}

	public SurroundSubscribeRequest setTopicMessageId(String topicId) {
		findByName("topicId").set(topicId);
		return this;
	}

	public SurroundActorReference getReturnAddress() {
		return (SurroundActorReference) findByName("returnAddress");
	}

	public SurroundSubscribeRequest setReturnAddress(SurroundActorReference returnAddress) {
		replace("returnAddress", returnAddress);
		return this;
	}

}
