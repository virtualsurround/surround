/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.message;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundMessage extends SurroundDynamicEntity {

	public static class SurroundHeader extends SurroundDynamicEntity {

		public SurroundHeader() {
			super();
			initHeader();
		}

		public SurroundHeader(String name) {
			super(name);
			initHeader();
		}

		private void initHeader() {
			add(new SurroundString("messageID").set("not_set"));
			add(new SurroundString("correlationID").set("not_set"));
			add(new SurroundString("error").set(""));
			add(new SurroundString("serviceName").set(""));
			add(new SurroundActorReference("targetAddress"));
			add(new SurroundActorReference("returnAddress"));
			add(new SurroundString("sessionId").set("not_set"));
			add(new SurroundBoolean("broadcast").set(false));
		}

		public String getMessageId() {
			return findByName("messageID").toString();
		}

		public SurroundHeader setMessageId(String messageId) {
			findByName("messageID").set(messageId);
			return this;
		}

		public String getCorrelationId() {
			return findByName("correlationID").toString();
		}

		public SurroundHeader setCorrelationId(String correlationId) {
			findByName("correlationID").set(correlationId);
			return this;
		}

		public String getError() {
			return findByName("error").toString();
		}

		public SurroundHeader setError(String error) {
			findByName("error").set(error);
			return this;
		}

		public boolean isError() {
			String errorStr = getError();
			return errorStr != null && errorStr.length() > 0;
		}

		public SurroundActorReference getTargetAddress() {
			return (SurroundActorReference) findByName("targetAddress");
		}

		public SurroundHeader setTargetAddress(SurroundActorReference address) {

			SurroundActorReference targetAddress = address;
			try {
				replace("targetAddress", (SurroundActorReference) targetAddress.clone());
			} catch (CloneNotSupportedException e) {
				throw new SurroundMessagingException("cloning tagetAdress failed", e);
			}

			return this;
		}

		public SurroundActorReference getReturnAddress() {
			return (SurroundActorReference) findByName("returnAddress");
		}

		public SurroundHeader setReturnAddress(SurroundActorReference address) throws CloneNotSupportedException {
			SurroundActorReference returnAddress = address;

			replace("returnAddress", (SurroundActorReference) returnAddress.clone());

			return this;
		}

		public String getSessionId() {
			return findByName("sessionId").toString();
		}

		public SurroundHeader setSessionId(String sessionId) {
			findByName("").set(sessionId);
			return this;
		}

		public boolean isBroadcast() {
			return findByName("broadcast").asBoolean();
		}

		public SurroundHeader setBroadcast(boolean isBroadcast) {
			((SurroundBoolean) findByName("broadcast")).set(isBroadcast);
			return this;
		}
	}

	public SurroundMessage() {
		super();
		initMessage();
	}

	public SurroundMessage(String name) {
		super(name);
		initMessage();
	}

	private void initMessage() {
		add(new SurroundHeader("header"));
		add(new SurroundString("payload"));
	}

	public SurroundHeader getHeader() {
		return (SurroundHeader) findByName("header");
	}

	public String getPayload() {
		return findByName("payload").toString();
	}

	public void setPayload(String newData) {
		findByName("payload").set(newData);
	}
}
