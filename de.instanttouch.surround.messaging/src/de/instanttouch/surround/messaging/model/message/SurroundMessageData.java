/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.message;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundMessageData extends SurroundDynamicEntity {

	public SurroundMessageData() {
		super();
		initMethod();
	}

	public SurroundMessageData(String name) {
		super(name);
		initMethod();
	}

	private void initMethod() {

		String messageId = getClass().getCanonicalName();

		add(new SurroundString("messageID").set(messageId));
		add(new SurroundString("correlationID"));
	}

	public String getMessageId() {
		String messageId = findByName("messageID").toString();
		return messageId;
	}

	public SurroundMessageData setMessageId(String messageId) {
		findByName("messageID").set(messageId);
		return this;
	}

	public String getCorrelationId() {
		String correlationId = findByName("correlationID").toString();
		return correlationId;
	}

	public SurroundMessageData setCorrelationId(String correlationId) {
		findByName("correlationID").set(correlationId);
		return this;
	}

}
