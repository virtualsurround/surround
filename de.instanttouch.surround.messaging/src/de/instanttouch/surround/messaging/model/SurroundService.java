/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model;

import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.messaging.model.message.SurroundSubscribeRequest;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundStringMap;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundService extends SurroundDynamicEntity {

	private SurroundActor actor;

	public SurroundService(SurroundActor actor) {
		super();
		this.actor = actor;
		initService();
	}

	public SurroundService(String name, SurroundActor actor) {
		super(name);
		this.actor = actor;
		initService();
	}

	private void initService() {
		add(new SurroundStringMap<SurroundList<SurroundSubscribeRequest>>("subscriber"));
	}

	public SurroundMessagingModel getMessagingModel() {
		return (SurroundMessagingModel) findByName("messagingModel");
	}

	public SurroundService setMessagingModel(SurroundMessagingModel model) {
		replace("messagingModel", model);
		return this;
	}

	public SurroundStringMap<SurroundList<SurroundSubscribeRequest>> getSubscriptionMap() {
		return (SurroundStringMap<SurroundList<SurroundSubscribeRequest>>) findByName("subscriber");
	}

	public SurroundService send(SurroundMessageData response, SurroundActorReference client) {

		SurroundMessage message = actor.createMessage(response, client);
		actor.send(message);

		return this;
	}

	public void onSubscribe(SurroundSubscribeRequest subscription, SurroundActorReference returnAddress) {

		SurroundStringMap<SurroundList<SurroundSubscribeRequest>> subscriptionMap = getSubscriptionMap();

		String topicId = subscription.getTopicMessageId();
		SurroundList<SurroundSubscribeRequest> subscriber = subscriptionMap.get(topicId);

		if (subscriber == null) {
			subscriber = new SurroundList<SurroundSubscribeRequest>(subscription.getName());
			subscriptionMap.put(topicId, subscriber);
		}

		boolean alreadyRegistered = false;
		for (SurroundSubscribeRequest checkSubscription : subscriber.getChildren()) {
			if (checkSubscription.getReturnAddress().equals(subscription)) {
				alreadyRegistered = true;
				break;
			}
		}

		if (!alreadyRegistered) {
			subscriber.add(subscription);
		}

	}

	public void onUnsubscribe(SurroundSubscribeRequest subscription, SurroundActorReference returnAddress) {

		SurroundStringMap<SurroundList<SurroundSubscribeRequest>> subscriptionMap = getSubscriptionMap();

		String topicId = subscription.getTopicMessageId();
		SurroundList<SurroundSubscribeRequest> subscriber = subscriptionMap.get(topicId);

		if (subscriber != null) {

			for (SurroundSubscribeRequest removeSubscription : subscriber.getChildren()) {
				if (removeSubscription.getReturnAddress().equals(subscription)) {
					subscriber.remove(removeSubscription);
					break;
				}
			}

		}
	}

	public SurroundService publish(SurroundMessageData topic) throws CloneNotSupportedException {

		SurroundStringMap<SurroundList<SurroundSubscribeRequest>> subscriptionMap = getSubscriptionMap();

		String topicId = topic.getMessageId();
		SurroundList<SurroundSubscribeRequest> subscriber = subscriptionMap.get(topicId);

		if (subscriber == null) {
			return this;
		}

		for (SurroundSubscribeRequest subscription : subscriber.getChildren()) {

			SurroundMessage message = actor.createMessage(topic, subscription.getReturnAddress());
			actor.send(message);
		}

		return this;
	}

}
