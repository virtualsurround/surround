package de.instanttouch.surround.messaging.model.statemachine;

import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;

public class SurroundStatePath extends SurroundList<SurroundString> {

	public SurroundStatePath() {
		super(SurroundString.class);
	}

	public SurroundStatePath(String name) {
		super(name, SurroundString.class, "segment");
	}

	@Override
	public String toString() {

		String completeStr = "";
		boolean first = true;
		for (SurroundString token : getChildren()) {
			if (!first) {
				completeStr += "/";
			}
			completeStr += token;
		}

		return completeStr;
	}

}
