package de.instanttouch.surround.messaging.model.statemachine;

import java.io.IOException;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.channel.SurroundMessageHandler;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundState extends SurroundModel {

	private SurroundStateMachine parentState;
	private SurroundMap<SurroundClassInfo, SurroundMessageHandler> messageHandlers = new SurroundMap<>(
			"messageHandlers");

	public SurroundState() {
		super();
	}

	public SurroundState(String name) {
		super(name);
	}

	public SurroundStateMachine getParentState() {
		return parentState;
	}

	public void setParentState(SurroundStateMachine parentState) {
		this.parentState = parentState;
	}

	public SurroundAction getEntryAction() {
		return (SurroundAction) findByName("entryAction");
	}

	public SurroundState setEntryAction(SurroundAction newAction) {
		replace("entryAction", newAction);
		return this;
	}

	public SurroundAction getExitAction() {
		return (SurroundAction) findByName("exitAction");
	}

	public SurroundState setExitAction(SurroundAction newAction) {
		replace("exitAction", newAction);
		return this;
	}

	public void start(SurroundMessage message) {

		if (getEntryAction() != null) {
			SurroundAction entryAction = getEntryAction();

			if (getParentState() != null) {
				SurroundPlayer player = getParentState().getPlayer();
				player.play(entryAction);
			}
		}
	}

	public void stop(SurroundMessage message) {

		if (getExitAction() != null) {
			SurroundAction exitAction = getExitAction();

			if (getParentState() != null) {
				SurroundPlayer player = getParentState().getPlayer();
				player.play(exitAction);
			}
		}
	}

	public SurroundState addMessageHandler(Class<? extends SurroundMessage> messageCLass,
			SurroundMessageHandler handler) {

		SurroundClassInfo messageClassInfo = SurroundRuntimeTools.determineClassInfo(messageCLass);
		messageHandlers.put(messageClassInfo, handler);

		return this;
	}

	public SurroundMessageHandler getMessageHandler(Class<? extends SurroundMessage> messageCLass) {

		SurroundClassInfo messageClassInfo = SurroundRuntimeTools.determineClassInfo(messageCLass);
		return messageHandlers.get(messageClassInfo);
	}

	public void onMessage(SurroundMessage message) throws SurroundMessagingException, IOException {

		SurroundMessageHandler messageHandler = getMessageHandler(message.getClass());
		if (messageHandler != null) {
			messageHandler.handle(getParentState().getActor(), message);
		} else {
			throw new SurroundWrongTypeException("unknow message type:" + message.getClass().getSimpleName());
		}
	}

	public SurroundStatePath createPath()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, SurroundBaseException {

		SurroundStatePath path = new SurroundStatePath();
		SurroundString segment = path.newElement().set(getName());
		path.add(segment);

		SurroundStateMachine stateMachine = getParentState();
		while (stateMachine != null) {
			segment = path.newElement().set(stateMachine.getName());
			path.addTop(segment);

			stateMachine = stateMachine.getParentState();
		}

		return path;
	}

}
