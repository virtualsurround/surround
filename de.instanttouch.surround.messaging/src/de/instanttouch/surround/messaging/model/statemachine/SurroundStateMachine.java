package de.instanttouch.surround.messaging.model.statemachine;

import java.io.IOException;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundValueType;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.util.SurroundLog;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundStateMachine extends SurroundState {

	public static class SurroundTransition extends SurroundValueType {

		private SurroundString sourceState = new SurroundString("sourceState");
		private SurroundClassInfo messageClass = new SurroundClassInfo("messageClass");
		private SurroundString targetState = new SurroundString("targetState");

		public SurroundTransition() {
			super();
		}

		public SurroundTransition(String name) {
			super(name);
		}

		public SurroundString getSourceState() {
			return sourceState;
		}

		public SurroundClassInfo getMessageClass() {
			return messageClass;
		}

		public void setMessageClass(SurroundClassInfo messageClass) {
			this.messageClass = messageClass;
		}

		public SurroundString getTargetState() {
			return targetState;
		}

	}

	private SurroundActor actor;
	private SurroundState currentState;

	public SurroundStateMachine() {
		super();
		initStateMachine();
	}

	public SurroundStateMachine(String name) {
		super(name);
		initStateMachine();
	}

	private void initStateMachine() {

		SurroundList<SurroundState> states = new SurroundList<SurroundState>("states");
		states.setElementFactory(new ISurroundFactory<SurroundState>() {

			@Override
			public SurroundState create() throws SurroundBaseException, InstantiationException, IllegalAccessException {
				SurroundState state = new SurroundState();
				state.setParentState(SurroundStateMachine.this);

				return state;
			}
		});

		SurroundList<SurroundTransition> transitions = new SurroundList<>("transitions");
		transitions.setElementFactory(new ISurroundFactory<SurroundStateMachine.SurroundTransition>() {

			@Override
			public SurroundTransition create()
					throws SurroundBaseException, InstantiationException, IllegalAccessException {
				return new SurroundTransition();
			}
		});

		try {
			SurroundState start = states.newElement();
			start.setName("start");
			states.add(start);

			SurroundState stop = states.newElement();
			start.setName("stop");
			states.add(stop);

		} catch (Exception e) {
			SurroundLog.log(e);
		}

		add(states);
	}

	public SurroundActor getActor() {
		return actor;
	}

	public void setActor(SurroundActor actor) {
		this.actor = actor;
	}

	public SurroundPlayer getPlayer() {

		SurroundPlayer player = null;
		if (getActor() != null) {
			player = getActor().getPlayer();
		}

		return player;
	}

	public SurroundList<SurroundState> getStates() {
		return (SurroundList<SurroundState>) findByName("states");
	}

	public SurroundList<SurroundTransition> getTransitions() {
		return (SurroundList<SurroundTransition>) findByName("transitions");
	}

	public SurroundState getCurrentState() {
		return currentState;
	}

	public SurroundState registerTransition(String sourceState, Class<? extends SurroundMessage> messageClass,
			String targetState) throws SurroundMessagingException {

		try {
			SurroundClassInfo messageClassInfo = SurroundRuntimeTools.determineClassInfo(messageClass);
			SurroundTransition newElement = getTransitions().newElement();

			newElement.getSourceState().set(sourceState);
			newElement.setMessageClass(messageClassInfo);
			newElement.getTargetState().set(targetState);

		} catch (Exception e) {
			throw new SurroundMessagingException("error creating transition", e);
		}

		return this;
	}

	public String getTargetState(String sourceState, Class<? extends SurroundMessage> messageClass) {

		String targetState = null;

		SurroundClassInfo messageClassInfo = SurroundRuntimeTools.determineClassInfo(messageClass);
		for (SurroundTransition transition : getTransitions().getChildren()) {
			if (transition.getSourceState().equals(sourceState)
					&& transition.getMessageClass().equals(messageClassInfo)) {

				targetState = transition.getTargetState().toString();
				break;
			}
		}

		if (targetState == null) {
			for (SurroundTransition transition : getTransitions().getChildren()) {
				if (transition.getSourceState().equals("*") && transition.getMessageClass().equals(messageClassInfo)) {
					targetState = transition.getTargetState().toString();
					break;
				}
			}
		}

		return targetState;
	}

	public SurroundState getStart() {
		return getStates().findByName("start");
	}

	public SurroundState getEnd() {
		return getStates().findByName("end");
	}

	public void dispatch(SurroundMessage message) throws SurroundMessagingException, IOException {

		if (message.getHeader().isBroadcast()) {
			for (SurroundState surroundState : getStates().getChildren()) {
				surroundState.onMessage(message);
			}
		} else if (currentState == null) {
			throw new SurroundMessagingException("no state set");
		} else {

			String targetStateName = getTargetState(currentState.getName(), message.getClass());
			if (targetStateName.equals("*") || targetStateName.equals(currentState.getName())) {
				currentState.onMessage(message);
			} else {
				currentState.onMessage(message);

				SurroundState targetState = getStates().findByName(targetStateName);
				if (targetState != null) {
					currentState.stop(message);
					targetState.start(message);
					currentState = targetState;
				}
			}
		}
	}

}
