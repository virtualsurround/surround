/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.json.simple.JSONObject;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.io.SurroundJSONReader;
import de.instanttouch.surround.model.io.SurroundJSONWriter;

public class SurroundJsonMessageTranslator extends SurroundMessageTranslator {

	public static final String JSON_MESSAGE_FORMAT = "de.instanttouch.messaging.protocol.translators.json.format";

	public SurroundJsonMessageTranslator() {
		super(JSON_MESSAGE_FORMAT);
	}

	@Override
	public SurroundMessage readMessage(byte[] msg) throws IOException {

		SurroundMessage message = null;

		String text = new String(msg);
		SurroundJSONReader reader = null;
		try {
			reader = new SurroundJSONReader(text);
			message = readMessage(reader);

		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		return message;
	}

	@Override
	public SurroundMessage readMessage(InputStream input) throws IOException {

		SurroundMessage rawMessage = null;
		SurroundJSONReader reader = null;
		try {
			reader = new SurroundJSONReader(input);
			rawMessage = readMessage(reader);

		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		return rawMessage;
	}

	private SurroundMessage readMessage(SurroundJSONReader reader) throws IOException {

		SurroundMessage message = new SurroundMessage();

		JSONObject jsonObject = (JSONObject) reader.parse();
		JSONObject header = (JSONObject) jsonObject.get("header");

		try {
			reader.readEntity(message.getHeader(), header);
		} catch (Exception e) {
			throw new SurroundMessagingException("error parsing message header", e);
		}

		JSONObject payload = (JSONObject) jsonObject.get("payload");
		if (payload != null) {
			String dataStr = payload.toJSONString();
			message.setPayload(dataStr);
		}
		return message;
	}

	@Override
	public void writeMessage(SurroundMessage message, OutputStream output) throws IOException {

		SurroundJSONWriter writer = null;
		try {

			writer = new SurroundJSONWriter(output);
			writer.write(message);
			writer.flush();

		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	@Override
	public void translateText2Data(String text, SurroundBase convertedData) throws IOException {

		if (!(convertedData instanceof SurroundBase)) {
			throw new SurroundWrongTypeException(convertedData.getClass().getCanonicalName() + " expected, is: "
					+ convertedData.getClass().getCanonicalName());
		}

		SurroundJSONReader reader = null;
		try {
			reader = new SurroundJSONReader(text);

			JSONObject jsonObject = (JSONObject) reader.parse();
			try {
				reader.read(convertedData, jsonObject);
			} catch (Exception e) {
				throw new SurroundMessagingException("error parsing data", e);
			}

		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	@Override
	public String translateData2Text(SurroundBase data) throws IOException {

		String converted = "";

		SurroundJSONWriter writer = null;
		try {

			writer = new SurroundJSONWriter();
			writer.write(data);

			converted = writer.getContent();

		} finally {
			if (writer != null) {
				writer.close();
			}
		}
		return converted;
	}

}
