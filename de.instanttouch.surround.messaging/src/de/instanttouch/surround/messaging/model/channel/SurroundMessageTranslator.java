/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public abstract class SurroundMessageTranslator extends SurroundDynamicEntity {

	public SurroundMessageTranslator() {
		super();
		initMessageTranslator();
	}

	public SurroundMessageTranslator(String name) {
		super(name);
		initMessageTranslator();
	}

	private void initMessageTranslator() {

	}

	public abstract SurroundMessage readMessage(byte[] msg) throws IOException;

	public abstract SurroundMessage readMessage(InputStream input) throws IOException;

	public abstract void writeMessage(SurroundMessage message, OutputStream output) throws IOException;

	public abstract void translateText2Data(String text, SurroundBase target) throws IOException;

	public abstract String translateData2Text(SurroundBase data) throws IOException;
}
