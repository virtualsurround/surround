/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundConnectionInfo extends SurroundDynamicEntity {

	public SurroundConnectionInfo() {
		super();
		initConnectionInfo();
	}

	public SurroundConnectionInfo(String name) {
		super(name);
		initConnectionInfo();
	}

	private void initConnectionInfo() {
		add(new SurroundActorReference("self"));
		add(new SurroundActorReference("target"));
	}

	public SurroundActorReference self() {
		return (SurroundActorReference) findByName("self");
	}

	public SurroundConnectionInfo assignSelf(SurroundActorReference self) {
		replace("self", self);
		return this;
	}

	public SurroundActorReference target() {
		return (SurroundActorReference) findByName("target");
	}

	public SurroundConnectionInfo assignTarget(SurroundActorReference target) {
		replace("target", target);
		return this;
	}

}
