/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.collection.SurroundStringMap;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundSession extends SurroundDynamicEntity {

	public SurroundSession() {
		super();
		initSession();
	}

	public SurroundSession(String name) {
		super(name);
		initSession();
	}

	private void initSession() {
		add(new SurroundStringMap<SurroundBase>("attributes"));
	}

	public SurroundStringMap<SurroundBase> getAttributes() {
		return (SurroundStringMap<SurroundBase>) findByName("attributes");
	}

}
