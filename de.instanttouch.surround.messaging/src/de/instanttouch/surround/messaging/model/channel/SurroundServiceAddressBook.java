package de.instanttouch.surround.messaging.model.channel;

import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.model.collection.SurroundList;

@SuppressWarnings("serial")
public class SurroundServiceAddressBook extends SurroundList<SurroundActorReference> {

	public SurroundServiceAddressBook() {
		super("serviceAddressBook", SurroundActorReference.class, "serviceAddress");
	}

	public SurroundServiceAddressBook(String name) {
		super(name, SurroundActorReference.class, "serviceAddress");
	}

	public SurroundActorReference findAddressByNameAndRole(String serviceName, String role) {

		SurroundActorReference serviceAddress = null;

		for (SurroundActorReference address : getChildren()) {
			if (address.getName().equals(serviceName)) {

				if (role.isEmpty() || role.equals(address.getRole())) {
					serviceAddress = address;
					break;
				}
			}
		}

		return serviceAddress;
	}

}
