/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import java.io.IOException;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public abstract class SurroundMessageHandler extends SurroundDynamicEntity {

	protected SurroundActor component;

	public SurroundMessageHandler() {
		super();
	}

	public SurroundMessageHandler(String name) {
		super(name);
	}

	public SurroundActor getComponent() {
		return component;
	}

	public void setComponent(SurroundActor component) {
		this.component = component;
	}

	public abstract boolean handle(SurroundActor component, SurroundMessage message)
			throws IOException, SurroundMessagingException;

}
