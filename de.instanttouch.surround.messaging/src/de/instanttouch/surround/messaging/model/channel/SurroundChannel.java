/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.messaging.model.SurroundClient;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.messaging.model.message.SurroundMessage.SurroundHeader;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundStringMap;
import de.instanttouch.surround.model.exception.SurroundNotFoundException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;

public abstract class SurroundChannel extends SurroundActor {

	protected static AtomicLong correlationId = new AtomicLong();

	final protected List<SurroundMessage> invalidMessages = new ArrayList<SurroundMessage>();

	ExecutorService receiverPool;
	ExecutorService senderPool;

	public SurroundChannel() {
		super();
		initChannel(1, 3);
	}

	public SurroundChannel(String name) {
		super(name);
		initChannel(1, 3);
	}

	public SurroundChannel(String name, int senderThreadCount, int receiverThreadCount) {
		super(name);
		initChannel(senderThreadCount, receiverThreadCount);
	}

	private void initChannel(int senderThreadCount, int receiverThreadCount) {

		add(new SurroundServiceAddressBook("serviceAdressBook"));
		add(new SurroundStringMap<SurroundSession>("sessionMap"));
		add(new SurroundList<SurroundActor>("components"));

		setupConcurrency(senderThreadCount, receiverThreadCount);
	}

	public void setupConcurrency(int senderThreadCount, int receiverThreadCount) {

		if (senderThreadCount == 0) {
		} else if (senderThreadCount == 1) {
			senderPool = Executors.newSingleThreadExecutor();
		} else {
			senderPool = Executors.newFixedThreadPool(senderThreadCount);
		}
		if (receiverThreadCount == 0) {
		} else if (receiverThreadCount == 1) {
			receiverPool = Executors.newSingleThreadExecutor();
		} else {
			receiverPool = Executors.newFixedThreadPool(receiverThreadCount);
		}
	}

	public ExecutorService getReceiverPool() {
		return receiverPool;
	}

	public ExecutorService getSenderPool() {
		return senderPool;
	}

	public SurroundMessageTranslator getMessageTranslator() {
		return (SurroundMessageTranslator) findByName("translator");
	}

	public SurroundChannel setMessageTranslator(SurroundMessageTranslator translator) {
		replace("translator", translator);
		return this;
	}

	public SurroundServiceAddressBook getServiceAddressBook() {
		return (SurroundServiceAddressBook) findByName("serviceAdressBook");
	}

	public SurroundChannel addServiceAddress(SurroundActorReference serviceAddress) {
		getServiceAddressBook().add(serviceAddress);
		return this;
	}

	public SurroundStringMap<SurroundSession> getSessionMap() {
		return (SurroundStringMap<SurroundSession>) findByName("sessionMap");
	}

	public SurroundSession getSession(String sessionId) {
		return getSessionMap().get(sessionId);
	}

	public SurroundSession createSession(String sessionId) {

		SurroundSession newSession = new SurroundSession(sessionId);
		getSessionMap().put(sessionId, newSession);

		return newSession;
	}

	public SurroundSession deleteSession(String sessionId) {

		SurroundSession removedSession = getSessionMap().remove(sessionId);
		return removedSession;
	}

	public SurroundList<SurroundActor> getComponents() {
		return (SurroundList<SurroundActor>) findByName("components");
	}

	public SurroundMessage createMessage(SurroundMessageData messageData, SurroundActor component,
			SurroundActorReference targetAddress) {

		SurroundMessage message;
		try {

			message = new SurroundMessage(messageData.getName());

			message.getHeader().setTargetAddress(targetAddress);
			SurroundActorReference modelCoordinate = (SurroundActorReference) component.getModelCoordinate(component);
			message.getHeader().setReturnAddress(modelCoordinate);

			fillMessageHeader(messageData, message);

			SurroundMessageTranslator translator = getChannel().getMessageTranslator();
			String text = translator.translateData2Text(messageData);
			message.setPayload(text);

		} catch (Exception e) {
			throw new SurroundMessagingException("error during message creation", e);
		}

		return message;
	}

	private void fillMessageHeader(SurroundMessageData messageData, SurroundMessage message) {

		message.getHeader().setMessageId(messageData.getMessageId());

		String newCorrelationId = "";
		try {
			newCorrelationId = messageData.getCorrelationId();
		} catch (Exception e) {
			newCorrelationId = messageData.getName();
			newCorrelationId += ":";
			newCorrelationId += correlationId.incrementAndGet();
			messageData.setCorrelationId(newCorrelationId);
		}
		message.getHeader().setCorrelationId(newCorrelationId);
	}

	@Override
	public SurroundMessage createErrorMessage(SurroundMessage affectedMessage, String error) {

		SurroundMessage message;
		try {
			message = new SurroundMessage("error on" + affectedMessage.getHeader().getMessageId());

			SurroundHeader header = message.getHeader();

			header.setTargetAddress(affectedMessage.getHeader().getReturnAddress());
			header.setReturnAddress(affectedMessage.getHeader().getTargetAddress());
			header.setMessageId(affectedMessage.getHeader().getMessageId());
			header.setError(error);
			header.setCorrelationId(affectedMessage.getHeader().getCorrelationId());

		} catch (Exception e) {
			throw new SurroundMessagingException("failed on error message creation", e);
		}
		return message;
	}

	@Override
	public SurroundChannel send(final SurroundMessage message) {

		Callable<Void> sendMesage = new Callable<Void>() {

			@Override
			public Void call() throws Exception {

				sendInternal(message);
				return null;
			}
		};
		senderPool.submit(sendMesage);

		return this;
	}

	@Override
	public SurroundChannel dispatch(final SurroundMessage message) {

		Callable<Void> receiveWorker = new Callable<Void>() {

			@Override
			public Void call() throws Exception {

				SurroundActorReference address = message.getHeader().getTargetAddress();
				if (address == null) {
					throw new SurroundNotFoundException("no target address available");
				}

				SurroundActor comp = findComponentByAddress(address);

				if (comp != null) {
					comp.onMessage(message);
				} else {
					logInvalidMessage(message);
				}
				return null;
			}
		};
		receiverPool.submit(receiveWorker);

		return this;
	}

	public void register(SurroundActor actor) throws IOException {

		SurroundList<SurroundActor> components = getComponents();
		if (!components.contains(actor)) {

			components.add(actor);

			SurroundActorReference address = new SurroundActorReference(actor.getName());
			address.setHost("localhost");

			for (SurroundClientInfo clientInfo : actor.getClients().keySet()) {
				SurroundClient surroundClient = actor.getClients().get(clientInfo);

				SurroundConnectionInfo connectionInfo = lookupService(clientInfo);
				surroundClient.connect(connectionInfo);
			}
		}
	}

	public void unregister(SurroundActor actor) throws SurroundMessagingException, IOException {
		getComponents().remove(actor);
	}

	public void checkMessageTranslator() {
		if (getMessageTranslator() == null) {
			throw new SurroundNotInitializedException("no message translator available");
		}
	}

	public SurroundActor findComponentByAddress(SurroundActorReference address) {

		if (address == null) {
			throw new SurroundMessagingException("adress is null");
		}

		SurroundActor comp = null;

		for (SurroundActor check : getComponents().getChildren()) {
			SurroundActorReference clientAddress = (SurroundActorReference) check.getModelCoordinate(check);

			throw new SurroundNotYetImplementedException("implement actor adress check");
			// if (clientAddress.match(address)) {
			// comp = check;
			// break;
			// }
		}

		return comp;
	}

	public void logInvalidMessage(SurroundMessage message) {
		invalidMessages.add(message);
	}

	public SurroundMessage receive(byte[] data) throws IOException, SurroundMessagingException {

		SurroundMessage message = getMessageTranslator().readMessage(data);
		return message;
	}

	public SurroundMessage receive(InputStream input) throws IOException, SurroundMessagingException {

		SurroundMessage message = getMessageTranslator().readMessage(input);
		return message;
	}

	public abstract SurroundChannel start() throws SurroundMessagingException, IOException;

	public abstract SurroundChannel stop() throws SurroundMessagingException, IOException;

	public abstract SurroundConnectionInfo lookupService(SurroundClientInfo clientInfo) throws IOException;

	public abstract void sendInternal(SurroundMessage message) throws IOException;

}
