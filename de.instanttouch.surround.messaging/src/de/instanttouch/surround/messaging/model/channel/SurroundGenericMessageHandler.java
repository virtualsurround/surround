/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import java.io.IOException;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;
import de.instanttouch.surround.model.util.SurroundLog;

public abstract class SurroundGenericMessageHandler extends SurroundMessageHandler {

	public SurroundGenericMessageHandler() {
		super();
	}

	public SurroundGenericMessageHandler(String name) {
		super(name);
	}

	@Override
	public boolean handle(SurroundActor component, SurroundMessage message)
			throws IOException, SurroundMessagingException {

		boolean handled = false;

		try {// todo is generic needed anymore?
				// String messageId = message.getHeader().getMessageId();
				// SurroundMessageData messageData =
				// getComponent().getMessagingModel().getMessage(messageId);
				//
				// if (messageData != null) {
				// handled = translate(message, messageData);
				// }
			if (true) {
				throw new SurroundNotYetImplementedException("use MessagingModel for handle message");
			}

		} catch (Exception e) {

			throw new SurroundMessagingException("error on handling message", e);
		}

		return handled;
	}

	public boolean translate(SurroundMessage message, SurroundBase target)
			throws IOException, SurroundMessagingException {

		boolean bHandled = false;

		try {
			String text = message.getPayload();
			getComponent().getChannel().getMessageTranslator().translateText2Data(text, target);

			if (target instanceof SurroundMessageData) {
				SurroundMessageData messageData = (SurroundMessageData) target;
				messageData.setCorrelationId(message.getHeader().getCorrelationId());
			}
			bHandled = true;

		} catch (Exception e) {
			SurroundLog.log("couldn't translate message to target", e);
		}

		return bHandled;
	}
}
