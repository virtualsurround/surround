/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.model.channel;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.messaging.model.message.SurroundMessage.SurroundHeader;
import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundBlockingMessageHandler extends SurroundMessageHandler {

	protected boolean isError = false;
	protected String error;
	protected Map<String, SurroundMessage> receivedMessages = new ConcurrentHashMap<String, SurroundMessage>();
	protected SurroundMessageData response;

	public SurroundBlockingMessageHandler(SurroundMessageData response) {
		super();
		this.response = response;
	}

	public SurroundBlockingMessageHandler(String name) {
		super(name);
	}

	public boolean hasReceived(String correlationId) {

		SurroundMessage message = receivedMessages.get(correlationId);

		return message != null;
	}

	@Override
	public synchronized boolean handle(SurroundActor component, SurroundMessage message)
			throws IOException, SurroundMessagingException {
		boolean bHandled = false;

		SurroundHeader header = message.getHeader();

		String messageId = header.getMessageId();
		if (messageId != response.getMessageId()) {
			throw new SurroundWrongTypeException(
					"wrong message: " + messageId + " expected: " + response.getMessageId());
		}

		if (!message.getHeader().isError()) {

			String text = message.getPayload();
			getComponent().getChannel().getMessageTranslator().translateText2Data(text, response);

		} else {
			isError = true;
			error = header.getError();
		}

		String correlationId = header.getCorrelationId();
		receivedMessages.put(correlationId, message);
		bHandled = true;

		notifyAll();

		return bHandled;
	}

	public synchronized void clear() {
		receivedMessages.clear();
	}

}
