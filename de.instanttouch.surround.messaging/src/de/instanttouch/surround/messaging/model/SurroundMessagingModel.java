package de.instanttouch.surround.messaging.model;

import de.instanttouch.surround.messaging.model.message.SurroundMessageData;
import de.instanttouch.surround.model.collection.SurroundStringMap;
import de.instanttouch.surround.model.graph.SurroundNode;

public class SurroundMessagingModel extends SurroundNode {

	public SurroundMessagingModel() {
		initMessagingModel();
	}

	public SurroundMessagingModel(String name) {
		super(name);
		initMessagingModel();
	}

	private void initMessagingModel() {
		add(new SurroundStringMap<SurroundMessageData>("messages", SurroundMessageData.class));
	}

	public SurroundStringMap<SurroundMessageData> getMessages() {
		return (SurroundStringMap<SurroundMessageData>) findByName("messages");
	}

	public SurroundMessagingModel addMessage(SurroundMessageData request) {
		getMessages().put(request.getMessageId(), request);
		return this;
	}

	public SurroundMessageData getMessage(String messageId) {
		return getMessages().get(messageId);
	}

	public boolean isProvided(String messageId) {
		return getMessages().containsKey(messageId);
	}
}
