/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.messaging.exception;

import de.instanttouch.surround.model.exception.SurroundBaseException;

@SuppressWarnings("serial")
public class SurroundMessagingException extends SurroundBaseException {

	public SurroundMessagingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SurroundMessagingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
