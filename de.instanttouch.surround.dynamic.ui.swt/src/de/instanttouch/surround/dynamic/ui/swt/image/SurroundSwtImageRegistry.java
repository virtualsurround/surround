/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.image;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.graphics.Image;

import de.instanttouch.surround.dynamic.ui.swt.Activator;
import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundSwtImageRegistry {

	public interface IImagePathCreator {

		public String getImagePath(SurroundBase type);

		public String getOverlayPath(SurroundBase type);
	}

	public class ImageInfo {

		String path;
		String overlayPath;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((overlayPath == null) ? 0 : overlayPath.hashCode());
			result = prime * result + ((path == null) ? 0 : path.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ImageInfo other = (ImageInfo) obj;
			if (overlayPath == null) {
				if (other.overlayPath != null)
					return false;
			} else if (!overlayPath.equals(other.overlayPath))
				return false;
			if (path == null) {
				if (other.path != null)
					return false;
			} else if (!path.equals(other.path))
				return false;
			return true;
		}

	}

	protected static SurroundSwtImageRegistry instance = null;

	protected Map<Class<? extends SurroundBase>, String> imagePaths = new HashMap<Class<? extends SurroundBase>, String>();
	protected Map<Class<? extends SurroundBase>, IImagePathCreator> creators = new HashMap<Class<? extends SurroundBase>, IImagePathCreator>();

	protected Map<ImageInfo, Image> images = new HashMap<SurroundSwtImageRegistry.ImageInfo, Image>();

	public SurroundSwtImageRegistry() {
		super();
	}

	public static SurroundSwtImageRegistry getInstance() {
		if (instance == null) {
			instance = new SurroundSwtImageRegistry();
		}

		return instance;
	}

	public void registerImage(Class<? extends SurroundBase> type, String path) {
		imagePaths.put(type, path);
	}

	public void registerImageCreator(Class<? extends SurroundBase> type, IImagePathCreator creator) {
		creators.put(type, creator);
	}

	public Image getImage(String path) {
		return Activator.getImage(path);
	}

	public Image getImage(String bundle, String path) {
		return Activator.getImage(bundle, path);
	}

	public ImageInfo create(String bundleName, String path) {
		ImageInfo info = new ImageInfo();
		info.path = bundleName + ":" + path;

		return info;
	}

	public Image getImage(SurroundBase type) {
		Image image = null;
		String path = null;
		String overlayPath = null;

		// determine image path per class directly
		IImagePathCreator creator = creators.get(type.getClass());
		if (creator != null) {
			path = creator.getImagePath(type);
			overlayPath = creator.getOverlayPath(type);
		}

		if (path == null) {
			path = imagePaths.get(type.getClass());
		}

		// lookup for super class settings

		if (path == null) {
			Class base = type.getClass().getSuperclass();
			while (base != SurroundBase.class) {
				creator = creators.get(base);
				if (creator != null) {
					path = creator.getImagePath(type);
					overlayPath = creator.getOverlayPath(type);
					break;
				}

				base = base.getSuperclass();
			}
		}

		// lookup for available base images
		if (path == null) {
			Class base = type.getClass().getSuperclass();
			while (base != SurroundBase.class) {
				path = imagePaths.get(base);
				if (path != null) {
					break;
				}

				base = base.getSuperclass();
			}

			if (path == null) {
				path = imagePaths.get(SurroundBase.class);
			}
		}

		if (path != null) {
			ImageInfo check = new ImageInfo();
			check.path = path;
			check.overlayPath = overlayPath;

			image = images.get(check);

			if (image == null) {
				if (overlayPath != null) {
					// base image
					ImageInfo base = new ImageInfo();
					base.path = path;

					Image baseImage = images.get(base);
					if (baseImage == null) {
						baseImage = Activator.getImage(path);
						images.put(base, baseImage);
					}

					// overlay
					ImageInfo overlay = new ImageInfo();
					overlay.overlayPath = overlayPath;

					Image overlayImage = images.get(overlay);
					if (overlayImage == null) {
						overlayImage = Activator.getImage(overlayPath);
						images.put(overlay, overlayImage);
					}

					SurroundSwtOverlayIcon composed = new SurroundSwtOverlayIcon(baseImage, overlayImage);
					image = composed.createImage();

					images.put(check, image);

				} else {
					image = Activator.getImage(path);
					images.put(check, image);
				}
			}
		} else if (overlayPath != null) {
			// overlay
			ImageInfo overlay = new ImageInfo();
			overlay.overlayPath = overlayPath;

			image = images.get(overlay);
			if (image == null) {
				image = Activator.getImage(overlayPath);
				images.put(overlay, image);
			}
		}

		return image;
	}

	public void registerImage(Class<? extends SurroundBase> type, String bundleName, String path) {

		String completePath = bundleName + ":" + path;
		registerImage(type, completePath);
	}

	public void registerImage(Class<? extends SurroundBase> type, String bundleName, String path, Image image) {

		ImageInfo info = create(bundleName, path);
		images.put(info, image);
		registerImage(type, info.path);
	}
}
