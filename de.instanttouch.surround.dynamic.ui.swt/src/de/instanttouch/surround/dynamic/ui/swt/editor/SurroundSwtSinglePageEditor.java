package de.instanttouch.surround.dynamic.ui.swt.editor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import de.instanttouch.surround.data.model.SurroundDataReference;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.editor.SurroundEditorNode.ISurroundEditor;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtOutline;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtToolbar;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.domain.SurroundModel;

public abstract class SurroundSwtSinglePageEditor extends EditorPart implements ISurroundEditor {

	protected SurroundModel model = null;

	protected boolean bDirty = false;
	protected SurroundEditorNode editorNode;
	protected ExecutorService senderPool = Executors.newSingleThreadExecutor();

	protected SurroundSwtOutline outline;
	protected SurroundPlayer player;
	protected SurroundPresenter<SurroundBase> presenter;

	public SurroundSwtSinglePageEditor() {

		player = new SurroundPlayer();
		player.setExecutor(senderPool);

		editorNode = new SurroundEditorNode("editor");
		editorNode.setEditor(this);
		editorNode.setPlayer(player);
	}

	@Override
	public boolean isDirty() {
		return bDirty;
	}

	public void setDirty(boolean dirty) {
		bDirty = dirty;
	}

	public void fireDirty() {
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public void markDirty(boolean isDirty) {
		bDirty = isDirty;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public void setModel(SurroundModel model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input) {

		setSite(site);
		setInput(input);

		SurroundDataReference reference = null;

		if (input instanceof FileEditorInput) {
			FileEditorInput fileEditorInput = (FileEditorInput) input;

			String pathName = fileEditorInput.getPath().toString();

			reference = new SurroundDataReference();
			reference.setFile(pathName);

			String title = pathName;
			setTitle(title);

		} else if (input instanceof SurroundSwtEditorInput) {
			reference = ((SurroundSwtEditorInput) input).getReference();
		}
		getModel().setReference(reference);

		try {
			load();
		} catch (Exception e) {
			SurroundSwtExceptionHandler.handle(e);
		}

		getSite().setSelectionProvider(new ISelectionProvider() {
			private List<ISelectionChangedListener> selectionListeners = new ArrayList<>();

			@Override
			public void setSelection(ISelection selection) {
				for (ISelectionChangedListener listener : selectionListeners) {
					listener.selectionChanged(new SelectionChangedEvent(this, selection));
				}
			}

			@Override
			public void removeSelectionChangedListener(ISelectionChangedListener listener) {
				selectionListeners.remove(listener);
			}

			@Override
			public ISelection getSelection() {
				return new StructuredSelection(getModel());
			}

			@Override
			public void addSelectionChangedListener(ISelectionChangedListener listener) {
				if (!selectionListeners.contains(listener)) {
					selectionListeners.add(listener);
				}
			}
		});
	}

	public SurroundEditorNode getEditorNode() {
		return editorNode;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		getEditorNode().doSave(monitor);
	}

	@Override
	public void doSaveAs() {

		FileDialog dialog = new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		dialog.setText("select file");

		String newPath = dialog.open();
		if (newPath != null) {
			getModel().getReference().set(newPath);
		}

		doSave(null);
	}

	@Override
	public SurroundModel getModel() {
		return model;
	}

	@Override
	public void setTitle(String title) {
		super.setTitle(title);
	}

	@Override
	public Object getAdapter(Class required) {

		if (IContentOutlinePage.class.equals(required)) {
			if (outline == null) {
				outline = new SurroundSwtOutline(getModel(), this);
			}
			return outline;
		}

		return super.getAdapter(required);
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {

		SurroundSwtParent layoutContainer = new SurroundSwtParent(parent);
		SurroundSwtToolbar toolbar = new SurroundSwtToolbar(layoutContainer.getSwtContainer());

		SurroundDynamicEntity model = getModel();

		presenter = SurroundPresenterRegistry.getPresenter(model);
		toolbar.setContentPresenter(presenter);

		presenter.setFilter(toolbar.getViewerFilter());

		presenter.show(layoutContainer);
		getEditorNode().connectDelegate(presenter);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	public void adapt(FormToolkit toolkit, Composite composite) {
		toolkit.adapt(composite);
		for (Control control : composite.getChildren()) {
			if (control instanceof Composite) {
				Composite child = (Composite) control;
				adapt(toolkit, child);
			} else {
				toolkit.adapt(control, false, false);
			}
		}

	}

	public void load() {
		getEditorNode().load();
	}

	public SurroundSwtOutline getOutline() {
		return outline;
	}

}
