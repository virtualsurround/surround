/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.layout;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundSwtComposite extends Composite {

	public SurroundSwtComposite(Composite parent, int style) {
		super(parent, style);

		setLayout(new GridLayout());
		setLayoutData(new GridData(GridData.FILL_BOTH));

		adaptToolkit();
	}

	public void setHints(int width, int height) {

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.widthHint = width;
		gridData.heightHint = height;

		setLayoutData(gridData);
	}

	public void adaptToolkit() {

		if (SurroundRuntimeTools.isEclipseRunning()) {

			FormToolkit toolkit = new FormToolkit(Display.getCurrent());
			toolkit.adapt(this);

			for (Control control : getChildren()) {
				if (control instanceof SurroundSwtComposite) {
					SurroundSwtComposite child = (SurroundSwtComposite) control;
					child.adaptToolkit();
				} else {
					toolkit.adapt(control, false, false);
				}
			}
		}
	}

	public void udpate(SurroundChange<?> change) {
		// TODO Auto-generated method stub

	}

}
