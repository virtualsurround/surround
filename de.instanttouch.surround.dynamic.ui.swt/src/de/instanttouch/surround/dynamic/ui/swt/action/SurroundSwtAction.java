/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.action;

import org.eclipse.jface.action.Action;

import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.action.SurroundPlayer;

public abstract class SurroundSwtAction extends SurroundAction {

	public SurroundSwtAction() {
		super();
	}

	public SurroundSwtAction(String name) {
		super(name);
	}

	public Action play4JFace() {

		Action action = new Action() {

			@Override
			public void run() {
				play(new SurroundPlayer.ProgressAdapter());
			}

		};
		return action;
	}
}
