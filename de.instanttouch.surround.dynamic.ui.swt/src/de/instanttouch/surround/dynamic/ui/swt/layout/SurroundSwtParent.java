package de.instanttouch.surround.dynamic.ui.swt.layout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.swt.javafx.bridge.SurroundFXSwtCanvas;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;
import javafx.scene.control.ScrollPane;

public class SurroundSwtParent implements ISurroundLayoutContainer {

	private SurroundSwtComposite composite;
	protected eLayoutContraint constraints;

	public SurroundSwtParent(Composite parent) {
		this(parent, SWT.NONE);
	}

	public SurroundSwtParent(Composite parent, int style) {

		composite = new SurroundSwtComposite(parent, style);
		composite.adaptToolkit();
	}

	@Override
	public SurroundSwtComposite getSwtContainer() {
		return composite;
	}

	public void adaptToolkit() {

		if (SurroundRuntimeTools.isEclipseRunning()) {

			FormToolkit toolkit = new FormToolkit(Display.getCurrent());
			toolkit.adapt(composite);

			for (Control control : composite.getChildren()) {
				if (control instanceof SurroundSwtComposite) {
					SurroundSwtComposite child = (SurroundSwtComposite) control;
					child.adaptToolkit();
				} else if (control instanceof SurroundSwtScrolledComposite) {
					SurroundSwtScrolledComposite child = (SurroundSwtScrolledComposite) control;
					child.adaptToolkit();
				} else {
					toolkit.adapt(control, false, false);
				}
			}
		}
	}

	@Override
	public ScrollPane getJavaFXContainer() {

		SurroundFXSwtCanvas canvas = new SurroundFXSwtCanvas(composite, SWT.NONE);
		return canvas.getScrollPane();
	}

	@Override
	public eLayoutContraint getLayoutConstraint() {
		return constraints;
	}

	public SurroundSwtParent setLayoutConstraints(eLayoutContraint constraint) {
		this.constraints = constraints;
		return this;
	}

	@Override
	public void layout() {
		adaptToolkit();
	}

}
