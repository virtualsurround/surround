package de.instanttouch.surround.dynamic.ui.swt.presenter;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.instanttouch.surround.dynamic.ui.exceptions.SurroundUIException;
import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtChooseFileAction;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtChooseFolderAction;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtLinkAction;
import de.instanttouch.surround.dynamic.ui.swt.dialog.SurroundSwtHexDialog;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtComposite;
import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundByteArray;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.ref.SurroundDirectory;
import de.instanttouch.surround.model.ref.SurroundEmailAddress;
import de.instanttouch.surround.model.ref.SurroundFile;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundSwtTextPresenter extends SurroundSwtPresenter<SurroundBase> {

	protected static Color errorMarker = null;
	protected static Color normalMarker = null;
	protected static Color textColor = null;

	private Text textField;

	public SurroundSwtTextPresenter() {
		super();
		initTextPresenter();
	}

	public SurroundSwtTextPresenter(String name) {
		super(name);
		initTextPresenter();
	}

	private void initTextPresenter() {
	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Object container = layoutContainer.getSwtContainer();
		if (!(container instanceof Composite)) {
			throw new SurroundUIException("wrong type of container: " + container);
		}

		Composite parent = (Composite) container;
		
		GridData gridData = getModel().getConstraints().isLarge()? 
				new GridData(GridData.FILL_BOTH):
				new GridData(GridData.FILL_HORIZONTAL);
			
		Composite textComposite = new SurroundSwtComposite(parent, SWT.NONE);
		textComposite.setLayoutData(gridData);

		int columns = 2;
		int style = SWT.BORDER;
		if (model.getConstraints().isPassword()) {
			style |= SWT.PASSWORD;
		}
		if (model.getConstraints().isLarge())

		{
			style |= SWT.MULTI;
			style |= SWT.V_SCROLL;
			if (model.getConstraints().isWrapped()) {
				style |= SWT.WRAP;
			} else {
				style |= SWT.H_SCROLL;
			}
		}

		FormToolkit toolkit = new FormToolkit(Display.getCurrent());

		Label label = new Label(textComposite, SWT.NONE);
		label.setText(model.getName());

		gridData = new GridData(150, 25);
		label.setLayoutData(gridData);
		toolkit.adapt(label, false, false);


		textField = new Text(textComposite, style);
		textField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		textField.setText(model.toString());

		GridData layoutData = new GridData(GridData.FILL_HORIZONTAL);
		textField.setLayoutData(layoutData);

		if (errorMarker == null) {
			errorMarker = parent.getDisplay().getSystemColor(SWT.COLOR_RED);
			normalMarker = textField.getBackground();
			textColor = textField.getForeground();
		}

		textField.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				String content = textField.getText();
				model.set(content);
				textField.setForeground(textColor);
			}
		});
		// model.addChangeNotificationListener(new ISurroundChangeListener() {
		//
		// @Override
		// public void changed(SurroundBase type, SurroundChange<?> change) {
		//
		// refresh();
		// }
		// });
		textField.addListener(SWT.DefaultSelection, new Listener() {

			@Override
			public void handleEvent(Event event) {
			}

		});
		textField.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				String content = textField.getText();
				model.set(content);
			}

			@Override
			public void focusGained(FocusEvent e) {
				textField.setText(model.toString());
			}
		});

		if (model instanceof SurroundFile)

		{

			columns++;

			Button btFindFile = new Button(textComposite, SWT.PUSH);
			btFindFile.setText("...");
			btFindFile.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					SurroundSwtChooseFileAction chooser = new SurroundSwtChooseFileAction(parent.getShell(),
							"choose file");
					chooser.setModel(model);
					chooser.setThreadType(SurroundAction.UI_THREAD);
					getPlayer().play(chooser);
				}
			});

		} else if (model instanceof SurroundDirectory) {

			columns++;

			Button btFindFolder = new Button(textComposite, SWT.PUSH);
			btFindFolder.setText("...");
			btFindFolder.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					SurroundSwtChooseFolderAction chooser = new SurroundSwtChooseFolderAction(parent.getShell(),
							"choose folder");
					chooser.setModel(model);
					chooser.setThreadType(SurroundAction.UI_THREAD);
					getPlayer().play(chooser);
				}
			});

		} else if (model instanceof SurroundURL) {

			columns++;

			Button followURL = new Button(textComposite, SWT.PUSH);
			followURL.setText("...");
			followURL.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					SurroundSwtLinkAction linkAction = new SurroundSwtLinkAction("follow url");
					linkAction.setModel(model);
					linkAction.setThreadType(SurroundAction.CURRENT);
					getPlayer().play(linkAction);
				}
			});
		} else if (model instanceof SurroundEmailAddress) {

			columns++;

			String emailUrl = model.toString();
			if (!emailUrl.startsWith("mailto:")) {
				model.set("mailto:" + emailUrl);
			}

			Button mailTo = new Button(textComposite, SWT.PUSH);
			mailTo.setText("...");
			mailTo.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					SurroundSwtLinkAction linkAction = new SurroundSwtLinkAction("follow url");
					linkAction.setModel(model);
					linkAction.setThreadType(SurroundAction.CURRENT);
					getPlayer().play(linkAction);
				}
			});
		} else if (model instanceof SurroundInteger) {
			columns++;

			SurroundInteger surroundInteger = (SurroundInteger) model;

			final Button hex = new Button(textComposite, SWT.PUSH);
			hex.setText(surroundInteger.isAsHexadecimal() ? "dec" : "hex");
			hex.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					textField.getDisplay().asyncExec(new Runnable() {

						@Override
						public void run() {

							String textValue = textField.getText();
							surroundInteger.set(textValue);

							boolean isHex = surroundInteger.isAsHexadecimal();

							surroundInteger.setAsHexadecimal(!isHex);
							textField.setText(surroundInteger.toString());

							hex.setText(surroundInteger.isAsHexadecimal() ? "dec" : "hex");
						}
					});
				}
			});
		} else if (model instanceof SurroundLong) {
			columns++;

			SurroundLong surroundLong = (SurroundLong) model;

			final Button hex = new Button(textComposite, SWT.PUSH);
			hex.setText(surroundLong.isAsHexadecimal() ? "dec" : "hex");
			hex.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					textField.getDisplay().asyncExec(new Runnable() {

						@Override
						public void run() {

							String textValue = textField.getText();
							surroundLong.set(textValue);

							boolean isHex = surroundLong.isAsHexadecimal();

							surroundLong.setAsHexadecimal(!isHex);
							textField.setText(surroundLong.toString());

							hex.setText(surroundLong.isAsHexadecimal() ? "dec" : "hex");
						}
					});
				}
			});
		} else if (model instanceof SurroundByteArray) {
			columns++;

			SurroundByteArray surroundByteArray = (SurroundByteArray) model;

			final Button show = new Button(textComposite, SWT.PUSH);
			show.setText("...");
			show.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					SurroundSwtHexDialog hexDialog = new SurroundSwtHexDialog(null, surroundByteArray);
					if (hexDialog.open() == Dialog.OK) {
					}
				}
			});
		}

		textComposite.setLayout(new GridLayout(columns, false));
	}

	@Override
	public void refresh() {

		if (textField != null && !textField.isDisposed()) {
			textField.getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {

					String content = textField.getText();

					if (!content.equals(model.toString())) {
						textField.setText(model.toString());
					}
				}
			});
		}
	}
}
