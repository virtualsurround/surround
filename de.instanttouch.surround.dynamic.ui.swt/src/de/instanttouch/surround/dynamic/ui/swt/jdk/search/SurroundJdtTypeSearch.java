package de.instanttouch.surround.dynamic.ui.swt.jdk.search;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.core.search.SearchParticipant;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.SearchRequestor;
import org.eclipse.jdt.internal.core.BinaryType;
import org.eclipse.jdt.internal.core.JavaModelManager;
import org.eclipse.jdt.internal.core.search.JavaSearchParticipant;
import org.eclipse.jdt.internal.core.search.JavaWorkspaceScope;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundJdtTypeSearch<T extends SurroundBase> {

	public SurroundList<SurroundJdtClassInfo<T>> findSurroundTypes(Class<T> surroundClass)
			throws JavaModelException, SurroundBaseException {

		final SurroundList<SurroundJdtClassInfo<T>> allTypes = new SurroundList<SurroundJdtClassInfo<T>>();

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		if (workspace == null) {
			throw new SurroundNotInitializedException("no workspace available");
		}

		JavaModelManager modelManager = JavaModelManager.getJavaModelManager();

		final JavaWorkspaceScope workspaceScope = modelManager.getWorkspaceScope();

		SearchPattern snakePattern = SearchPattern.createPattern(surroundClass.getCanonicalName(),
				IJavaSearchConstants.TYPE, IJavaSearchConstants.IMPLEMENTORS, SearchPattern.R_EXACT_MATCH);

		final SearchEngine searchEngine = new SearchEngine();

		final SearchParticipant participants[] = { new JavaSearchParticipant() };

		final SearchRequestor requestor = new SearchRequestor() {

			@Override
			public void acceptSearchMatch(SearchMatch match) throws CoreException {

				Object element = match.getElement();
				if (element instanceof BinaryType) {
					BinaryType binaryType = (BinaryType) element;

					SurroundJdtClassInfo<T> info = new SurroundJdtClassInfo<T>();
					try {
						info.createFrom(binaryType);

					} catch (SurroundWrongTypeException e) {
						SurroundLog.log(e);
					}
					allTypes.add(info);

					addChildType(this, binaryType);
				}

			}

			private void addChildType(SearchRequestor requestor, BinaryType binaryType) {

				SearchPattern childPattern = SearchPattern.createPattern(binaryType.getFullyQualifiedName(),
						IJavaSearchConstants.TYPE, IJavaSearchConstants.IMPLEMENTORS, SearchPattern.R_EXACT_MATCH);

				try {
					searchEngine.search(childPattern, participants, workspaceScope, requestor, null);
				} catch (CoreException e) {
					throw new SurroundBaseException(e.getMessage());
				}

			};

		};

		try {
			searchEngine.search(snakePattern, participants, workspaceScope, requestor, null);
		} catch (CoreException e) {
			throw new SurroundBaseException(e.getMessage());
		}

		return allTypes;

	}

}
