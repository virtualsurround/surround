/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.jface;

import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtScrolledComposite;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.exception.SurroundBaseException;

public class SurroundSwtSection {

	private Composite composite = null;
	private boolean isSection = false;
	private Object filter;
	private FormToolkit toolkit;

	public SurroundSwtSection(Composite parent, Object Filter) {

		this.filter = filter;

		try {
			toolkit = new FormToolkit(parent.getDisplay());
			ScrolledForm form = toolkit.createScrolledForm(parent);

			Section section = toolkit.createSection(parent, Section.TWISTIE | Section.TITLE_BAR | Section.EXPANDED);
			toolkit.adapt(section);

			// section.setExpanded(true);

			isSection = true;
			composite = section;

		} catch (Throwable e) {
			composite = parent;
		}
	}

	public SurroundSwtScrolledComposite create(SurroundBase type, String title) throws SurroundBaseException {

		SurroundSwtScrolledComposite content = new SurroundSwtScrolledComposite(composite, SWT.NONE);

		SurroundSwtParent layoutContainer = new SurroundSwtParent(content);
		SurroundPresenter<SurroundBase> presenter = SurroundPresenterRegistry.getPresenter(type);
		presenter.show(layoutContainer);

		if (isSection) {
			Section section = (Section) composite;

			section.setClient(content);
			section.setText(title);
		}

		return content;
	}

	public void create(Composite content, String title) {
		if (isSection) {
			Section section = (Section) composite;

			section.setClient(content);
			section.setText(title);
		}
	}

	public void setContent(Composite content) {
		if (isSection) {
			content.setLayout(new GridLayout());
			content.setLayoutData(new GridData(GridData.FILL_BOTH));

			Section section = (Section) composite;
			section.setClient(content);
		}
	}

	public Composite getComposite() {
		return composite;
	}

	public ToolBar createToolbar() {


		ToolBarManager toolBarManager = new ToolBarManager(SWT.FLAT);
		ToolBar toolBar = toolBarManager.createControl(composite);
		toolkit.adapt(toolBar);

		if (isSection) {
			Section section = (Section) composite;
			section.setTextClient(toolBar);
		}

		return toolBar;
	}

	public void setTitle(String title) {

		if (isSection) {
			Section section = (Section) composite;
			section.setText(title);
		}
	}

	public void setLayoutData(Object layoutData) {
		// composite.setLayoutData(layoutData);
	}

	public void setExpanded(boolean expanded) {
		if (isSection) {
			Section section = (Section) composite;
			section.setExpanded(expanded);
		}
	}

}
