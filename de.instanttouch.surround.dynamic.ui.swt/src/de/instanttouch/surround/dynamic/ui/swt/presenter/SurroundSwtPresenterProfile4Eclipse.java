package de.instanttouch.surround.dynamic.ui.swt.presenter;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtMasterDetailPresenter;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundByteArray;
import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundRange;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.collection.SurroundOptions;
import de.instanttouch.surround.model.composite.SurroundEntity;
import de.instanttouch.surround.model.ref.SurroundDirectory;
import de.instanttouch.surround.model.ref.SurroundFile;
import de.instanttouch.surround.model.time.SurroundDate;
import de.instanttouch.surround.model.time.SurroundTimestamp;
import de.instanttouch.surround.model.url.SurroundURL;

public class SurroundSwtPresenterProfile4Eclipse {

	public static void registerEclipsePresenter() {

		SurroundPresenterRegistry.register(SurroundString.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundInteger.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundLong.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundDouble.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundRange.class, SurroundSwtRangePresenter.class);
		SurroundPresenterRegistry.register(SurroundBoolean.class, SurroundSwtBooleanPresenter.class);

		SurroundPresenterRegistry.register(SurroundDate.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundTimestamp.class, SurroundSwtTextPresenter.class);

		SurroundPresenterRegistry.register(SurroundFile.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundDirectory.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundURL.class, SurroundSwtTextPresenter.class);
		SurroundPresenterRegistry.register(SurroundByteArray.class, SurroundSwtTextPresenter.class);

		SurroundPresenterRegistry.register(SurroundEntity.class, SurroundSwtCompositePresenter.class);
		SurroundPresenterRegistry.register(SurroundList.class, SurroundSwtListPresenter.class);
		SurroundPresenterRegistry.register(SurroundMap.class, SurroundSwtMapPresenter.class);
		SurroundPresenterRegistry.register(SurroundOptions.class, SurroundSwtOptionsPresenter.class);

		SurroundPresenterRegistry.register(SurroundBase.class, SurroundSwtMasterDetailPresenter.class);

	}
}
