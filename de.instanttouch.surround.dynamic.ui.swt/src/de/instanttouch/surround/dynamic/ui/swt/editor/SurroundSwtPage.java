/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.editor;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtToolbar;
import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundSwtPage extends FormPage {

	SurroundSwtEditor editor = null;

	public SurroundSwtPage(SurroundSwtEditor editor, String id, String title) {
		super(editor, id, title);
		this.editor = editor;
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {

		ScrolledForm form = managedForm.getForm();
		form.setText(getTitle());

		managedForm.getToolkit().decorateFormHeading(form.getForm());
		form.getBody().setLayout(new GridLayout(1, false));

		SurroundSwtParent layoutContainer = new SurroundSwtParent(managedForm.getForm().getBody());

		SurroundSwtToolbar toolbar = new SurroundSwtToolbar(layoutContainer.getSwtContainer());

		SurroundPresenter<SurroundBase> presenter = SurroundPresenterRegistry.getPresenter(editor.getModel());
		editor.getEditorNode().connectDelegate(presenter);
		presenter.setFilter(toolbar.getViewerFilter());
		toolbar.setContentPresenter(presenter);

		presenter.show(layoutContainer);

		form.setContent(layoutContainer.getSwtContainer());
		Menu menu = layoutContainer.getSwtContainer().getMenu();
	}

	public void adapt(FormToolkit toolkit, Composite composite) {
		toolkit.adapt(composite);
		for (Control control : composite.getChildren()) {
			if (control instanceof Composite) {
				Composite child = (Composite) control;
				adapt(toolkit, child);
			} else {
				toolkit.adapt(control, false, false);
			}
		}

	}

	public void createAdditionalTools(Composite toolbarComposite) {

	}

}
