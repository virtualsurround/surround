/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.view;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtToolbar;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundViewNode;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.util.SurroundLog;

public abstract class SurroundSwtView<T extends SurroundBase> extends ViewPart {

	public abstract T getModel();

	protected SurroundViewNode viewNode = new SurroundViewNode("viewPart");
	protected SurroundPresenter<T> presenter;

	@Override
	public void createPartControl(Composite parent) {

		try {

			SurroundSwtParent layoutContainer = new SurroundSwtParent(parent);
			SurroundSwtToolbar toolbar = new SurroundSwtToolbar(layoutContainer.getSwtContainer());

			presenter = SurroundPresenterRegistry.getPresenter(getModel());
			presenter.setFilter(toolbar.getViewerFilter());
			toolbar.setContentPresenter(presenter);

			presenter.show(layoutContainer);
			viewNode.connectDelegate(presenter);

		} catch (Exception e) {
			SurroundLog.log(e);
		}
	}

	@Override
	public void setFocus() {

	}

}
