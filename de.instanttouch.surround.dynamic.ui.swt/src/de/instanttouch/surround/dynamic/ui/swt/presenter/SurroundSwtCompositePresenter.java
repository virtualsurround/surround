package de.instanttouch.surround.dynamic.ui.swt.presenter;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtMasterDetailPresenter;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtSection;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtComposite;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtScrolledComposite;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.composite.SurroundEntity;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundSwtCompositePresenter extends SurroundSwtPresenter<SurroundEntity> {

	public List<SurroundPresenter> presenterList = new ArrayList<>();

	public SurroundSwtCompositePresenter() {
	}

	public SurroundSwtCompositePresenter(String name) {
		super(name);
	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		FormToolkit toolkit = new FormToolkit(Display.getCurrent());

		Object obj = layoutContainer.getSwtContainer();
		if (!(obj instanceof Composite)) {
			throw new SurroundWrongTypeException("invalid layout container: " + obj + " composite expected");
		}
		Composite parent = (Composite) obj;
		SurroundEntity model = getModel();

		Group group = new Group(parent, SWT.NONE);
		group.setText(model.getName());
		group.setLayout(new GridLayout());
		group.setLayoutData(new GridData(GridData.FILL_BOTH));
		toolkit.adapt(group);

		SurroundSwtComposite composite = new SurroundSwtComposite(group, SWT.NONE);
		composite.setHints(800, 500);
		composite.adaptToolkit();

		if (model.hasOnlyBasicVisibleTypes() && model.size() <= 12) {
			SurroundSwtScrolledComposite scrolledComposite = new SurroundSwtScrolledComposite(composite, SWT.NONE);


			SurroundSwtParent childContainer = new SurroundSwtParent(scrolledComposite);
			childContainer.getSwtContainer().setLayout(new GridLayout(1, false));
			childContainer.getSwtContainer().setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			for (SurroundBase child : getModel().getVisibleChildren()) {

				/*
				 * TODO label in text presenter or in entty presenter Label label = new
				 * Label(childContainer.getSwtContainer(), SWT.NONE);
				 * label.setText(child.getName());
				 */

				SurroundPresenter<SurroundBase> childPresenter = SurroundPresenterRegistry.getPresenter(child);
				childPresenter.show(childContainer);
				connectDelegate(childPresenter);

				presenterList.add(childPresenter);
			}

			scrolledComposite.setContent(childContainer.getSwtContainer());
			childContainer.getSwtContainer().adaptToolkit();
		} else {
			SurroundSwtMasterDetailPresenter masterDetailView = new SurroundSwtMasterDetailPresenter();
			masterDetailView.setModel(getModel());

			SurroundSwtParent masterContainer = new SurroundSwtParent(composite);
			masterDetailView.show(masterContainer);

			connectDelegate(masterDetailView);

			presenterList.add(masterDetailView);
		}
	}

	@Override
	public void refresh() {

		for (SurroundPresenter surroundPresenter : presenterList) {
			surroundPresenter.refresh();
		}
	}

}
