/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.image;

import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;

/**
 * An OverlayIcon consists of a main icon and several adornments.
 */
public class SurroundSwtOverlayIcon extends CompositeImageDescriptor {

	static final int DEFAULT_WIDTH = 16;
	static final int DEFAULT_HEIGHT = 16;

	private Point size = null;

	private final ImageData overlay;
	private final ImageData base;

	public SurroundSwtOverlayIcon(ImageDescriptor base, ImageDescriptor overlay) {
		this.base = base.getImageData();
		this.overlay = overlay.getImageData();
		this.size = new Point(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	public SurroundSwtOverlayIcon(Image base, Image overlay) {
		this.base = base.getImageData();
		this.overlay = overlay.getImageData();
		this.size = new Point(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	protected void drawBottomLeft(ImageData data) {
		drawImage(data, 0, getSize().y - data.height);
	}

	@Override
	protected void drawCompositeImage(int width, int height) {
		drawImage(base, 0, 0);
		drawBottomLeft(overlay);
	}

	@Override
	protected Point getSize() {
		return size;
	}
}
