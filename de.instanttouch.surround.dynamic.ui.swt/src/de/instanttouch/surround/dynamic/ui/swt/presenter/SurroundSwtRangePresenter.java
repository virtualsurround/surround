package de.instanttouch.surround.dynamic.ui.swt.presenter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Scale;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.model.base.SurroundRange;

public class SurroundSwtRangePresenter extends SurroundSwtPresenter<SurroundRange> {

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Scale slider = new Scale((Composite) layoutContainer.getSwtContainer(), SWT.BORDER);
		slider.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		slider.setMinimum((int) getModel().getMinimum());
		slider.setMaximum((int) getModel().getMaximum());
		slider.setIncrement(1);
		slider.setPageIncrement(2);

		if (!getModel().isNull()) {
			slider.setSelection(getModel().asInt());
		}

		slider.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int selection = slider.getSelection();
				getModel().set(selection);
			}
		});
	}
}
