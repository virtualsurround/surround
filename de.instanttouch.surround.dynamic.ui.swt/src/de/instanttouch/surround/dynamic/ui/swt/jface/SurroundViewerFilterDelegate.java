package de.instanttouch.surround.dynamic.ui.swt.jface;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;

public class SurroundViewerFilterDelegate extends ViewerFilter {

	SurroundPresenter<?> presenter;

	public SurroundViewerFilterDelegate(SurroundPresenter<?> presenter) {
		super();
		this.presenter = presenter;
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {

		boolean ok = true;

		Object filter = presenter.getFilter();
		if (filter instanceof ViewerFilter) {
			ok = ((ViewerFilter) filter).select(viewer, parentElement, element);
		}

		return ok;
	}

}
