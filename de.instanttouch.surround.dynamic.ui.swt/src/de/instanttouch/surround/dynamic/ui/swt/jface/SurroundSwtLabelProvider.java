/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.jface;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageRegistry;
import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundSwtLabelProvider extends LabelProvider {

	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image getImage(Object element) {

		if (element instanceof SurroundBase) {
			return SurroundSwtImageRegistry.getInstance().getImage((SurroundBase) element);
		}
		return super.getImage(element);
	}

	@Override
	public String getText(Object element) {

		String label = "";
		if (element instanceof SurroundBase) {
			label = ((SurroundBase) element).getName();
		}

		return label;
	}
}
