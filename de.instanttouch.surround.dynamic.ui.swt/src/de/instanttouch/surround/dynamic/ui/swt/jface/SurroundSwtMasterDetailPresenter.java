/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.jface;

import java.util.Stack;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.WorkbenchPart;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.messaging.SurroundRefreshInfo;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtAction;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtOpenAction;
import de.instanttouch.surround.dynamic.ui.swt.dialog.SurroundSwtDialog;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtComposite;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtScrolledComposite;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtPresenter;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.base.ISurroundChangeListener;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.exception.SurroundBaseException;

public class SurroundSwtMasterDetailPresenter extends SurroundSwtPresenter<SurroundBase>
		implements ISurroundChangeListener {

	public class SurroundDetailPage extends Composite implements ISurroundChangeListener {

		private final StackLayout stackLayout;
		private SurroundSwtComposite composite;
		private SurroundBase currentSurroundBase = null;
		private SurroundPresenter<SurroundBase> detailPresenter;

		public SurroundDetailPage(Composite parent, int style) {
			super(parent, style);

			stackLayout = new StackLayout();
			stackLayout.topControl = null;

			setLayout(stackLayout);
		}

		public void onSelectionChanges(Object selected) {
			if (selected instanceof SurroundBase) {
				try {

					if (currentSurroundBase != null) {
						currentSurroundBase.removeChangeNotificationListener(this);
					}

					currentSurroundBase = (SurroundBase) selected;
					// TODO
					// currentSurroundBase.addChangeNotificationListener(this);

					Composite rootComposite = new SurroundSwtScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL);

					SurroundSwtComposite formComposite = new SurroundSwtComposite(rootComposite, SWT.NONE);
					((SurroundSwtScrolledComposite) rootComposite).setContent(formComposite);

					SurroundSwtParent layoutContainer = new SurroundSwtParent(formComposite);
					composite = layoutContainer.getSwtContainer();

					SurroundBase value = currentSurroundBase.getConstraints().getValue();
					SurroundBase detailsInput = value != null ? value : currentSurroundBase;

					detailPresenter = SurroundPresenterRegistry.getPresenter(detailsInput);
					connectDelegate(detailPresenter);

					detailPresenter.show(layoutContainer);

					stackLayout.topControl = rootComposite;
					layout();
				} catch (SurroundBaseException e) {
					SurroundSwtExceptionHandler.handle(e);
				}

			} else {
				if (currentSurroundBase != null) {
					currentSurroundBase.removeChangeNotificationListener(this);
					currentSurroundBase = null;
				}

				composite = null;
				stackLayout.topControl = null;
				layout();
			}

		}

		public SurroundBase getCurrentSurroundBase() {
			return currentSurroundBase;
		}

		@Override
		public void changed(SurroundBase type, SurroundChange<?> change) {
			if (composite != null) {
				composite.udpate(change);
			}
		}

		public SurroundSwtComposite getComposite() {
			return composite;
		}

		public void refresh() {
			if (detailPresenter != null) {
				detailPresenter.refresh();
			}
		}

	}

	protected WorkbenchPart part = null;
	private SurroundSwtTreePresenter surroundTree = null;
	private SurroundDetailPage detailsPage = null;
	protected int sashDirection = SWT.HORIZONTAL;
	private Composite toolBarComposite = null;

	public SurroundSwtMasterDetailPresenter() {
		super("masterDetailPresenter");
		init();
		sashDirection = SWT.HORIZONTAL;
	}

	private void init() {
	}

	public WorkbenchPart getPart() {
		return part;
	}

	public void setPart(WorkbenchPart part) {
		this.part = part;
	}

	public int getSashDirection() {
		return sashDirection;
	}

	public void setSashDirection(int sashDirection) {
		this.sashDirection = sashDirection;
	}

	public class SelectDetailsAction extends SurroundSwtAction {

		public SelectDetailsAction(String text) {
			super(text);
		}

		@Override
		public boolean play(IProgressInfoListener progressInfoListener) {
			detailsPage.onSelectionChanges(getModel());
			return true;
		}
	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Composite contentArea = (Composite) layoutContainer.getSwtContainer();

		// Composite toolComposite = new Composite(contentArea, SWT.NONE);
		// toolComposite.setLayout(new GridLayout(2, false));
		// createToolbar(toolComposite);

		SashForm sashForm = new SashForm(contentArea, sashDirection | SWT.NONE);

		sashForm.setLayout(new GridLayout());
		GridData gridData = new GridData(GridData.FILL_BOTH);
		sashForm.setLayoutData(gridData);

		surroundTree = new SurroundSwtTreePresenter();
		surroundTree.setModel(model);
		surroundTree.setFilter(getFilter());

		SurroundSwtParent treeContainer = new SurroundSwtParent(sashForm, SWT.BORDER);
		surroundTree.show(treeContainer);

		connectDelegate(surroundTree);

		detailsPage = new SurroundDetailPage(sashForm, SWT.BORDER);
		surroundTree.setSelectAction(new SelectDetailsAction("select detail"));
		surroundTree.setDoubleClickAction(new MyDoubleClickAction());
		surroundTree.getViewer().addTreeListener(new ITreeViewerListener() {

			@Override
			public void treeExpanded(TreeExpansionEvent arg0) {
			}

			@Override
			public void treeCollapsed(TreeExpansionEvent arg0) {
			}
		});

		SurroundViewerFilterDelegate filter = new SurroundViewerFilterDelegate(this);
		surroundTree.getViewer().setFilters(filter);

		int weights[] = { 30, 70 };
		sashForm.setWeights(weights);
	}

	private void createToolbar(Composite parent) {

		SurroundSwtParent toolContainer = new SurroundSwtParent(parent);

		Composite toolBarComposite = toolContainer.getSwtContainer();

		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);

		toolBarComposite.setLayoutData(gridData);
		toolBarComposite.setLayout(new GridLayout(2, false));

		Image showAllImage = SurroundSwtUtils.getImage("icons/cubes-icon.png");

		Button expand = new Button(toolBarComposite, SWT.PUSH);
		expand.setImage(showAllImage);
		expand.setToolTipText("expand whole tree");
		expand.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				surroundTree.getViewer().expandAll();
			}
		});

		FormToolkit toolkit = new FormToolkit(toolBarComposite.getDisplay());
		toolkit.adapt(toolBarComposite);
	}

	public class MyDoubleClickAction extends SurroundSwtOpenAction {

		public MyDoubleClickAction() {
			super("open deiails");
		}

		@Override
		public boolean play(IProgressInfoListener progressListener) {

			SurroundSwtDialog dialog = new SurroundSwtDialog(null, getModel());
			int open = dialog.open();
			if (open == Dialog.OK) {

				refresh();

				if (getChannel() != null) {
					SurroundRefreshInfo refreshInfo = new SurroundRefreshInfo();
					SurroundMessage refreshMessage = createMessage(refreshInfo,
							SurroundSwtMasterDetailPresenter.this.getReference());

					superVision(refreshMessage);
				}

				return true;
			}

			return false;
		}
	}

	@Override
	public void refresh() {
		if (surroundTree != null) {
			ITreeSelection selection = (ITreeSelection) surroundTree.getViewer().getSelection();

			surroundTree.refresh();

			Object input = surroundTree.getViewer().getInput();
			surroundTree.getViewer().setInput(input);
			surroundTree.getViewer().setSelection(selection);

			getDetailsPage().refresh();
		}
	}

	@Override
	public void changed(SurroundBase type, SurroundChange<?> change) {
		// TODO Auto-generated method stub

	}

	public void changeInput(SurroundBase newInput, SurroundBase detail) {

		surroundTree.getViewer().setInput(model);

		if (detail != null) {
			Stack<SurroundBase> segmentStack = new Stack<SurroundBase>();

			segmentStack.add(detail);
			SurroundBase pathElement = detail;

			// TODO restore selection
			// while (!pathElement.equals(newInput) && pathElement != null) {
			// pathElement = pathElement.getParent();
			// if (pathElement != null) {
			// segmentStack.push(pathElement);
			// }
			// }
			//
			// Object[] segments = new Object[segmentStack.size()];
			// for (int i = 0; i < segments.length; i++) {
			// segments[i] = segmentStack.pop();
			// }
			// TreePath treePath = new TreePath(segments);
			//
			// TreeSelection selection = new TreeSelection(treePath);
			// getSurroundTree().getViewer().setSelection(selection);

		} else {
			detailsPage.onSelectionChanges(null);
		}
	}

	public SurroundSwtTreePresenter getSurroundTree() {
		return surroundTree;
	}

	public SurroundDetailPage getDetailsPage() {
		return detailsPage;
	}

	public Composite getToolBarComposite() {
		return toolBarComposite;
	}

	public SurroundBase getCurrentSelected() {
		return detailsPage.getCurrentSurroundBase();
	}
}
