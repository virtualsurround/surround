package de.instanttouch.surround.dynamic.ui.swt.view;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtToolbar;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.util.SurroundLog;

public abstract class SurroundSwtSelectionBasedView extends SurroundSwtView {

	protected abstract SurroundBase getModel4Selection(SurroundBase selected);

	private SurroundBase model;
	private SurroundSwtParent dataContainer;
	private StackLayout stackLayout;

	public SurroundSwtSelectionBasedView() {
		super();
	}

	@Override
	public SurroundBase getModel() {
		return model;
	}

	@Override
	public void createPartControl(Composite parent) {

		try {

			dataContainer = new SurroundSwtParent(parent);

			stackLayout = new StackLayout();
			stackLayout.topControl = null;

			dataContainer.getSwtContainer().setLayout(stackLayout);

		} catch (Exception e) {
			SurroundLog.log(e);
		}

		ISelectionListener selectionListener = new ISelectionListener() {

			@Override
			public void selectionChanged(IWorkbenchPart part, ISelection selection) {

				if(dataContainer.getSwtContainer().isDisposed()){
					return;
				}
				
				if (selection instanceof IStructuredSelection) {
					Object selected = ((IStructuredSelection) selection).getFirstElement();
					if (selected instanceof SurroundBase) {
						model = getModel4Selection((SurroundBase) selected);
						createPresenter();
					}
				}

			}
		};
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(selectionListener);
	}

	protected void createPresenter() {

		SurroundBase model = getModel();
		if (model != null) {
			
			SurroundSwtParent selectionContainer = new SurroundSwtParent(dataContainer.getSwtContainer());
			
			SurroundPresenter<SurroundBase> presenter = SurroundPresenterRegistry.getPresenter(model);
			presenter.show(selectionContainer);

			viewNode.connectDelegate(presenter);

			stackLayout.topControl = selectionContainer.getSwtContainer();
		} else {
			stackLayout.topControl = null;
		}
		dataContainer.layout();
	}

}
