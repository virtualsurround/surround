package de.instanttouch.surround.dynamic.ui.swt.jface;

import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;

import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtOpenAction;
import de.instanttouch.surround.dynamic.ui.swt.editor.SurroundEditorNode.ISurroundEditor;
import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageRegistry;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtScrolledComposite;
import de.instanttouch.surround.model.base.ISurroundChangeListener;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundChange;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundSwtOutline extends ContentOutlinePage {

	private SurroundSwtScrolledComposite treeComposite;
	private TreeViewer outlineTree;
	private SurroundDynamicEntity data;
	private IContentProvider contentProvider = new SurroundSwtTreePresenter.SurroundSwtTreeContentProvider();
	private IBaseLabelProvider labelProvider = new SurroundSwtLabelProvider();
	private boolean expandAll = false;
	private ISurroundEditor editor;

	public SurroundSwtOutline(SurroundDynamicEntity data, ISurroundEditor editor) {
		this.data = data;
		this.editor = editor;
	}

	public IContentProvider getContentProvider() {
		return contentProvider;
	}

	public void setContentProvider(IContentProvider contentProvider) {
		this.contentProvider = contentProvider;
	}

	public IBaseLabelProvider getLabelProvider() {
		return labelProvider;
	}

	public void setLabelProvider(IBaseLabelProvider labelProvider) {
		this.labelProvider = labelProvider;
	}

	public boolean isExpandAll() {
		return expandAll;
	}

	public void setExpandAll(boolean expandAll) {
		this.expandAll = expandAll;
	}

	@Override
	public void createControl(Composite parent) {

		super.createControl(parent);

		createToolbar(parent);

		outlineTree = getTreeViewer();
		outlineTree.setLabelProvider(labelProvider);
		outlineTree.setContentProvider(contentProvider);

		outlineTree.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				ISelection selection = getSelection();
				if (selection instanceof ITreeSelection) {
					ITreeSelection treeSelection = (ITreeSelection) selection;
					Object selected = treeSelection.getFirstElement();
					if (selected instanceof SurroundBase) {

						SurroundBase surroundType = (SurroundBase) selected;

						SurroundSwtOpenAction openAction = new SurroundSwtOpenAction("open");
						openAction.setModel(surroundType);
						openAction.play(null);

						outlineTree.refresh();
						if (editor != null) {
							editor.markDirty(true);
						}
					}
				}
			}
		});
		outlineTree.setAutoExpandLevel(5);
		if (isExpandAll()) {
			outlineTree.expandAll();
		}

		final SurroundDynamicEntity root = new SurroundDynamicEntity("root");
		root.add(data);
		getSite().setSelectionProvider(new ISelectionProvider() {

			@Override
			public void setSelection(ISelection selection) {
				// TODO Auto-generated method stub
			}

			@Override
			public void removeSelectionChangedListener(ISelectionChangedListener listener) {
				// TODO Auto-generated method stub
			}

			@Override
			public ISelection getSelection() {
				ISelection selection = new StructuredSelection(root);
				return selection;
			}

			@Override
			public void addSelectionChangedListener(ISelectionChangedListener listener) {
				// TODO Auto-generated method stub
			}
		});

		outlineTree.setInput(root);
		outlineTree.refresh();

		data.addChangeNotificationListener(new ISurroundChangeListener() {

			@Override
			public void changed(SurroundBase type, SurroundChange<?> change) {

				if (!outlineTree.getControl().isDisposed()) {
					outlineTree.getControl().getDisplay().asyncExec(new Runnable() {

						@Override
						public void run() {
							if (!outlineTree.getControl().isDisposed()) {
								outlineTree.refresh();
							}
						}
					});
				}
			}
		});
	}

	private void createToolbar(Composite parent) {

		SurroundSwtParent toolContainer = new SurroundSwtParent(parent);

		Composite toolBarComposite = toolContainer.getSwtContainer();
		toolBarComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		toolBarComposite.setLayout(new GridLayout(5, false));

		Image showAllImage = SurroundSwtImageRegistry.getInstance().getImage("icons/cubes-icon.png");

		Button expand = new Button(toolBarComposite, SWT.PUSH);
		expand.setImage(showAllImage);
		expand.setToolTipText("expand whole tree");
		expand.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				outlineTree.expandAll();
				treeComposite.updateSize();
			}
		});
	}

	public TreeViewer getOutlineTree() {
		return outlineTree;
	}

	public void setAutoExpandLevel(int level) {
		// TODO Auto-generated method stub

	}

}
