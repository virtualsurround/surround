package de.instanttouch.surround.dynamic.ui.swt.presenter;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.swt.dialog.SurroundSwtDialog;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtMasterDetailPresenter;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtUtils;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtComposite;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.collection.SurroundOptions;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundSwtMapPresenter extends SurroundSwtPresenter<SurroundMap<SurroundBase, SurroundBase>> {

	private SurroundSwtComposite parent;
	private SurroundSwtMasterDetailPresenter masterDetailView;

	public SurroundSwtMapPresenter() {
		// TODO Auto-generated constructor stub
	}

	public SurroundSwtMapPresenter(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Object obj = layoutContainer.getSwtContainer();
		if (!(obj instanceof Composite)) {
			throw new SurroundWrongTypeException("invalid layout container: " + obj + " composite expected");
		}
		parent = (SurroundSwtComposite) obj;

		Composite toolbarComposite = new Composite(parent, SWT.NONE);
		toolbarComposite.setLayout(new GridLayout(3, false));
		toolbarComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		if (model.getConstraints().isEnabled()) {

			Label label = new Label(toolbarComposite, SWT.NONE);
			label.setText(getModel().getName());

			Button configure = new Button(toolbarComposite, SWT.PUSH);

			String path = "icons/add.png";
			Image image = SurroundSwtUtils.getImage(path);
			configure.setImage(image);
			configure.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					add();
				}
			});

			Button delete = new Button(toolbarComposite, SWT.PUSH);

			path = "icons/delete.png";
			image = SurroundSwtUtils.getImage(path);
			delete.setImage(image);
			delete.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					delete();
				}

			});
		}
		masterDetailView = new SurroundSwtMasterDetailPresenter();
		masterDetailView.setModel(getModel());

		SurroundSwtParent masterContainer = new SurroundSwtParent(parent);
		masterDetailView.show(masterContainer);

		connectDelegate(masterDetailView);

		parent.adaptToolkit();
	}

	protected void delete() {
		SurroundBase surroundBase = masterDetailView.getCurrentSelected();

		SurroundString key = new SurroundString("key").set(surroundBase.getName());

		if (surroundBase != null) {
			getModel().remove(key);
			masterDetailView.refresh();
		}
	}

	protected void add() {

		try {

			SurroundBase key = null;
			SurroundBase value = null;

			SurroundMap<SurroundBase, SurroundBase> model = getModel();

			ISurroundFactory<SurroundBase> keyCreator = model.keyFactory();
			if (keyCreator instanceof SurroundOptions<?>) {
				key = (SurroundOptions<?>) keyCreator;
			} else {
				key = keyCreator.create();
			}
			key.setName("key");

			ISurroundFactory<SurroundBase> valueCreator = model.valueFactory();
			if (valueCreator instanceof SurroundOptions<?>) {
				value = (SurroundOptions<?>) valueCreator;

			} else {
				value = keyCreator.create();
			}
			value.setName("value");

			SurroundDynamicEntity mapEntry = new SurroundDynamicEntity();
			mapEntry.add(key).add(value);

			SurroundSwtMasterDetailPresenter presenter = new SurroundSwtMasterDetailPresenter();
			connectDelegate(presenter);

			SurroundSwtDialog dialog = new SurroundSwtDialog(parent.getShell(), mapEntry, presenter, 400, 200);
			if (dialog.open() == Dialog.OK) {

				if (value instanceof SurroundOptions<?>) {
					SurroundOptions<? extends SurroundBase> options = (SurroundOptions<? extends SurroundBase>) value;
					List<? extends SurroundBase> selected = options.getSelected();

					if (selected.size() == 1) {
						value = selected.get(0);

						if (value instanceof SurroundClassInfo) {
							SurroundClassInfo classInfo = (SurroundClassInfo) value;
							value = SurroundRuntimeTools.createInstanceFor(classInfo);
						}
					}
				}

				model.put(key, value);
				masterDetailView.refresh();
			}

		} catch (

		Exception e) {
			SurroundSwtExceptionHandler.handle(e);
		}

	}
}
