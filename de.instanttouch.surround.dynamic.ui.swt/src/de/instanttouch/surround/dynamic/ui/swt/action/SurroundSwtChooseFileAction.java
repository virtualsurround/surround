/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.action;

import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.ref.SurroundFile;

public class SurroundSwtChooseFileAction extends SurroundSwtAction {

	protected Shell shell;

	public SurroundSwtChooseFileAction(Shell shell, String text) {
		super(text);
		this.shell = shell;
	}

	@Override
	public boolean play(IProgressInfoListener progressListener) {

		FileDialog dialog = new FileDialog(shell);
		dialog.setText("select file");
		dialog.setFileName(getModel().toString());

		String newPath = dialog.open();
		if (newPath != null) {
			try {
				if (!(getModel() instanceof SurroundFile)) {
					throw new SurroundWrongTypeException("wrong typw: " + getModel().getClass().getCanonicalName()
							+ "only SurroundFile is supported");
				}

				getModel().set(newPath);

				return true;
			} catch (SurroundWrongTypeException e) {
				SurroundSwtExceptionHandler.handle(e);
			}
		}

		return false;
	}

}
