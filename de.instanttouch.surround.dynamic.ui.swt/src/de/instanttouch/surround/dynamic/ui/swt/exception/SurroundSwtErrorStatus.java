/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.StringTokenizer;
import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class SurroundSwtErrorStatus extends Status {

	@Override
	public boolean isMultiStatus() {
		// TODO Auto-generated method stub
		return true;
	}

	public SurroundSwtErrorStatus(int severity, String pluginId, int code, String message, Throwable exception) {
		super(severity, pluginId, code, message, exception);
	}

	@Override
	public IStatus[] getChildren() {

		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);

		Throwable ex = getException();
		ex.printStackTrace(printWriter);

		String detailMessage = stringWriter.getBuffer().toString();
		detailMessage = detailMessage.replaceAll("\r", "\n");

		Vector<IStatus> colStatus = new Vector<IStatus>();

		StringTokenizer tokenizer = new StringTokenizer(detailMessage, "\n");
		int i = 1;
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			token = token.replaceAll("\t", "");
			Status singleStatus = new Status(getSeverity(), getPlugin(), i, token, null);
			colStatus.add(singleStatus);
			i++;
		}

		IStatus[] statusArray = new Status[colStatus.size()];
		for (int j = 0; j < colStatus.size(); j++) {
			statusArray[j] = colStatus.get(j);
		}

		return statusArray;
	}
}
