/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.layout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundSwtScrolledComposite extends ScrolledComposite {

	public SurroundSwtScrolledComposite(Composite parent, int style) {
		super(parent, style | SWT.H_SCROLL | SWT.V_SCROLL);
		addControlListener(new ControlAdapter() {

			@Override
			public void controlResized(ControlEvent e) {
				update();
			}
		});
		setExpandHorizontal(true);
		setExpandVertical(true);

		setLayout(new GridLayout());
		setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	public void updateSize() {
		Point size = getContent() != null ? getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT) : new Point(0, 0);
		setMinSize(size);
	}

	@Override
	public void setContent(Control content) {
		super.setContent(content);

		content.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent arg0) {
				updateSize();
			}

			@Override
			public void controlMoved(ControlEvent arg0) {
				updateSize();
			}
		});
	}

	public void adaptToolkit() {

		if (SurroundRuntimeTools.isEclipseRunning()) {

			FormToolkit toolkit = new FormToolkit(Display.getCurrent());
			toolkit.adapt(this);

			for (Control control : getChildren()) {
				if (control instanceof SurroundSwtComposite) {
					SurroundSwtComposite child = (SurroundSwtComposite) control;
					child.adaptToolkit();
				} else {
					toolkit.adapt(control, false, false);
				}
			}
		}
	}

}
