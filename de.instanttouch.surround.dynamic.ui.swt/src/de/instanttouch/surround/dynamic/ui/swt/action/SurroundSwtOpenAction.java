/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.action;

import org.eclipse.jface.dialogs.Dialog;

import de.instanttouch.surround.dynamic.ui.swt.dialog.SurroundSwtDialog;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;

public class SurroundSwtOpenAction extends SurroundSwtAction {

	protected boolean allowOk = true;
	protected boolean needsValidate = false;

	public SurroundSwtOpenAction(String text) {
		super(text);
		setThreadType(UI_THREAD);
	}

	@Override
	public boolean play(IProgressInfoListener progressListener) {
		try {

			SurroundSwtDialog dialog = new SurroundSwtDialog(null, getModel());
			if (Dialog.OK == dialog.open()) {
				return true;
			}

		} catch (Exception e) {
			SurroundSwtExceptionHandler.handle(e);
		}

		return false;
	}

	public boolean isAllowOk() {
		return allowOk;
	}

	public void setAllowOk(boolean allowOk) {
		this.allowOk = allowOk;
	}

	public boolean isNeedsValidate() {
		return needsValidate;
	}

	public void setNeedsValidate(boolean needsValidate) {
		this.needsValidate = needsValidate;
	}

}
