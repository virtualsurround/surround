package de.instanttouch.surround.dynamic.ui.swt.presenter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.model.base.SurroundBoolean;

public class SurroundSwtBooleanPresenter extends SurroundSwtPresenter<SurroundBoolean> {

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Composite composite = (Composite) layoutContainer.getSwtContainer();

		Composite childComposite = new Composite(composite, SWT.NONE);
		childComposite.setLayout(new org.eclipse.swt.layout.GridLayout(2, false));

		FormToolkit toolkit = new FormToolkit(Display.getCurrent());
		toolkit.adapt(childComposite);

		Label label = new Label(childComposite, SWT.NONE);
		label.setText(model.getName());

		GridData gridData = new GridData(150, 25);
		label.setLayoutData(gridData);
		toolkit.adapt(label, false, false);

		Button checkBox = new Button(childComposite, SWT.CHECK);
		checkBox.setSelection(getModel().asBoolean());
		checkBox.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				getModel().set(checkBox.getSelection());
			}
		});
	}

}
