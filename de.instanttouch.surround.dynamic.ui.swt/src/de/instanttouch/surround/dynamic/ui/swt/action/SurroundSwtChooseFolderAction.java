/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.action;

import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;

import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.ref.SurroundDirectory;

public class SurroundSwtChooseFolderAction extends SurroundSwtAction {

	protected Shell shell;

	public SurroundSwtChooseFolderAction(Shell shell, String text) {
		super(text);
		this.shell = shell;
	}

	@Override
	public boolean play(IProgressInfoListener progressListener) {

		DirectoryDialog dialog = new DirectoryDialog(shell);
		dialog.setText("select directory");
		dialog.setFilterPath(getModel().toString());

		String newDir = dialog.open();
		if (newDir != null) {
			try {
				if (!(getModel() instanceof SurroundDirectory)) {
					throw new SurroundWrongTypeException("wrong typw: " + getModel().getClass().getCanonicalName()
							+ "only SurroundFolder is supported");
				}

				getModel().set(newDir);

				return true;
			} catch (SurroundWrongTypeException e) {
				SurroundSwtExceptionHandler.handle(e);
			}
		}

		return false;
	}
}
