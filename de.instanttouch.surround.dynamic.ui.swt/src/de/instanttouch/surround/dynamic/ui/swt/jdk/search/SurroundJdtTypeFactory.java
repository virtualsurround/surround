package de.instanttouch.surround.dynamic.ui.swt.jdk.search;

import java.util.List;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBase.ISurroundFactory;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundOptions;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

@SuppressWarnings("serial")
public class SurroundJdtTypeFactory<T extends SurroundBase> extends SurroundOptions<SurroundJdtClassInfo<T>>
		implements ISurroundFactory<T> {

	protected SurroundList<SurroundJdtClassInfo<T>> types;

	public SurroundJdtTypeFactory() {
		super();
	}

	public SurroundJdtTypeFactory(String name) {
		super(name);
	}

	public SurroundJdtTypeFactory<T> search(Class<T> typeClass) {

		SurroundJdtTypeSearch<T> search = new SurroundJdtTypeSearch<T>();

		try {
			types = search.findSurroundTypes(typeClass);
		} catch (Exception e) {
			throw new SurroundBaseException(e.getMessage());
		}
		return this;
	}

	@Override
	public T create() throws SurroundBaseException, InstantiationException, IllegalAccessException {

		T surroundType = null;

		List<SurroundJdtClassInfo<T>> selected = getSelected();

		if (selected.size() == 1) {
			SurroundJdtClassInfo<T> jdtClassInfo = selected.get(0);
			surroundType = SurroundRuntimeTools.createInstanceFor(jdtClassInfo);

		} else {
			throw new SurroundBaseException("select one type");
		}

		return surroundType;
	}
}
