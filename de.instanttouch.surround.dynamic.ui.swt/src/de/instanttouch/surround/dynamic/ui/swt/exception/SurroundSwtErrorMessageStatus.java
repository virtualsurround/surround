/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.exception;

import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class SurroundSwtErrorMessageStatus extends Status {

	protected Vector<String> messages;

	public SurroundSwtErrorMessageStatus(Vector<String> messages, String title, int severity, String pluginId,
			int code) {
		super(severity, pluginId, code, title, null);
		this.messages = messages;
	}

	@Override
	public boolean isMultiStatus() {
		return true;
	}

	@Override
	public IStatus[] getChildren() {

		IStatus[] statusArray = new Status[messages.size()];

		for (int i = 0; i < messages.size(); i++) {
			String message = messages.get(i);
			Status singleStatus = new Status(getSeverity(), "my plugin id", i, message, null);
			statusArray[i] = singleStatus;
		}

		return statusArray;
	}

}
