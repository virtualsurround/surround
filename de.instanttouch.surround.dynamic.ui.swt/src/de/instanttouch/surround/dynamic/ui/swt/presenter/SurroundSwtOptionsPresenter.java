package de.instanttouch.surround.dynamic.ui.swt.presenter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtComposite;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.collection.SurroundOptions;

public class SurroundSwtOptionsPresenter<T extends SurroundBase> extends SurroundSwtPresenter<SurroundOptions<T>> {

	public SurroundSwtOptionsPresenter() {
		initOptionsPresenter();
	}

	public SurroundSwtOptionsPresenter(String name) {
		super(name);
		initOptionsPresenter();
	}

	private void initOptionsPresenter() {

	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		SurroundSwtComposite parent = (SurroundSwtComposite) layoutContainer.getSwtContainer();
		SurroundOptions<T> surroundOptions = getModel();

		Combo combo = new Combo(parent, SWT.NONE);
		combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		for (T element : surroundOptions.getChildren()) {
			String title = surroundOptions.getTitle(element);
			combo.add(title);
		}
		combo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int selected = combo.getSelectionIndex();
				if (selected >= 0 && selected < surroundOptions.size()) {
					surroundOptions.clearSelection();
					surroundOptions.setSelected(surroundOptions.get(selected));
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

	}

}
