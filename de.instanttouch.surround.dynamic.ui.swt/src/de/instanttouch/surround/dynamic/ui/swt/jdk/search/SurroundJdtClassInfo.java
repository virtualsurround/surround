package de.instanttouch.surround.dynamic.ui.swt.jdk.search;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.internal.core.BinaryType;
import org.eclipse.jdt.internal.core.ExternalPackageFragmentRoot;
import org.eclipse.jdt.internal.core.JarPackageFragmentRoot;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

@SuppressWarnings("serial")
public class SurroundJdtClassInfo<T extends SurroundBase> extends SurroundClassInfo {

	public SurroundJdtClassInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SurroundJdtClassInfo(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public void createFrom(BinaryType binaryType) throws SurroundWrongTypeException {
		String className = binaryType.getFullyQualifiedName();
		findByName("className").set(className);

		ITypeRoot typeRoot = binaryType.getTypeRoot();

		IPackageFragment packageFragment = binaryType.getPackageFragment();
		IJavaElement parent = packageFragment.getParent();
		if (parent instanceof ExternalPackageFragmentRoot) {

			ExternalPackageFragmentRoot root = (ExternalPackageFragmentRoot) parent;
			String path = root.getPath().toString();

			int end = path.lastIndexOf("/");
			int start = path.lastIndexOf("/", end - 1);

			String bundleName = path.substring(++start, end);
			findByName("bundleName").set(bundleName);
		} else if (parent instanceof JarPackageFragmentRoot) {

			JarPackageFragmentRoot root = (JarPackageFragmentRoot) parent;
			String path = root.getPath().toString();

			int start = path.lastIndexOf("plugins/");
			String bundle = path.substring(start + "plugins/".length());
			int end = bundle.indexOf("_");

			String bundleName = bundle.substring(0, end);
			findByName("bundleName").set(bundleName);
		}
	}
}
