/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenterRegistry;
import de.instanttouch.surround.dynamic.ui.swt.Activator;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtLinkAction;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtToolbar;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.ref.SurroundFile;
import de.instanttouch.surround.model.url.SurroundURL;
import de.instanttouch.surround.model.util.SurroundLog;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundSwtDialog extends Dialog {

	protected SurroundBase model = null;
	boolean enabled = true;

	protected Point initialSize = null;
	protected String expression = "";
	protected SurroundPresenter<SurroundBase> presenter;
	protected SurroundActor dialogNode = new SurroundActor("dialogNode");
	protected int width = 0;
	protected int height = 0;
	protected boolean withToolbar = false;

	public SurroundSwtDialog(Shell parentShell, SurroundBase model) throws SurroundWrongTypeException {
		super(parentShell);
		this.model = model;
		this.width = 850;
		this.height = 550;
	}

	public SurroundSwtDialog(Shell parentShell, SurroundBase model, SurroundPresenter<SurroundBase> presenter,
			int width, int height) throws SurroundWrongTypeException {
		super(parentShell);
		this.model = model;
		this.presenter = presenter;
		this.presenter.setModel(model);
		this.width = width;
		this.height = height;
	}

	public boolean isWithToolbar() {
		return withToolbar;
	}

	public void setWithToolbar(boolean withToolbar) {
		this.withToolbar = withToolbar;
	}

	public SurroundActor getDialogNode() {
		return dialogNode;
	}

	public SurroundBase getModel() {
		return model;
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		SurroundSwtParent layoutContainer = new SurroundSwtParent(parent);
		SurroundPresenter<?> presenter = SurroundPresenterRegistry.getPresenter(model);

		if (withToolbar) {
			SurroundSwtToolbar toolbar = new SurroundSwtToolbar(layoutContainer.getSwtContainer());
			presenter.setFilter(toolbar.getViewerFilter());
			toolbar.setContentPresenter(presenter);
		}

		presenter.show(layoutContainer);

		dialogNode.connectDelegate(presenter);
		layoutContainer.getSwtContainer().adaptToolkit();

		return layoutContainer.getSwtContainer();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);

		parent.setLayout(new GridLayout(6, false));

		Button okButton = getButton(IDialogConstants.OK_ID);
		if (okButton != null && !isEnabled()) {
			okButton.setVisible(false);
		}

		if (false) {
			Button saveButton = new Button(parent, SWT.NONE);
			saveButton.setText("save");
			saveButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					try {
						save();
					} catch (SurroundWrongTypeException e) {
						SurroundSwtExceptionHandler.handle(e);
					}
				}
			});
		}

		if (parent instanceof Composite && model.getConstraints().hasWiki()) {

			Button helpButton = new Button(parent, SWT.NONE);
			helpButton.setImage(Activator.getImage("icons/help-icon.png"));
			helpButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					try {
						handleHelp();
					} catch (SurroundWrongTypeException e1) {
						SurroundSwtExceptionHandler.handle(e1);
					}
				}

			});
		}

		if (SurroundRuntimeTools.isEclipseRunning()) {

			FormToolkit toolkit = new FormToolkit(parent.getDisplay());
			toolkit.adapt(parent);
		}

	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control control = super.createButtonBar(parent);

		if (SurroundRuntimeTools.isEclipseRunning()) {
			FormToolkit toolkit = new FormToolkit(parent.getDisplay());
			toolkit.adapt(parent);
		}

		return control;
	}

	protected void save() {

		SurroundFile file = new SurroundFile();

		FileDialog dialog = new FileDialog(getShell());
		dialog.setText("select file");
		dialog.setFileName(model.getName() + ".surround");

		String newPath = dialog.open();
		if (newPath != null) {
			try {
				file.set(newPath);
				model.save(file);
			} catch (Exception e) {
				SurroundLog.log(e);
			}
		}
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
	}

	@Override
	protected void okPressed() {
		super.okPressed();
	}

	@Override
	public boolean close() {

		return super.close();
	}

	public void setInitialSize(Point initialSize) {
		this.initialSize = initialSize;
	}

	@Override
	protected Point getInitialSize() {
		if (initialSize != null) {
			return initialSize;
		} else {
			return super.getInitialSize();
		}
	}

	public SurroundBase getType() {
		return model;
	}

	public String getExpression() {
		return expression;
	}

	protected void handleHelp() throws SurroundWrongTypeException {

		SurroundBase myType = getType();
		if (!myType.getConstraints().hasWiki()) {
			MessageDialog.openInformation(getShell(), "No Docu", "link to wiki is mising");
		} else {
			SurroundURL link = myType.getConstraints().getWiki();
			link.setExternal(true);

			SurroundSwtLinkAction action = new SurroundSwtLinkAction("wiki");
			action.setModel(link);

			SurroundPlayer player = new SurroundPlayer();
			player.play(action);
		}
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}

	@Override
	protected Control createContents(Composite parent) {

		Control control = super.createContents(parent);

		if (width > 0 && height > 0) {
			parent.getShell().setSize(width, height);
		} else {
			parent.getShell().setSize(500, 400);
		}
		return control;
	}

}
