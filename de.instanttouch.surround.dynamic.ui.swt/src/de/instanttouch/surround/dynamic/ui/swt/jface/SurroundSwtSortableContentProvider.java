/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.jface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;

import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundSwtSortableContentProvider implements IStructuredContentProvider {

	static enum eOrder {
		ASCENDING, DESCENDING
	}

	public class Sorting {

		String column = "";
		eOrder order;

	}

	protected Sorting sorting = new Sorting();

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object[] getElements(Object inputElement) {

		Object[] elements = null;

		if (inputElement instanceof SurroundDynamicEntity) {

			SurroundDynamicEntity asStruct = (SurroundDynamicEntity) inputElement;
			elements = asStruct.getChildren().toArray();

		} else if (inputElement instanceof SurroundList) {

			SurroundList asList = (SurroundList) inputElement;

			if (sorting.column != null && sorting.column.length() > 0 && asList.size() > 0) {

				try {
					SurroundBase child = asList.newElement();

					int index = 0;
					for (SurroundBase type : child.getChildren()) {
						if (type.getName().equals(sorting.column)) {
							break;
						}
						index++;
					}

					List<SurroundBase> list = new ArrayList<SurroundBase>();
					list.addAll(asList.getChildren());

					Comparator<SurroundBase> compartor = asList.comparator(index, sorting.order == eOrder.ASCENDING);
					Collections.sort(list, compartor);

					elements = list.toArray();
				} catch (Exception e) {
					SurroundSwtExceptionHandler.handle(e);
				}
			} else {
				elements = asList.getChildren().toArray();
			}

		} else if (inputElement instanceof SurroundMap) {
			SurroundMap asMap = (SurroundMap) inputElement;
			elements = asMap.getChildren().toArray();
		}

		return elements;
	}

	public void sortBy(String name, int swtSortDirection) {
		sorting.column = name;
		sorting.order = swtSortDirection == SWT.UP ? eOrder.ASCENDING : eOrder.DESCENDING;
	}

	public void noSort() {
		sorting.column = "";
	}
}
