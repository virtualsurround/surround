/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.jface;

import java.util.List;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.messaging.SurroundRefreshInfo;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtAction;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtOpenAction;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageRegistry;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtPresenter;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.action.SurroundActionRegistry;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.collection.SurroundMap.SurroundMapEntry;
import de.instanttouch.surround.model.composite.SurroundEntity;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;

public class SurroundSwtTablePresenter extends SurroundSwtPresenter<SurroundBase> {

	public static class SurroundSwtTableViewer extends TableViewer {

		public SurroundSwtTableViewer(Composite parent, int style) {
			super(parent, style | SWT.FULL_SELECTION);

			initTableViewer();
		}

		private void initTableViewer() {
			setLabelProvider(new ITableLabelProvider() {

				@Override
				public void removeListener(ILabelProviderListener listener) {
				}

				@Override
				public boolean isLabelProperty(Object element, String property) {
					return false;
				}

				@Override
				public void dispose() {

				}

				@Override
				public void addListener(ILabelProviderListener listener) {

				}

				@Override
				public Image getColumnImage(Object element, int columnIndex) {
					if (element instanceof SurroundBase && columnIndex == 0) {
						return SurroundSwtImageRegistry.getInstance().getImage((SurroundBase) element);
					}
					return null;
				}

				@Override
				public String getColumnText(Object element, int columnIndex) {
					String label = "";
					if (element instanceof SurroundMapEntry) {
						SurroundMapEntry helper = (SurroundMapEntry) element;

						switch (columnIndex) {
						case 1:
							label = helper.getKey().toString();
							break;
						case 2:
							label = helper.getValue().toString();
							break;
						}

						return label;
					} else if (element instanceof SurroundEntity) {
						SurroundEntity entity = (SurroundEntity) element;

						List<SurroundBase> visible = entity.getVisibleChildren();

						if (columnIndex > 0 && columnIndex <= visible.size()) {
							SurroundBase child = visible.get(columnIndex - 1);
							if (child instanceof SurroundString) {
								SurroundString string = (SurroundString) child;
								label = string.getConstraints().isPassword() ? "****" : string.toString();
							} else {
								label = child.toString();
							}
						}
					}

					else if (element instanceof SurroundString) {
						SurroundString string = (SurroundString) element;
						switch (columnIndex) {
						case 1:
							label = string.getConstraints().isPassword() ? "****" : string.toString();
							break;
						}

					} else if (element instanceof SurroundBase) {
						SurroundBase type = (SurroundBase) element;
						switch (columnIndex) {
						case 1:
							label = type.toString();
							break;
						}
					}

					return label;
				}
			});
		}
	}

	protected SurroundString pattern = null;

	protected SurroundSwtAction doubleClickAction = new SurroundSwtOpenAction("open type dialog");
	protected SurroundSwtAction selectAction = null;
	private IContentProvider contentProvider = null;

	private SurroundSwtTableViewer tableViewer;

	public SurroundSwtTablePresenter() throws SurroundBaseException {
		super("table presenter");
	}

	public SurroundSwtTableViewer getTableViewer() {
		return tableViewer;
	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Composite composite = (Composite) layoutContainer.getSwtContainer();

		tableViewer = new SurroundSwtTableViewer(composite, SWT.BORDER);

		Table table = tableViewer.getTable();

		if (getModel() instanceof SurroundMap) {
			TableColumn column = new TableColumn(table, SWT.NONE);
			column.setWidth(20);
			column.setText("");
			column.setResizable(false);

			column = new TableColumn(table, SWT.NONE);
			column.setWidth(100);
			column.setText("key");
			column.setResizable(true);

			column = new TableColumn(table, SWT.NONE);
			column.setWidth(300);
			column.setText("value");
			column.setResizable(true);

		} else if (getModel() instanceof SurroundList) {
			SurroundBase innerType;
			try {
				innerType = ((SurroundList) getModel()).newElement();

			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e2) {
				throw new SurroundBaseException(e2.getMessage());
			}

			TableColumn column = new TableColumn(table, SWT.NONE);
			column.setWidth(20);
			column.setText("");
			column.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					if (e.getSource() instanceof TableColumn) {
						try {
							sort((TableColumn) e.getSource());
						} catch (SurroundBaseException e1) {
							SurroundSwtExceptionHandler.handle(e1);
						}
					}
				}

			});

			if (innerType.handleAsBasic()) {
				column = new TableColumn(table, SWT.NONE);
				column.setWidth(300);
				column.setText(innerType.getName());
				column.setResizable(true);

			} else if (innerType instanceof SurroundEntity) {
				SurroundEntity entity = (SurroundEntity) innerType;

				for (SurroundBase child : entity.getVisibleChildren()) {
					column = new TableColumn(table, SWT.NONE);

					double width = child.getConstraints().hasWidth() ? child.getConstraints().getWidth() : 100;
					Double convert = new Double(width);
					column.setWidth(convert.intValue());
					column.setResizable(true);

					column.setText(child.getName());
					column.addSelectionListener(new SelectionAdapter() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							if (e.getSource() instanceof TableColumn) {
								try {
									sort((TableColumn) e.getSource());
								} catch (SurroundBaseException e1) {
									SurroundSwtExceptionHandler.handle(e1);
								}
							}
						}

					});

					if (child instanceof SurroundString) {
						SurroundString asString = (SurroundString) child;
						if (asString.getConstraints().isLarge()) {
							column.setWidth(250);
						}

					}

				}
			}

		}

		GridData layoutData = new GridData(GridData.FILL_BOTH);
		table.setLayoutData(layoutData);

		contentProvider = new SurroundSwtSortableContentProvider();
		tableViewer.setContentProvider(contentProvider);

		table.setLinesVisible(true);
		tableViewer.setInput(getModel());

		tableViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {

				ISelection selection = event.getSelection();
				if (selection instanceof IStructuredSelection) {
					IStructuredSelection structuredSelection = (IStructuredSelection) selection;
					Object selected = structuredSelection.getFirstElement();
					if (selected instanceof SurroundBase) {
						if (selected != tableViewer.getInput() && doubleClickAction != null) {
							doubleClickAction.setModel((SurroundBase) selected);
							boolean succeeded = doubleClickAction.play(null);

							if (succeeded) {

								tableViewer.setInput(tableViewer.getInput());
								tableViewer.refresh();

								if (getChannel() != null) {
									SurroundRefreshInfo refreshInfo = new SurroundRefreshInfo();
									SurroundMessage refreshMessage = createMessage(refreshInfo, getReference());

									superVision(refreshMessage);
								}
							}
						}
					}

				}
			}
		});

		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				ISelection selection = event.getSelection();
				if (selection instanceof IStructuredSelection) {
					IStructuredSelection structuredSelection = (IStructuredSelection) selection;
					Object selected = structuredSelection.getFirstElement();
					if (selected instanceof SurroundBase) {
						if (selected != tableViewer.getInput() && selectAction != null) {
							selectAction.setModel((SurroundBase) selected);
							selectAction.play(null);
							refresh();
						}
					}

				}
			}
		});

		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		final IMenuListener menuListener = new IMenuListener() {

			@Override
			public void menuAboutToShow(IMenuManager manager) {
				final Object selected;
				if (tableViewer.getSelection() instanceof TreeSelection) {
					selected = ((TreeSelection) tableViewer.getSelection()).getFirstElement();
				} else {
					selected = null;
				}

				if (selected instanceof SurroundBase) {
					SurroundBase type = (SurroundBase) selected;
					for (SurroundAction action : SurroundActionRegistry.getAllAvailableActions(type)) {
						if (action instanceof SurroundSwtAction) {
							action.setModel(type);
							manager.add(((SurroundSwtAction) action).play4JFace());
						}
					}
				}

				manager.updateAll(true);
			}
		};

		tableViewer.getTable().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {
				if (tableViewer != null && !tableViewer.getTable().isDisposed()) {

					MenuManager menuManager = new MenuManager();
					menuManager.setRemoveAllWhenShown(true);
					Menu menu = menuManager.createContextMenu(tableViewer.getTable());
					tableViewer.getTable().setMenu(menu);

					menuManager.addMenuListener(menuListener);
				}
			}
		});
		SurroundViewerFilterDelegate filter = new SurroundViewerFilterDelegate(this);
		tableViewer.setFilters(filter);

	}

	public void setDoubleClickAction(SurroundSwtAction doubleClickAction) {

		this.doubleClickAction = doubleClickAction;
	}

	public SurroundSwtAction getSelectAction() {

		return selectAction;
	}

	public void setSelectAction(SurroundSwtAction selectAction) {

		this.selectAction = selectAction;
	}

	@SuppressWarnings("unchecked")
	protected void sort(TableColumn column) throws SurroundBaseException {

		int index = -1;

		if (getModel() instanceof SurroundList) {
			for (int i = 0; i < tableViewer.getTable().getColumnCount(); i++) {
				TableColumn checkColumn = tableViewer.getTable().getColumn(i);
				if (checkColumn == column) {

					index = i;
					break;
				}
			}

			if (tableViewer.getTable().getSortColumn() == column) {
				int sortDirection = tableViewer.getTable().getSortDirection();
				sortDirection = sortDirection == SWT.UP ? SWT.DOWN : SWT.UP;
				tableViewer.getTable().setSortDirection(sortDirection);
			} else {
				tableViewer.getTable().setSortDirection(SWT.UP);
				tableViewer.getTable().setSortColumn(column);
			}
			if (contentProvider instanceof SurroundSwtSortableContentProvider) {
				SurroundSwtSortableContentProvider tableContentProvider = (SurroundSwtSortableContentProvider) contentProvider;

				if (index == 0) {
					tableContentProvider.noSort();
				} else {

					tableContentProvider.sortBy(column.getText(), tableViewer.getTable().getSortDirection());
				}
			}

			else {
				throw new SurroundNotYetImplementedException("implement sort in lazy Vector");
			}
			refresh();
		} else {
			throw new SurroundWrongTypeException("are any other types than list needed?");
		}

	}

	public SurroundBase getFirstSelected() {

		SurroundBase selected = null;

		ISelection selection = tableViewer.getSelection();
		if (selection instanceof IStructuredSelection) {
			Object type = ((IStructuredSelection) selection).getFirstElement();
			if (type instanceof SurroundBase) {
				selected = (SurroundBase) type;
			}
		}

		return selected;
	}

	public ISelection getSelection() {
		return tableViewer.getSelection();
	}

	public void setSelection(StructuredSelection selection) {
		tableViewer.setSelection(selection);
	}

	public void addSelectionChangedListener(ISelectionChangedListener iSelectionChangedListener) {
		tableViewer.addSelectionChangedListener(iSelectionChangedListener);
	}

	public Control getTable() {
		return tableViewer.getTable();
	}

	@Override
	public void refresh() {
		tableViewer.refresh();
	}
}
