/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.dialog;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.model.base.SurroundByteArray;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import net.sourceforge.javahexeditor.BinaryContent;
import net.sourceforge.javahexeditor.HexTexts;

public class SurroundSwtHexDialog extends SurroundSwtDialog {

	protected SurroundByteArray data = null;
	private BufferContent bufferContent = null;
	private HexTexts hexTexts = null;

	class BufferContent extends BinaryContent {

		public BufferContent() {
		}

		public void init() {
			byte[] bytes = data.get();

			if (bytes != null && bytes.length > 0) {
				long i = 0;
				for (byte singleByte : bytes) {
					try {
						insert(singleByte, i++);
					} catch (IOException e) {
						SurroundSwtExceptionHandler.handle(e);
					}
				}
			}
		}

		public void merge() throws SurroundBaseException {
			try {
				long size = length();

				if (size > 0) {
					ByteBuffer myData = ByteBuffer.allocate(4096);

					get(myData, 0);
					byte[] asArray = myData.array();

					int total = (int) size;

					byte[] newData = new byte[total];

					for (int i = 0; i < total; i++) {
						newData[i] = asArray[i];
					}
					data.set(newData);
				}
			} catch (Exception e) {
				throw new SurroundBaseException(e.getMessage());
			}
		}

	}

	public SurroundSwtHexDialog(Shell parentShell, SurroundByteArray byteArray) throws SurroundWrongTypeException {
		super(parentShell, byteArray);
		this.data = byteArray;

		setShellStyle(SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.RESIZE | getDefaultOrientation());
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridData gridData = new GridData(GridData.FILL_BOTH);
		// gridData.widthHint = 680;

		composite.setLayoutData(gridData);
		composite.setLayout(new GridLayout(1, false));

		bufferContent = new BufferContent();

		hexTexts = new HexTexts(composite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		hexTexts.setContentProvider(bufferContent);
		hexTexts.setLayoutData(new GridData(GridData.FILL_BOTH));

		bufferContent.init();

		return composite;
	}

	@Override
	public boolean close() {
		try {
			if (bufferContent != null) {
				bufferContent.merge();
			}
		} catch (Exception e) {
			SurroundSwtExceptionHandler.handle(e);
		}
		return super.close();
	}

}
