/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.javafx.bridge;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import javafx.application.Platform;
import javafx.embed.swt.FXCanvas;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;

public class SurroundFXSwtCanvas extends FXCanvas {

	private ScrollPane scrollPane;

	public SurroundFXSwtCanvas(Composite parent, int style) {

		super(parent, style /* | SWT.H_SCROLL | SWT.V_SCROLL */);
		setLayoutData(new GridData(GridData.FILL_BOTH));
		setLayout(new GridLayout());

		scrollPane = new ScrollPane();
		scrollPane.setPrefWidth(600);
		scrollPane.setPrefHeight(600);
		scrollPane.setStyle("-fx-background: #FFFFFF;-fx-border-color: #FFFFFF;");

		setScene(new Scene(scrollPane));

		Platform.setImplicitExit(false);
	}

	public ScrollPane getScrollPane() {
		return scrollPane;
	}

	@Override
	public Point computeSize(int wHint, int hHint, boolean changed) {

		Point newSize = new Point(wHint, wHint);

		if (scrollPane != null) {

			int width = (int) scrollPane.prefWidth(0);
			int height = (int) scrollPane.prefHeight(0);

			newSize = new Point(width, height);
		}

		return newSize;
	}

	public void autoContentResize() {

	}

}
