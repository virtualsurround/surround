/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.action;

import java.net.URL;

import org.eclipse.swt.program.Program;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;

import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.model.url.SurroundURL;;

public class SurroundSwtLinkAction extends SurroundSwtAction {

	public SurroundSwtLinkAction(String text) {
		super(text);
	}

	@Override
	public boolean play(IProgressInfoListener progressListener) {

		if (getModel() instanceof SurroundURL) {
			try {
				SurroundURL link = (SurroundURL) getModel();

				String linkText = link.toString();
				URL url = link.asURL();

				if (linkText.startsWith("mailto:")) {
					Program.launch(linkText);
				} else

				if (true) {
					PlatformUI.getWorkbench().getBrowserSupport().getExternalBrowser().openURL(url);

				} else {
					final IWebBrowser browser = PlatformUI.getWorkbench().getBrowserSupport()
							.createBrowser(link.getName());
					browser.openURL(url);
				}

				return true;

			} catch (Exception ex) {
				SurroundSwtExceptionHandler.handle(ex);
			}

		}

		return false;
	}

}
