package de.instanttouch.surround.dynamic.ui.swt.editor;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import de.instanttouch.surround.data.model.SurroundDataReference;

public class SurroundSwtEditorInput implements IEditorInput {

	private SurroundDataReference reference;

	public SurroundSwtEditorInput(SurroundDataReference reference) {
		super();
		this.reference = reference;
	}

	public SurroundDataReference getReference() {
		return reference;
	}

	@Override
	public <T> T getAdapter(Class<T> adapter) {
		return null;
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getName() {
		return "";
	}

	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return null;
	}

}
