package de.instanttouch.surround.dynamic.ui.swt.exception;

import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class SurroundSwtExceptionHandler {

	public class SurroundErrorDialog extends ErrorDialog {

		private Clipboard clipboard;
		IStatus status;
		Shell shell;

		public SurroundErrorDialog(Shell parentShell, String dialogTitle, String message, IStatus status, int displayMask) {
			super(parentShell, dialogTitle, message, status, displayMask);
			this.status = status;
			shell = parentShell;
		}

		@Override
		protected void createButtonsForButtonBar(Composite parent) {
			super.createButtonsForButtonBar(parent);

			((GridLayout) parent.getLayout()).numColumns++;
			Button copy = new Button(parent, SWT.PUSH);
			copy.setText("copy stacktrace");
			copy.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					copyToClipboard();
				}
			});

		}

		private void copyToClipboard() {
			if (clipboard != null) {
				clipboard.dispose();
			}
			StringBuffer statusBuffer = new StringBuffer();
			IStatus[] childs = status.getChildren();
			for (IStatus single : childs) {
				statusBuffer.append(single.toString() + "\n");
			}

			clipboard = new Clipboard(shell.getDisplay());
			clipboard.setContents(new Object[] { statusBuffer.toString() },
					new Transfer[] { TextTransfer.getInstance() });
		}

		@Override
		public boolean close() {
			if (clipboard != null) {
				clipboard.dispose();
			}
			return super.close();
		}

	}

	public static void handle(Exception ex) {
		handle(ex, "unknown plugin");
	}

	public static void handle(Exception ex, String pluginId) {

		try {
			Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			SurroundSwtErrorStatus status = new SurroundSwtErrorStatus(IStatus.ERROR, pluginId, 0,
					"Status Error Message", ex);
			SurroundSwtExceptionHandler handler = new SurroundSwtExceptionHandler();

			SurroundErrorDialog errorDialog = handler.new SurroundErrorDialog(shell, "Error", ex.toString(), status, IStatus.ERROR);
			errorDialog.open();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	public static void handle(Shell shell, Exception ex) {
		handle(shell, ex, "unknown plugin");
	}

	public static void handle(Shell shell, Exception ex, String pluginId) {
		SurroundSwtErrorStatus status = new SurroundSwtErrorStatus(IStatus.ERROR, pluginId, 0, "Status Error Message",
				ex);
		SurroundSwtExceptionHandler handler = new SurroundSwtExceptionHandler();

		SurroundErrorDialog errorDialog = handler.new SurroundErrorDialog(shell, "Error", ex.toString(), status, IStatus.ERROR);
		errorDialog.open();
	}

	public static void handleErrorMessges(Vector<String> messages, String title) {
		handleErrorMessges(messages, title, "unknown plugin");
	}

	public static void handleErrorMessges(Vector<String> messages, String title, String pluginId) {
		try {
			Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();

			SurroundSwtErrorMessageStatus status = new SurroundSwtErrorMessageStatus(messages, title, IStatus.ERROR,
					pluginId, 0);
			SurroundSwtExceptionHandler handler = new SurroundSwtExceptionHandler();

			SurroundErrorDialog errorDialog = handler.new SurroundErrorDialog(shell, "Error", title, status, IStatus.ERROR);
			errorDialog.open();
		} catch (Throwable e) {

		}

	}
}
