package de.instanttouch.surround.dynamic.ui.swt.wizards;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;

import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtAction;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.jdk.search.SurroundJdtClassInfo;
import de.instanttouch.surround.dynamic.ui.swt.jdk.search.SurroundJdtTypeSearch;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtTablePresenter;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundViewerFilter;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBase.ISurroundFactory;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.io.SurroundJSONWriter;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundNewWizard extends BasicNewResourceWizard implements INewWizard {

	private WizardNewFileCreationPage resourcePage;
	private SelectClassPage selectPage;

	private final SurroundList<SurroundClassInfo> allModels = new SurroundList<SurroundClassInfo>("models",
			SurroundClassInfo.class, "classInfo");
	private SurroundSwtTablePresenter tablePresenter;

	class SelectClassPage extends WizardPage {

		SurroundClassInfo selected = null;

		protected SelectClassPage(String pageName) {

			super(pageName);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void createControl(Composite parent) {

			Composite classComposite = new Composite(parent, SWT.NONE);
			classComposite.setLayout(new GridLayout(1, false));
			classComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

			FormToolkit toolkit = new FormToolkit(parent.getDisplay());

			Composite filterComposite = new Composite(classComposite, SWT.BORDER);
			filterComposite.setLayout(new GridLayout(3, false));
			filterComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			Label label = new Label(filterComposite, SWT.NONE);
			label.setText("filter");

			SurroundViewerFilter filter = new SurroundViewerFilter();

			Text searchField = new Text(filterComposite, SWT.BORDER);
			searchField.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent e) {
					String searchText = searchField.getText();
					filter.getPattern().set(searchText);
					tablePresenter.refresh();
				}
			});
			searchField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			Button searchButton = new Button(filterComposite, SWT.BORDER);
			searchButton.setText("search");
			searchButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					try {
						findSurroundModels();
					} catch (Exception ex) {
						SurroundSwtExceptionHandler.handle(ex);
					}
				}
			});

			Label lbClass = new Label(classComposite, SWT.NONE);
			lbClass.setText("Entity Class:");

			try {

				tablePresenter = new SurroundSwtTablePresenter();
				tablePresenter.setModel(allModels);

				SurroundSwtParent layoutContainer = new SurroundSwtParent(classComposite,
						SWT.BORDER | SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.H_SCROLL);
				tablePresenter.show(layoutContainer);
				tablePresenter.getTableViewer().setFilters(filter);

				tablePresenter.setSelectAction(new SurroundSwtAction() {

					@Override
					public boolean play(IProgressInfoListener progressInfoListener) {
						ISelection selection = tablePresenter.getSelection();
						if (selection instanceof IStructuredSelection) {
							IStructuredSelection structuredSelection = (IStructuredSelection) selection;
							Object selectedObjectect = structuredSelection.getFirstElement();
							if (selectedObjectect instanceof SurroundClassInfo) {
								SelectClassPage.this.selected = (SurroundClassInfo) selectedObjectect;
							}
						}

						return true;
					}
				});

				toolkit.adapt(parent);
				toolkit.adapt(classComposite);
				toolkit.adapt(filterComposite);
				toolkit.adapt(layoutContainer.getSwtContainer());

			} catch (Exception e) {
				SurroundSwtExceptionHandler.handle(e);
			}

			setControl(classComposite);

		}

	}

	public SurroundNewWizard() {
		super();
		ISurroundFactory<SurroundClassInfo> creator = new ISurroundFactory() {

			@Override
			public SurroundClassInfo create()
					throws SurroundBaseException, InstantiationException, IllegalAccessException {
				return new SurroundClassInfo("model");
			}
		};
		allModels.setElementFactory(creator);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection currentSelection) {

		super.init(workbench, currentSelection);
	}

	@Override
	public boolean performFinish() {

		IFile file = resourcePage.createNewFile();
		if (file == null) {
			return false;
		}

		FileOutputStream output = null;
		SurroundJSONWriter writer = null;
		OutputStreamWriter fileWriter = null;
		if (selectPage.selected != null) {
			try {

				SurroundBase model = SurroundRuntimeTools.createInstanceFor(selectPage.selected);

				FileEditorInput input = new FileEditorInput(file);
				String fileName = input.getPath().toString();

				output = new FileOutputStream(fileName);

				writer = new SurroundJSONWriter();
				writer.write(model);
				String content = writer.beautify();

				fileWriter = new OutputStreamWriter(output);
				fileWriter.write(content);

			} catch (Exception e) {
				SurroundSwtExceptionHandler.handle(e);
			} finally {
				if (writer != null) {
					writer.close();
				}
				if (fileWriter != null) {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (output != null) {
					try {
						output.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		}

		selectAndReveal(file);

		// Open editor on new file.
		IWorkbenchWindow dw = getWorkbench().getActiveWorkbenchWindow();
		try {
			if (dw != null) {
				IWorkbenchPage page = dw.getActivePage();
				if (page != null) {
					IDE.openEditor(page, file, true);
				}
			}
		} catch (PartInitException e) {
			SurroundSwtExceptionHandler.handle(e);
		}

		return true;
	}

	@Override
	public void addPages() {

		resourcePage = new WizardNewFileCreationPage("myModel", getSelection());
		resourcePage.setTitle("create new snake entity");
		resourcePage.setDescription("choose new snake file");
		resourcePage.setFileName("myEntity");
		resourcePage.setFileExtension("surround_entity");
		addPage(resourcePage);

		selectPage = new SelectClassPage("select model class");
		selectPage.setTitle("select model class");
		selectPage.setDescription("a snake editor cann handle any domain class, which is dereived from SurroundModel");
		addPage(selectPage);

	}

	public void findSurroundModels() throws JavaModelException, SurroundBaseException {

		SurroundJdtTypeSearch<SurroundModel> search = new SurroundJdtTypeSearch<SurroundModel>();
		SurroundList<SurroundJdtClassInfo<SurroundModel>> surroundTypes = search.findSurroundTypes(SurroundModel.class);

		for (SurroundJdtClassInfo<SurroundModel> entity : surroundTypes.getChildren()) {
			allModels.add(entity);
		}

		tablePresenter.refresh();
	}

}
