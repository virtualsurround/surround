package de.instanttouch.surround.dynamic.ui.swt;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageRegistry;
import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageRegistry.IImagePathCreator;
import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageUtil;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtPresenterProfile4Eclipse;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundByteArray;
import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundLong;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundMap;
import de.instanttouch.surround.model.collection.SurroundMap.SurroundMapEntry;
import de.instanttouch.surround.model.composite.SurroundEntity;
import de.instanttouch.surround.model.ref.SurroundDirectory;
import de.instanttouch.surround.model.ref.SurroundEmailAddress;
import de.instanttouch.surround.model.ref.SurroundFile;
import de.instanttouch.surround.model.time.SurroundDate;
import de.instanttouch.surround.model.url.SurroundURL;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.instanttouch.surround.dynamic.ui.swt"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
	 * BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;

		SurroundSwtPresenterProfile4Eclipse.registerEclipsePresenter();
		registerImages();

		SurroundSwtImageUtil imageUtil = new SurroundSwtImageUtil();
		de.instanttouch.surround.dynamic.ui.Activator.getDefault().setImageReader(imageUtil);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.
	 * BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String bundle, String path) {
		return imageDescriptorFromPlugin(bundle, path);
	}

	/**
	 * Gets an image from this plugin's image registry. If the image is not yet
	 * part of the registry, it will be added.
	 * 
	 * @param path
	 *            of the image to retrieve
	 * @return An image from the registry
	 */
	public static Image getImage(String bundle, String path) {
		Image image = getDefault().getImageRegistry().get(bundle + ":" + path);
		if (image == null) {
			ImageDescriptor descriptor = getImageDescriptor(bundle, path);
			if (descriptor != null) {
				image = descriptor.createImage();
				getDefault().getImageRegistry().put(bundle + ":" + path, descriptor);

				descriptor.getImageData();
			}
		}
		return image;
	}

	/**
	 * get registered image with a special height
	 * 
	 * @param bundle
	 * @param path
	 * @param width
	 * @return
	 */
	public static Image getImage(String bundle, String path, int prefWidth, int prefHeight) {
		Image image = getDefault().getImageRegistry().get(bundle + ":" + path + ":" + prefWidth + "," + prefHeight);
		if (image == null) {
			ImageDescriptor descriptor = getImageDescriptor(bundle, path);
			if (descriptor != null) {
				image = descriptor.createImage();

				double width = image.getBounds().width;
				double height = image.getBounds().height;

				double scaleX = prefWidth / width;
				double scaleY = prefHeight / height;

				double scale = Math.min(scaleX, scaleY);

				double minWidth = width * scale;
				double minHeight = height * scale;

				ImageData imageData = image.getImageData();
				ImageData newData = imageData.scaledTo((int) minWidth, (int) minHeight);

				image = new Image(image.getDevice(), newData);

				getDefault().putImage(image, bundle, path, prefWidth, prefHeight);
			}
		}

		return image;
	}

	/**
	 * get registered image with a special height
	 * 
	 * @param bundle
	 * @param path
	 * @param height2
	 * @param width
	 * @return
	 */
	public static void putImage(Image image, String bundle, String path, int width, int height) {
		ImageDescriptor descriptor = ImageDescriptor.createFromImage(image);
		image = descriptor.createImage();
		getDefault().getImageRegistry().put(bundle + ":" + path + ":" + width + "," + height, descriptor);
	}

	/**
	 * Gets an image from this plugin's image registry. If the image is not yet
	 * part of the registry, it will be added.
	 * 
	 * @param path
	 *            of the image to retrieve
	 * @return An image from the registry
	 */
	public static Image getImage(String path) {

		Image image = null;

		int sep = path.indexOf(":");
		if (sep >= 0) {
			String bundleName = path.substring(0, sep);
			String imagePath = path.substring(sep + 1);

			image = getImage(bundleName, imagePath);
		} else {
			image = getImage(PLUGIN_ID, path);
		}
		return image;
	}

	private void registerImages() {
		SurroundSwtImageRegistry imageRegistry = SurroundSwtImageRegistry.getInstance();
		if (imageRegistry != null) {

			imageRegistry.registerImage(SurroundMapEntry.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/type_net.png");
			imageRegistry.registerImage(SurroundList.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/list.png");
			imageRegistry.registerImage(SurroundEntity.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/entity.png");

			imageRegistry.registerImage(SurroundMap.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/model_icon.png");
			imageRegistry.registerImage(SurroundByteArray.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/binary-icon.png");

			imageRegistry.registerImage(SurroundInteger.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/number2.png");
			imageRegistry.registerImage(SurroundLong.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/number2.png");
			imageRegistry.registerImage(SurroundDouble.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/number2.png");

			imageRegistry.registerImage(SurroundDate.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/clock-icon.png");

			imageRegistry.registerImage(SurroundURL.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/globe-Vista-icon.png");

			imageRegistry.registerImage(SurroundEmailAddress.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/Places-mail-message-icon.png");

			imageRegistry.registerImageCreator(SurroundBoolean.class, new IImagePathCreator() {

				@Override
				public String getOverlayPath(SurroundBase type) {
					return null;
				}

				@Override
				public String getImagePath(SurroundBase type) {
					String path = "icons/bool_disable.png";

					if (type instanceof SurroundBoolean) {
						SurroundBoolean asBool = (SurroundBoolean) type;
						if (!asBool.isNull() && asBool.get()) {
							path = "icons/bool.png";
						}
					}

					return path;
				}
			});
			imageRegistry.registerImage(SurroundDirectory.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/folder_icon.png");
			imageRegistry.registerImage(SurroundFile.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/file-icon.png");
			imageRegistry.registerImage(SurroundString.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/pencil-icon.png");
			imageRegistry.registerImage(SurroundBase.class, "de.instanttouch.surround.dynamic.ui.swt",
					"icons/type.png");
		}
	}

}
