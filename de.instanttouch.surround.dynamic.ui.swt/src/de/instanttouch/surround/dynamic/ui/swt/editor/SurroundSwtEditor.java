/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.editor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import de.instanttouch.surround.data.model.SurroundDataReference;
import de.instanttouch.surround.dynamic.ui.swt.editor.SurroundEditorNode.ISurroundEditor;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtOutline;
import de.instanttouch.surround.model.action.SurroundPlayer;
import de.instanttouch.surround.model.domain.SurroundModel;

public class SurroundSwtEditor extends FormEditor implements ISurroundEditor {

	protected SurroundModel model = null;
	protected boolean bDirty = false;
	protected SurroundEditorNode editorNode;

	protected ExecutorService senderPool = Executors.newSingleThreadExecutor();

	private SurroundSwtOutline outline;
	private SurroundPlayer player;

	public SurroundSwtEditor() {
		super();

		player = new SurroundPlayer();
		player.setExecutor(senderPool);

		editorNode = new SurroundEditorNode("editor");
		editorNode.setPlayer(player);
		editorNode.setEditor(this);
	}

	public SurroundEditorNode getEditorNode() {
		return editorNode;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {

		getEditorNode().doSave(monitor);
	}

	@Override
	public void doSaveAs() {

		FileDialog dialog = new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		dialog.setText("select file");

		String newPath = dialog.open();
		if (newPath != null) {
			getModel().getReference().getFile().set(newPath);
		}

		doSave(null);

	}

	public void load() {

		getEditorNode().load();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {

		super.init(site, input);

		SurroundDataReference<SurroundModel> reference = null;

		if (input instanceof FileEditorInput) {
			FileEditorInput fileEditorInput = (FileEditorInput) input;

			String pathName = fileEditorInput.getPath().toString();

			reference = new SurroundDataReference();
			reference.getFile().set(pathName);

			String title = pathName;
			setTitle(title);

		} else if (input instanceof SurroundSwtEditorInput) {
			reference = ((SurroundSwtEditorInput) input).getReference();
		}

		getModel().setReference(reference);

		try {
			load();
		} catch (Exception e) {
			SurroundSwtExceptionHandler.handle(e);
		}

	}

	@Override
	public boolean isDirty() {
		return bDirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	@Override
	protected void addPages() {
		try {
			addPage(new SurroundSwtPage(this, "main", "main section"));
		} catch (PartInitException e) {
			SurroundSwtExceptionHandler.handle(e);
		}
	}

	@Override
	public SurroundModel getModel() {
		return model;
	}

	@Override
	public Object getAdapter(Class required) {

		if (IContentOutlinePage.class.equals(required)) {
			if (outline == null) {
				outline = new SurroundSwtOutline(getModel(), this);
			}
			return outline;
		}

		return super.getAdapter(required);
	}

	@Override
	public void markDirty(boolean isDirty) {

		bDirty = isDirty;
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public void setModel(SurroundModel entity) {
		model = entity;
	}

	@Override
	public void refresh() {
	}

}
