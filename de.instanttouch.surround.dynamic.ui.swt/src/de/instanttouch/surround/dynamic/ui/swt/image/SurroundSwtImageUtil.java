package de.instanttouch.surround.dynamic.ui.swt.image;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

import de.instanttouch.surround.dynamic.ui.image.SurroundImageReader;
import de.instanttouch.surround.dynamic.ui.swt.Activator;
import de.instanttouch.surround.model.image.SurroundImage;
import javafx.embed.swt.SWTFXUtils;
import javafx.scene.image.WritableImage;

public class SurroundSwtImageUtil extends SurroundImageReader {

	public SurroundSwtImageUtil() {

	}

	@Override
	public <I> I readImage(Class<I> imageType, SurroundImage surroundImage) {

		I image = null;

		if (imageType == WritableImage.class) {

			ImageDescriptor imageDescriptor = Activator.getImageDescriptor(surroundImage.getBundle(),
					surroundImage.getPath());
			if (imageDescriptor == null) {
				return null;
			}
			ImageData imageData = imageDescriptor.getImageData();
			if (imageData == null) {
				return null;
			}
			image = (I) SWTFXUtils.toFXImage(imageData, null);

		} else if (imageType == Image.class) {
			image = (I) Activator.getImage(surroundImage.getBundle(), surroundImage.getPath());
		}

		return image;
	}

	public WritableImage readFXImage(SurroundImage surroundImage) {
		return readImage(WritableImage.class, surroundImage);
	}

	public Image readSwtImage(SurroundImage surroundImage) {
		return readImage(Image.class, surroundImage);
	}

}
