package de.instanttouch.surround.dynamic.ui.swt.presenter;

import java.util.List;

import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;

import de.instanttouch.surround.dynamic.ui.exceptions.SurroundUIException;
import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.messaging.SurroundRefreshInfo;
import de.instanttouch.surround.dynamic.ui.swt.dialog.SurroundSwtDialog;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtTablePresenter;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundSwtUtils;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtComposite;
import de.instanttouch.surround.dynamic.ui.swt.layout.SurroundSwtParent;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundOptions;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundSwtListPresenter extends SurroundSwtPresenter<SurroundList<SurroundBase>> {

	private SurroundSwtListPresenter listPresenter;
	private SurroundSwtTablePresenter tablePresenter;

	protected IDoubleClickListener doubleClickListener;
	private SurroundSwtComposite composite;

	public SurroundSwtListPresenter() {
		super();
	}

	public SurroundSwtListPresenter(String name) {
		super(name);
	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) throws SurroundUIException {

		composite = (SurroundSwtComposite) layoutContainer.getSwtContainer();

		SurroundSwtParent toolContainer = new SurroundSwtParent(composite);
		createToolbar(toolContainer);

		try {
			tablePresenter = new SurroundSwtTablePresenter();
			tablePresenter.setModel(getModel());
			connectDelegate(tablePresenter);

			tablePresenter.show(layoutContainer);
			tablePresenter.setFilter(getFilter());

			if (model instanceof SurroundOptions) {
				List<SurroundBase> selected = ((SurroundOptions) model).getSelected();

				StructuredSelection selection = new StructuredSelection(selected);
				tablePresenter.setSelection(selection);
			}

			tablePresenter.addSelectionChangedListener(new ISelectionChangedListener() {

				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					selectOption(event);
				}
			});

		} catch (SurroundBaseException e) {
			SurroundLog.warning("failed creating SurroundTableViewer for list:" + model.getClass().getCanonicalName());
		}
	}

	private SurroundSwtComposite createToolbar(SurroundSwtParent toolContainer) {

		SurroundSwtComposite toolbarComposite = toolContainer.getSwtContainer();

		toolbarComposite.setLayout(new GridLayout(8, false));

		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.heightHint = 32;
		gridData.minimumHeight = 32;
		toolbarComposite.setLayoutData(gridData);

		Label label = new Label(toolbarComposite, SWT.NONE);
		label.setText(model.getName());

		String desc = model.getConstraints().getDescription();
		if (desc != null) {
			label.setToolTipText(desc);
		}

		if (model.getConstraints().isEnabled()) {
			Button configure = new Button(toolbarComposite, SWT.PUSH);

			String path = "icons/add.png";
			Image image = SurroundSwtUtils.getImage(path);
			configure.setImage(image);
			configure.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					add();
				}
			});

			Button delete = new Button(toolbarComposite, SWT.PUSH);

			path = "icons/delete.png";
			image = SurroundSwtUtils.getImage(path);
			delete.setImage(image);
			delete.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					delete();
				}

			});

			if (model instanceof SurroundOptions) {
				Button edit = new Button(toolbarComposite, SWT.PUSH);

				path = "icons/pencil-icon.png";
				image = SurroundSwtUtils.getImage(path);
				edit.setImage(image);
				edit.addSelectionListener(new SelectionAdapter() {

					@Override
					public void widgetSelected(SelectionEvent event) {

						ISelection selection = tablePresenter != null ? tablePresenter.getSelection()
								: listPresenter.getSelection();
						if (selection instanceof IStructuredSelection) {
							IStructuredSelection structuredSelection = (IStructuredSelection) selection;
							Object selected = structuredSelection.getFirstElement();
							if (selected instanceof SurroundBase) {
								SurroundBase type = (SurroundBase) selected;
								open(type);
							}

						}
					}

				});

			}

			String count = "(" + model.size() + " elements)";
			label = new Label(toolbarComposite, SWT.NONE);
			label.setText(count);
			desc = model.getConstraints().getDescription();
			if (desc != null) {
				label.setToolTipText(desc);
			}

		}
		return toolbarComposite;
	}

	protected ISelection getSelection() {
		// TODO Auto-generated method stub
		return null;
	}

	protected void open(SurroundBase type) {

		SurroundSwtDialog dialog = new SurroundSwtDialog(composite.getShell(), type);
		if (dialog.open() == dialog.OK) {

			if (getChannel() != null) {
				SurroundRefreshInfo refreshInfo = new SurroundRefreshInfo();
				SurroundMessage refreshMessage = createMessage(refreshInfo, getReference());

				superVision(refreshMessage);
			}
		}
	}

	protected void add() {

		try {

			SurroundBase newElement = model.newElement();
			SurroundBase ref = newElement.getReference() != null ? newElement.getReference() : newElement;

			SurroundSwtDialog dialog = new SurroundSwtDialog(composite.getShell(), ref);
			if (dialog.open() == dialog.OK) {
				model.add(newElement);
				if (tablePresenter != null) {
					tablePresenter.refresh();
				} else if (listPresenter != null) {
					listPresenter.refresh();
				}

				if (getChannel() != null) {
					SurroundRefreshInfo refreshInfo = new SurroundRefreshInfo();
					SurroundMessage refreshMessage = createMessage(refreshInfo, getReference());

					superVision(refreshMessage);
				}
			}

		} catch (Exception e) {
			SurroundSwtExceptionHandler.handle(e);
		}
	}

	private void delete() {

		ISelection selection = tablePresenter != null ? tablePresenter.getSelection() : listPresenter.getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;

			Object selected = structuredSelection.getFirstElement();
			if (selected instanceof SurroundBase) {

				SurroundBase type = (SurroundBase) selected;
				model.remove(type);

				refresh();

				if (getChannel() != null) {
					SurroundRefreshInfo refreshInfo = new SurroundRefreshInfo();
					SurroundMessage refreshMessage = createMessage(refreshInfo, getReference());

					superVision(refreshMessage);
				}
			}
		}
	}

	@Override
	public void refresh() {

		if (tablePresenter != null) {
			tablePresenter.setModel(model);
			tablePresenter.refresh();
		} else if (listPresenter != null) {
			listPresenter.setModel(model);
			listPresenter.refresh();
		}
	}

	private void selectOption(SelectionChangedEvent event) {

		if (!(model instanceof SurroundOptions)) {
			return;
		}

		SurroundOptions options = (SurroundOptions) model;

		ISelection selection = event.getSelection();
		if (selection instanceof IStructuredSelection) {

			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			Object selected = structuredSelection.getFirstElement();
			if (selected instanceof SurroundBase) {

				SurroundBase selectedChild = (SurroundBase) selected;

				options.clearSelection();
				options.setSelected(selectedChild);
			}
		}
	}

}
