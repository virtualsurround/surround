package de.instanttouch.surround.dynamic.ui.swt.jface;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.ui.dialogs.SearchPattern;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundString;

public class SurroundViewerFilter extends ViewerFilter {

	protected SurroundString pattern = new SurroundString("searchPattern");

	public SurroundViewerFilter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		boolean matches = pattern.isEmpty();
		if (!matches) {
			if (element instanceof SurroundBase) {
				SurroundBase type = (SurroundBase) element;
				matches = matches(type);
			}
		}
		return matches;
	}

	@Override
	public Object[] filter(Viewer viewer, Object parent, Object[] elements) {
		Object[] founded = super.filter(viewer, parent, elements);
		return founded;
	}

	public boolean matches(SurroundBase candidate) {
		boolean matches = false;
		if (candidate == null) {
			return false;
		}
		SearchPattern matcher = new SearchPattern();
		matcher.setPattern(pattern.toString());
		if (candidate.toString() != null) {
			matches = matcher.matches(candidate.toString());
		}
		if (!matches) {
			if (!matches && candidate.getName() != null) {
				matches = matcher.matches(candidate.getName());
			}
		}
		if (!matches && candidate.getChildren() != null) {
			for (SurroundBase child : candidate.getChildren()) {
				matches = matches(child);
				if (matches) {
					break;
				}
			}
		}
		return matches;
	}

	public SurroundString getPattern() {
		return pattern;
	}
}
