/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.jface;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import de.instanttouch.surround.dynamic.ui.swt.Activator;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundSwtUtils {

	public static Image createImage(String urlStr) {

		if (urlStr != null && urlStr.length() > 0) {
			try {
				URL url = new URL(urlStr);
				ImageDescriptor imageDescriptor = ImageDescriptor.createFromURL(url);

				Image image = imageDescriptor.createImage();
				return image;
			} catch (Exception e) {
				try {
					Image image = new Image(Display.getCurrent(), urlStr);
					return image;
				} catch (Exception e1) {
				}
			}
		}

		return null;
	}

	public static Image getImage(String path) {

		Image image = null;
		try {
			image = Activator.getImage(path);
		} catch (Throwable e) {

			try {

				String workspacePath = "images" + File.separator + path;
				File file = new File(workspacePath);
				String absPath = file.getAbsolutePath();

				image = new Image(Display.getCurrent(), absPath);

			} catch (Exception e1) {
				try {
					File file = new File(path);
					URL fileURL = file.toURI().toURL();
					ImageDescriptor imageDescriptor = ImageDescriptor.createFromURL(fileURL);
					if (imageDescriptor != null) {
						image = imageDescriptor.createImage();
					} else {
						SurroundLog.warning(e1.getMessage());
					}
				} catch (MalformedURLException ex) {
					SurroundLog.log(ex);
				}
			}
		}

		return image;
	}

	public static Image getImage(String bundle, String path) {

		Image image = null;
		try {
			image = Activator.getImage(bundle, path);
		} catch (Throwable e) {
			SurroundLog.warning(e.getMessage());
		}

		return image;
	}

	public static Color createFromHexString(String colorStr) {

		Color color = null;
		try {
			if (colorStr != null) {
				int r = Integer.valueOf(colorStr.substring(1, 3), 16);
				int g = Integer.valueOf(colorStr.substring(3, 5), 16);
				int b = Integer.valueOf(colorStr.substring(5, 7), 16);

				RGB rgb = new RGB(r, g, b);
				color = new Color(Display.getCurrent(), rgb);
			}

		} catch (Exception e) {
		}

		return color;
	}
}
