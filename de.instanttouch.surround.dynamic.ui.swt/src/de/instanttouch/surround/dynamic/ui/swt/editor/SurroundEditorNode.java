package de.instanttouch.surround.dynamic.ui.swt.editor;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;

import de.instanttouch.surround.data.model.SurroundData.ISurroundDataListener;
import de.instanttouch.surround.data.model.SurroundDataReference;
import de.instanttouch.surround.dynamic.ui.messaging.SurroundRefreshInfo;
import de.instanttouch.surround.dynamic.ui.swt.exception.SurroundSwtExceptionHandler;
import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.channel.SurroundMessageHandler;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.domain.SurroundModel;

public class SurroundEditorNode extends SurroundActor {

	protected ISurroundEditor editor;

	public interface ISurroundEditor {

		public void markDirty(boolean isDirty);

		public SurroundModel getModel();

		public void setModel(SurroundModel model);

		public void refresh();
	}

	public SurroundEditorNode() {
		initEditorNode();
	}

	public SurroundEditorNode(String name) {
		super(name);
		initEditorNode();
	}

	private void initEditorNode() {

		addMessageHandler(SurroundRefreshInfo.MESSAGE_ID, new SurroundMessageHandler() {

			@Override
			public boolean handle(SurroundActor component, SurroundMessage message)
					throws IOException, SurroundMessagingException {

				ISurroundEditor editor = getEditor();
				if (editor != null) {
					editor.markDirty(true);
				}
				return true;
			}
		});
	}

	public ISurroundEditor getEditor() {
		return editor;
	}

	public void setEditor(ISurroundEditor editor) {
		this.editor = editor;
	}

	public void doSave(IProgressMonitor monitor) {

		SurroundDataReference<SurroundModel> ref = (SurroundDataReference<SurroundModel>) getEditor().getModel()
				.getReference();

		try {
			ref.update(getEditor().getModel(), new ISurroundDataListener() {

				@Override
				public void onSuccess(SurroundModel result) {
					getEditor().markDirty(false);
					getEditor().refresh();
				}

				@Override
				public void onError(String errorStr) {
					// TODO Auto-generated method stub

				}
			});

			getEditor().refresh();
		} catch (IOException e) {
			SurroundSwtExceptionHandler.handle(e);
		}
	}

	public void load() {

		SurroundDataReference<SurroundModel> ref = (SurroundDataReference<SurroundModel>) getEditor().getModel()
				.getReference();

		try {
			ref.read(getEditor().getModel(), new ISurroundDataListener() {

				@Override
				public void onSuccess(SurroundModel result) {
					getEditor().markDirty(false);
					getEditor().refresh();
				}

				@Override
				public void onError(String errorStr) {
					// TODO Auto-generated method stub

				}
			});

			getEditor().refresh();
		} catch (IOException e) {
			SurroundSwtExceptionHandler.handle(e);
		}
	}

}
