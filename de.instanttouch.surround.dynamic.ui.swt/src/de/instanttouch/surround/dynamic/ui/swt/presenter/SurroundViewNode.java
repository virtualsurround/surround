package de.instanttouch.surround.dynamic.ui.swt.presenter;

import de.instanttouch.surround.messaging.model.SurroundActor;

public class SurroundViewNode extends SurroundActor {

	protected Object view;

	public SurroundViewNode() {
		super();
	}

	public SurroundViewNode(String name) {
		super(name);
	}

	public Object getView() {
		return view;
	}

	public void setView(Object view) {
		this.view = view;
	}

}
