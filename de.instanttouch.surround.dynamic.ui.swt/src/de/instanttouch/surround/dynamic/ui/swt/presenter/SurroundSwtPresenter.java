package de.instanttouch.surround.dynamic.ui.swt.presenter;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.model.base.SurroundBase;

public abstract class SurroundSwtPresenter<M extends SurroundBase> extends SurroundPresenter<M> {

	public SurroundSwtPresenter() {
		super();
		initSwtPresenter();
	}

	public SurroundSwtPresenter(String name) {
		super(name);
		initSwtPresenter();
	}

	private void initSwtPresenter() {
	}

	@Override
	public String getFrameworkType() {
		return TYPE_SWT;
	}

}
