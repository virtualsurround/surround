/*******************************************************************************
 * Copyright (c) 2015 Joachim Tessmer
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.dynamic.ui.swt.jface;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;

import de.instanttouch.surround.dynamic.ui.layout.ISurroundLayoutContainer;
import de.instanttouch.surround.dynamic.ui.messaging.SurroundRefreshInfo;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtAction;
import de.instanttouch.surround.dynamic.ui.swt.action.SurroundSwtOpenAction;
import de.instanttouch.surround.dynamic.ui.swt.presenter.SurroundSwtPresenter;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.action.SurroundAction;
import de.instanttouch.surround.model.action.SurroundActionRegistry;
import de.instanttouch.surround.model.base.SurroundBase;

public class SurroundSwtTreePresenter extends SurroundSwtPresenter<SurroundBase> {

	public static class SurroundSwtTreeContentProvider implements ITreeContentProvider {

		protected SurroundBase input = null;

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			if (!(newInput instanceof SurroundBase)) {
				input = null;
				return;
			} else {
				input = (SurroundBase) newInput;
			}
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			Object[] elements = null;

			if (parentElement instanceof SurroundBase) {
				SurroundBase type = (SurroundBase) parentElement;
				elements = type.getVisibleChildren().toArray();
			}

			return elements;
		}

		@Override
		public Object[] getElements(Object inputElement) {

			Object[] elements = null;

			if (inputElement instanceof SurroundBase) {
				SurroundBase type = (SurroundBase) inputElement;
				elements = type.getVisibleChildren().toArray();
			}

			return elements;
		}

		@Override
		public Object getParent(Object element) {
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			boolean hasChildren = false;

			if (element instanceof SurroundBase) {
				SurroundBase type = (SurroundBase) element;
				List<SurroundBase> children = type.getVisibleChildren();
				hasChildren = children.size() > 0;
			}

			return hasChildren;
		}

	}

	public static class SurroundSwtTreeViewer extends TreeViewer {

		public SurroundSwtTreeViewer(Composite parent, int style) {
			super(parent, style);
			// TODO Auto-generated constructor stub
		}

	}

	protected SurroundSwtAction doubleClickAction = new SurroundSwtOpenAction("open type dialog");
	protected SurroundSwtAction selectAction = null;
	protected SurroundSwtTreeViewer treeViewer;

	public SurroundSwtTreePresenter() {
	}

	@Override
	public void show(ISurroundLayoutContainer layoutContainer) {

		Composite composite = (Composite) layoutContainer.getSwtContainer();

		treeViewer = new SurroundSwtTreeViewer(composite, SWT.NONE);

		treeViewer.setLabelProvider(new SurroundSwtLabelProvider());
		treeViewer.setContentProvider(new SurroundSwtTreeContentProvider());
		treeViewer.setInput(getModel());

		treeViewer.setAutoExpandLevel(2);
		treeViewer.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));

		treeViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				if (null != doubleClickAction) {
					ISelection selection = treeViewer.getSelection();
					if (selection instanceof ITreeSelection) {
						ITreeSelection treeSelection = (ITreeSelection) selection;
						Object selected = treeSelection.getFirstElement();
						if (selected instanceof SurroundBase) {
							if (selected != treeViewer.getInput() && doubleClickAction != null) {

								doubleClickAction.setModel((SurroundBase) selected);
								if (doubleClickAction.play(null)) {

									refresh();

									if (getChannel() != null) {
										SurroundRefreshInfo refreshInfo = new SurroundRefreshInfo();
										SurroundMessage refreshMessage = createMessage(refreshInfo, getReference());

										superVision(refreshMessage);
									}
								}
							}
						}
					}
				}
			}
		});

		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if (null != selectAction) {
					ISelection selection = treeViewer.getSelection();
					if (selection instanceof ITreeSelection) {
						ITreeSelection treeSelection = (ITreeSelection) selection;
						Object selected = treeSelection.getFirstElement();
						if (selected instanceof SurroundBase) {
							selected = selected != treeViewer.getInput() ? selected : null;
							if (selectAction != null) {
								selectAction.setModel((SurroundBase) selected);
								selectAction.play(null);
								// refresh();
							}
						}
					}
				}
			}
		});

		final IMenuListener menuListener = new IMenuListener() {

			@Override
			public void menuAboutToShow(IMenuManager manager) {
				final Object selected;
				if (treeViewer.getSelection() instanceof TreeSelection) {
					selected = ((TreeSelection) treeViewer.getSelection()).getFirstElement();
				} else {
					selected = null;
				}

				if (selected instanceof SurroundBase) {
					SurroundBase type = (SurroundBase) selected;

					List<SurroundAction> availableActions = SurroundActionRegistry.getAllAvailableActions(type);
					for (SurroundAction action : availableActions) {
						if (action instanceof SurroundSwtAction) {
							action.setModel(type);
							manager.add(((SurroundSwtAction) action).play4JFace());
						}
					}
				}
				manager.updateAll(true);
			}
		};

		treeViewer.getTree().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {

				if (!treeViewer.getControl().isDisposed()) {

					MenuManager menuManager = new MenuManager();
					menuManager.setRemoveAllWhenShown(true);
					Menu menu = menuManager.createContextMenu(treeViewer.getControl());

					treeViewer.getControl().setMenu(menu);

					menuManager.addMenuListener(menuListener);
				}
			}
		});
		SurroundViewerFilterDelegate filter = new SurroundViewerFilterDelegate(this);
		treeViewer.setFilters(filter);

	}

	public SurroundSwtAction getDoubleClickAction() {
		return doubleClickAction;
	}

	public void setDoubleClickAction(SurroundSwtAction doubleClickAction) {
		this.doubleClickAction = doubleClickAction;
	}

	public void setSelectAction(SurroundSwtAction selectAction) {
		this.selectAction = selectAction;
	}

	@Override
	public void refresh() {

		if (!treeViewer.getTree().isDisposed()) {
			treeViewer.getTree().getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					if (!treeViewer.getTree().isDisposed()) {
						// treeViewer.setInput(getModel());
						// treeViewer.expandAll();
						treeViewer.refresh();
					}
				}
			});
		}
	}

	public SurroundBase getFirstChild() {
		SurroundBase type = null;
		Object selected = null;
		if (treeViewer.getSelection() instanceof TreeSelection) {
			selected = ((TreeSelection) treeViewer.getSelection()).getFirstElement();
		} else {
			selected = null;
		}

		if (selected instanceof SurroundBase) {
			type = (SurroundBase) selected;
		}
		return type;
	}

	public List<SurroundBase> getAllSelected() {
		List<SurroundBase> all = new ArrayList<SurroundBase>();

		if (treeViewer.getSelection() instanceof TreeSelection) {
			TreeSelection treeSelection = (TreeSelection) treeViewer.getSelection();

			for (Object selected : treeSelection.toList()) {
				if (selected instanceof SurroundBase) {
					SurroundBase type = (SurroundBase) selected;
					all.add(type);
				}
			}

		}
		return all;
	}

	public SurroundSwtTreeViewer getViewer() {
		return treeViewer;
	}

	public void select(SurroundBase type) {
		// TODO Auto-generated method stub

	}

}
