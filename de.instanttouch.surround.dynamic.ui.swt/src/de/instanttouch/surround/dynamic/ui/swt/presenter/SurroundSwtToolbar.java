package de.instanttouch.surround.dynamic.ui.swt.presenter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.instanttouch.surround.dynamic.ui.presenter.SurroundPresenter;
import de.instanttouch.surround.dynamic.ui.swt.image.SurroundSwtImageRegistry;
import de.instanttouch.surround.dynamic.ui.swt.jface.SurroundViewerFilter;

public class SurroundSwtToolbar extends Composite {

	protected SurroundPresenter<?> presenter;
	private SurroundViewerFilter viewerFilter;

	public SurroundSwtToolbar(Composite parent) {
		super(parent, SWT.NONE);

		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.heightHint = 30;
		setLayoutData(gridData);

		createToolbar(parent);
	}

	private void createToolbar(Composite parent) {

		viewerFilter = new SurroundViewerFilter();

		Image image = SurroundSwtImageRegistry.getInstance().getImage("de.instanttouch.surround.dynamic.ui.swt",
				"icons/search-icon.png");
		Label searchIcon = new Label(this, SWT.NONE);
		searchIcon.setImage(image);

		Text searchStr = new Text(this, SWT.BORDER);

		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		searchStr.setLayoutData(gridData);

		searchStr.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				String text = searchStr.getText();
				viewerFilter.getPattern().set(text);
				presenter.refresh();
			}
		});

		setLayout(new GridLayout(2, false));
	}

	public SurroundViewerFilter getViewerFilter() {
		return viewerFilter;
	}

	public SurroundPresenter<?> getContentPresenter() {
		return presenter;
	}

	public void setContentPresenter(SurroundPresenter<?> presenter) {
		this.presenter = presenter;
	}

}
