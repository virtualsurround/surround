package de.instanttouch.surround.vertx.channel;

import java.io.IOException;

import de.instanttouch.surround.messaging.exception.SurroundMessagingException;
import de.instanttouch.surround.messaging.model.SurroundActor;
import de.instanttouch.surround.messaging.model.SurroundActorReference;
import de.instanttouch.surround.messaging.model.SurroundClient;
import de.instanttouch.surround.messaging.model.channel.SurroundChannel;
import de.instanttouch.surround.messaging.model.channel.SurroundConnectionInfo;
import de.instanttouch.surround.messaging.model.channel.SurroundMessageHandler;
import de.instanttouch.surround.messaging.model.channel.SurroundServiceAddressBook;
import de.instanttouch.surround.messaging.model.message.SurroundMessage;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundClassInfo;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.io.SurroundJSONWriter;
import de.instanttouch.surround.model.url.SurroundURL;
import de.instanttouch.surround.model.util.SurroundLog;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;

public class SurroundVertxChannel extends SurroundChannel {

	protected Vertx vertx;
	private EventBus eventBus;

	public class VerticleRef extends SurroundBase {

		protected SurroundVerticle verticle;

		public VerticleRef(SurroundVerticle verticle) {
			super();
			this.verticle = verticle;
		}

		public SurroundVerticle getVerticle() {
			return verticle;
		}
	}

	public class SurroundVerticle extends AbstractVerticle {

		protected SurroundActor surroundActor;
		private MessageConsumer<String> consumer;

		public SurroundVerticle(SurroundActor surroundActor) {
			super();
			this.surroundActor = surroundActor;
		}

		@Override
		public void start() throws Exception {

			String surroundURL = createURL(surroundActor);
			surroundActor.getReference().getURL().set(surroundURL);

			consumer = eventBus.consumer(surroundURL, message -> {

				try {
					String body = message.body().toString();

					byte[] data = body.getBytes();
					SurroundMessage surroundMessage = receive(data);

					String messageId = surroundMessage.getHeader().getMessageId();
					SurroundMessageHandler messageHandler = surroundActor.getMessageHandler(messageId);

					messageHandler.handle(surroundActor, surroundMessage);

				} catch (Exception e) {
					SurroundLog.log(e);
				}
			});
		}

		@Override
		public void stop() throws Exception {
			super.stop();
		}

		public void unregister() {
			consumer.unregister();
		}

	}

	public SurroundVertxChannel() {
		super("Vertx Channel", 0, 0);

		VertxOptions options = new VertxOptions();
		Vertx.clusteredVertx(options, res -> {
			vertx = res.result();
			eventBus = vertx.eventBus();
		});
	}

	public Vertx getVertx() {
		return vertx;
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	String createURL(SurroundActor actor) {

		SurroundClassInfo classInfo = SurroundRuntimeTools.determineClassInfo(actor.getClass());

		String urlString = "vertx://";
		urlString += classInfo.getBundleName() + "/";
		urlString += classInfo.getClassName();

		return urlString;
	}

	@Override
	public void register(SurroundActor actor) throws IOException {

		SurroundList<SurroundActor> components = getComponents();
		if (!components.contains(actor)) {

			components.add(actor);

			SurroundVerticle verticle = new SurroundVerticle(actor);
			getVertx().deployVerticle(verticle);
			actor.getProperties().put("verticle", new VerticleRef(verticle));

			SurroundActorReference address = new SurroundActorReference(actor.getName());
			address.setHost("localhost");

			for (SurroundClientInfo clientInfo : actor.getClients().keySet()) {
				SurroundClient surroundClient = actor.getClients().get(clientInfo);

				SurroundConnectionInfo connectionInfo = lookupService(clientInfo);
				surroundClient.connect(connectionInfo);
			}
		}
	}

	@Override
	public void unregister(SurroundActor actor) throws SurroundMessagingException, IOException {

		SurroundBase obj = actor.getProperties().get("consumer");
		if (obj instanceof VerticleRef) {

			SurroundVerticle verticle = ((VerticleRef) obj).getVerticle();
			verticle.unregister();
		}
		super.unregister(actor);
	}

	@Override
	public SurroundChannel start() throws SurroundMessagingException, IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SurroundChannel stop() throws SurroundMessagingException, IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SurroundConnectionInfo lookupService(SurroundClientInfo clientInfo) throws IOException {

		SurroundServiceAddressBook addressBook = getChannel().getServiceAddressBook();
		SurroundActorReference serviceAddress = addressBook
				.findAddressByNameAndRole(clientInfo.getServiceName().toString(), clientInfo.getRole().toString());

		SurroundConnectionInfo connectionInfo;
		try {
			connectionInfo = new SurroundConnectionInfo("connection");
			connectionInfo.assignSelf((SurroundActorReference) clientInfo.getModelCoordinate().clone());
			connectionInfo.assignTarget(serviceAddress);

		} catch (CloneNotSupportedException e) {
			throw new SurroundMessagingException("error creating conenctionInfo", e);
		}

		return connectionInfo;
	}

	@Override
	public void sendInternal(SurroundMessage message) throws IOException {

		SurroundJSONWriter writer = new SurroundJSONWriter();
		writer.write(message);

		String content = writer.getContent();

		SurroundURL url = message.getHeader().getTargetAddress().getURL();
		getEventBus().send(url.toString(), content);
	}

}
