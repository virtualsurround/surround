package de.instanttouch.surround.data.elasticsearch;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import de.instanttouch.surround.data.elasticsearch.impl.SurroundElasticsearchDataFactory;
import de.instanttouch.surround.data.model.SurroundDataFactory;

public class Activator implements BundleActivator {

	private static BundleContext context;
	private static Activator plugin;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.
	 * BundleContext)
	 */
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext bundleContext) throws Exception {

		Activator.context = null;
		plugin = null;
	}

	public static Activator getDefault() {
		return plugin;
	}
}
