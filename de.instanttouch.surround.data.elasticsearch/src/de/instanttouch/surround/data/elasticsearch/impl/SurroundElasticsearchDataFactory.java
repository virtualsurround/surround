package de.instanttouch.surround.data.elasticsearch.impl;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import de.instanttouch.surround.data.model.SurroundDataFactory.IDataSourceFactory;
import de.instanttouch.surround.data.model.SurroundDataReference;
import de.instanttouch.surround.model.collection.SurroundStringList;

public class SurroundElasticsearchDataFactory implements IDataSourceFactory {

	public SurroundElasticsearchDataFactory() {
		super();
	}

	@Override
	public SurroundElasticsearchDataSource createDataSource(SurroundDataReference<?> connection) throws IOException {

		SurroundElasticsearchDataSource dataSource = new SurroundElasticsearchDataSource("elasticsearchDataSource");
		Client client = null;

		String host = connection.getHost() != null && connection.getHost().length() > 0 ? connection.getHost()
				: "localhost";

		String clusterName = connection.getCluster();

		List<String> clusterNodes = new ArrayList<>();
		clusterNodes.add(host);

		SurroundStringList clusterNodeList = connection.getClusterNodes();
		if (clusterNodeList != null) {
			for (String nodeName : clusterNodeList.asSimpleList()) {
				if (!clusterNodes.contains(nodeName)) {
					clusterNodes.add(nodeName);
				}
			}
		}

		int port = connection.getPort();
		port = port < 0 ? 9300 : port;

		Settings settings = Settings.builder().put("cluster.name", clusterName).put("client.transport.sniff", false)
				.build();

		TransportClient transportClient = new PreBuiltTransportClient(settings, Collections.EMPTY_LIST);
		client = transportClient;

		for (String hostName : clusterNodes) {

			InetAddress address = InetAddress.getByName(hostName);
			InetSocketTransportAddress socketAddress = new InetSocketTransportAddress(address, port);

			transportClient.addTransportAddress(socketAddress);
		}

		dataSource.setConnection(connection);
		dataSource.setClient(client);

		return dataSource;
	}
}
