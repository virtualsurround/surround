package de.instanttouch.surround.data.elasticsearch.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.ListenableActionFuture;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.index.get.GetField;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortOrder;

import de.instanttouch.surround.data.exception.SurroundDataException;
import de.instanttouch.surround.data.model.SurroundData;
import de.instanttouch.surround.data.query.SurroundDataFilter;
import de.instanttouch.surround.data.query.SurroundDataFilter.SurroundDataFilterExpression;
import de.instanttouch.surround.data.query.SurroundDataOrderBy;
import de.instanttouch.surround.data.query.SurroundDataQuery;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.time.SurroundTimeRange;
import de.instanttouch.surround.model.time.SurroundTimestamp;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundElasticsearchData<M extends SurroundModel> extends SurroundData<M> {

	public SurroundElasticsearchData() {
		super("elasticsearch");
	}

	@Override
	public SurroundElasticsearchDataSource getDataSource() {
		return (SurroundElasticsearchDataSource) super.getDataSource();
	}

	@Override
	public void create(final M newModel, final ISurroundDataListener<M> listener) throws IOException {

		IndexRequestBuilder indexBuilder = null;

		SurroundReference<M> modelCoordinate = newModel.getModelCoordinate(newModel);
		if (modelCoordinate == null) {
			throw new SurroundNotInitializedException("no model coordinate given");
		}
		String index = modelCoordinate.getIndex();
		String type = modelCoordinate.getType();
		String dbId = modelCoordinate.getKey();

		if (!dbId.isEmpty()) {
			indexBuilder = getDataSource().getClient().prepareIndex(index, type, dbId);
		} else {
			indexBuilder = getDataSource().getClient().prepareIndex(index, type);
		}

		indexBuilder.setSource(newModel.asJSON());
		ListenableActionFuture<IndexResponse> actionFuture = indexBuilder.execute();
		actionFuture.addListener(new ActionListener<IndexResponse>() {

			@Override
			public void onResponse(IndexResponse response) {
				listener.onSuccess(newModel);
			}

			@Override
			public void onFailure(Exception e) {
				listener.onError(e.getMessage());
			}
		});

	}

	@Override
	public void read(final M model, final ISurroundDataListener<M> listener) throws IOException {

		SurroundReference<M> modelCoordinate = model.getModelCoordinate(model);
		if (modelCoordinate == null) {
			throw new SurroundNotInitializedException("no model coordinate given");
		}

		String index = modelCoordinate.getIndex();
		String type = modelCoordinate.getType();
		String dbId = modelCoordinate.getKey();

		ListenableActionFuture<GetResponse> actionFuture = getDataSource().getClient().prepareGet(index, type, dbId)
				.execute();
		actionFuture.addListener(new ActionListener<GetResponse>() {

			@Override
			public void onResponse(GetResponse response) {

				if (response.isExists()) {

					try {
						Map<String, GetField> fields = response.getFields();
						if (fields.size() > 0) {
							for (String field : fields.keySet()) {

								GetField value = fields.get(field);
								String text = value.getValue().toString();

								model.findByName(field).set(text);
							}
						} else {
							String jsonStr = response.getSourceAsString();
							model.fromJSON(jsonStr);
						}
						listener.onSuccess(model);
					} catch (Exception e) {
						listener.onError("no get response on read: " + e.getMessage());
					}

				} else {
					listener.onError("no get response on read");
				}
			}

			@Override
			public void onFailure(Exception e) {
				listener.onError(e.getMessage());
			}
		});
	}

	@Override
	public void update(final M model, final ISurroundDataListener<M> listener) throws IOException {

		String jsonStr = model.asJSON();

		SurroundReference<M> modelCoordinate = model.getModelCoordinate(model);
		if (modelCoordinate == null) {
			throw new SurroundNotInitializedException("no model coordinate given");
		}
		String index = modelCoordinate.getIndex();
		String type = modelCoordinate.getType();
		String dbId = modelCoordinate.getKey();

		UpdateRequestBuilder updateRequestBuilder = getDataSource().getClient().prepareUpdate(index, type, dbId)
				.setDoc(jsonStr);
		ListenableActionFuture<UpdateResponse> actionFuture = updateRequestBuilder.execute();

		actionFuture.addListener(new ActionListener<UpdateResponse>() {

			@Override
			public void onResponse(UpdateResponse response) {
				listener.onSuccess(model);
			}

			@Override
			public void onFailure(Exception e) {
				listener.onError(e.getMessage());
			}
		});
	}

	@Override
	public void delete(final M model, final ISurroundDataListener<M> listener) throws IOException {

		SurroundReference<M> modelCoordinate = model.getModelCoordinate(model);
		if (modelCoordinate == null) {
			throw new SurroundNotInitializedException("no model coordinate given");
		}
		String index = modelCoordinate.getIndex();
		String type = modelCoordinate.getType();
		String dbId = modelCoordinate.getKey();

		ListenableActionFuture<DeleteResponse> actionFuture = getDataSource().getClient()
				.prepareDelete(index, type, dbId).execute();

		actionFuture.addListener(new ActionListener<DeleteResponse>() {

			@Override
			public void onResponse(DeleteResponse response) {
				listener.onSuccess(model);
			}

			@Override
			public void onFailure(Exception e) {
				listener.onError(e.getMessage());
			}
		});
	}

	private SimpleQueryStringBuilder fulltextQuery(SurroundDataQuery<M> query) {

		String searchText = query.getSearchString();
		SimpleQueryStringBuilder fulltextQuery = QueryBuilders.simpleQueryStringQuery(searchText);

		return fulltextQuery;
	}

	private List<TermQueryBuilder> fieldQueries(final SurroundDataQuery<M> query) {

		List<TermQueryBuilder> fieldFilters = new ArrayList<>();

		try {
			SurroundDataFilter currentFilter = query.getFilter();
			if (currentFilter != null) {

				SurroundList<SurroundDataFilterExpression> expressions = currentFilter.getExpressions();

				for (SurroundDataFilterExpression expression : expressions.getChildren()) {

					String field = expression.getField();
					String value = expression.getValue();

					TermQueryBuilder fieldQuery = QueryBuilders.termQuery(field, value);
					fieldFilters.add(fieldQuery);
				}
			}
		} catch (Exception e1) {
		}

		return fieldFilters;
	}

	@Override
	public void findByQuery(final SurroundDataQuery<M> query, final ISurroundFindListener<M> listener)
			throws IOException {

		SurroundReference<M> modelCoordinate = query.getModelCoordinate();

		String index = modelCoordinate.getIndex();
		String type = modelCoordinate.getType();

		long offset = query.getOffset();
		int pageSize = query.getPageSize();

		SearchRequestBuilder searchRequest = getDataSource().getClient().prepareSearch(index).setTypes(type)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setFrom((int) offset).setSize(pageSize);

		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

		List<TermQueryBuilder> fieldQueries = fieldQueries(query);

		fulltextQuery(query);

		SimpleQueryStringBuilder fulltextQuery = fulltextQuery(query);
		// boolQuery.filter(fulltextQuery);

		SurroundTimeRange curentRange = query.getTimeRange();
		RangeQueryBuilder timeRangeQuery = timeRangeQuery(query);

		boolQuery.filter(timeRangeQuery);
		searchRequest.setQuery(boolQuery);

		for (SurroundDataOrderBy orderBy : query.getSortingList().getChildren()) {
			String sort4column = orderBy.getField();
			searchRequest.addSort(sort4column, orderBy.isAscending() ? SortOrder.ASC : SortOrder.DESC);
		}

		// execute query
		try

		{
			final ListenableActionFuture actionFuture = searchRequest.execute();
			actionFuture.addListener(new ActionListener<SearchResponse>() {

				@Override
				public void onResponse(SearchResponse searchResponse) {

					SurroundList<M> result = new SurroundList<>("result");

					SearchHits resultHits = searchResponse.getHits();

					for (SearchHit hit : resultHits) {
						try {
							M model = query.getModelCreator().create();

							String jsonStr = hit.getSourceAsString();
							model.fromJSON(jsonStr);

							String dbId = hit.getId();
							model.getModelCoordinate(model).setKey(dbId);

							result.add(model);

						} catch (Exception e) {
							SurroundLog.log(e);
						}
					}

					listener.onSuccess(result);
				}

				@Override
				public void onFailure(Exception e) {
					listener.onError(e.getMessage());
				}
			});

		} catch (Exception e) {

			throw new SurroundDataException("error on findby query", e);
		}
	}

	private RangeQueryBuilder timeRangeQuery(final SurroundDataQuery<M> query) {

		SurroundTimeRange timeRange = query.getTimeRange();

		SurroundTimestamp start = timeRange.getStart();
		SurroundTimestamp end = timeRange.getEnd();

		RangeQueryBuilder timeRangeFilter = null;
		if (!start.isNull() || !end.isNull()) {
			String timeField = query.getTimeField();

			timeRangeFilter = QueryBuilders.rangeQuery(timeField);

			if (!start.isNull()) {
				timeRangeFilter.gte(start.asLong());
			}
			if (!end.isNull()) {
				timeRangeFilter.lte(end.asLong());
			}
		}
		return timeRangeFilter;
	}

	@Override
	public void bulkInsert(final SurroundList<M> models, final ISurroundBulkListener<M> listener) throws IOException {

		BulkRequestBuilder bulk = getDataSource().getClient().prepareBulk();
		for (Object iModel : models.getChildren()) {
			M newModel = (M) iModel;
			IndexRequestBuilder indexBuilder = null;

			SurroundReference<M> modelCoordinate = newModel.getModelCoordinate(newModel);
			if (modelCoordinate == null) {
				throw new SurroundNotInitializedException("no model coordinate given");
			}
			String index = modelCoordinate.getIndex();
			String type = modelCoordinate.getType();
			String dbId = modelCoordinate.getKey();

			if (!dbId.isEmpty()) {
				indexBuilder = getDataSource().getClient().prepareIndex(index, type, dbId);
			} else {
				indexBuilder = getDataSource().getClient().prepareIndex(index, type);
			}

			indexBuilder.setSource(newModel.asJSON());

			bulk.add(indexBuilder);
		}
		ListenableActionFuture<BulkResponse> actionFuture = bulk.execute();
		actionFuture.addListener(new ActionListener<BulkResponse>() {

			@Override
			public void onResponse(BulkResponse arg0) {
				if (listener != null) {
					listener.onSuccess(models);
				}
			}

			@Override
			public void onFailure(Exception e) {
				if (listener != null) {
					listener.onError(e.getMessage());
				}
			}
		});
	}

	@Override
	public void bulkUpdate(final SurroundList<M> models, final ISurroundBulkListener<M> listener) throws IOException {

		BulkRequestBuilder bulk = getDataSource().getClient().prepareBulk();
		for (Object iModel : models.getChildren()) {
			M model = (M) iModel;

			String jsonStr = model.asJSON();

			SurroundReference<M> modelCoordinate = model.getModelCoordinate(model);
			if (modelCoordinate == null) {
				throw new SurroundNotInitializedException("no model coordinate given");
			}
			String index = modelCoordinate.getIndex();
			String type = modelCoordinate.getType();
			String dbId = modelCoordinate.getKey();

			UpdateRequestBuilder updateRequestBuilder = getDataSource().getClient().prepareUpdate(index, type, dbId)
					.setDoc(jsonStr);
			bulk.add(updateRequestBuilder);
		}
		ListenableActionFuture<BulkResponse> actionFuture = bulk.execute();
		actionFuture.addListener(new ActionListener<BulkResponse>() {

			@Override
			public void onResponse(BulkResponse arg0) {
				if (listener != null) {
					listener.onSuccess(models);
				}
			}

			@Override
			public void onFailure(Exception e) {
				if (listener != null) {
					listener.onError(e.getMessage());
				}
			}
		});
	}

	public void refreshIndex(String index) {

		getDataSource().getClient().admin().indices().prepareRefresh(index);
	}

}
