package de.instanttouch.surround.data.elasticsearch.impl;

import java.io.IOException;

import org.elasticsearch.client.Client;

import de.instanttouch.surround.data.model.SurroundData;
import de.instanttouch.surround.data.model.SurroundDataSource;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundElasticsearchDataSource extends SurroundDataSource {

	private Client client;

	public SurroundElasticsearchDataSource() {
		super();
	}

	public SurroundElasticsearchDataSource(String name) {
		super(name);
	}
	public <M extends SurroundModel> SurroundElasticsearchData<M> createData() {

		SurroundElasticsearchData<M> data = new SurroundElasticsearchData<>();
		data.setDataSource(this);

		return data;
	}

	Client getClient() {
		return client;
	}

	void setClient(Client client) {
		this.client = client;
	}

	@Override
	public void shutdown() throws IOException {

		try {
			if (client != null) {
				client.close();
			}
		} catch (Exception e) {
			SurroundLog.log(e);
		}
	}
}
