package de.instanttouch.surround.data.orientdb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.orientechnologies.orient.core.command.OCommandRequest;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.index.OPropertyIndexDefinition;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchemaProxy;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.server.OServer;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientEdge;
import com.tinkerpop.blueprints.impls.orient.OrientEdgeType;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import com.tinkerpop.blueprints.impls.orient.OrientVertexType;

import de.instanttouch.surround.data.exception.SurroundDataException;
import de.instanttouch.surround.data.model.SurroundData;
import de.instanttouch.surround.data.query.SurroundDataQuery;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundByteArray;
import de.instanttouch.surround.model.base.SurroundDouble;
import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundStringMap;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundNotFoundException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;
import de.instanttouch.surround.model.exception.SurroundWrongTypeException;
import de.instanttouch.surround.model.graph.SurroundNode;
import de.instanttouch.surround.model.graph.SurroundNode.ISurroundNodeCreator;
import de.instanttouch.surround.model.graph.SurroundRelationship;
import de.instanttouch.surround.model.io.SurroundJSONReader;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.time.SurroundDate;
import de.instanttouch.surround.model.time.SurroundTimestamp;
import de.instanttouch.surround.model.url.SurroundURL;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundOrientDBData<M extends SurroundModel> extends SurroundData<M> {

	public static final String ORIENTDB_DAO = "de.ids.snake.db.orientdb.dao";

	public class SurroundNode4OrientDBCreator implements ISurroundNodeCreator {

		@Override
		public SurroundNode create(Object source) {

			if (!(source instanceof Vertex)) {
				throw new SurroundWrongTypeException("invalid type: " + source.getClass().getName());
			}

			try {
				Vertex vertex = (Vertex) source;
				SurroundNode surroundNode = new SurroundNode(vertex.getProperty("name"));
				propsFromVertex(surroundNode, vertex);

				return surroundNode;

			} catch (MalformedURLException e) {
				throw new SurroundDataException("failed reading properties from Vertex: ", e);
			}
		}
	}

	private OServer server;

	public SurroundOrientDBData() {
		super("orientdb");
	}

	public SurroundOrientDBData(String name) {
		super(name);
	}

	@Override
	public SurroundOrientDBDataSource getDataSource() {
		return (SurroundOrientDBDataSource) super.getDataSource();
	}

	@Override
	public SurroundOrientDBData close() throws IOException {

		getDataSource().getGraph().shutdown();
		if (server != null) {
			server.shutdown();
		}

		return this;
	}

	@Override
	public void create(final M newModel, ISurroundDataListener<M> listener) throws IOException {

		SurroundReference<M> modelCoordinate = newModel.getModelCoordinate(newModel);
		if (modelCoordinate == null) {
			throw new SurroundNotInitializedException("no model coordinate given");
		}
		String modelType = modelCoordinate.getType();

		OrientVertexType vertexType = getOrCreateVertexType(newModel);

		OrientGraph graph = getDataSource().getGraph();

		try {

			OrientVertex newVertex = graph.addVertex("class:" + modelType);
			for (SurroundBase child : newModel.getChildren()) {
				String prop = child.getName();
				try {
					newVertex.setProperty(prop, child.toString());
				} catch (Exception e) {
					SurroundLog.log(e);
				}
			}
			newVertex.save();

			// commit and get id
			graph.commit();

			Object id = newVertex.getId();
			modelCoordinate.setKey(id.toString());
			listener.onSuccess(newModel);

		} catch (Exception e) {
			graph.rollback();
			listener.onError(e.getMessage());
		}

	}

	private OrientVertexType getOrCreateVertexType(SurroundModel model) {

		String modelType = model.getModelCoordinate(model).getType();
		if (modelType == null || modelType.isEmpty()) {
			return null;
		}

		OrientGraph graph = getDataSource().getGraph();

		OrientVertexType vertexType = null;
		try {
			vertexType = graph.getVertexType(modelType);
			if (vertexType == null) {

				vertexType = graph.createVertexType(modelType);

				List<String> props = new ArrayList<String>();
				for (SurroundBase child : model.getChildren()) {
					String prop = child.getName();
					props.add(prop);

					OType dbType = determineOType(child);

					vertexType.createProperty(child.getName(), dbType);
				}
				// OIndex<?> index = vertexType.createIndex(modelType
				// + "Index", "SBTREE");

			}

		} catch (Exception e) {
			throw new SurroundDataException("error getting Vertex Type", e);
		}

		return vertexType;
	}

	private OType determineOType(SurroundBase child) {

		if (child instanceof SurroundByteArray) {
			return OType.BINARY;
		}

		if (child instanceof SurroundBoolean) {
			return OType.BOOLEAN;
		}

		// if (child instanceof SurroundMap) {
		// return OType.EMBEDDEDMAP;
		// }
		//
		// if (child instanceof SurroundList) {
		// return OType.EMBEDDEDLIST;
		// }
		//
		// if (child instanceof SurroundEntity) {
		// return OType.EMBEDDED;
		// }

		if (child instanceof SurroundTimestamp) {
			return OType.DATETIME;
		}

		if (child instanceof SurroundDate) {
			return OType.DATE;
		}

		if (child instanceof SurroundInteger) {
			return OType.INTEGER;
		}

		if (child instanceof SurroundDouble) {
			return OType.DOUBLE;
		}

		return OType.STRING;
	}

	@Override
	public void read(final M model, ISurroundDataListener<M> listener) throws IOException {

		OrientGraph graph = getDataSource().getGraph();

		try {
			String dbId = model.getModelCoordinate(model).getKey();

			OrientVertex vertex = graph.getVertex(dbId);
			if (vertex == null) {
				throw new SurroundNotFoundException("unknown vertex: " + dbId);
			}

			for (String key : vertex.getPropertyKeys()) {
				String value = vertex.getProperty(key);

				SurroundBase child = model.findByName(key);
				if (child != null) {
					child.set(value);
				}
			}

			graph.commit();
			listener.onSuccess(model);

		} catch (Exception e) {
			graph.rollback();
			listener.onError(e.getMessage());
		}
	}

	@Override
	public void update(final M model, ISurroundDataListener<M> listener) throws IOException {

		OrientGraph graph = getDataSource().getGraph();

		try {
			String dbId = model.getModelCoordinate(model).getKey();

			OrientVertex vertex = graph.getVertex(dbId);
			if (vertex == null) {
				throw new SurroundNotFoundException("unknown vertex: " + dbId);
			}

			for (SurroundBase child : model.getChildren()) {
				vertex.setProperty(child.getName(), child.toString());
			}
			vertex.save();

			graph.commit();
			listener.onSuccess(model);

		} catch (Exception e) {
			graph.rollback();
			listener.onError(e.getMessage());
		}
	}

	@Override
	public void delete(final M model, ISurroundDataListener<M> listener) throws IOException {

		OrientGraph graph = getDataSource().getGraph();

		try {
			String dbId = model.getModelCoordinate(model).getKey();

			OrientVertex vertex = graph.getVertex(dbId);
			if (vertex == null) {
				throw new SurroundNotFoundException("unknown vertex: " + dbId);
			}

			graph.removeVertex(vertex);

			graph.commit();
			listener.onSuccess(model);

		} catch (Exception e) {
			graph.rollback();
			listener.onError(e.getMessage());
		}
	}

	@Override
	public void findByQuery(final SurroundDataQuery<M> query, final ISurroundFindListener<M> result)
			throws IOException {

	}

	public OrientVertex findOrCreateNode(SurroundNode SurroundNode) throws IOException {

		OrientVertex vertex = findNode(SurroundNode);
		if (vertex == null) {
			vertex = createNode(SurroundNode);
		}

		return vertex;
	}

	public OrientVertex createNode(SurroundNode surroundNode) throws IOException {

		OrientVertex newVertex = null;

		SurroundReference<SurroundNode> modelCoordinate = surroundNode.getModelCoordinate(surroundNode);
		String modelType = modelCoordinate.getType();
		if (modelType == null) {
			throw new SurroundNotInitializedException("modelType is not valid");
		}

		// TODO
		// OrientVertexType vertexType = getOrCreateVertexType(SurroundNode);

		OrientGraph graph = getDataSource().getGraph();
		newVertex = graph.addVertex("class:" + modelType);

		SurroundURL url = surroundNode.getModelCoordinate(surroundNode).getURL();
		if (url != null) {
			newVertex.setProperty("modelURL", url.toString());
		}

		String name = surroundNode.getName();
		if (name != null) {
			newVertex.setProperty("name", name);
		}

		for (SurroundString key : surroundNode.getProperties().keySet()) {
			SurroundBase prop = surroundNode.getProperties().get(key);
			newVertex.setProperty(key.toString(), prop.toString());
		}
		newVertex.save();

		// commit and get id
		graph.commit();

		Object id = newVertex.getId();
		modelCoordinate.setKey(id.toString());

		return newVertex;
	}

	public OrientVertex findNode(SurroundNode node) throws IOException {

		OrientVertex vertex = null;

		String dbId = node.getModelCoordinate(node).getKey();

		if (dbId != null && dbId.length() > 0) {
			OrientGraph graph = getDataSource().getGraph();
			vertex = graph.getVertex(dbId);

			StringBuilder builder = new StringBuilder();
			vertex.getIdentity().toString(builder);

			propsFromVertex(node, vertex);
		} else {

			SurroundURL url = node.getModelCoordinate(node).getURL();
			if (url != null) {

				OrientGraph graph = getDataSource().getGraph();
				String urlStr = url.toString();

				Iterable<Vertex> vertices = graph.getVertices("modelPath", url);
				if (vertices.iterator().hasNext()) {

					Vertex checkVertex = vertices.iterator().next();
					if (checkVertex instanceof OrientVertex) {

						vertex = (OrientVertex) checkVertex;
						node.getModelCoordinate(node).setKey(vertex.getIdentity().toString());
						propsFromVertex(node, vertex);
					}
				}
			}
		}

		return vertex;
	}

	public void propsFromVertex(SurroundNode node, Vertex vertex) throws MalformedURLException {

		OrientVertex orientVertex = (OrientVertex) vertex;
		String label = orientVertex.getLabel();

		String name = vertex.getProperty("name");
		if (name != null) {
			node.setName(name);
		}

		Object modelPath = vertex.getProperty("modelPath");
		String urlString = modelPath.toString();
		node.getModelCoordinate(node).getURL().fromURL(urlString);

		SurroundStringMap<SurroundBase> SurroundProperties = node.getProperties();
		for (String key : vertex.getPropertyKeys()) {

			Object prop = vertex.getProperty(key);

			SurroundString SurroundKey = new SurroundString("key").set(key);
			SurroundString SurroundValue = new SurroundString("value").set(prop.toString());
			SurroundBase SurroundBase = SurroundProperties.put(SurroundKey, SurroundValue);
		}
	}

	public OrientVertex updateNode(SurroundNode node) throws IOException {

		OrientVertex vertex = null;

		vertex = findNode(node);
		if (vertex != null) {

			OrientGraph graph = getDataSource().getGraph();

			SurroundURL url = node.getModelCoordinate(node).getURL();
			if (url != null) {
				vertex.setProperty("modelPath", url.toString());
			}

			for (SurroundString key : node.getProperties().keySet()) {
				SurroundBase prop = node.getProperties().get(key);
				vertex.setProperty(key.toString(), prop.toString());
			}
			vertex.save();
			graph.commit();

		} else {
			vertex = createNode(node);
		}

		return vertex;
	}

	private OrientEdgeType getOrCreateEdgeType(SurroundModel model) {

		String modelType = model.getModelCoordinate(model).getType();

		OrientGraph graph = getDataSource().getGraph();

		OrientEdgeType edgeType = null;
		try {
			edgeType = graph.getEdgeType(modelType);
			if (edgeType == null) {

				edgeType = graph.createEdgeType(modelType);

				List<String> props = new ArrayList<String>();
				for (SurroundBase child : model.getChildren()) {
					String prop = child.getName();
					props.add(prop);

					OType dbType = determineOType(child);

					edgeType.createProperty(child.getName(), dbType);
				}
				// OIndex<?> index = vertexType.createIndex(modelType
				// + "Index", "SBTREE");

			}

		} catch (Exception e) {
			throw new SurroundDataException(e.getMessage());
		}

		return edgeType;
	}

	public OrientEdge createRelationShip(SurroundRelationship relationShip, SurroundNode from, SurroundNode to)
			throws IOException {

		OrientEdge newEdge = null;

		SurroundReference<SurroundRelationship> modelCoordinate = relationShip.getModelCoordinate(relationShip);
		String modelType = modelCoordinate.getType();

		// TODO
		// OrientEdgeType edgeType = getOrCreateEdgeType(SurroundRelationShip);

		OrientGraph graph = getDataSource().getGraph();

		OrientVertex startVertex = findOrCreateNode(from);
		OrientVertex endVertex = findOrCreateNode(to);

		try {

			newEdge = graph.addEdge("class:" + modelType, startVertex, endVertex, relationShip.getName());
			for (SurroundString key : relationShip.getProperties().keySet()) {
				SurroundBase prop = relationShip.getProperties().get(key);
				newEdge.setProperty(key.toString(), prop.toString());
			}
			newEdge.save();

			// commit and get id
			graph.commit();

			Object id = newEdge.getId();
			modelCoordinate.setKey(id.toString());

		} catch (Exception e) {
			graph.rollback();
		}

		return newEdge;
	}

	public OrientEdge findRelationShip(SurroundRelationship surroundRelationShip) throws IOException {

		OrientEdge edge = null;

		String dbId = surroundRelationShip.getModelCoordinate(surroundRelationShip).getKey();

		if (dbId != null && dbId.length() > 0) {
			OrientGraph graph = getDataSource().getGraph();
			edge = graph.getEdge(dbId);

			StringBuilder builder = new StringBuilder();
			edge.getIdentity().toString(builder);

			for (String key : edge.getPropertyKeys()) {

				Object prop = edge.getProperty(key);

				SurroundString SurroundKey = new SurroundString("key").set(key);
				SurroundBase SurroundBase = surroundRelationShip.getProperties().get(SurroundKey);
				if (SurroundBase != null) {
					SurroundBase.set(prop.toString());
				}
			}
		}
		return edge;
	}

	public OrientEdge findOrCreateRelationShip(SurroundRelationship relationShip) throws IOException {

		OrientEdge edge = findRelationShip(relationShip);
		if (edge == null) {

			throw new SurroundNotYetImplementedException("determine node by url");

			//
			// SurroundNode source = relationShip.getStartNode();
			// SurroundNode target = relationShip.getEndNode();
			//
			// edge = createRelationShip(relationShip, source, target);
		}

		return edge;
	}

	public void resolveIncomingRelationShips(SurroundNode surroundNode, ISurroundNodeCreator nodeCreator)
			throws IOException {

		OrientVertex vertex = findNode(surroundNode);
		if (vertex == null) {
			throw new SurroundNotFoundException(
					"node: " + surroundNode.getModelCoordinate(surroundNode) + " was not found in graph");
		}

		for (Edge edge : vertex.getEdges(Direction.IN)) {

			Vertex source = edge.getVertex(Direction.OUT);

			SurroundNode sourceNode = nodeCreator.create(source);
			sourceNode.getModelCoordinate(surroundNode).setKey(source.getId().toString());

			propsFromVertex(sourceNode, source);

			Object url = source.getProperty("modelPath");
			sourceNode.getModelCoordinate(surroundNode).getURL().fromURL(url.toString());

			SurroundRelationship relationship = SurroundRelationship.create(sourceNode, surroundNode, "parent");
			relationship.getModelCoordinate(surroundNode).setKey(edge.getId().toString());

			surroundNode.addRelationShip(relationship);
			sourceNode.addRelationShip(relationship);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.instanttouch.surround.data.orientdb.ISurroundGraphData#
	 * resolveOutgoingRelationShips(de.instanttouch.surround.graph.model.
	 * SurroundNode,
	 * de.instanttouch.surround.graph.model.SurroundNode.ISurroundNodeCreator)
	 */
	public void resolveOutgoingRelationShips(SurroundNode surroundNode, ISurroundNodeCreator nodeCreator)
			throws IOException {

		OrientVertex vertex = findNode(surroundNode);
		if (vertex == null) {
			throw new SurroundNotFoundException(
					"node: " + surroundNode.getModelCoordinate(surroundNode) + " was not found in graph");
		}

		for (Edge edge : vertex.getEdges(Direction.OUT)) {
			Vertex target = edge.getVertex(Direction.OUT);

			SurroundNode targetNode = nodeCreator.create(target);
			targetNode.getModelCoordinate(surroundNode).setKey(target.getId().toString());

			Object url = target.getProperty("modelPath");
			targetNode.getModelCoordinate(surroundNode).getURL().fromURL(url.toString());

			propsFromVertex(surroundNode, target);

			SurroundRelationship relationship = SurroundRelationship.create(surroundNode, targetNode, "parent");
			relationship.getModelCoordinate(surroundNode).setKey(edge.getId().toString());

			surroundNode.addRelationShip(relationship);
			targetNode.addRelationShip(relationship);
		}
	}

	public void updateProperties(SurroundNode SurroundNode) throws IOException {

		OrientVertex vertex = findNode(SurroundNode);

		for (SurroundString key : SurroundNode.getProperties().keySet()) {
			SurroundBase prop = SurroundNode.getProperties().get(key);
			vertex.setProperty(key.toString(), prop.toString());
		}
		vertex.save();
	}

	public List<SurroundNode> findNodesByProperty(String modelType, String prop, String value,
			ISurroundNodeCreator nodeCreator) throws MalformedURLException {

		List<SurroundNode> nodes = new ArrayList<>();

		OrientGraph graph = getDataSource().getGraph();

		// Iterable<Vertex> vertices = graph.getVertices(modelType, new String[]
		// { prop }, new String[] { value });
		//
		// Iterator<Vertex> verticesIterator = vertices.iterator();
		// SurroundOrientDbNodeIterable SurroundNodeIterable =
		// createSurroundIterable(nodeCreator, verticesIterator);

		String sql = "select * from V where IPID like '" + value + "'";
		OCommandRequest commandRequest = graph.command(new OCommandSQL(sql));
		Iterable<Vertex> verticesIterable = commandRequest.execute();

		int index = 0;
		for (Vertex vertex : verticesIterable) {
			if (vertex != null) {
				if (index >= 500) {
					break;
				}

				SurroundNode surroundNode = nodeCreator.create(vertex);

				surroundNode.getModelCoordinate(surroundNode).setKey(vertex.getId().toString());
				Object url = vertex.getProperty("modelPath");
				surroundNode.getModelCoordinate(surroundNode).getURL().fromURL(url.toString());

				propsFromVertex(surroundNode, vertex);

				nodes.add(surroundNode);
				index++;
			}
		}

		return nodes;
	}

	public class NodeIterator {

		Iterator<Vertex> vertices;
		ISurroundNodeCreator nodeCreator;

		NodeIterator(Iterator<Vertex> vertices, ISurroundNodeCreator nodeCreator) {
			this.vertices = vertices;
			this.nodeCreator = nodeCreator;
		}

		public boolean hasNext() {
			return vertices.hasNext();
		}

		public SurroundNode next() {
			OrientGraph graphDb = getDataSource().getGraph();

			SurroundNode node = nodeCreator.create(null);
			try {
				Vertex vertex = vertices.next();
				if (vertex != null) {
					String url = vertex.getProperty("url").toString();
					node.getModelCoordinate(node).getURL().set(url);
					String jsonStr = vertex.getProperty("content");
					if (jsonStr != null) {
						SurroundJSONReader jsonReader = new SurroundJSONReader(jsonStr.toString());
						try {
							Object jsonObject = jsonReader.parse();
							jsonReader.read(node, jsonObject);
						} catch (Exception e) {
							graphDb.rollback();
							throw new SurroundDataException("couldn't read data");
						}
					}
				}
				graphDb.commit();
			} finally {
			}
			return node;
		}
	}

	public NodeIterator allNodes(ISurroundNodeCreator nodeCreator) throws MalformedURLException {

		OrientGraph graph = getDataSource().getGraph();
		Iterable<Vertex> vertices = graph.getVertices();

		int index = 0;
		for (Vertex vertex : vertices) {

			SurroundNode surroundNode = nodeCreator.create(vertex);
			surroundNode.getModelCoordinate(surroundNode).setKey(vertex.getId().toString());

			propsFromVertex(surroundNode, vertex);

			Object url = vertex.getProperty("modelPath");
			surroundNode.getModelCoordinate(surroundNode).getURL().fromURL(url.toString());

			if (index >= 100) {
				break;
			}
		}

		return null;
	}

	public void create_vertex_ci_index(String classname, String key) {

		OrientGraph graph = getDataSource().getGraph();

		OPropertyIndexDefinition index = new OPropertyIndexDefinition(classname, key, OType.STRING);
		index.setCollate("ci");

		ODatabaseDocumentTx db = graph.getRawGraph();
		OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OSchemaProxy schema = db.getMetadata().getSchema();
		OClass cls = schema.getOrCreateClass(classname, schema.getClass("V"));
		String indexName = classname + "." + key;
		ODocument metadata = new ODocument();
		im.createIndex(indexName, "UNIQUE_HASH_INDEX", index, cls.getPolymorphicClusterIds(), null, metadata);
	}

	public void create_vertex_index(String classname, String key) {

		OrientGraph graph = getDataSource().getGraph();

		final Parameter<?, ?> uniqueIndex = new Parameter<String, String>("type", "UNIQUE_HASH_INDEX");
		final Parameter<?, ?> indexClass = new Parameter<String, String>("class", classname);

		graph.createKeyIndex(key, Vertex.class, indexClass, uniqueIndex);
	}

	public void createIndex(String modelType, String key, String... fields) {

		OrientGraph graph = getDataSource().getGraph();

		final Parameter<?, ?> uniqueIndex = new Parameter<String, String>("type", "UNIQUE_HASH_INDEX");
		final Parameter<?, ?> indexClass = new Parameter<String, String>("class", modelType);

		String indexName = modelType + "." + key;

		Parameter[] params = new Parameter[fields.length + 2];
		params[0] = uniqueIndex;
		params[0] = indexClass;
		for (int i = 0; i < fields.length; i++) {
			String field = fields[i];
			Parameter<String, String> parameter = new Parameter<String, String>("class", field);
			params[i + 2] = parameter;
		}

		graph.createIndex(indexName, Vertex.class, params);
	}

}
