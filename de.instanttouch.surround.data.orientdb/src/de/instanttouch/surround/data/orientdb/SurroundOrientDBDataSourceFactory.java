package de.instanttouch.surround.data.orientdb;

import java.io.IOException;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

import de.instanttouch.surround.data.exception.SurroundDataException;
import de.instanttouch.surround.data.model.SurroundDataFactory.IDataSourceFactory;
import de.instanttouch.surround.data.model.SurroundDataReference;
import de.instanttouch.surround.model.util.SurroundRuntimeTools;

public class SurroundOrientDBDataSourceFactory implements IDataSourceFactory {

	public SurroundOrientDBDataSourceFactory() {
		super();
	}

	@Override
	public SurroundOrientDBDataSource createDataSource(SurroundDataReference<?> connection) throws IOException {

		SurroundOrientDBDataSource dataSource = new SurroundOrientDBDataSource("orientDbdataSource");

		ODatabaseDocumentTx odb = null;
		if (connection.isEmbedded()) {
			try {

				String path = "plocal:/" + SurroundRuntimeTools.getRuntimeLocation();
				path = path.replaceAll("\\\\", "/");
				path += "/orientdb/";

				odb = new ODatabaseDocumentTx(path);
				if (!odb.exists()) {
					odb.create();
				}

			} catch (Exception e) {
				throw new SurroundDataException("error opening orient db", e);
			}
		} else {

			String dbUrl = "remote:/localhost/" + connection.getDataStoreName().toString();
			odb = new ODatabaseDocumentTx(dbUrl);
		}

		odb.open("admin", "admin");
		OrientGraph graph = new OrientGraph(odb);

		dataSource.setConnection(connection);
		dataSource.setGraph(graph);

		return dataSource;
	}

}
