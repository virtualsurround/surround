package de.instanttouch.surround.data.orientdb;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

import de.instanttouch.surround.data.model.SurroundData;
import de.instanttouch.surround.data.model.SurroundDataSource;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;

public class SurroundOrientDBDataSource extends SurroundDataSource {

	protected OrientGraph graph;

	public SurroundOrientDBDataSource() {
		super();
	}

	public SurroundOrientDBDataSource(String name) {
		super(name);
	}
	
	

	public OrientGraph getGraph() {
		return graph;
	}

	public void setGraph(OrientGraph graph) {
		this.graph = graph;
	}

	public ODatabaseDocumentTx getDocumentDB() {

		if (graph == null) {
			throw new SurroundNotInitializedException("graph db not initialized");
		}

		ODatabaseDocumentTx rawGraph = graph.getRawGraph();

		return rawGraph;
	}

	@Override
	public <M extends SurroundModel> SurroundData<M> createData() {
		return new SurroundOrientDBData<>();
	}
}
