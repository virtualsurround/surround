package de.instanttouch.surround.data.exception;

import de.instanttouch.surround.model.exception.SurroundBaseException;

@SuppressWarnings("serial")
public class SurroundDataException extends SurroundBaseException {

	public SurroundDataException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SurroundDataException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
