package de.instanttouch.surround.data.model;

import java.io.IOException;

import de.instanttouch.surround.data.Activator;
import de.instanttouch.surround.data.model.SurroundData.ISurroundDataListener;
import de.instanttouch.surround.data.model.SurroundData.ISurroundFindListener;
import de.instanttouch.surround.data.query.SurroundDataQuery;
import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundStringList;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundNotFoundException;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundNotYetImplementedException;
import de.instanttouch.surround.model.ref.SurroundReference;

public class SurroundDataReference<M extends SurroundModel> extends SurroundReference<M> {

	public SurroundDataReference() {
		super();
		initDataReference();
	}

	public SurroundDataReference(String name) {
		super(name);
		initDataReference();
	}

	private void initDataReference() {
		setProtocol("surroundDB");
	}

	@Override
	public M resolve() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SurroundDataReference<M> buildURL() {

		String urlStr = getProtocol() + "://";

		if (getHost() != null && getHost().length() > 0) {
			urlStr += getHost();
		}
		if (getPort() > 0) {
			urlStr += ":" + getPort();
		}

		if (getCluster() != null && getCluster().length() > 0) {
			urlStr += "/" + getCluster();
		}
		if (getType() != null && getType().length() > 0) {
			urlStr += "/" + getType();
		}

		if (getKey() != null) {
			urlStr += "#" + getKey();
		} else if (getQuery() != null) {
			urlStr += "#" + getQuery();
		}

		getURL().set(urlStr);
		return this;
	}

	public boolean isEmbedded() {
		return findByName("embedded") != null ? findByName("embedded").asBoolean() : false;
	}

	public SurroundDataReference<M> setEmbedded(boolean embedded) {
		replace("embedded", new SurroundBoolean("embedded").set(embedded));
		buildURL();
		return this;
	}

	public boolean isInMemory() {
		return findByName("inMemory") != null ? findByName("inMemory").asBoolean() : false;
	}

	public SurroundDataReference<M> setInMemory(boolean inMemory) {
		replace("inMemory", new SurroundBoolean("inMemory").set(inMemory));
		buildURL();
		return this;
	}

	public SurroundStringList getClusterNodes() {
		return (SurroundStringList) findByName("clusterNodes");
	}

	public SurroundDataReference<M> setClusterNodes(SurroundStringList clusterNodes) {
		replace("clusterNodes", clusterNodes);
		buildURL();
		return this;
	}

	public String getDataStoreName() {
		return findByName("dbName").toString();
	}

	public SurroundDataReference<M> setDataStoreName(String dbName) {
		SurroundBase databaseName = new SurroundString("dbName").set(dbName);
		replace("dbName", databaseName);
		buildURL();
		return this;
	}

	public SurroundDataQuery<M> getQuery() {
		return (SurroundDataQuery<M>) findByName("query");
	}

	public SurroundDataReference<M> setQuery(SurroundDataQuery<M> query) {
		replace("query", query);
		buildURL();
		return this;
	}

	public SurroundDataReference<M> create(M model, ISurroundDataListener<M> listener) throws IOException {

		if (isFile()) {
			try {
				getFile().create(model);
				listener.onSuccess(model);
			} catch (Exception ex) {
				listener.onError(ex.getMessage());
			}
		} else {
			SurroundDataFactory dataFactory = Activator.getDefault().getDataFactory();
			SurroundData<M> surroundData = dataFactory.open(this);
			if (surroundData == null) {
				throw new SurroundNotFoundException("no data access for dataStore: " + getDataStoreName());
			}
			try {
				surroundData.create(model, listener);
			} finally {
				surroundData.close();
			}
		}

		return this;
	}

	public SurroundDataReference<M> read(M model, ISurroundDataListener<M> listener) throws IOException {

		if (isFile()) {
			try {
				getFile().read(model);
				listener.onSuccess(model);
			} catch (Exception ex) {
				listener.onError(ex.getMessage());
			}
		} else {
			SurroundDataFactory dataFactory = Activator.getDefault().getDataFactory();
			SurroundData<M> surroundData = dataFactory.open(this);
			if (surroundData == null) {
				throw new SurroundNotFoundException("no data access for dataStore: " + getDataStoreName());
			}
			try {
				surroundData.read(model, listener);
			} finally {
				surroundData.close();
			}
		}

		return this;
	}

	public SurroundDataReference<M> update(M model, ISurroundDataListener<M> listener) throws IOException {

		if (isFile()) {
			try {
				getFile().write(model);
				listener.onSuccess(model);
			} catch (Exception ex) {
				listener.onError(ex.getMessage());
			}
		} else {
			SurroundDataFactory dataFactory = Activator.getDefault().getDataFactory();
			SurroundData<M> surroundData = dataFactory.open(this);
			if (surroundData == null) {
				throw new SurroundNotFoundException("no data access for dataStore: " + getDataStoreName());
			}
			try {
				surroundData.update(model, listener);
			} finally {
				surroundData.close();
			}
		}

		return this;
	}

	public SurroundDataReference<M> delete(M model, ISurroundDataListener<M> listener) throws IOException {

		if (isFile()) {
			try {
				getFile().delete(model);
				listener.onSuccess(model);
			} catch (Exception ex) {
				listener.onError(ex.getMessage());
			}
		} else {
			SurroundDataFactory dataFactory = Activator.getDefault().getDataFactory();
			SurroundData<M> surroundData = dataFactory.open(this);
			if (surroundData == null) {
				throw new SurroundNotFoundException("no data access for dataStore: " + getDataStoreName());
			}

			try {
				surroundData.delete(model, listener);
			} finally {
				surroundData.close();
			}
		}

		return this;
	}

	public SurroundDataReference<M> find(ISurroundFindListener<M> result) throws IOException {

		if (getQuery() == null) {
			throw new SurroundNotInitializedException("no query set");
		}

		if (isFile()) {
			throw new SurroundNotYetImplementedException("search file in fileset has to be implemented");
		} else {

			SurroundDataFactory dataFactory = Activator.getDefault().getDataFactory();
			SurroundData<M> surroundData = dataFactory.open(this);
			if (surroundData == null) {
				throw new SurroundNotFoundException("no data access for dataStore: " + getDataStoreName());
			}
			try {
				surroundData.findByQuery(getQuery(), result);
			} finally {
				surroundData.close();
			}
		}

		return this;
	}

}
