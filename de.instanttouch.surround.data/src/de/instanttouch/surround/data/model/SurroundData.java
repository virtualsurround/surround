/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.data.model;

import java.io.IOException;

import de.instanttouch.surround.data.exception.SurroundDataException;
import de.instanttouch.surround.data.query.SurroundDataQuery;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundNotInitializedException;
import de.instanttouch.surround.model.exception.SurroundUnsupportedException;

public class SurroundData<M extends SurroundModel> extends SurroundDynamicEntity {

	public interface ISurroundDataListener<M extends SurroundModel> {

		public void onSuccess(M result);

		public void onError(String errorStr);
	}

	public interface ISurroundFindListener<M extends SurroundModel> {

		public void onSuccess(SurroundList<M> result);

		public void onError(String errorStr);
	}

	public interface ISurroundBulkListener<M extends SurroundModel> {

		public void onSuccess(SurroundList<M> result);

		public void onError(String errorStr);
	}
	
	protected SurroundDataSource dataSource;

	public SurroundData() {
		super();
		initData();
	}

	public SurroundData(String name) {
		super(name);
		initData();
	}

	private void initData() {
	}

	public SurroundDataSource createDatasource(SurroundDataReference<?> connection) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

	public SurroundDataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(SurroundDataSource dataSource) {
		this.dataSource = dataSource;
	}

	public SurroundData<M> close() throws IOException {

		if (dataSource == null) {
			throw new SurroundNotInitializedException("already closed");
		}

		dataSource.close(this);
		return this;
	}
	
	protected void checkDataSource() {
		
		if(dataSource == null) {
			throw new SurroundDataException("no dataSource available");
		}
	}

	public SurroundDataReference<?> getConnection() {
		return dataSource != null ? dataSource.getConnection() : null;
	}

	public void create(M newModel, ISurroundDataListener<M> listener) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

	public void read(M model, ISurroundDataListener<M> listener) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

	public void update(M model, ISurroundDataListener<M> listener) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

	public void delete(M model, ISurroundDataListener<M> listener) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

	public void findByQuery(SurroundDataQuery<M> query, ISurroundFindListener<M> result) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

	public void bulkInsert(SurroundList<M> models, ISurroundBulkListener<M> listener) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

	public void bulkUpdate(SurroundList<M> models, ISurroundBulkListener<M> listener) throws IOException {
		throw new SurroundUnsupportedException("implement in derived dao");
	}

}
