/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.data.model;

import java.io.IOException;

import de.instanttouch.surround.data.model.SurroundDataFactory.SurroundConnectionPool;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.domain.SurroundModel;

public abstract class SurroundDataSource extends SurroundDynamicEntity {

	public abstract <M extends SurroundModel> SurroundData<M> createData();

	SurroundConnectionPool connectionPool;

	public SurroundDataSource() {
		super();
		initDaoSource();
	}

	public SurroundDataSource(String name) {
		super(name);
		initDaoSource();
	}

	private void initDaoSource() {
	}

	public SurroundConnectionPool getConnectionPool() {
		return connectionPool;
	}

	public SurroundDataReference<?> getConnection() {
		return (SurroundDataReference<?>) findByName("connection");
	}

	public SurroundDataSource setConnection(SurroundDataReference<?> connection) {
		replace("connection", connection);
		return this;
	}

	public void close(SurroundData<?> surroundData) {
		getConnectionPool().close(this);
		surroundData.setDataSource(null);
	}

	public void shutdown() throws IOException {

	}
}
