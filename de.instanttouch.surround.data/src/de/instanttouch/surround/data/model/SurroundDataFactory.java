package de.instanttouch.surround.data.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.instanttouch.surround.data.exception.SurroundDataException;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.exception.SurroundNotFoundException;
import de.instanttouch.surround.model.util.SurroundLog;

public class SurroundDataFactory extends SurroundDynamicEntity {

	Map<String, IDataSourceFactory> dataSourceMap = new HashMap<>();
	Map<String, SurroundConnectionPool> dataPoolMap = new HashMap<>();

	public interface IDataSourceFactory {

		public SurroundDataSource createDataSource(SurroundDataReference<?> dataReference) throws IOException;
	}

	public class SurroundConnection {

		SurroundDataSource dataSource;
		Boolean isBusy = false;

		public SurroundConnection(SurroundDataSource dataSource) {
			super();
			this.dataSource = dataSource;
		}

		public SurroundDataSource getDataSource() {
			return dataSource;
		}
	}

	public static class SurroundConnectionPool {

		protected List<SurroundConnection> connections = new ArrayList<>();

		public SurroundConnectionPool() {
			super();
		}

		protected SurroundConnectionPool addConnection(SurroundConnection connection) {

			connections.add(connection);
			return this;
		}

		public SurroundConnection open() {

			synchronized (connections) {

				for (SurroundConnection connection : connections) {
					if (!connection.isBusy) {
						connection.isBusy = true;
						return connection;
					}
				}
			}
			return null;
		}

		public SurroundConnectionPool close(SurroundDataSource dataSource) {

			synchronized (connections) {
				for (SurroundConnection connection : connections) {
					if (connection.dataSource == dataSource) {
						connection.isBusy = false;
						break;
					}
				}
			}
			return null;
		}

		public SurroundConnectionPool shutdown() {

			synchronized (connections) {
				for (SurroundConnection connection : connections) {
					try {
						connection.getDataSource().shutdown();
					} catch (IOException e) {
						SurroundLog.log("error on datasource shutdown", e);
					}
				}
			}
			return this;
		}
	}

	public SurroundDataFactory() {
		super();
		initFactory();
	}

	public SurroundDataFactory(String name) {
		super(name);
		initFactory();
	}

	private void initFactory() {
	}

	public <M extends SurroundModel> void registerDataStore(SurroundDataReference<M> dataReference,
			IDataSourceFactory dataSourceFactory, int poolSize) {

		try {
			dataSourceMap.put(dataReference.getDataStoreName(), dataSourceFactory);
			createDataProvider(dataReference, poolSize);
		} catch (Exception e) {
			throw new SurroundDataException("couldn't create datasource", e);
		}
	}

	public <M extends SurroundModel> void createDataProvider(SurroundDataReference<M> dataReference)
			throws SurroundBaseException, InstantiationException, IllegalAccessException, IOException {

		createDataProvider(dataReference, 5);
	}

	public <M extends SurroundModel> void createDataProvider(SurroundDataReference<M> dataReference, int poolSize)
			throws SurroundBaseException, InstantiationException, IllegalAccessException, IOException {

		String dataStoreName = dataReference.getDataStoreName();

		IDataSourceFactory dataSourceFactory = dataSourceMap.get(dataStoreName);
		if (dataSourceFactory == null) {
			throw new SurroundNotFoundException("no dataprovider for database: " + dataReference.getDataStoreName());
		}

		SurroundConnectionPool connectionPool = new SurroundConnectionPool();

		for (int i = 0; i < poolSize; i++) {

			SurroundDataSource dataSource = dataSourceFactory.createDataSource(dataReference);
			dataSource.connectionPool = connectionPool;

			connectionPool.addConnection(new SurroundConnection(dataSource));
		}

		dataPoolMap.put(dataStoreName, connectionPool);
	}

	public <M extends SurroundModel> SurroundData<M> open(SurroundDataReference<M> dataReference) {

		String dataStoreName = dataReference.getDataStoreName();
		SurroundConnectionPool connectionPool = dataPoolMap.get(dataStoreName);
		if (connectionPool == null) {
			throw new SurroundNotFoundException("no datastore factory for: " + dataStoreName);
		}

		SurroundConnection connection = connectionPool.open();
		if (connection == null || connection.getDataSource() == null) {
			throw new SurroundDataException("no datasource available for:" + dataStoreName);
		}
		return connection.getDataSource().createData();
	}

	public void closeConnections(String dataStoreName) {

		SurroundConnectionPool connectionPool = dataPoolMap.remove(dataStoreName);
		if (connectionPool != null) {
			connectionPool.shutdown();
		}
	}
}
