/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.data.query;

import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;

public class SurroundDataOrderBy extends SurroundDynamicEntity {

	private static final long serialVersionUID = 1L;

	public SurroundDataOrderBy() {
		super();
		initOrderBy();
	}

	public SurroundDataOrderBy(String name) {
		super(name);
		initOrderBy();
	}

	private void initOrderBy() {
		add(new SurroundString("field"));
		add(new SurroundBoolean("ascending"));
	}

	public boolean isAscending() {
		return findByName("ascending").asBoolean();
	}

	public SurroundDataOrderBy setAscending(boolean ascending) {
		((SurroundBoolean) findByName("ascending")).set(ascending);
		return this;
	}

	public String getField() {
		return findByName("field").toString();
	}

	public SurroundDataOrderBy setField(String fieldName) {
		findByName("field").set(fieldName);
		return this;
	}

}
