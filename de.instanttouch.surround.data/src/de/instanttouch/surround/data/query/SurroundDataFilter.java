/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.data.query;

import de.instanttouch.surround.model.base.SurroundBase;
import de.instanttouch.surround.model.base.SurroundBoolean;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.collection.SurroundOptions;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.exception.SurroundBaseException;
import de.instanttouch.surround.model.util.SurroundLog;

@SuppressWarnings("serial")
public class SurroundDataFilter<M extends SurroundModel> extends SurroundDynamicEntity {

	public class SurroundDataFilterExpression extends SurroundDynamicEntity {

		public SurroundDataFilterExpression() {
			super("expression");

			SurroundString field = new SurroundString("field");
			SurroundOptions<SurroundString> availableFields = new SurroundOptions<SurroundString>("availableFields");
			try {
				M model = modelCreator.create();
				if (model != null) {
					for (SurroundBase child : model.getConstraints().getVisibleChildren()) {
						availableFields.add(new SurroundString("field").set(child.getName()));
					}
				}
			} catch (Exception e) {
				SurroundLog.log(e);
			}
			field.getConstraints().setOptions(availableFields);
			add(field);

			add(new SurroundString("value"));
		}

		public String getField() {
			return findByName("field").toString();
		}

		@Override
		public String getValue() {
			return findByName("value").toString();
		}
	}

	protected ISurroundFactory<M> modelCreator;

	public SurroundDataFilter() {
		super();
		initFilter();
	}

	public SurroundDataFilter(String name) {
		super(name);
		initFilter();
	}

	private void initFilter() {

		SurroundList<SurroundDataFilterExpression> expressions = new SurroundList<SurroundDataFilterExpression>(
				"expressions");
		expressions.setElementFactory(new ISurroundFactory<SurroundDataFilter<M>.SurroundDataFilterExpression>() {

			@Override
			public SurroundDataFilter<M>.SurroundDataFilterExpression create()
					throws SurroundBaseException, InstantiationException, IllegalAccessException {
				return new SurroundDataFilterExpression();
			}
		});

		add(expressions);

		add(new SurroundString("fulltext").set(""));

		add(new SurroundBoolean("matchAll").set(false));
	}

	public ISurroundFactory<M> getModelCreator() {
		return modelCreator;
	}

	public SurroundDataFilter<M> setModelCreator(ISurroundFactory<M> modelCreator) {
		this.modelCreator = modelCreator;
		return this;
	}

	public SurroundList<SurroundDataFilterExpression> getExpressions() {
		return (SurroundList<SurroundDataFilterExpression>) findByName("expressions");
	}

	public String getFulltext() {
		return findByName("fulltext").toString();
	}

	public SurroundDataFilter<M> setFulltext(String fullText) {
		findByName("fulltext").set(fullText);
		return this;
	}

	public boolean isMatchAll() {
		return ((SurroundBoolean) findByName("matchAll")).asBoolean();
	}

	public String evaluate() {

		String prefix = isMatchAll() ? "+" : "";

		String fulltext = getFulltext();

		String queryString = getExpressions().size() > 0 && fulltext.length() > 0 ? prefix + fulltext : fulltext;

		for (SurroundDataFilterExpression exp : getExpressions().getChildren()) {
			String expressionStr = exp.getField() + "=" + exp.getValue();
			queryString += prefix + expressionStr;
		}

		return queryString;
	}

	@Override
	public String toString() {

		// String name = getName();
		// name += " : ";
		String name = evaluate();

		return name;
	}

}
