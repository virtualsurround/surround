/*******************************************************************************
 *
 *   Copyright (c) 2016 Joachim Tessmer
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License v1.0
 *   which accompanies this distribution, and is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 *  
 *   Contributors:
 *   Joachim Tessmer (http://www.instanttouch.de)
 *******************************************************************************/
package de.instanttouch.surround.data.query;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import de.instanttouch.surround.model.base.SurroundInteger;
import de.instanttouch.surround.model.base.SurroundString;
import de.instanttouch.surround.model.collection.SurroundList;
import de.instanttouch.surround.model.composite.SurroundDynamicEntity;
import de.instanttouch.surround.model.domain.SurroundModel;
import de.instanttouch.surround.model.ref.SurroundReference;
import de.instanttouch.surround.model.time.SurroundTimeRange;
import de.instanttouch.surround.model.time.SurroundTimestamp;

public class SurroundDataQuery<M extends SurroundModel> extends SurroundDynamicEntity {

	protected ISurroundFactory<M> modelCreator;

	public SurroundDataQuery() {
		super();
		initQuery();
	}

	public SurroundDataQuery(String name) {
		super(name);
		initQuery();
	}

	public ISurroundFactory<M> getModelCreator() {
		return modelCreator;
	}

	public SurroundDataQuery<M> setModelCreator(ISurroundFactory<M> modelCreator) {
		this.modelCreator = modelCreator;
		return this;
	}

	private void initQuery() {
		add(new SurroundString("modelId"));
		add(new SurroundList<SurroundString>("requestedFields", SurroundString.class, "field"));
		add(new SurroundList<SurroundDataOrderBy>("sorting", SurroundDataOrderBy.class, "orderby"));
		add(new SurroundDataFilter<M>("filter"));
		add(new SurroundString("searchStr"));
		add(new SurroundInteger("offset").set(0));
		add(new SurroundInteger("pageSize").set(1));
		add(new SurroundTimestamp("createdDate"));
		add(new SurroundTimeRange("timeRange"));
		add(new SurroundString("timefield"));
	}

	public String getModelId() {
		return findByName("modelId").toString();
	}

	public SurroundDataQuery<M> setModelId(String modelId) {
		findByName("modelId").set(modelId);
		return this;
	}

	public List<SurroundDataOrderBy> getSortBy() {
		List<SurroundDataOrderBy> sortList = new ArrayList<SurroundDataOrderBy>();
		for (SurroundDataOrderBy orderBy : getSortingList().getChildren()) {
			sortList.add(orderBy);
		}
		return sortList;
	}

	protected SurroundList<SurroundString> getRequestedInternal() {
		return (SurroundList<SurroundString>) findByName("requestedFields");
	}

	public SurroundList<SurroundDataOrderBy> getSortingList() {
		return (SurroundList<SurroundDataOrderBy>) findByName("sorting");
	}

	public void addRequestedField(String field) {
		getRequestedInternal().add(new SurroundString(field));
	}

	public List<String> getRequestedFields() {
		List<String> requestedFields = new ArrayList<String>();
		for (SurroundString field : getRequestedInternal().getChildren()) {
			requestedFields.add(field.getName());
		}
		return requestedFields;
	}

	public String getSearchString() {
		return findByName("searchStr").toString();
	}

	public SurroundDataQuery<M> setSearchString(String searchStr) {

		SurroundString fullTextStr = new SurroundString("searchStr").set(searchStr);
		replace("search", fullTextStr);
		return this;
	}

	public int getOffset() {
		return findByName("offset").asInt();
	}

	public SurroundDataQuery<M> setOffset(int offset) {
		findByName("offset").set(offset);
		return this;
	}

	public int getPageSize() {
		return findByName("pageSize").asInt();
	}

	public SurroundDataQuery<M> setPageSize(int pageSize) {
		findByName("pageSize").set(pageSize);
		return this;
	}

	public LocalDateTime getCreatedDate() {
		return ((SurroundTimestamp) findByName("createdDate")).getDateTime();
	}

	public SurroundDataQuery<M> setCreatedDate(LocalDateTime created) {
		((SurroundTimestamp) findByName("createdDate")).setDateTime(created);
		return this;
	}

	public SurroundReference<M> getModelCoordinate() {

		return (SurroundReference<M>) findByName("modelCoordinate");
	}

	public SurroundDataQuery<M> setModelCoordinate(SurroundReference<M> modelCoordinate) {
		replace("modelCoordinate", modelCoordinate);
		return this;
	}

	public SurroundTimeRange getTimeRange() {
		return (SurroundTimeRange) findByName("timeRange");
	}

	public SurroundDataQuery<M> setTimeRange(SurroundTimeRange timeRange) {
		replace("timeRange", timeRange);
		return this;
	}

	public String getTimeField() {
		return findByName("timefield").toString();
	}

	public SurroundDataQuery<M> setTimeField(String field) {
		findByName("timefield").set(field);
		return this;
	}

	public SurroundDataFilter<M> getFilter() {
		return (SurroundDataFilter<M>) findByName("filter");
	}

	public SurroundDataQuery<M> setFilter(SurroundDataFilter<M> filter) {
		replace("filter", filter);
		return this;
	}

	@Override
	public String toString() {
		return asJSON();
	}

}
